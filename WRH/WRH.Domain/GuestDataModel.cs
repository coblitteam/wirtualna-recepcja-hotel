﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WRH.Domain.Transfer.GuestData;

namespace WRH.Domain
{
    public class GuestDataModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }

        public virtual int BookingId { get; set; }

        [StringLength(50)]
        public virtual string Name { get; set; }

        [StringLength(100)]
        public virtual string Surname { get; set; }

        public virtual string Address { get; set; }

        public virtual DateTime Birthday { get; set; }

        [ForeignKey("BookingId")]
        public virtual BookingModel Booking { get; set; }

        public void Override(GuestDataTransfer transfer) 
        {
            Name = transfer.Name;
            Surname = transfer.Surname;
            Address = transfer.Address;
            Birthday = transfer.Birthday;
        }

        public static GuestDataModel Create(GuestDataTransfer transfer) =>
            new GuestDataModel
            {
                BookingId = transfer.BookingId,
                Name = transfer.Name,
                Surname = transfer.Surname,
                Address = transfer.Address,
                Birthday = transfer.Birthday
            };
    }
}
