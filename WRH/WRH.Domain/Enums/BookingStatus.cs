﻿using System.ComponentModel;

namespace WRH.Domain.Enums
{
    public enum BookingStatus
    {
        [Description("Nierozpoczęta")]
        NotStarted = 0,

        [Description("W trakcie")]
        InProgress = 1,

        [Description("Nierozliczona")]
        Undone = 2,

        [Description("Zakończona")]
        Done = 3,

        [Description("Wymeldowano")]
        CheckedOut = 4
    }
}
