﻿namespace WRH.Domain.Enums
{
    public enum BusyStatus
    {
        Pending = 0,
        Confirmed = 1
    }
}
