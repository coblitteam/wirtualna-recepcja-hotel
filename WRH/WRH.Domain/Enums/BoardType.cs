﻿namespace WRH.Domain.Enums
{
    public enum BoardType
    {
        None = 0,
        Breakfast = 1,
        Full = 2
    }
}
