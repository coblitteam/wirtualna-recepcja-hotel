﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WRH.Domain.Enums
{
    public enum AttractionPaymentType
    {
        PerPiece = 0,
        PerDay = 1,
        PerPersonDay = 2
    }
}
