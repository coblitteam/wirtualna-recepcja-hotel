﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WRH.Domain.Transfer.CustomerData;

namespace WRH.Domain
{
    [Table("CustomerData")]
    public class CustomerDataModel
    {
        public CustomerDataModel()
        {
            PersonalBookings = new HashSet<BookingModel>();
            CompanyBookings = new HashSet<BookingModel>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }

        [StringLength(50)]
        public virtual string Name { get; set; }

        [StringLength(100)]
        public virtual string Surname { get; set; }

        [StringLength(2)]
        public virtual string Country { get; set; }

        [StringLength(10)]
        public virtual string PostalCode { get; set; }

        [StringLength(100)]
        public virtual string City { get; set; }

        [StringLength(200)]
        public virtual string? Street { get; set; }

        [StringLength(20)]
        public virtual string HouseNumber { get; set; }

        [StringLength(20)]
        public virtual string? DoorsNumber { get; set; }

        [StringLength(4)]
        public virtual string? PhoneCountryNumber { get; set; }

        [StringLength(15)]
        public virtual string? PhoneNumber { get; set; }

        public virtual string? Email { get; set; }

        [StringLength(50)]
        public virtual string? Nip { get; set; }

        [StringLength(250)]
        public virtual string? CompanyName { get; set; }

        [NotMapped]
        public virtual string FullPersonalName
        {
            get => $"{Surname} {Name}";
        }

        [NotMapped]
        public virtual string Address
        {
            get => $"{((string.IsNullOrEmpty(Street)) ? City : Street)} {HouseNumber} {(!string.IsNullOrEmpty(DoorsNumber) ? " / " + DoorsNumber : "")} {PostalCode} {City}";
        }

        #region Collections

        public virtual ICollection<BookingModel> PersonalBookings { get; set; }

        public virtual ICollection<BookingModel> CompanyBookings { get; set; }

        #endregion

        public void OverridePersonalData(PersonalDataTransfer transfer)
        {
            Name = transfer.Name;
            Surname = transfer.Surname;
            Country = transfer.Country;
            PostalCode = transfer.PostalCode;
            City = transfer.City;
            Street = transfer.Street;
            HouseNumber = transfer.HouseNumber;
            DoorsNumber = transfer.DoorsNumber;
            PhoneCountryNumber = transfer.PhoneCountryNumber;
            PhoneNumber = transfer.PhoneNumber;
            Email = transfer.Email;
        }

        public void OverrideCompanyData(CompanyDataTransfer transfer)
        {
            OverridePersonalData(transfer);

            Nip = transfer.Nip;
            CompanyName = transfer.CompanyName;
        }

        public static CustomerDataModel CreatePersonalData(PersonalDataTransfer transfer) =>
            new CustomerDataModel
            {
                Name = transfer.Name,
                Surname = transfer.Surname,
                Country = transfer.Country,
                PostalCode = transfer.PostalCode,
                City = transfer.City,
                Street = transfer.Street,
                HouseNumber = transfer.HouseNumber,
                DoorsNumber = transfer.DoorsNumber,
                PhoneCountryNumber = transfer.PhoneCountryNumber,
                PhoneNumber = transfer.PhoneNumber,
                Email = transfer.Email
            };

        public static CustomerDataModel CreateCompanyData(CompanyDataTransfer transfer)
        {
            var data = CreatePersonalData(transfer);
            data.Nip = transfer.Nip;
            data.CompanyName = transfer.CompanyName;

            return data;
        }
    }
}
