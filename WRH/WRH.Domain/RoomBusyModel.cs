﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WRH.Domain.Enums;

namespace WRH.Domain
{
    [Table("RoomBusy")]
    public class RoomBusyModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }

        public virtual int RoomId { get; set; }

        public virtual int? BookingId { get; set; }

        public virtual DateTime BusyDateFrom { get; set; }

        public virtual DateTime BusyDateTo { get; set; }

        public virtual DateTime? PendingExpiration { get; set; }

        public virtual BusyStatus Status { get; set; }

        [ForeignKey("RoomId")]
        public virtual RoomModel Room { get; set; }

        [ForeignKey("BookingId")]
        public virtual BookingModel? Booking { get; set; }

        public static List<RoomBusyModel> MakeBusies(BookingModel booking, List<RoomModel> freeRooms)
        {
            var list = new List<RoomBusyModel>();

            foreach (var room in booking.Rooms)
            {
                var freeRoom = freeRooms.FirstOrDefault(e => e.RoomTypeId == room.RoomId && !list.Any(x => x.RoomId != e.Id));

                if (freeRoom == null)
                    continue;

                for (var i = 0; i < booking.Days(); i++)
                {
                    list.Add(new RoomBusyModel
                    {
                        RoomId = freeRoom.Id,
                        BookingId = booking.Id,
                        BusyDateFrom = booking.ArrivalDate.AddDays(i),
                        BusyDateTo = booking.ArrivalDate.AddDays(i + 1),
                        PendingExpiration = DateTime.Now.AddMinutes(15),
                        Status = BusyStatus.Pending
                    });
                }
            }

            return list;
        }
    }
}
