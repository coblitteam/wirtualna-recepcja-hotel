﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WRH.Domain.Transfer.RoomType;

namespace WRH.Domain
{

    [Table("RoomType")]
    public class RoomTypeModel
    {
        public RoomTypeModel()
        {
            Bookings = new HashSet<BookingRoomModel>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }

        [StringLength(50)]
        public virtual string Name { get; set; }
        public virtual string? Name_En { get; set; }
        public virtual string? Name_De { get; set; }
        public virtual string? Name_Es { get; set; }
        public virtual string? Name_Fr { get; set; }

        public virtual string Details { get; set; }
        public virtual string? Details_En { get; set; }
        public virtual string? Details_De { get; set; }
        public virtual string? Details_Es { get; set; }
        public virtual string? Details_Fr { get; set; }

        [StringLength(120)]
        public virtual string ShortDetails { get; set; }
        public virtual string? ShortDetails_En { get; set; }
        public virtual string? ShortDetails_De { get; set; }
        public virtual string? ShortDetails_Es { get; set; }
        public virtual string? ShortDetails_Fr { get; set; }

        public virtual int Capacity { get; set; }

        public virtual int ExtraBeds { get; set; }

        public virtual decimal Price { get; set; }

        public virtual decimal PriceOnce { get; set; }

        public virtual decimal BreakfastPrice { get; set; }

        public virtual decimal FullBoardPrice { get; set; }

        public virtual string? Photo { get; set; }

        #region Translations

        public object NameTranslations
        {
            get => new
            {
                pl = Name,
                en = (!string.IsNullOrEmpty(Name_En)) ? Name_En : Name,
                de = (!string.IsNullOrEmpty(Name_De)) ? Name_De : Name,
                es = (!string.IsNullOrEmpty(Name_Es)) ? Name_Es : Name,
                fr = (!string.IsNullOrEmpty(Name_Fr)) ? Name_Fr : Name,
            };
        }

        public object DetailsTranslations
        {
            get => new
            {
                pl = Details,
                en = (!string.IsNullOrEmpty(Details_En)) ? Details_En : Details,
                de = (!string.IsNullOrEmpty(Details_De)) ? Details_De : Details,
                es = (!string.IsNullOrEmpty(Details_Es)) ? Details_Es : Details,
                fr = (!string.IsNullOrEmpty(Details_Fr)) ? Details_Fr : Details,
            };
        }

        public object ShortDetailsTranslations
        {
            get => new
            {
                pl = ShortDetails,
                en = (!string.IsNullOrEmpty(ShortDetails_En)) ? ShortDetails_En : ShortDetails,
                de = (!string.IsNullOrEmpty(ShortDetails_De)) ? ShortDetails_De : ShortDetails,
                es = (!string.IsNullOrEmpty(ShortDetails_Es)) ? ShortDetails_Es : ShortDetails,
                fr = (!string.IsNullOrEmpty(ShortDetails_Fr)) ? ShortDetails_Fr : ShortDetails,
            };
        }

        #endregion

        #region Collections

        public virtual ICollection<BookingRoomModel> Bookings { get; set; }

        #endregion

        public void OverrideData(RoomTypeTransferModel transfer)
        {
            Name = transfer.Name;
            Name_En = transfer.Name_En;
            Name_De = transfer.Name_De;
            Name_Es = transfer.Name_Es;
            Name_Fr = transfer.Name_Fr;

            Details = transfer.Details;
            Details_En = transfer.Details_En;
            Details_De = transfer.Details_De;
            Details_Es = transfer.Details_Es;
            Details_Fr = transfer.Details_Fr;

            ShortDetails = transfer.ShortDetails;
            ShortDetails_En = transfer.ShortDetails_En;
            ShortDetails_De = transfer.ShortDetails_De;
            ShortDetails_Es = transfer.ShortDetails_Es;
            ShortDetails_Fr = transfer.ShortDetails_Fr;

            Capacity = transfer.Capacity;
            ExtraBeds = transfer.ExtraBeds;
            Price = transfer.Price;
            PriceOnce = transfer.PriceOnce;
            BreakfastPrice = transfer.BreakfastPrice;
            FullBoardPrice = transfer.FullBoardPrice;
        }

        #region Seed

        public static List<RoomTypeModel> DefaultData()
        {
            int[] roomCapacities = new int[] { 1, 3, 2, 2, 1, 3 };
            decimal[] roomPrices = new decimal[] { 140m, 220m, 180m, 180m, 140m, 220m };

            var rooms = new List<RoomTypeModel>();

            for (var i = 0; i < roomCapacities.Length; i++)
            {
                rooms.Add(new RoomTypeModel
                {
                    Id = i + 1,
                    Name = $"Pokój osób {roomCapacities[i]} nr {i + 1}",
                    Details = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
                    ShortDetails = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliq",
                    Capacity = roomCapacities[i],
                    Price = roomPrices[i],
                    PriceOnce = roomPrices[i] * 1.3m,
                    BreakfastPrice = 36m,
                    FullBoardPrice = 74m
                });
            }

            return rooms;
        }

        #endregion
    }
}
