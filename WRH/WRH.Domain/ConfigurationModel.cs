﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WRH.Domain
{
    public class ConfigurationModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }

        public virtual string ReceptionEmail { get; set; }

        public void OverrideData(ConfigurationModel model)
        {
            ReceptionEmail = model.ReceptionEmail;
        }

        public static List<ConfigurationModel> DefaultData()
        {
            var configurations = new List<ConfigurationModel>();

            configurations.Add(new ConfigurationModel
            {
                Id = 1,
                ReceptionEmail = "test@test.pl"
            });

            return configurations;
        }
    }
}
