﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WRH.Domain.Transfer.News;

namespace WRH.Domain
{
    [Table("News")]
    public class NewsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }

        public virtual int CategoryId { get; set; }

        public virtual string Name { get; set; }
        public virtual string? Name_En { get; set; }
        public virtual string? Name_De { get; set; }
        public virtual string? Name_Es { get; set; }
        public virtual string? Name_Fr { get; set; }

        public virtual string? Address { get; set; }

        public virtual string Details { get; set; }
        public virtual string? Details_En { get; set; }
        public virtual string? Details_De { get; set; }
        public virtual string? Details_Es { get; set; }
        public virtual string? Details_Fr { get; set; }

        public virtual string ShortDetails { get; set; }
        public virtual string? ShortDetails_En { get; set; }
        public virtual string? ShortDetails_De { get; set; }
        public virtual string? ShortDetails_Es { get; set; }
        public virtual string? ShortDetails_Fr { get; set; }

        public virtual string Photo { get; set; }

        public virtual bool IsActive { get; set; }

        #region Foreigns

        [ForeignKey("CategoryId")]
        public virtual NewsCategoryModel Category { get; set; }

        #endregion

        #region Create/Edit

        public static NewsModel CreateData(NewsTransfer transfer, NewsCategoryModel category) =>
            new NewsModel
            {
                Id = transfer.Id,
                CategoryId = transfer.CategoryId,
                Category = category,
                Name = transfer.Name,
                Name_En = transfer.Name_En,
                Name_De = transfer.Name_De,
                Name_Es = transfer.Name_Es,
                Name_Fr = transfer.Name_Fr,
                Details = transfer.Details,
                Details_En = transfer.Details_En,
                Details_De = transfer.Details_De,
                Details_Es = transfer.Details_Es,
                Details_Fr = transfer.Details_Fr,
                ShortDetails = transfer.ShortDetails,
                ShortDetails_En = transfer.ShortDetails_En,
                ShortDetails_De = transfer.ShortDetails_De,
                ShortDetails_Es = transfer.ShortDetails_Es,
                ShortDetails_Fr = transfer.ShortDetails_Fr,
                Address = transfer.Address,
                IsActive = transfer.IsActive
            };

        public void OverrideData(NewsTransfer transfer)
        {
            CategoryId = transfer.CategoryId;
            Name = transfer.Name;
            Name_En = transfer.Name_En;
            Name_De = transfer.Name_De;
            Name_Es = transfer.Name_Es;
            Name_Fr = transfer.Name_Fr;

            Details = transfer.Details;
            Details_En = transfer.Details_En;
            Details_De = transfer.Details_De;
            Details_Es = transfer.Details_Es;
            Details_Fr = transfer.Details_Fr;

            ShortDetails = transfer.ShortDetails;
            ShortDetails_En = transfer.ShortDetails_En;
            ShortDetails_De = transfer.ShortDetails_De;
            ShortDetails_Es = transfer.ShortDetails_Es;
            ShortDetails_Fr = transfer.ShortDetails_Fr;
            Address = transfer.Address;
            IsActive = transfer.IsActive;
        }

        #endregion

        #region Seed

        public static List<NewsModel> DefaultData()
        {
            var categories = new int[] { 1, 2, 3 };
            var news = new List<NewsModel>();

            var id = 1;
            foreach (var category in categories)
                for (var i = 1; i <= 4; i++)
                {
                    news.Add(new NewsModel
                    {
                        Id = id,
                        CategoryId = category,
                        Name = $"News nr {i}",
                        Details = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                                    totam rem aperiam,
                                    eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,
                                    sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
                        ShortDetails = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
                        Photo = String.Empty,
                        IsActive = true
                    });

                    id++;
                }

            return news;
        }

        #endregion
    }
}
