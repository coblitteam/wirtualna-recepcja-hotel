﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WRH.Domain
{
    public class InvoiceNumberModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }

        public virtual int BookingId { get; set; }

        public virtual int Number { get; set; }
        public virtual int Month { get; set; }
        public virtual int Year { get; set; }
    }
}
