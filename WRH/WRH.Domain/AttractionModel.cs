﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WRH.Domain.Enums;
using WRH.Domain.Transfer.Attraction;

namespace WRH.Domain
{
    [Table("Attraction")]
    public class AttractionModel
    {
        public AttractionModel()
        {
            Bookings = new HashSet<BookingAttractionModel>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }

        public virtual int CategoryId { get; set; }

        public virtual AttractionPaymentType PaymentType { get; set; }

        public virtual string Name { get; set; }
        public virtual string? Name_En { get; set; }
        public virtual string? Name_De { get; set; }
        public virtual string? Name_Es { get; set; }
        public virtual string? Name_Fr { get; set; }

        public virtual string Details { get; set; }
        public virtual string? Details_En { get; set; }
        public virtual string? Details_De { get; set; }
        public virtual string? Details_Es { get; set; }
        public virtual string? Details_Fr { get; set; }

        public virtual string ShortDetails { get; set; }
        public virtual string? ShortDetails_En { get; set; }
        public virtual string? ShortDetails_De { get; set; }
        public virtual string? ShortDetails_Es { get; set; }
        public virtual string? ShortDetails_Fr { get; set; }

        public virtual decimal NetPrice { get; set; }

        public virtual decimal GrosPrice { get => NetPrice * (1 + (VAT / 100)); }

        public virtual BoardType? Board { get; set; }

        public virtual decimal VAT { get; set; }

        public virtual string Photo { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual bool ImmediatePayment { get; set; }

        public virtual string? NotificationEmailAdresses { get; set; }
        public virtual string? NotificationHotelMessage { get; set; }
        public virtual string? NotificationGuestMessage { get; set; }

        #region Translations

        public object NameTranslations
        {
            get => new
            {
                pl = Name,
                en = (!string.IsNullOrEmpty(Name_En)) ? Name_En : Name,
                de = (!string.IsNullOrEmpty(Name_De)) ? Name_De : Name,
                es = (!string.IsNullOrEmpty(Name_Es)) ? Name_Es : Name,
                fr = (!string.IsNullOrEmpty(Name_Fr)) ? Name_Fr : Name,
            };
        }

        public object DetailsTranslations
        {
            get => new
            {
                pl = Details,
                en = (!string.IsNullOrEmpty(Details_En)) ? Details_En : Details,
                de = (!string.IsNullOrEmpty(Details_De)) ? Details_De : Details,
                es = (!string.IsNullOrEmpty(Details_Es)) ? Details_Es : Details,
                fr = (!string.IsNullOrEmpty(Details_Fr)) ? Details_Fr : Details,
            };
        }

        public object ShortDetailsTranslations
        {
            get => new
            {
                pl = ShortDetails,
                en = (!string.IsNullOrEmpty(ShortDetails_En)) ? ShortDetails_En : ShortDetails,
                de = (!string.IsNullOrEmpty(ShortDetails_De)) ? ShortDetails_De : ShortDetails,
                es = (!string.IsNullOrEmpty(ShortDetails_Es)) ? ShortDetails_Es : ShortDetails,
                fr = (!string.IsNullOrEmpty(ShortDetails_Fr)) ? ShortDetails_Fr : ShortDetails,
            };
        }

        #endregion

        #region Foreigns

        [ForeignKey("CategoryId")]
        public virtual AttractionCategoryModel Category { get; set; }

        #endregion

        #region Collections

        public virtual ICollection<BookingAttractionModel> Bookings { get; set; }

        #endregion

        #region Create/Edit

        public static AttractionModel CreateData(AttractionTransfer transfer, AttractionCategoryModel category) =>
            new AttractionModel
            {
                Id = transfer.Id,
                CategoryId = transfer.CategoryId,
                PaymentType = transfer.PaymentType,
                Category = category,
                Name = transfer.Name,
                Name_En = transfer.Name_En,
                Name_De = transfer.Name_De,
                Name_Es = transfer.Name_Es,
                Name_Fr = transfer.Name_Fr,
                Details = transfer.Details,
                Details_En = transfer.Details_En,
                Details_De = transfer.Details_De,
                Details_Es = transfer.Details_Es,
                Details_Fr = transfer.Details_Fr,
                ShortDetails = transfer.ShortDetails,
                ShortDetails_En = transfer.ShortDetails_En,
                ShortDetails_De = transfer.ShortDetails_De,
                ShortDetails_Es = transfer.ShortDetails_Es,
                ShortDetails_Fr = transfer.ShortDetails_Fr,
                NetPrice = transfer.NetPrice,
                VAT = transfer.VAT,
                IsActive = transfer.IsActive,
                ImmediatePayment = transfer.ImmediatePayment,
                NotificationEmailAdresses = transfer.NotificationEmailAdresses,
                NotificationHotelMessage = transfer.NotificationHotelMessage,
                NotificationGuestMessage = transfer.NotificationGuestMessage,
                Photo = String.Empty,
            };

        public void OverrideData(AttractionTransfer transfer)
        {
            CategoryId = transfer.CategoryId;
            PaymentType = transfer.PaymentType;
            Name = transfer.Name;
            Name_En = transfer.Name_En;
            Name_De = transfer.Name_De;
            Name_Es = transfer.Name_Es;
            Name_Fr = transfer.Name_Fr;

            Details = transfer.Details;
            Details_En = transfer.Details_En;
            Details_De = transfer.Details_De;
            Details_Es = transfer.Details_Es;
            Details_Fr = transfer.Details_Fr;

            ShortDetails = transfer.ShortDetails;
            ShortDetails_En = transfer.ShortDetails_En;
            ShortDetails_De = transfer.ShortDetails_De;
            ShortDetails_Es = transfer.ShortDetails_Es;
            ShortDetails_Fr = transfer.ShortDetails_Fr;
            NetPrice = transfer.NetPrice;
            VAT = transfer.VAT;
            IsActive = transfer.IsActive;
            ImmediatePayment = transfer.ImmediatePayment;
            NotificationEmailAdresses = transfer.NotificationEmailAdresses;
            NotificationHotelMessage = transfer.NotificationHotelMessage;
            NotificationGuestMessage = transfer.NotificationGuestMessage;
        }

        #endregion

        #region Seed

        public static List<AttractionModel> DefaultData()
        {
            var attractions = new List<AttractionModel>();

            attractions.Add(new AttractionModel
            {
                Id = 9997,
                CategoryId = 999,
                Name = "Śniadanie",
                Details = String.Empty,
                ShortDetails = String.Empty,
                NetPrice = 40,
                VAT = 8,
                Photo = String.Empty,
                IsActive = true,
                Board = BoardType.Breakfast,
                ImmediatePayment = true,
                PaymentType = AttractionPaymentType.PerPersonDay
            });

            attractions.Add(new AttractionModel
            {
                Id = 9998,
                CategoryId = 999,
                Name = "Pełne wyżywienie",
                Details = String.Empty,
                ShortDetails = String.Empty,
                NetPrice = 76,
                VAT = 8,
                Photo = String.Empty,
                IsActive = true,
                Board = BoardType.Full,
                ImmediatePayment = true,
                PaymentType = AttractionPaymentType.PerPersonDay
            });

            attractions.Add(new AttractionModel
            {
                Id = 9999,
                CategoryId = 1000,
                Name = "Opłata klimatyczna",
                Details = String.Empty,
                ShortDetails = String.Empty,
                NetPrice = 3.3m,
                VAT = 0,
                Photo = String.Empty,
                IsActive = true,
                ImmediatePayment = true,
                PaymentType = AttractionPaymentType.PerPersonDay
            });

            return attractions;
        }

        #endregion
    }
}
