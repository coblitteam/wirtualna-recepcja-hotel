﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WRH.Domain.Transfer.Room;

namespace WRH.Domain
{
    [Table("Room")]
    public class RoomModel
    {
        public RoomModel()
        {
            Busy = new HashSet<RoomBusyModel>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }

        public virtual int RoomTypeId { get; set; }

        public virtual int Number { get; set; }

        public virtual string? Code { get; set; }

        public virtual bool IsActive { get; set; }

        [ForeignKey("RoomTypeId")]
        public virtual RoomTypeModel Type { get; set; }

        public virtual ICollection<RoomBusyModel> Busy { get; set; }

        public virtual string[] BusyDays
        {
            get
            {
                var days = new List<string>();

                foreach (var busy in Busy)
                {
                    var day = busy.BusyDateFrom;

                    do
                    {
                        days.Add(day.ToString("dd.MM.yyyy"));
                        day = day.AddDays(1);
                    } while (day != busy.BusyDateTo);
                }

                return days.ToArray();
            }
        }

        public void OverrideData(RoomTransferModel transfer)
        {
            RoomTypeId = transfer.RoomTypeId;
            Number = transfer.Number;
            Code = transfer.Code;
            IsActive = transfer.IsActive;
        }

        #region Seed

        public static List<RoomModel> DefaultData()
        {
            var roomsDictionary = new Dictionary<int, int>()
            {
                {1, 1},
                {2, 2},
                {3, 3},
                {4, 4},
                {5, 5},
                {6, 6},
                {7, 1},
                {8, 2},
                {9, 3},
                {10, 4},
                {11, 5},
                {12, 6},
                {13, 1},
                {14, 2},
                {15, 3},
                {16, 4},
                {17, 5},
                {18, 6},
                {19, 1},
                {20, 2},
                {21, 3},
                {22, 4},
                {23, 5},
                {24, 6},
                {25, 1},
                {26, 2},
                {27, 3},
                {28, 4},
                {29, 5},
                {30, 6},
                {31, 1},
                {32, 2},
                {33, 3},
                {34, 4},
                {35, 5},
                {36, 6},
                {37, 1},
                {38, 2},
                {39, 3},
                {40, 4},
                {41, 5},
                {42, 6},
            };

            var rooms = new List<RoomModel>();
            var id = 1;

            foreach (var room in roomsDictionary)
            {
                rooms.Add(new RoomModel
                {
                    Id = id,
                    RoomTypeId = room.Value,
                    Number = room.Key,
                    Code = "00000",
                    IsActive = true
                });

                id++;
            }

            return rooms;
        }

        #endregion
    }
}
