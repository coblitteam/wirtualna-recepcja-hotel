﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WRH.Domain.Enums;

namespace WRH.Domain
{
    [Table("Booking")]
    public class BookingModel
    {
        public BookingModel()
        {
            GuestsData = new HashSet<GuestDataModel>();
            Attractions = new HashSet<BookingAttractionModel>();
            Rooms = new HashSet<BookingRoomModel>();
            RoomsBusy = new HashSet<RoomBusyModel>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }

        [StringLength(11)]
        public virtual string? BookingNumber { get; set; }

        [StringLength(9)]
        public virtual string? StayNumber { get; set; }

        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime ArrivalDate { get; set; }
        public virtual DateTime? DepartureDate { get; set; }
        public virtual int? PersonalDataId { get; set; }
        public virtual int? CompanyDataId { get; set; }
        public virtual string? LastEditUserName { get; set; }
        public virtual DateTime? LastEditTimeStamp { get; set; }
        public virtual int NumberOfAdults { get; set; }
        public virtual int NumberOfChildren { get; set; }
        public virtual int NumberOfChildrenToThree { get; set; }
        public virtual BoardType Board { get; set; }
        public virtual bool IsCheckedOut { get; set; }

        [NotMapped]
        public virtual BookingStatus Status
        {
            get
            {
                if (IsCheckedOut)
                    return BookingStatus.CheckedOut;
                else if (ArrivalDate > DateTime.Today)
                    return BookingStatus.NotStarted;
                else if (ArrivalDate <= DateTime.Today && DepartureDate > DateTime.Today)
                    return BookingStatus.InProgress;
                else if (DepartureDate < DateTime.Today && !AllPaid())
                    return BookingStatus.Undone;
                else if (DepartureDate < DateTime.Today && AllPaid())
                    return BookingStatus.Done;

                return BookingStatus.NotStarted;
            }
        }

        #region Foreigns

        [ForeignKey("PersonalDataId")]
        [InverseProperty("PersonalBookings")]
        public virtual CustomerDataModel? PersonalData { get; set; }

        [ForeignKey("CompanyDataId")]
        [InverseProperty("CompanyBookings")]
        public virtual CustomerDataModel? CompanyData { get; set; }

        #endregion

        #region Collections

        public virtual ICollection<GuestDataModel> GuestsData { get; set; }
        public virtual ICollection<BookingAttractionModel> Attractions { get; set; }
        public virtual ICollection<BookingRoomModel> Rooms { get; set; }
        public virtual ICollection<RoomBusyModel> RoomsBusy { get; set; }

        #endregion

        public decimal RoomsTotalPrice() => Rooms.Sum(e => e.Quantity * e.Price) * Days();

        public decimal ToPayLeft()
        {
            var days = (int)((DepartureDate - ArrivalDate)?.TotalDays ?? 0) + 1;

            var rooms = Rooms.Where(e => !e.IsPaid).Sum(e => e.Quantity * e.Price * days);
            var attractions = Attractions.Where(e => !e.IsPaid).Sum(e => e.Quantity * e.Price);

            return rooms + attractions;
        }

        public int Days() => (DepartureDate.Value - ArrivalDate).Days;

        public bool AllPaid() => Rooms.All(e => e.IsPaid) && Attractions.All(e => e.IsPaid);
    }
}
