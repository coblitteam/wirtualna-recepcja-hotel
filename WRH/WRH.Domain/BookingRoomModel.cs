﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WRH.Domain.Transfer.BookingRoom;

namespace WRH.Domain
{
    [Table("BookingRoom")]
    public class BookingRoomModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }

        public virtual int BookingId { get; set; }

        public virtual int RoomId { get; set; }

        public virtual int Quantity { get; set; }

        public virtual decimal Price { get; set; }

        public virtual bool IsPaid { get; set; }

        public virtual int? PaymentId { get; set; }

        public virtual string? PaymentNumber { get; set; }

        #region Foreigns

        [ForeignKey("BookingId")]
        public virtual BookingModel Booking { get; set; }

        [ForeignKey("RoomId")]
        public virtual RoomTypeModel Room { get; set; }

        #endregion

        public static BookingRoomModel Create(BookingRoomTransfer transfer, int? paymentId = null, RoomTypeModel room = null) =>
            new BookingRoomModel
            {
                Id = transfer.Id ?? 0,
                RoomId = transfer.RoomId,
                Room = room,
                Quantity = transfer.Quantity,
                Price = transfer.Price,
                IsPaid = transfer.IsPaid,
                PaymentId = paymentId
            };

        public void Override(BookingRoomTransfer transfer)
        {
            RoomId = transfer.RoomId;
            Quantity = transfer.Quantity;
            Price = transfer.Price;
        }
    }
}
