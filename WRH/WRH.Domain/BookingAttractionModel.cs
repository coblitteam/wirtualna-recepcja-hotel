﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WRH.Domain.Transfer.BookingAttraction;

namespace WRH.Domain
{
    [Table("BookingAttraction")]
    public class BookingAttractionModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }

        public virtual int BookingId { get; set; }

        public virtual int AttractionId { get; set; }

        public virtual int Quantity { get; set; }

        public virtual decimal Price { get; set; }

        public virtual bool IsPaid { get; set; }

        public virtual DateTime CreatedAt { get; set; }

        public virtual int? PaymentId { get; set; }

        public virtual string? PaymentNumber { get; set; }

        #region Foreigns

        [ForeignKey("BookingId")]
        public virtual BookingModel Booking { get; set; }

        [ForeignKey("AttractionId")]
        public virtual AttractionModel Attraction { get; set; }

        #endregion

        public static BookingAttractionModel Create(BookingAttractionTransfer transfer, int? paymentId = null, AttractionModel attraction = null) =>
            new BookingAttractionModel
            {
                BookingId = transfer.BookingId ?? 0,
                AttractionId = transfer.AttractionId,
                Attraction = attraction,
                Quantity = transfer.Quantity,
                Price = transfer.Price,
                IsPaid = transfer.IsPaid,
                CreatedAt = DateTime.Now,
                PaymentId = paymentId,
            };

        public void Override(BookingAttractionTransfer transfer)
        {
            Quantity = transfer.Quantity;
            Price = transfer.Price;
        }
    }
}
