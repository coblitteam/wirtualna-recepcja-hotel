﻿using CommonComponent.Security;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WRH.Domain
{
    [Table("User")]
    public class UserModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }

        [StringLength(255)]
        public virtual string UserName { get; set; }

        [StringLength(255)]
        public virtual string Password { get; set; }

        [StringLength(255)]
        public virtual string Email { get; set; }

        #region Seed

        public static List<UserModel> DefaultData()
        {
            var users = new List<UserModel>();

            users.Add(new UserModel
            {
                Id = 1,
                UserName = "admin",
                Password = PasswordEncoding.Hash("admin"),
                Email = "wrh@mailinator.com"
            });

            return users;
        }

        #endregion
    }
}
