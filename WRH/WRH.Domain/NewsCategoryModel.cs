﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WRH.Domain.Transfer.NewsCategory;

namespace WRH.Domain
{
    [Table("NewsCategory")]
    public class NewsCategoryModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Photo { get; set; }

        public bool IsActive { get; set; }

        public static NewsCategoryModel CreateData(NewsCategoryTransfer transfer) =>
            new NewsCategoryModel
            {
                Id = transfer.Id,
                Name = transfer.Name,
                Photo = "",
                IsActive = transfer.IsActive,
            };

        public void OverrideData(NewsCategoryTransfer transfer)
        {
            Name = transfer.Name;
            IsActive = transfer.IsActive;
        }

        #region Seed

        public static List<NewsCategoryModel> DefaultData()
        {
            var categories = new List<NewsCategoryModel>();

            for (var i = 1; i <= 3; i++)
                categories.Add(new NewsCategoryModel
                {
                    Id = i,
                    Name = $"Kategoria {i}",
                    Photo = String.Empty,
                    IsActive = true
                });

            return categories;
        }

        #endregion
    }
}
