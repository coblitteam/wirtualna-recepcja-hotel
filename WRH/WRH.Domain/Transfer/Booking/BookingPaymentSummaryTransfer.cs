﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WRH.Domain.Transfer.Booking
{
    public class BookingPaymentSummaryTransfer
    {
        public string Name { get; set; }

        public int Quantity { get; set; }

        public decimal UnitPrice { get; set; }
    }
}
