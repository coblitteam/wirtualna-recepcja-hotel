﻿namespace WRH.Domain.Transfer.Booking
{
    public class BookingPaymentNotifyTransfer
    {
        public Content Order { get; set; }

        public class Content
        {
            public string OrderId { get; set; }
            public int ExtOrderId { get; set; }
            public string Status { get; set; }
        }
    }
}
