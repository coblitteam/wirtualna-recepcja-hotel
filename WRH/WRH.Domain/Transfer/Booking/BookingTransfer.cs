﻿using WRH.Domain.Enums;
using WRH.Domain.Transfer.BookingAttraction;
using WRH.Domain.Transfer.BookingRoom;
using WRH.Domain.Transfer.CustomerData;
using WRH.Domain.Transfer.GuestData;

namespace WRH.Domain.Transfer.Booking
{
    public class BookingTransfer
    {
        public int Id { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }
        public int NumberOfAdults { get; set; }
        public int NumberOfChildren { get; set; }
        public int NumberOfChildrenToThree { get; set; }
        public BoardType Board { get; set; }
        public PersonalDataTransfer PersonalData { get; set; }
        public CompanyDataTransfer? CompanyData { get; set; }
        public List<GuestDataTransfer> Guests { get; set; }
        public List<BookingRoomTransfer> Rooms { get; set; }
        public List<BookingAttractionTransfer> Attractions { get; set; }
        public bool? IsAdmin { get; set; }
    }
}