﻿using Newtonsoft.Json;
using WRH.Domain.Transfer.CustomerData;

namespace WRH.Domain.Transfer.Booking
{
    public class BookingPayUOrderModel
    {
        [JsonProperty("notifyUrl")]
        public string NotifyUrl { get; set; }

        [JsonProperty("continueUrl")]
        public string ContinueUrl { get; set; }

        [JsonProperty("customerIp")]
        public string CustomerIp { get; set; }

        [JsonProperty("merchantPosId")]
        public string MerchantPosId { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("currencyCode")]
        public string CurrencyCode { get; set; }

        [JsonProperty("totalAmount")]
        public int TotalAmount { get; set; }

        [JsonProperty("extOrderId")]
        public int ExtOrderId { get; set; }

        [JsonProperty("buyer")]
        public CustomerPayUModel Buyer { get; set; }

        [JsonProperty("products")]
        public List<BookingPayUProductModel> Products { get; set; }

        public static BookingPayUOrderModel Create(BookingModel booking, string customerIp, string baseAddress)
        {
            var diffDays = (booking.DepartureDate.Value - booking.ArrivalDate).Days;

            var rooms = booking.Rooms.Where(e => !e.IsPaid).Select(e => new BookingPayUProductModel
            {
                Name = e.Room.Name,
                UnitPrice = (int)(e.Price * 100 * diffDays),
                Quantity = e.Quantity,
                PaymentId = e.PaymentId
            }).ToList();

            var attraction = booking.Attractions.Where(e => !e.IsPaid && e.Attraction.ImmediatePayment && e.Quantity > 0).Select(e => new BookingPayUProductModel
            {
                Name = e.Attraction.Name,
                UnitPrice = (int)(e.Price * 100),
                Quantity = e.Quantity,
                PaymentId = e.PaymentId
            }).ToList();

            var products = rooms.Concat(attraction).ToList();
            var paymentId = products.FirstOrDefault()?.PaymentId ?? 0;

            return new BookingPayUOrderModel
            {
                NotifyUrl = $"{baseAddress}/api/booking/payment/notify",
                ContinueUrl = $"{baseAddress}/finish-payment/{booking.Id}/{paymentId}",
                CustomerIp = customerIp,
                Description = $"Pobyt nr. {booking.StayNumber}",
                CurrencyCode = "PLN",
                TotalAmount = products.Sum(e => e.UnitPrice * e.Quantity),
                ExtOrderId = paymentId,
                Buyer = new CustomerPayUModel
                {
                    Email = booking.PersonalData.Email,
                    Phone = booking.PersonalData.PhoneCountryNumber + booking.PersonalData.PhoneNumber,
                    FirstName = booking.PersonalData.Name,
                    LastName = booking.PersonalData.Surname,
                    Language = "pl"
                },
                Products = products,
            };
        }
    }
}
