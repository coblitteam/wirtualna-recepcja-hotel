﻿using Newtonsoft.Json;

namespace WRH.Domain.Transfer.Booking
{
    public class BookingPayUProductModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("unitPrice")]
        public int UnitPrice { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        public int? PaymentId { get; set; }
    }
}
