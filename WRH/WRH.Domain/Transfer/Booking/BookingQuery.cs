﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WRH.Domain.Enums;

namespace WRH.Domain.Transfer.Booking
{
    public class BookingQuery
    {
        public string OrderBy { get; set; }
        public string OrderDestination { get; set; }
        public int Start { get; set; }
        public int Size { get; set; }
        public string? Search { get; set; }
        public DateTime? ArrivalDateFrom { get; set; }
        public DateTime? ArrivalDateTo { get; set; }
        public DateTime? DepartureDateFrom { get; set; }
        public DateTime? DepartureDateTo { get; set; }
        public int? RoomType { get; set; }
        public decimal? PriceFrom { get; set; }
        public decimal? PriceTo { get; set; }
        public BookingStatus? Status { get; set; }
    }
}
