﻿namespace WRH.Domain.Transfer.GuestData
{
    public class GuestDataTransfer
    {
        public int? Id { get; set; }
        public int BookingId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public DateTime Birthday { get; set; }
    }
}
