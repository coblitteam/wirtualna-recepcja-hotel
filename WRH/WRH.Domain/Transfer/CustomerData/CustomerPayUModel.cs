﻿using Newtonsoft.Json;

namespace WRH.Domain.Transfer.CustomerData
{
    public class CustomerPayUModel
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; }
    }
}
