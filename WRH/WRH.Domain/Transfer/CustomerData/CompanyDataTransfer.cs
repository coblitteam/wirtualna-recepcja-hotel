﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WRH.Domain.Transfer.CustomerData
{
    public class CompanyDataTransfer : PersonalDataTransfer
    {
        public virtual string Nip { get; set; }
        public virtual string CompanyName { get; set; }
    }
}
