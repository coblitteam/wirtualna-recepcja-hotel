﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WRH.Domain.Transfer.CustomerData
{
    public class PersonalDataTransfer
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string Country { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public string? Street { get; set; }

        public string HouseNumber { get; set; }

        public string? DoorsNumber { get; set; }

        public string? PhoneCountryNumber { get; set; }

        public string? PhoneNumber { get; set; }

        public string? Email { get; set; }
    }
}
