﻿namespace WRH.Domain.Transfer.AttractionCategory
{
    public class AttractionCategoryTransfer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
