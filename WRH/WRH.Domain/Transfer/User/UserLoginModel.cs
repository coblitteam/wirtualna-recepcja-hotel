﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WRH.Domain.Transfer.User
{
    public class UserLoginModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
