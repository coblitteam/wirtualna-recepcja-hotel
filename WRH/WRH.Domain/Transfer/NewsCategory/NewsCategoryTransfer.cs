﻿namespace WRH.Domain.Transfer.NewsCategory
{
    public class NewsCategoryTransfer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
