﻿namespace WRH.Domain.Transfer.Attraction
{
    public class AttractionQuery
    {
        public string OrderBy { get; set; }
        public string OrderDestination { get; set; }
        public int Start { get; set; }
        public int Size { get; set; }
        public string? Search { get; set; }
        public int? CategoryId { get; set; }
        public decimal? NetPriceFrom { get; set; }
        public decimal? NetPriceTo { get; set; }
        public decimal? GrosPriceFrom { get; set; }
        public decimal? GrosPriceTo { get; set; }
        public bool? IsActive { get; set; }

    }
}
