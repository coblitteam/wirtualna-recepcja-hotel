﻿using Microsoft.AspNetCore.Http;
using WRH.Domain.Enums;

namespace WRH.Domain.Transfer.Attraction
{
    public class AttractionTransfer
    {
        public int Id { get; set; }

        public int CategoryId { get; set; }

        public AttractionPaymentType PaymentType { get; set; }

        public string Name { get; set; }
        public string? Name_En { get; set; }
        public string? Name_De { get; set; }
        public string? Name_Es { get; set; }
        public string? Name_Fr { get; set; }

        public string Details { get; set; }
        public string? Details_En { get; set; }
        public string? Details_De { get; set; }
        public string? Details_Es { get; set; }
        public string? Details_Fr { get; set; }

        public string ShortDetails { get; set; }
        public string? ShortDetails_En { get; set; }
        public string? ShortDetails_De { get; set; }
        public string? ShortDetails_Es { get; set; }
        public string? ShortDetails_Fr { get; set; }

        public decimal NetPrice { get; set; }

        public decimal VAT { get; set; }

        public IFormFile? Photo { get; set; }

        public bool IsActive { get; set; }

        public bool ImmediatePayment { get; set; }

        public string? NotificationEmailAdresses { get; set; }
        public string? NotificationHotelMessage { get; set; }
        public string? NotificationGuestMessage { get; set; }
    }
}
