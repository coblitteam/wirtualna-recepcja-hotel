﻿namespace WRH.Domain.Transfer.BookingAttraction
{
    public class BookingAttractionTransfer
    {
        public int? Id { get; set; }

        public string? StayNumber { get; set; }

        public int? BookingId { get; set; }

        public int AttractionId { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }
        public bool IsPaid { get; set; }
    }
}
