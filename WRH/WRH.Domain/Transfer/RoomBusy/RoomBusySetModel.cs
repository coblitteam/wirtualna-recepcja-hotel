﻿namespace WRH.Domain.Transfer.RoomBusy
{
    public class RoomBusySetModel
    {
        public int RoomId { get; set; }
        public string Date { get; set; }
    }
}
