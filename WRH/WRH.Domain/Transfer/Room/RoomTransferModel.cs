﻿namespace WRH.Domain.Transfer.Room
{
    public class RoomTransferModel
    {
        public int Id { get; set; }
        public int RoomTypeId { get; set; }
        public int Number { get; set; }
        public string Code { get; set; }
        public bool IsActive { get; set; }
    }
}
