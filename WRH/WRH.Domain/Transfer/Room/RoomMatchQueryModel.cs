﻿namespace WRH.Domain.Transfer.Room
{
    public class RoomMatchQueryModel
    {
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }
    }
}
