﻿namespace WRH.Domain.Transfer.News
{
    public class NewsQuery
    {
        public string OrderBy { get; set; }
        public string OrderDestination { get; set; }
        public int Start { get; set; }
        public int Size { get; set; }
        public string? Search { get; set; }
        public int? CategoryId { get; set; }
        public bool? IsActive { get; set; }

    }
}
