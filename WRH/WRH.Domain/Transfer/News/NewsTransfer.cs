﻿using Microsoft.AspNetCore.Http;

namespace WRH.Domain.Transfer.News
{
    public class NewsTransfer
    {
        public virtual int Id { get; set; }

        public virtual int CategoryId { get; set; }

        public string Name { get; set; }
        public string? Name_En { get; set; }
        public string? Name_De { get; set; }
        public string? Name_Es { get; set; }
        public string? Name_Fr { get; set; }

        public string Details { get; set; }
        public string? Details_En { get; set; }
        public string? Details_De { get; set; }
        public string? Details_Es { get; set; }
        public string? Details_Fr { get; set; }

        public string ShortDetails { get; set; }
        public string? ShortDetails_En { get; set; }
        public string? ShortDetails_De { get; set; }
        public string? ShortDetails_Es { get; set; }
        public string? ShortDetails_Fr { get; set; }

        public virtual string? Address { get; set; }

        public virtual IFormFile? Photo { get; set; }

        public virtual bool IsActive { get; set; }
    }
}
