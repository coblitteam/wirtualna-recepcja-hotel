﻿using Microsoft.AspNetCore.Http;

namespace WRH.Domain.Transfer.RoomType
{
    public class RoomTypeTransferModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string? Name_En { get; set; }
        public string? Name_De { get; set; }
        public string? Name_Es { get; set; }
        public string? Name_Fr { get; set; }

        public string Details { get; set; }
        public string? Details_En { get; set; }
        public string? Details_De { get; set; }
        public string? Details_Es { get; set; }
        public string? Details_Fr { get; set; }

        public string ShortDetails { get; set; }
        public string? ShortDetails_En { get; set; }
        public string? ShortDetails_De { get; set; }
        public string? ShortDetails_Es { get; set; }
        public string? ShortDetails_Fr { get; set; }

        public int Capacity { get; set; }
        public int ExtraBeds { get; set; }
        public decimal Price { get; set; }
        public decimal PriceOnce { get; set; }
        public decimal BreakfastPrice { get; set; }
        public decimal FullBoardPrice { get; set; }

        public IFormFile? Photo { get; set; }
    }
}
