﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WRH.Domain.Transfer.AttractionCategory;

namespace WRH.Domain
{
    [Table("AttractionCategory")]
    public class AttractionCategoryModel
    {
        public AttractionCategoryModel()
        {
            Attractions = new HashSet<AttractionModel>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Photo { get; set; }

        public bool IsActive { get; set; }

        public bool IsDefault { get; set; }

        public bool IsBoard { get; set; }

        public bool CanDelete { get; set; }

        #region Collections

        public virtual ICollection<AttractionModel> Attractions { get; set; }

        #endregion

        #region Create/Edit

        public static AttractionCategoryModel CreateData(AttractionCategoryTransfer transfer) =>
            new AttractionCategoryModel
            {
                Id = transfer.Id,
                Name = transfer.Name,
                Photo = "",
                IsActive = transfer.IsActive,
                CanDelete = true
            };

        public void OverrideData(AttractionCategoryTransfer transfer)
        {
            Name = transfer.Name;
            IsActive = transfer.IsActive;
        }

        #endregion

        #region Seed

        public static List<AttractionCategoryModel> DefaultData()
        {
            var categories = new List<AttractionCategoryModel>();

            categories.Add(new AttractionCategoryModel
            {
                Id = 999,
                Name = "Wyżywienie",
                Photo = String.Empty,
                IsActive = true,
                IsBoard = true
            });

            categories.Add(new AttractionCategoryModel
            {
                Id = 1000,
                Name = "Podatek",
                Photo = String.Empty,
                IsActive = true,
                IsDefault = true,
                CanDelete = false
            });

            return categories;
        }

        #endregion
    }
}
