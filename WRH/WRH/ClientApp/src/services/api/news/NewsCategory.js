﻿import ApiCore from '../utilities/ApiCore';

export const apiNewsCategory = new ApiCore({
    getCustom: true,
    post: true,
    put: true,
    remove: true,
    url: 'news/category'
});