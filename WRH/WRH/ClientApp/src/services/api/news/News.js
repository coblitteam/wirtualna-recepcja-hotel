﻿import ApiCore from '../utilities/ApiCore';

export const apiNews = new ApiCore({
    getCustom: true,
    getSingle: true,
    getList: true,
    post: true,
    put: true,
    remove: true,
    url: 'news'
});