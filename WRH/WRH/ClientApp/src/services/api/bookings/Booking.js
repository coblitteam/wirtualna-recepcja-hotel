import ApiCore from '../utilities/ApiCore';

export const apiBooking = new ApiCore({
    init: true,
    getCustom: true,
    getList: true,
    getSingle: true,
    postCustom: true,
    put: true,
    remove: true,
    url: 'booking',
});