﻿import ApiCore from '../utilities/ApiCore';

export const apiAttraction = new ApiCore({
    getCustom: true,
    getList: true,
    getSingle: true,
    post: true,
    put: true,
    remove: true,
    url: 'attraction'
});