import ApiCore from '../utilities/ApiCore';

export const apiConfig = new ApiCore({
    get: true,
    put: true,
    url: 'config',
});