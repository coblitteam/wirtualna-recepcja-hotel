﻿import ApiCore from '../utilities/ApiCore';

export const apiAttractionCategory = new ApiCore({
    getCustom: true,
    post: true,
    put: true,
    remove: true,
    url: 'attraction/category'
});