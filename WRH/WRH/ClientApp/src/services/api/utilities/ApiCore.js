import { apiProvider } from './Provider';

export class ApiCore {
    constructor(options) {
        if (options.init) {
            this.init = async () => {
                return await apiProvider.init(options.url);
            }
        }

        if (options.get) {
            this.get = async () => {
                return await apiProvider.get(options.url);
            };
        }

        if (options.getCustom) {
            this.getCustom = async (customUrl) => {
                return await apiProvider.get(options.url + customUrl);
            };
        }

        if (options.getSingle) {
            this.getSingle = async (id) => {
                return await apiProvider.getSingle(options.url, id);
            };
        }

        if (options.getList) {
            this.getList = async (query) => {
                return await apiProvider.getList(options.url, query);
            }
        }

        if (options.search) {
            this.search = async (search) => {
                return await apiProvider.search(options.url, search);
            };
        }

        if (options.post) {
            this.post = async (model) => {
                return await apiProvider.post(options.url, model);
            };
        }

        if (options.postCustom) {
            this.postCustom = async (customUrl, model) => {
                return await apiProvider.post(options.url + customUrl, model);
            };
        }

        if (options.put) {
            this.put = async (model) => {
                return await apiProvider.put(options.url, model);
            };
        }

        if (options.putCustom) {
            this.putCustom = async (customUrl, model) => {
                return await apiProvider.put(options.url + customUrl, model);
            }
        }

        if (options.remove) {
            this.remove = async (id) => {
                return await apiProvider.remove(options.url, id);
            };
        }
    }
}

export default ApiCore;