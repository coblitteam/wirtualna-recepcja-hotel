import ApiCore from '../utilities/ApiCore';

export const apiRoom = new ApiCore({
    init: true,
    get: true,
    getCustom: true,
    getSingle: true,
    getList: true,
    search: false,
    post: false,
    postCustom: true,
    put: true,
    putCustom: true,
    delete: false,
    url: 'room',
    plural: 'room',
    single: 'room'
});