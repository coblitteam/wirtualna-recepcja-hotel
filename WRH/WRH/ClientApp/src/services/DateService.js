function diffDays(date1, date2) {
    date1 = new Date(date1)/1;
    date2 = new Date(date2)/1
    return parseInt((date2 - date1) / (1000 * 60 * 60 * 24), 10)
}

export const dateService = {
    diffDays
}