function capitalized(text) {
    text = text.trimStart();

    if (text.length > 0)
        return text[0].toUpperCase() + text.substring(1);

    return text;
};

export const textService = {
    capitalized
};