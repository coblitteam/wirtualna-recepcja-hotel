import React from 'react';

import {
    Button,
    Container,
    Grid
} from '@mui/material';
import {
    Header,
    ReservationForm
} from '../../components';

import { useTranslation } from "react-i18next";
import { apiBooking } from '../../services/api/bookings/Booking';

import styles from './CheckIn.module.css';

export default function CheckIn() {
    const { t } = useTranslation();
    const [haveReservation, setHaveReservation] = React.useState(null);
    const [showForm, setShowForm] = React.useState(false);
    const [booking, setBooking] = React.useState(null);

    if (!booking) {
        apiBooking.init().then((data) => {
            if (data) {
                setBooking(data);
            }
            else {
                alert("Błąd");
            }
        });
    }

    const initBooking = async () => {


        if (booking) {
            setBooking(booking);
            setShowForm(true);
        }
        else {
            alert("Błąd");
        }
    }

    return (
        <Grid>
            <Header title={t('check_in.title')} />
            {booking &&
            <div className={styles.ReservationCheck}>
                <ReservationForm data={booking} checkIn={true} checkOut={false}/>
            </div>
            }
        </Grid>
    );
}
