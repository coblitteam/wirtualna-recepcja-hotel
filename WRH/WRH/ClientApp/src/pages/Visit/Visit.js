import React, { Component } from 'react';

import {
    Button,
    Container,
    Grid,
    TextField
} from '@mui/material';
import {
    Header,
    ReservationForm
} from '../../components';

import { useTranslation } from "react-i18next";
import { apiBooking } from '../../services/api/bookings/Booking';

import styles from './Visit.module.css';

export default function Visit() {
    const { t } = useTranslation();
    const [stayNumber, setStayNumber] = React.useState('');
    const [showForm, setShowForm] = React.useState(false);
    const [booking, setBooking] = React.useState(null);
    const [notFound, setNotFound] = React.useState(false);

    const getBooking = async () => {
        var data = await apiBooking.getCustom(`/by/number/${stayNumber}`);

        if (data.booking) {
            setNotFound(false);
            setBooking(data);
            setShowForm(true);
        }
        else {
            setNotFound(true);
        }
    }

    return (
        <Grid className={styles.Visit}>
            <Header title={t('my_visit.title')} />
            {showForm === false &&
                <div className={styles.Container}>
                    {notFound &&
                        <Grid>
                            <h1 className="text-danger">{t('reservation.data.not_found')}</h1>
                        </Grid>
                    }
                    <Container maxWidth="sm">
                        <Grid container className="modified-form">
                            <Grid item xs={12}>
                                <TextField
                                    margin="dense"
                                    id="stay_number"
                                    label={t('reservation.data.stay_number')}
                                    type="text"
                                    fullWidth
                                    variant="filled"
                                    inputProps={{ maxLength: 9 }}
                                    value={stayNumber}
                                    onChange={(event) => {
                                        setStayNumber(event.target.value.replace(' ', ''));
                                    }}
                                />
                            </Grid>
                            <Grid item xs={3} />
                            <Grid item xs={6}>
                                <Button className={styles.Button} onClick={async () => {
                                    getBooking();
                                }}>
                                    {t('button.next')}
                                </Button>
                            </Grid>
                            <Grid item xs={3} />
                        </Grid>
                    </Container>
                </div>
            }
            {(showForm === true && !notFound) &&
                <div className={styles.FormContainer}>
                    <ReservationForm data={booking} checkIn={false} checkOut={false} view={true} />
                </div>
            }
        </Grid>
    );
}
