import Admin from './Admin/Admin';
import Attraction from './Attraction/Attraction';
import CheckIn from './CheckIn/CheckIn';
import CheckOut from './CheckOut/CheckOut';
import CheckOutSummary from './CheckOutSummary/CheckOutSummary';
import FinishPayment from './FinishPayment/FinishPayment';
import Home from './Home/Home';
import NewsList from './NewsList/NewsList';
import News from './News/News';
import Offer from './Offer/Offer';
import Visit from './Visit/Visit';

export {
    Admin,
    Attraction,
    CheckIn,
    CheckOut,
    CheckOutSummary,
    FinishPayment,
    Home,
    NewsList,
    News,
    Offer,
    Visit
}