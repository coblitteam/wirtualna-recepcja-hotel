import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './News.module.css';

import {
    NewsDetails,
    Header
} from '../../components';

import {
    Button,
    Container,
    Grid,
    TextField
} from '@mui/material';
import { withTranslation } from "react-i18next";

import { apiNews } from '../../services/api/news/News';

export class News extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.match.params.id,
            news: null
        }

        this.getNews();
    }

    getNews = async () => {
        var data = await apiNews.getSingle(this.state.id);
        console.log(data);
        this.setState({
            news: data.news
        });
    }

    render() {
        return (
            <div className={styles.News}>
                {this.state.news &&
                    <Grid className={styles.News}>
                        <Header
                            title={`${this.props.t('news.title')}`}
                            showBackButton={true}
                        />
                        <Container className={styles.NewsContainer}>
                            <h1 className="text-center mb-5">{this.state.news.nameTranslations[this.props.i18n.language]}</h1>
                            <NewsDetails news={this.state.news} />
                        </Container>
                    </Grid>
                }
            </div>
        )
    }
}

News.propTypes = {};

News.defaultProps = {};

export default withTranslation()(News);
