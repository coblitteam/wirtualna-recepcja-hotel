import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Attraction.module.css';

import {
    AttractionDetails,
    Header
} from '../../components';

import {
    Button,
    Container,
    Grid,
    TextField
} from '@mui/material';
import { withTranslation } from "react-i18next";

import { apiAttraction } from '../../services/api/attractions/Attraction';
import { apiBooking } from '../../services/api/bookings/Booking';

export class Attraction extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.match.params.id,
            attraction: null,
            stayNumber: '',
            quantity: 1,
            errorMessage: ''
        }

        this.getAttraction();
    }

    getAttraction = async () => {
        var data = await apiAttraction.getSingle(this.state.id);

        this.setState({
            attraction: data.attraction
        });
    }

    assignAttraction = async () => {
        var data = await apiBooking.postCustom('/attraction/assign', {
            stayNumber: this.state.stayNumber,
            attractionId: this.state.id,
            price: this.state.attraction.grosPrice,
            quantity: this.state.quantity,
            errorMessage: ''
        });

        if (data.result) {
            window.location.href = '/';
        }
        else
            this.setState({
                errorMessage: 'Podany numer rezerwacji nie istnieje.'
            });
    }

    render() {
        return (
            <div className={styles.Attraction}>
                {this.state.attraction &&
                    <Grid className={styles.Attraction}>
                        <Header
                            title={this.props.t('hotels_offer.title')}
                            showBackButton={true}
                        />
                        <Container className={styles.AttractionContainer}>
                            <h1 className="text-center mb-5">{this.state.attraction.nameTranslations[this.props.i18n.language]}</h1>
                            <AttractionDetails attraction={this.state.attraction} />
                            <Grid container className="pb-5">
                                <Grid item md={12}>
                                    <h3 className="text-danger text-center">{this.state.errorMessage}</h3>
                                </Grid>
                                <Grid item md={2} />
                                <Grid item md={4} className="modified-form">
                                    <TextField
                                        margin="dense"
                                        id="stay_number"
                                        label={this.props.t('reservation.data.stay_number')}
                                        type="text"
                                        fullWidth
                                        variant="filled"
                                        inputProps={{ maxLength: 50 }}
                                        value={this.state.stayNumber}
                                        onChange={(event) => {
                                            this.setState({
                                                stayNumber: event.target.value
                                            });
                                        }}
                                    />
                                </Grid>
                                <Grid item md={1} className="modified-form">
                                    <TextField
                                        margin="dense"
                                        id="quantity"
                                        label="Ilość"
                                        type="number"
                                        fullWidth
                                        variant="filled"
                                        value={this.state.quantity}
                                        onChange={(event) => {
                                            this.setState({quantity: Number(event.target.value)});
                                        }}
                                    />
                                </Grid>
                                <Grid item md={3} className={styles.ButtonContainer}>
                                    <Button
                                        className={styles.Button}
                                        onClick={this.assignAttraction}
                                    >
                                        {this.props.t('button.order')}
                                    </Button>
                                </Grid>
                                <Grid item md={2} />
                            </Grid>
                        </Container>
                    </Grid>
                }
            </div>
        )
    }
}

Attraction.propTypes = {};

Attraction.defaultProps = {};

export default withTranslation()(Attraction);
