import React from 'react';

import {
    Container,
    Grid
} from '@mui/material';
import { LanguageSelect } from '../../components';
import { Link } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import styles from './Home.module.css';
import logo from '../../images/logo.png';

import { CheckIn, CheckOut, News, Offer, Visit } from '../../images/icons';


export default function Home() {
    const { t } = useTranslation();

    let blocks = [
        {
            url: '/visit',
            text: t("my_visit.title"),
            image: Visit
        },
        {
            url: '/check-in',
            text: t("check_in.title"),
            image: CheckIn
        },
        {
            url: '/check-out',
            text: t("check_out.title"),
            image: CheckOut
        },
        {
            url: '/offer',
            text: t("hotels_offer.title"),
            image: Offer
        },
        {
            url: '/news',
            text: t("news.title"),
            image: News
        },
    ];

    return (
        <Grid className={styles.Home}>
            <Grid className={styles.TopBar}>
                <Link to="/">
                    <img src={logo} alt="" />
                </Link>
                <LanguageSelect />
            </Grid>
            <Container>
                <Grid container className={styles.Blocks}>
                    {blocks.map((block, index) => {
                        return (
                            <Grid item md={4} key={index} className={styles.Block}>
                                <Link to={block.url}>
                                    <div className={styles.BlockContainer}>
                                        <div>
                                            <span>{block.text}</span>
                                            <img src={block.image} alt="" />
                                        </div>
                                    </div>
                                </Link>
                            </Grid>
                        );
                    })}
                </Grid>
            </Container>
        </Grid>
    )
}
