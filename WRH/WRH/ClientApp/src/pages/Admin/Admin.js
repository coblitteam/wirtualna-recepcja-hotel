﻿import React from 'react';

import {
    AdminAttractions,
    AdminBookings,
    AdminCalendar,
    AdminCategories,
    AdminConfig,
    AdminLogin,
    AdminNews,
    AdminRoomTypes
} from '../../components';

import {
    Box,
    Container,
    Grid,
    Tab,
    Tabs,
    TabPanel
} from '@mui/material';
import { styled } from '@mui/material/styles';

import styles from './Admin.module.css';

const AdminTabs = styled(Tabs)({
    borderBottom: 'none',
    '& .MuiTabs-indicator': {
        backgroundColor: 'transparent',
    },
});

const AdminTab = styled((props) => <Tab disableRipple {...props} />)(({ theme }) => ({
    textTransform: 'uppercase',
    minWidth: 0,
    [theme.breakpoints.up('sm')]: {
        minWidth: 0,
    },
    fontSize: '25px',
    fontWeight: 700,
    lineHeight: '42px',
    marginRight: theme.spacing(1),
    color: '#A7A7A7',
    padding: '36px',
    fontFamily: [
        'Roboto'
    ].join(','),
    '&:hover': {
        color: '#715D53',
        opacity: 0.8,
    },
    '&.Mui-selected': {
        color: '#715D53'
    },
    '&.Mui-focusVisible': {
        backgroundColor: '#d1eaff',
    },
}));

export default function Admin() {
    const [value, setValue] = React.useState(0);
    const [loggedIn, setLoggedIn] = React.useState(localStorage.getItem('token') ? true : false)

    let menu = [
        {
            title: "Pobyty",
            component: <AdminBookings />
        },
        {
            title: "Typy pokoju",
            component: <AdminRoomTypes />
        },
        {
            title: "Oferta hotelu",
            component: <AdminAttractions />
        },
        {
            title: "Aktualności",
            component: <AdminNews />
        },
        {
            title: "Kategorie",
            component: <AdminCategories />
        },
        {
            title: "Kalendarz",
            component: <AdminCalendar />
        },
        {
            title: "Konfiguracja",
            component: <AdminConfig />
        }
    ];

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const a11yProps = (index) => {
        return {
            id: `simple-tab-${index}`,
            'aria-controls': `simple-tabpanel-${index}`,
        };
    }

    return (
        <Box sx={{
            width: '100%',
            height: '100%'
        }}>
            {!loggedIn &&
                <AdminLogin onLoggedIn={() => {setLoggedIn(true)}}/>
            }
            {loggedIn &&
                <div>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }} className={styles.Tabs}>
                        <AdminTabs value={value} onChange={handleChange}>
                            {menu.map((item, index) => {
                                return <AdminTab key={index} label={item.title} {...a11yProps(index)} />
                            })}
                        </AdminTabs>
                    </Box>
                    {menu.map((item, index) => {
                        return (
                            <div
                                key={index}
                                role="tabpanel"
                                hidden={value !== index}
                                id={`simple-tabpanel-${index}`}
                                aria-labelledby={`simple-tab-${index}`}
                            >
                                {item.component}
                            </div>
                        );
                    })}
                </div>
            }

        </Box>
    );
}