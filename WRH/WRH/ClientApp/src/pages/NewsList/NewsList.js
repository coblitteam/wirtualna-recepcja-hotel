import React, { Component } from 'react';

import {
    NewsCarousel,
    Header
} from '../../components';

import {
    Button,
    Container,
    Grid
} from '@mui/material';
import { withTranslation } from "react-i18next";
import Carousel, { slidesToShowPlugin } from '@brainhubeu/react-carousel';

import styles from './NewsList.module.css';
import { apiNewsCategory } from '../../services/api/news/NewsCategory';
import { apiNews } from '../../services/api/news/News';

export class NewsList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            categories: [],
            newsList: [],
            selectedCategory: null
        }

        this.getCategories();
    }

    getCategories = async () => {
        var data = await apiNewsCategory.getCustom('/list/active');

        this.setState({
            categories: data.categories
        });
    }

    getNewsList = async (id) => {
        var data = await apiNews.getCustom('/by/category/' + id);

        this.setState({
            newsList: data.news
        });
    }

    render() {
        return (
            <Grid className={styles.News}>
                <Header
                    title={this.props.t('news.title')}
                    showBackButton={this.state.selectedCategory > 0}
                    onBackClick={() => this.setState({
                        newsList: [],
                        selectedCategory: null
                    })}
                />
                <div className={styles.NewsContainer}>
                    {(!this.state.selectedCategory && this.state.categories && this.state.categories.length > 0) &&
                        <Container maxWidth>
                            <Grid className={styles.Buttons}>
                                <Carousel
                                    className={styles.Carousel}
                                    plugins={[
                                        'arrows',
                                        {
                                            resolve: slidesToShowPlugin,
                                            options: {
                                                numberOfSlides: 4
                                            }
                                        },
                                    ]}
                                >
                                    {
                                        this.state.categories.map((category) =>
                                            <Button
                                                className={styles.Button}
                                                onClick={async () => {
                                                    await this.getNewsList(category.id);
                                                    this.setState({
                                                        selectedCategory: category.id
                                                    })
                                                }}
                                            >
                                                {category.name}
                                            </Button>
                                        )
                                    }
                                </Carousel>
                            </Grid>
                        </Container>
                    }
                    {this.state.selectedCategory &&
                        <NewsCarousel newsList={this.state.newsList} />
                    }
                </div>
            </Grid>
        );
    }
}

export default withTranslation()(NewsList)
