import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './FinishPayment.module.css';

import {
    Header
} from '../../components';

import {
    Button,
    Container,
    Grid,
    TextField
} from '@mui/material';
import { withTranslation } from "react-i18next";

import { apiBooking } from '../../services/api/bookings/Booking';

export class FinishPayment extends Component {
    constructor(props) {
        super(props);

        this.state = {
            bookingId: this.props.match.params.bookingId,
            paymentId: this.props.match.params.paymentId,
            booking: null,
            summary: []
        }

        this.getSummary();
    }

    getSummary = async () => {
        apiBooking.getCustom(`/payment/summary/${this.state.bookingId}/${this.state.paymentId}`)
                  .then((data) => {
                    this.setState({
                        booking: data.booking,
                        summary: data.summary
                    });
                  });
    }

    render() {
        return (
            <div className={styles.FinishPayment}>
                {this.state.booking &&
                <Grid className={styles.FinishPayment}>
                    <Header
                        title={this.props.t('summary.title')}
                        showBackButton={true}
                    />
                    <Container className={styles.FinishPaymentContainer}>
                        <h1 className="text-center mb-5">{this.props.t('summary.subtitle')} {this.state.booking.stayNumber}</h1>
                        <Grid container>
                            <Grid item xs={6}>
                                <h3>{this.props.t('reservation.arrival_date')}</h3>
                            </Grid>
                            <Grid item xs={6}>
                                <h3>{this.props.t('reservation.departure_date')}</h3>
                            </Grid>
                            <Grid item xs={6}>
                                <h1>{this.props.t('common.date', { val: this.state.booking.arrivalDate })} 14:00</h1>
                            </Grid>
                            <Grid item xs={6}>
                                <h1>{this.props.t('common.date', { val: this.state.booking.departureDate })} 10:00</h1>
                            </Grid>
                        </Grid>
                        <Grid className={styles.Table}>
                            <Grid container className={styles.TableHeader}>
                                <Grid item xs={2}/>
                                <Grid item xs={3}>
                                    <span>{this.props.t('summary.table.name')}</span>
                                </Grid>
                                <Grid item xs={2}>
                                    <span>{this.props.t('summary.table.quantity')}</span>
                                </Grid>
                                <Grid item xs={2}>
                                    <span>{this.props.t('summary.table.unitPrice')}</span>
                                </Grid>
                                <Grid item xs={3}>
                                    <span>{this.props.t('summary.table.price')}</span>
                                </Grid>
                            </Grid>
                            {this.state.summary.map(function (s, i) {
                                return <Grid key={i} container>
                                    <Grid item xs={2}/>
                                    <Grid item xs={3}>
                                        <span>{s.name}</span>
                                    </Grid>
                                    <Grid item xs={2}>
                                        <span>
                                            {s.quantity}
                                        </span>
                                    </Grid>
                                    <Grid item xs={2}>
                                        <span>
                                            {s.unitPrice}
                                        </span>
                                    </Grid>
                                    <Grid item xs={3}>
                                        <span>{s.unitPrice * s.quantity}</span>
                                    </Grid>
                                </Grid>
                            })}
                        </Grid>
                        <Grid container className="mt-5">
                            <Grid item xs={12}>
                                <h3>{this.props.t('summary.total')}</h3>
                            </Grid>
                            <Grid item xs={12}>
                                <h1>{this.props.t('common.currency_price', {val: this.state.booking.total})}</h1>
                            </Grid>
                        </Grid>
                        <Grid container className="mt-5">
                            <Grid item xs={4} />
                            <Grid item xs={4}>
                                <Button className={styles.Button} onClick={async () => {
                                    window.location.href = "/";
                                }}>
                                    {this.props.t('button.exit')}
                                </Button>
                            </Grid>
                            <Grid item xs={4} />
                        </Grid>
                    </Container>
                </Grid>
                }
            </div>
        )
    }
}

FinishPayment.propTypes = {};

FinishPayment.defaultProps = {};

export default withTranslation()(FinishPayment);
