import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './CheckOutSummary.module.css';

import {
    Header
} from '../../components';

import {
    Container,
    Grid
} from '@mui/material';
import { withTranslation } from "react-i18next";

export class CheckOutSummary extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={styles.CheckOutSummary}>
                <Grid className={styles.CheckOutSummary}>
                    <Header
                        title={`${this.props.t('check_out_summary.title')}`}
                        showBackButton={true}
                    />
                    <Container className={styles.CheckOutSummaryContainer}>
                        <Grid container>
                            <Grid item xs={12}>
                                <h1>{`${this.props.t('check_out_summary.body')}`}</h1>
                            </Grid>
                        </Grid>
                    </Container>
                </Grid>
            </div>
        )
    }
}

CheckOutSummary.propTypes = {};

CheckOutSummary.defaultProps = {};

export default withTranslation()(CheckOutSummary);
