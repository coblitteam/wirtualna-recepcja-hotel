import React, { Component } from 'react';

import {
    AttractionCarousel,
    Header
} from '../../components';

import {
    Button,
    Container,
    Grid
} from '@mui/material';
import { withTranslation } from "react-i18next";
import Carousel, { slidesToShowPlugin } from '@brainhubeu/react-carousel';

import styles from './Offer.module.css';
import { apiAttractionCategory } from '../../services/api/attractionCategories/AttractionCategory';
import { apiAttraction } from '../../services/api/attractions/Attraction';

export class Offer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            categories: [],
            attractions: [],
            selectedCategory: null
        }

        this.getCategories();
    }

    getCategories = async () => {
        var data = await apiAttractionCategory.getCustom('/list/active');

        this.setState({
            categories: data.categories.filter((category) => !category.isBoard && category.canDelete)
        });
    }

    getAttractions = async (id) => {
        var data = await apiAttraction.getCustom('/by/category/' + id);

        this.setState({
            attractions: data.attractions
        });
    }

    render() {
        return (
            <Grid className={styles.Offer}>
                <Header
                    title={this.props.t('hotels_offer.title')}
                    showBackButton={this.state.selectedCategory > 0}
                    onBackClick={() => this.setState({
                        attractions: [],
                        selectedCategory: null
                    })}
                />
                <div className={styles.OfferContainer}>
                    {(!this.state.selectedCategory && this.state.categories && this.state.categories.length > 0) &&
                        <Container maxWidth>
                            <Grid className={styles.Buttons}>
                                <Carousel
                                    className={styles.Carousel}
                                    plugins={[
                                        'arrows',
                                        {
                                            resolve: slidesToShowPlugin,
                                            options: {
                                                numberOfSlides: 4
                                            }
                                        },
                                    ]}
                                >
                                    {
                                        this.state.categories.map((category) =>
                                            <Button
                                                className={styles.Button}
                                                onClick={async () => {
                                                    await this.getAttractions(category.id);
                                                    this.setState({
                                                        selectedCategory: category.id
                                                    })
                                                }}
                                            >
                                                {category.name}
                                            </Button>
                                        )
                                    }
                                </Carousel>
                            </Grid>
                        </Container>
                    }
                    {this.state.selectedCategory &&
                        <AttractionCarousel attractions={this.state.attractions} />
                    }
                </div>
            </Grid>
        );
    }
}

export default withTranslation()(Offer)
