import { Route } from 'react-router';

//import Layout from './components/Layout/Layout';
//import Home from './components/Home/Home';

import {
    Layout
} from './components';

import {
    Admin,
    Attraction,
    CheckIn,
    CheckOut,
    CheckOutSummary,
    FinishPayment,
    Home,
    NewsList,
    News,
    Offer,
    Visit
} from './pages';

import './App.css';
import './fonts/fonts.css';

function App() {
    return (
        <Layout>
            <Route exact path='/' component={Home} />
            <Route exact path='/check-in' component={CheckIn} />
            <Route exact path='/check-out' component={CheckOut} />
            <Route exact path='/offer' component={Offer} />
            <Route exact path='/attraction/:id' component={Attraction} />
            <Route exact path='/news' component={NewsList} />
            <Route exact path='/news/:id' component={News} />
            <Route exact path='/visit' component={Visit} />
            <Route exact path='/admin' component={Admin} />
            <Route exact path='/finish-payment/:bookingId/:paymentId' component={FinishPayment} />
            <Route exact path='/checkout-summary' component={CheckOutSummary} />
        </Layout>
    );
}

export default App;