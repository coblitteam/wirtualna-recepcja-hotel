export const CheckIn = require('./checkin.png').default;
export const CheckOut = require('./checkout.png').default;
export const Home = require('./home.png').default;
export const News = require('./news.png').default;
export const Offer = require('./offer.png').default;
export const Visit = require('./visit.png').default;

export const Person = require('./person.png').default;
export const BLIK = require('./blik.png').default;
export const CreditCard = require('./credit_card.png').default;

export const Add = require('./add.png').default;
export const Remove = require('./remove.png').default;
