import React, {useState, useEffect} from 'react';
import styles from './NewsDetails.module.css';
import { useTranslation } from 'react-i18next';
import AWS from 'aws-sdk'
import PlayCircleIcon from '@mui/icons-material/PlayCircle';
import PauseCircleIcon from '@mui/icons-material/PauseCircle';

const Polly = new AWS.Polly({
    credentials: {
        accessKeyId: 'AKIARIPJ3CEFTR32YSUO',
        secretAccessKey: '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'
    },
    signatureVersion: 'v4',
    region: 'eu-central-1'
});

const speakers = {
    pl: 'Ewa',
    en: 'Emma',
    de: 'Vicki',
    es: 'Lucia',
    fr: 'Lea'
}

const NewsDetails = (props) => {
    const {
        news
    } = props;
    const { t, i18n } = useTranslation();
    const [audio, setAudio] = useState(new Audio());
    const [playing, setPlaying] = useState(false);
    const [playerInitializing, setPlayerInitializing] = useState(false);

    useEffect(async () => {
        pause();
        setPlayerInitializing(true);
        let params = {
            'Text': news.detailsTranslations[i18n.language],
            'OutputFormat': 'mp3',
            'VoiceId': speakers[i18n.language]
        };

        Polly.synthesizeSpeech(params, (err, data) => {
            if (err) {
                console.error('error', err);
            } else if (data) {
                var blob = new Blob([data.AudioStream.buffer]);
                var url = URL.createObjectURL(blob);
                setAudio(new Audio(url));
                setPlayerInitializing(false);
            }
        })
    }, [i18n.language]);

    const play = () => {
        audio.play();
        setPlaying(true);
    }

    const pause = () => {
        audio.pause();
        setPlaying(false);
    }

    return (
        <div className={styles.News}>
            <img src={news.photo} alt="" />
            <p>
                <button className={styles.SpeakerButton} onClick={!playing ? play : pause} disabled={playerInitializing}>
                    {!playing ? <><PlayCircleIcon /> {t('button.read')}</> : <><PauseCircleIcon /> {t('button.pause')}</>}
                </button>
                <br />
                {news.detailsTranslations[i18n.language]}
            </p>
            {news.address &&
                <p>Adres: <a href={news.address} target="_blank">{news.address}</a></p>
            }
            <hr className="mt-5 mb-5" />
        </div>
    );
}

NewsDetails.propTypes = {
};

NewsDetails.defaultProps = {};

export default NewsDetails;
