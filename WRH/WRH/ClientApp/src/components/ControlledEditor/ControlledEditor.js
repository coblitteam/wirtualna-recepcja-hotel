import React, { Component } from "react";
import PropTypes from 'prop-types';
import { EditorState, convertToRaw, ContentState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';

import styles from './ControlledEditor.module.css';

class ControlledEditor extends Component {
  constructor(props) {
    super(props);


    if (props.value) {
        const contentBlock = htmlToDraft(props.value);
        const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
        const editorState = EditorState.createWithContent(contentState);

        this.state = {
            editorState: editorState
        };
    } else {
        this.state = {
            editorState: EditorState.createEmpty()
        };
    }
  }

  onEditorStateChange = (editorState) => {
    this.props.onChange(draftToHtml(convertToRaw(editorState.getCurrentContent())));

    this.setState({
      editorState
    });
  };

  render() {
    const { editorState } = this.state;
    return (
        <Editor
            editorState={editorState}
            wrapperClassName={styles.EditorWrapper}
            editorClassName={styles.EditorArea}
            onEditorStateChange={this.onEditorStateChange}
        >
            <textarea
                disabled
                value={draftToHtml(convertToRaw(editorState.getCurrentContent()))}
            />
        </Editor>
    );
  }
}

ControlledEditor.propTypes = {
    value: PropTypes.string.isRequired
};

export default ControlledEditor;