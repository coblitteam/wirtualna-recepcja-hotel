﻿import React from 'react';
import styles from './NewsPicker.module.css';

import NewsPicker from './NewsPicker';

import Carousel, { slidesToShowPlugin } from '@brainhubeu/react-carousel';

function CarouselComponent(props) {
    const {
        newsList
    } = props;

    return (
        <Carousel
            className={styles.Carousel}
            plugins={[
                'arrows',
                {
                    resolve: slidesToShowPlugin,
                    options: {
                        numberOfSlides: window.innerWidth <= 1340 ? 2 : 4
                    }
                },
            ]}
        >
            {
                newsList.map((news, index) => {
                    return (
                        <NewsPicker
                            key={index}
                            news={news}
                        />
                    )
                })
            }
        </Carousel>
    );
}

export default CarouselComponent;