import React from 'react';
import PropTypes from 'prop-types';
import styles from './NewsPicker.module.css';

import {
    Button,
    Paper
} from '@mui/material';

import { useTranslation } from 'react-i18next';
import NewsImage from '../../images/news/1.png';

const NewsPicker = (props) => {
    const {
        news
    } = props;
    const { t, i18n } = useTranslation();

    const onSelect = (id) => {
        window.location.href = `/news/${news.id}`;
    }

    return (
        <Paper className={styles.NewsPicker}>
            <img src={news.photo} alt="" />
            <div className={styles.Body}>
                <h1 className={styles.Header}>
                    {news.nameTranslations[i18n.language]}
                </h1>
                <span className={styles.Description}>
                    {news.shortDetailsTranslations[i18n.language]}
                </span>
                <a
                    className={styles.Button}
                    onClick={() => onSelect(news.id)}
                >
                    {t('button.show')}
                </a>
            </div>
        </Paper>
    );
}

NewsPicker.propTypes = {};

NewsPicker.defaultProps = {};

export default NewsPicker;
