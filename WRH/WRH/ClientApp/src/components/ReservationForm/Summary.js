import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from "react-i18next";
import { dateService } from '../../services/DateService';
import styles from './ReservationForm.module.css';

import {
    Checkbox,
    FormControl,
    FormControlLabel,
    FormLabel,
    Grid,
    Radio,
    RadioGroup
} from '@mui/material';

import {
    Person
} from '../../images/icons';

import { apiBooking } from '../../services/api/bookings/Booking';

function Summary(props) {
    const {
        data,
        isCheckIn,
        roomSelectionRequest,
        disabledBoardSelection,
        onPaidChanged,
        onBoardChange
    } = props;
    const { t, i18n } = useTranslation();

    const capacityIcons = (capacity) => {
        var images = []
        for (var i = 0; i < capacity; i++) {
            images.push(<img key={i} src={Person} alt="" />);
        }

        return images
    }

    const setRoomPaid = async (id) => {
        await apiBooking.getCustom(`/room/paid/${id}`);
    }

    return (
        <div>
            <Grid container>
                <Grid item md={6} className={styles.SectionTitleWithButton}>
                    <h1 className={styles.SectionTitle}>{t('reservation.summary.title')}</h1>
                    {isCheckIn &&
                        <a
                            className={styles.Button}
                            onClick={roomSelectionRequest}
                        >
                            {t('button.change_room')}
                        </a>
                    }
                </Grid>
                <Grid item md={6} className={styles.SectionTitleWithPeriod}>
                    <p>
                        {t('reservation.summary.period',
                            {
                                val: data.arrivalDate,
                                val2: data.departureDate
                            }
                        )}
                    </p>
                </Grid>
            </Grid>
            <Grid container className="mt-3">
                <FormControl error={data.checkValid('board')} disabled={disabledBoardSelection}>
                    <FormLabel id="demo-error-radios">{t('reservation.summary.board_type')}</FormLabel>
                    <RadioGroup
                        name="board"
                        value={data.board}
                        onChange={onBoardChange}
                        className="flex-row"
                    >
                        <FormControlLabel value="1" control={<Radio />} label={t('reservation.summary.breakfast')} />
                        <FormControlLabel value="2" control={<Radio />} label={t('reservation.summary.full_board')}  />
                    </RadioGroup>
                </FormControl>
            </Grid>
            <Grid className={styles.Table}>
                <Grid container className={styles.TableHeader}>
                    <Grid item xs={5}>
                        <span>{t('reservation.summary.room.type.title')}</span>
                    </Grid>
                    <Grid item xs={2}>
                        <span>{t('reservation.summary.room.capacity')}</span>
                    </Grid>
                    <Grid item xs={1}>
                        <span>{t('reservation.summary.room.quantity')}</span>
                    </Grid>
                    <Grid item xs={3}>
                        <span>{t('reservation.summary.room.price_for', {
                            val: dateService.diffDays(data.arrivalDate, data.departureDate)
                        })}</span>
                    </Grid>
                    <Grid item xs={1}>
                        <span>{t('reservation.summary.room.paid')}</span>
                    </Grid>
                </Grid>
                {
                    data.roomsSelected &&
                    data.roomsSelected.map(function (room, i) {
                        console.log('room :>> ', room);
                        console.log('i18n :>> ', i18n);

                        return <Grid key={i} container>
                            <Grid item xs={5}>
                                <span>{room.nameTranslations[i18n.language]}</span>
                            </Grid>
                            <Grid item xs={2}>
                                <span>
                                    {capacityIcons(room.capacity)}
                                </span>
                            </Grid>
                            <Grid item xs={1}>
                                <span>
                                    {room.quantity}
                                </span>
                            </Grid>
                            <Grid item xs={3}>
                                <span>{t('common.currency_price', {
                                    val: room.price * dateService.diffDays(data.arrivalDate, data.departureDate)
                                })}</span>
                            </Grid>
                            {data.isAdmin &&
                                <Grid item xs={1} className="d-flex justify-content-center">
                                    <Checkbox
                                        padding="checkbox"
                                        checked={room.isPaid}
                                        sx={{
                                            color: '#715D53',
                                            padding: 0,
                                            '&.Mui-checked': {
                                            color: '#715D53',
                                            },
                                        }}
                                        onChange={async () => {
                                            await setRoomPaid(room.id);

                                            onPaidChanged();
                                        }}
                                    />
                                </Grid>
                            }
                            {!data.isAdmin &&
                                <Grid item xs={1} className="d-flex justify-content-center">
                                    <span>{(room.isPaid) ? "TAK" : "NIE"}</span>
                                </Grid>
                            }
                        </Grid>
                    })}
            </Grid>
        </div>
    );
}

Summary.propTypes = {
    data: PropTypes.object.isRequired,
    isCheckIn: PropTypes.bool.isRequired,
    roomSelectionRequest: PropTypes.func.isRequired
}

export default Summary;