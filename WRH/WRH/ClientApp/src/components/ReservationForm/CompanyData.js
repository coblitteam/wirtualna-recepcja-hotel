import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from "react-i18next";
import { textService } from '../../services/TextService';

import CountrySelector from './CountrySelector';

import {
    Grid,
    TextField
} from '@mui/material';
import InputMask from "react-input-mask";
import styles from './ReservationForm.module.css';
import { Button } from 'reactstrap';


function CompanyData(props) {
    const {
        data,
        disabled,
        onValueChange
    } = props;
    const { t } = useTranslation();

    useEffect(() => {
        const element = document.getElementById("company_data");

        if (element && data.isCheckIn && !data.view) {
            element.scrollIntoView({ behavior: "smooth", block: "start" })
        }
    }, []);

    const copyData = () => {
        onValueChange('companyOwnerName', data.name);
        onValueChange('companyOwnerSurname', data.surname);
        onValueChange('companyCountry', data.country);
        onValueChange('companyPostalCode', data.postalCode);
        onValueChange('companyCity', data.city);
        onValueChange('companyStreet', data.street);
        onValueChange('companyHouseNumber', data.houseNumber);
        onValueChange('companyDoorsNumber', data.doorsNumber);
    }

    return (
        <Grid container>
            <Grid item className="modified-form" md={6}>
                {data.companyCountry === 'PL' &&
                    <InputMask
                        disabled={disabled}
                        mask="999-99-99-999"
                        maskChar="_"
                        value={data.nip}
                        onChange={(event) => {
                            onValueChange('nip', textService.capitalized(event.target.value));
                        }}
                    >
                        {() =>
                            <TextField
                                required
                                disabled={disabled}
                                error={data.checkValid('nip')}
                                margin="dense"
                                id="company_nip"
                                label={t('reservation.company_data.nip')}
                                type="text"
                                fullWidth
                                variant="filled"
                            />
                        }
                    </InputMask>
                }
                {data.country !== 'PL' &&
                    <TextField
                        required
                        disabled={disabled}
                        error={data.checkValid('nip')}
                        margin="dense"
                        id="personal_data_postal_code"
                        label={t('reservation.company_data.nip')}
                        type="text"
                        fullWidth
                        inputProps={{ maxLength: 10 }}
                        variant="filled" value={data.postalCode}
                        onChange={(event) => {
                            onValueChange('nip', event.target.value);
                        }}
                    />
                }
            </Grid>
            <Grid item className="modified-form" md={6}>
                <TextField
                    required
                    disabled={disabled}
                    error={data.checkValid('companyName')}
                    margin="dense"
                    id="company_name"
                    label={t('reservation.company_data.name')}
                    type="text"
                    fullWidth
                    variant="filled"
                    inputProps={{ maxLength: 250 }}
                    value={data.companyName}
                    onChange={(event) => {
                        onValueChange('companyName', textService.capitalized(event.target.value));
                    }}
                />
            </Grid>
            <Grid item md={12}>
                <Button disabled={disabled} className={styles.CopyPersonalData} onClick={copyData}>{t('reservation.company_data.copy_data')}</Button>
            </Grid>
            <Grid item className="modified-form" md={6}>
                <TextField
                    required
                    disabled={disabled}
                    error={data.checkValid('companyOwnerName')}
                    margin="dense"
                    id="company_owner_name"
                    label={t('reservation.company_data.owner_name')}
                    type="text"
                    fullWidth
                    variant="filled"
                    inputProps={{ maxLength: 50 }}
                    value={data.companyOwnerName}
                    onChange={(event) => {
                        onValueChange('companyOwnerName', textService.capitalized(event.target.value));
                    }}
                />
            </Grid>
            <Grid item className="modified-form" md={6}>
                <TextField
                    required
                    disabled={disabled}
                    error={data.checkValid('companyOwnerSurname')}
                    margin="dense"
                    id="company_surname"
                    label={t('reservation.company_data.owner_surname')}
                    type="text"
                    fullWidth
                    variant="filled"
                    inputProps={{ maxLength: 100 }}
                    value={data.companyOwnerSurname}
                    onChange={(event) => {
                        onValueChange('companyOwnerSurname', textService.capitalized(event.target.value));
                    }}
                />
            </Grid>
            <Grid item className="modified-form" md={6}>
                <CountrySelector
                    disabled={disabled}
                    error={data.checkValid('companyCountry')}
                    country={data.companyCountry}
                    onValueChange={(country) => {
                        onValueChange('companyCountry', country);
                    }} />
            </Grid>
            <Grid item className="modified-form" md={6}>
                {data.country === 'PL' &&
                    <InputMask
                        disabled={disabled}
                        mask="99-999"
                        maskChar="_"
                        value={data.companyPostalCode}
                        onChange={(event) => {
                            onValueChange('companyPostalCode', event.target.value);
                        }}
                    >
                        {() =>
                            <TextField
                                required
                                disabled={disabled}
                                error={data.checkValid('companyPostalCode')}
                                margin="dense"
                                id="company_postal_code"
                                label={t('reservation.company_data.postal_code')}
                                type="text"
                                fullWidth
                                variant="filled"
                            />
                        }
                    </InputMask>
                }
                {data.country !== 'PL' &&
                    <TextField
                        required
                        disabled={disabled}
                        error={data.checkValid('companyPostalCode')}
                        margin="dense"
                        id="company_postal_code"
                        label={t('reservation.company_data.postal_code')}
                        type="text"
                        fullWidth
                        inputProps={{ maxLength: 10 }}
                        variant="filled" value={data.companyPostalCode}
                        onChange={(event) => {
                            onValueChange('companyPostalCode', event.target.value);
                        }}
                    />
                }
            </Grid>
            <Grid item className="modified-form" md={6}>
                <TextField
                    required
                    disabled={disabled}
                    error={data.checkValid('companyCity')}
                    margin="dense"
                    id="company_city"
                    label={t('reservation.company_data.city')}
                    type="text"
                    fullWidth
                    variant="filled"
                    inputProps={{ maxLength: 100 }}
                    value={data.companyCity}
                    onChange={(event) => {
                        onValueChange('companyCity', textService.capitalized(event.target.value));
                    }}
                />
            </Grid>
            <Grid item className="modified-form" md={6}>
                <TextField
                    disabled={disabled}
                    margin="dense"
                    id="company_street"
                    label={t('reservation.company_data.street')}
                    type="text"
                    fullWidth
                    variant="filled"
                    inputProps={{ maxLength: 200 }}
                    value={data.companyStreet}
                    onChange={(event) => {
                        onValueChange('companyStreet', textService.capitalized(event.target.value));
                    }}
                />
            </Grid>
            <Grid item className="modified-form" md={6}>
                <TextField
                    required
                    disabled={disabled}
                    error={data.checkValid('companyHouseNumber')}
                    margin="dense"
                    id="company_house_number"
                    label={t('reservation.company_data.house_number')}
                    type="text"
                    fullWidth
                    variant="filled"
                    inputProps={{ maxLength: 20 }}
                    value={data.companyHouseNumber}
                    onChange={(event) => {
                        onValueChange('companyHouseNumber', event.target.value);
                    }}
                />
            </Grid>
            <Grid item className="modified-form" md={6}>
                <TextField
                    disabled={disabled}
                    margin="dense"
                    id="company_doors_number"
                    label={t('reservation.company_data.doors_number')}
                    type="text"
                    fullWidth
                    variant="filled"
                    inputProps={{ maxLength: 20 }}
                    value={data.companyDoorsNumber}
                    onChange={(event) => {
                        onValueChange('companyDoorsNumber', event.target.value);
                    }}
                />
            </Grid>
        </Grid>
    );
}

CompanyData.propTypes = {
    data: PropTypes.object.isRequired,
    disabled: PropTypes.bool,
    onValueChange: PropTypes.func.isRequired
}

export default CompanyData;