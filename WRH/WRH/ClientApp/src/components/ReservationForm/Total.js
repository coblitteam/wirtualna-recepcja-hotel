import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from "react-i18next";
import styles from './ReservationForm.module.css';
import { dateService } from '../../services/DateService';

import {
    Grid
} from '@mui/material';

function Total(props) {
    const {
        data
    } = props;
    const { t, i18n } = useTranslation();
    const [total, setTotal] = React.useState(0);

    useEffect(() => {
        var sum = 0;
        data.roomsSelected.forEach(room => sum += room.price * dateService.diffDays(data.arrivalDate, data.departureDate) * room.quantity);
        data.attractionsSelected.forEach(attraction => sum += attraction.price * attraction.quantity);

        setTotal(sum);
    }, [data])

    return (
        <div>
            <Grid className={styles.Table}>
                <Grid container className={styles.TableHeader}>
                    <Grid item xs={4}>
                        <span>{t('summary.table.name')}</span>
                    </Grid>
                    <Grid item xs={2}>
                        <span>{t('summary.table.quantity')}</span>
                    </Grid>
                    <Grid item xs={3}>
                        <span>{t('summary.table.unitPrice')}</span>
                    </Grid>
                    <Grid item xs={3}>
                        <span>{t('summary.table.price')}</span>
                    </Grid>
                </Grid>
                {data.roomsSelected &&
                 data.roomsSelected.map(function (room, i) {
                    return <Grid key={i} container>
                        <Grid item xs={4}>
                            <span>{room.nameTranslations[i18n.language]}</span>
                        </Grid>
                        <Grid item xs={2}>
                            <span>
                                {room.quantity}
                            </span>
                        </Grid>
                        <Grid item xs={3}>
                            <span>{t('common.currency_price', {
                                    val: room.price * dateService.diffDays(data.arrivalDate, data.departureDate)
                            })}</span>
                        </Grid>
                        <Grid item xs={3}>
                            <span>{t('common.currency_price', {
                                    val: room.price * dateService.diffDays(data.arrivalDate, data.departureDate) * room.quantity
                            })}</span>
                        </Grid>
                    </Grid>
                 })
                }
                {data.attractionsSelected &&
                 data.attractionsSelected.map(function (attraction, i) {
                    return <Grid key={i} container>
                        <Grid item xs={4}>
                            <span>{attraction.nameTranslations[i18n.language]}</span>
                        </Grid>
                        <Grid item xs={2}>
                            <span>
                                {attraction.quantity}
                            </span>
                        </Grid>
                        <Grid item xs={3}>
                            <span>{t('common.currency_price', {
                                    val: attraction.price
                            })}</span>
                        </Grid>
                        <Grid item xs={3}>
                            <span>{t('common.currency_price', {
                                    val: attraction.price * attraction.quantity
                            })}</span>
                        </Grid>
                    </Grid>
                 })
                }
                <Grid container>
                    <Grid item xs={9} />
                    <Grid item xs={3}>
                        <h3>{t('common.currency_price', {
                                    val: total
                        })}</h3>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}

Total.propTypes = {
    data: PropTypes.object.isRequired
}

export default Total;