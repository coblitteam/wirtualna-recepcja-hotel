import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from "react-i18next";
import styles from './ReservationForm.module.css';

import {
    Button,
    Container,
    Checkbox,
    Grid,
    Dialog,
    DialogTitle,
    DialogContent,
    TextField
} from '@mui/material';
import Carousel, { slidesToShowPlugin } from '@brainhubeu/react-carousel';
import { format } from 'date-fns';

import {
    AttractionCarousel,
    AttractionDetails
} from '../../components';
import ArrowBackOutlinedIcon from '@mui/icons-material/ArrowBackOutlined';
import CloseIcon from '@mui/icons-material/Close';

import { apiBooking } from '../../services/api/bookings/Booking';
import { apiAttractionCategory } from '../../services/api/attractionCategories/AttractionCategory';
import { apiAttraction } from '../../services/api/attractions/Attraction';

import {
    Add,
    Remove
} from '../../images/icons';

const localeMap = {
    en: 'dd/MM/yyyy',
    fr: 'dd/MM/yyyy',
    pl: 'dd.MM.yyyy',
    de: 'dd.MM.yyyy',
    es: 'dd/MM/yyyy'
};

function AdditionalAttractions(props) {
    const {
        data,
        isCheckOut,
        isAdmin,
        onAttractionAssign,
        onAttractionQuantityChange,
        onPaidChanged,
        onAttractionRemoved
    } = props;
    const { t, i18n } = useTranslation();

    const [open, setOpen] = React.useState(false);
    const [selectedCategory, setSelectedCategory] = React.useState(null);
    const [selectedAttraction, setSelectedAttraction] = React.useState(null);
    const [categories, setCategories] = React.useState([]);
    const [attractions, setAttractions] = React.useState([]);
    const [quantity, setQuantity] = React.useState(1);

    const openDialog = async () => {
        await getCategories();
        setOpen(true);
    };

    const getCategories = async () => {
        var data = await apiAttractionCategory.getCustom('/list/active');
        setCategories(data.categories);
    }

    const getAttractions = (id) => {
        apiAttraction.getCustom('/by/category/' + id).then((data) => {
            setAttractions(data.attractions);
            setSelectedCategory(id);
        });
    }

    const handleClose = () => {
        setSelectedCategory(null);
        setSelectedAttraction(null);
        setOpen(false);
    }

    const setAttractionPaid = async (id) => {
        await apiBooking.getCustom(`/attraction/paid/${id}`);
    }

    return (
        <div>
            <Grid container className={styles.AdditionalAttractions}>
                <Grid item md={12} className={styles.SectionTitleWithButton}>
                    <h1 className={styles.SectionTitle}>{t('reservation.additional_attractions.title')}</h1>
                    {(!isCheckOut && !isAdmin) &&
                        <div>
                            <Button
                                className={styles.Button}
                                onClick={openDialog}
                            >
                                {t('button.add')}
                            </Button>
                            <Dialog
                                fullWidth={true}
                                maxWidth="xl"
                                open={open}
                                onClose={handleClose}>
                                <div
                                    className={`${selectedCategory == null ? styles.Dialog : ''}`}>
                                    <DialogTitle>
                                        <Grid container className={styles.AdditionalAttractionsDialogHeader}>
                                            <Grid item md="4">
                                                {selectedCategory != null &&
                                                    <ArrowBackOutlinedIcon sx={{
                                                        color: '#715D53',
                                                        fontSize: '2.5rem',
                                                        marginLeft: '10px',
                                                        cursor: 'pointer'
                                                    }}
                                                        onClick={() => {
                                                            if (selectedCategory != null && selectedAttraction == null)
                                                                setSelectedCategory(null);
                                                            else if (selectedAttraction != null)
                                                                setSelectedAttraction(null);
                                                        }}
                                                    />
                                                }
                                            </Grid>
                                            <Grid item md="4">
                                                <h1>{t('reservation.additional_attractions.dialog.title')}</h1>
                                            </Grid>
                                            <Grid item md="4" className={styles.CloseIcon}>
                                                <CloseIcon
                                                    sx={{
                                                        color: '#715D53',
                                                        fontSize: '2.5rem',
                                                        marginLeft: '10px',
                                                        cursor: 'pointer'
                                                    }}
                                                    onClick={handleClose}
                                                />
                                            </Grid>
                                        </Grid>
                                    </DialogTitle>
                                    <DialogContent className={styles.AdditionalAttractionsDialogContent}>
                                        {!selectedCategory &&
                                            <Container maxWidth>
                                                <Grid className={styles.CategoryButtons}>
                                                    <Carousel
                                                        className={styles.Carousel}
                                                        plugins={[
                                                            'arrows',
                                                            {
                                                                resolve: slidesToShowPlugin,
                                                                options: {
                                                                    numberOfSlides: 4
                                                                }
                                                            },
                                                        ]}
                                                    >
                                                        {
                                                            categories.filter((category) => !category.isBoard && category.canDelete).map((category, index) =>
                                                                <Button
                                                                    key={index}
                                                                    className={styles.CategoryButton}
                                                                    onClick={async () => {
                                                                        await getAttractions(category.id);
                                                                    }}
                                                                >
                                                                    {category.name}
                                                                </Button>
                                                            )
                                                        }
                                                    </Carousel>
                                                </Grid>
                                            </Container>
                                        }
                                        {(selectedCategory && !selectedAttraction) &&
                                            <AttractionCarousel
                                                attractions={attractions}
                                                onSelectAttraction={(attraction) => {
                                                    attraction.quantity = 1;
                                                    setSelectedAttraction(attraction);
                                                }}
                                            />
                                        }
                                        {(selectedCategory && selectedAttraction) &&
                                            <Container maxWidth>
                                                <AttractionDetails attraction={selectedAttraction} />
                                                <Grid container spacing={2} className="pb-5 d-flex align-items-baseline">
                                                    {selectedAttraction.paymentType !== 1 &&
                                                        <Grid item md={2} className="modified-form">
                                                            <TextField
                                                                type="number"
                                                                variant="filled"
                                                                label="Ilość"
                                                                size="small"
                                                                margin="none"
                                                                value={quantity}
                                                                onChange={(event) => {
                                                                    setQuantity(Number(event.target.value));
                                                                }}
                                                            />
                                                        </Grid>
                                                    }
                                                    <Grid item md={10} className="d-flex align-items-baseline">
                                                        <Button
                                                            className={styles.Button}
                                                            onClick={() => {
                                                                selectedAttraction.quantity = quantity;
                                                                onAttractionAssign(selectedAttraction);
                                                                setSelectedCategory(null);
                                                                setSelectedAttraction(null);
                                                                setOpen(false);
                                                            }}
                                                        >
                                                            {t('button.add')}
                                                        </Button>
                                                    </Grid>
                                                </Grid>
                                            </Container>
                                        }
                                    </DialogContent>
                                </div>
                            </Dialog>
                        </div>
                    }
                </Grid>
            </Grid>
            <Grid className={styles.Table}>
                <Grid container className={styles.TableHeader} spacing={2}>
                    <Grid item xs={4}>
                        <span>{t('reservation.additional_attractions.type.title')}</span>
                    </Grid>
                    <Grid item xs={1}>
                        <span>{t('reservation.additional_attractions.quantity')}</span>
                    </Grid>
                    <Grid item xs={2}>
                        <span>{t('reservation.additional_attractions.price')}</span>
                    </Grid>
                    <Grid item xs={2}>
                        <span>{t('reservation.additional_attractions.payment')}</span>
                    </Grid>
                    <Grid item xs={2}>
                        <span>{t('reservation.additional_attractions.ordered.title')}</span>
                    </Grid>
                    <Grid item xs={1}>
                        <span>{t('reservation.additional_attractions.paid')}</span>
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item xs={12}>
                        <span className="text-secondary text-underline"><u>{t('reservation.additional_attractions.when_checking_in')}</u></span>
                    </Grid>
                </Grid>
                {
                    data.attractionsSelected &&
                    data.attractionsSelected.map(function (attraction, i) {
                        if (!attraction.immediatePayment)
                            return;

                        return <Grid key={i} container spacing={2}>
                            <Grid item xs={4}>
                                <span>{attraction.nameTranslations[i18n.language]}</span>
                                {(!attraction.isPaid && attraction.canDelete) &&
                                    <CloseIcon className="text-danger" role="button" onClick={async () => await onAttractionRemoved(attraction)}/>
                                }
                            </Grid>
                            <Grid item xs={1}>
                                <TextField
                                    disabled={attraction.paymentType === 1 || attraction.board || attraction.isPaid || !attraction.canDelete}
                                    type="number"
                                    fullWidth
                                    variant="standard"
                                    size="small"
                                    margin="none"
                                    value={attraction.quantity}
                                    onChange={(event) => {
                                        let quantity = Number(event.target.value);
                                        let max = data.numberOfAdults + data.numberOfChildren;
                                        attraction.quantity = (attraction.paymentType == 2 && quantity > max) ? max : Number(event.target.value);

                                        onAttractionQuantityChange(attraction);
                                    }}
                                />
                            </Grid>
                            <Grid item xs={2}>
                                <span>{t('common.currency_price', {
                                    val: attraction.price * attraction.quantity
                                })}</span>
                            </Grid>
                            <Grid item xs={2}>
                                <span>
                                    {(attraction.immediatePayment) ? t('reservation.additional_attractions.when_checking_in') : t('reservation.additional_attractions.after_implementation')}
                                </span>
                            </Grid>
                            <Grid item xs={2}>
                                {attraction.createdAt &&
                                <span>
                                    {format(new Date(attraction.createdAt), localeMap[i18n.language])}
                                </span>
                                }
                            </Grid>
                            {data.isAdmin &&
                                <Grid item xs={1} className="d-flex justify-content-center">
                                    <Checkbox
                                        padding="checkbox"
                                        checked={attraction.isPaid}
                                        sx={{
                                            color: '#715D53',
                                            padding: 0,
                                            '&.Mui-checked': {
                                            color: '#715D53',
                                            },
                                        }}
                                        onChange={async () => {
                                            await setAttractionPaid(attraction.id);

                                            onPaidChanged();
                                        }}
                                    />
                                </Grid>
                            }
                            {!data.isAdmin &&
                                <Grid item xs={1} className="d-flex justify-content-center">
                                    <span>{(attraction.isPaid) ? t('reservation.additional_attractions.yes') : t('reservation.additional_attractions.no')}</span>
                                </Grid>
                            }
                        </Grid>
                    })
                }
                <Grid container className="mt-3">
                    <Grid item xs={12}>
                        <span className="text-secondary text-underline"><u>{t('reservation.additional_attractions.after_implementation')}</u></span>
                    </Grid>
                </Grid>
                {
                    data.attractionsSelected &&
                    data.attractionsSelected.map(function (attraction, i) {
                        if (attraction.immediatePayment)
                            return;

                        return <Grid key={i} container spacing={2}>
                            <Grid item xs={4}>
                                <span>{attraction.name}</span>
                                {!attraction.isPaid &&
                                    <CloseIcon className="text-danger" role="button" onClick={async () => await onAttractionRemoved(attraction)} />
                                }
                            </Grid>
                            <Grid item xs={1}>
                                <TextField
                                    disabled={attraction.paymentType === 1 || attraction.board || attraction.isPaid}
                                    type="number"
                                    fullWidth
                                    variant="standard"
                                    size="small"
                                    margin="none"
                                    inputProps={{ min: 0, max: data.numberOfAdults + data.numberOfChildren }}
                                    value={attraction.quantity}
                                    onChange={(event) => {
                                        attraction.quantity = Number(event.target.value);

                                        onAttractionQuantityChange(attraction);
                                    }}
                                />
                            </Grid>
                            <Grid item xs={2}>
                                <span>{t('common.currency_price', {
                                    val: attraction.price * attraction.quantity
                                })}</span>
                            </Grid>
                            <Grid item xs={2}>
                                <span>
                                    {(attraction.immediatePayment) ? t('reservation.additional_attractions.when_checking_in') : t('reservation.additional_attractions.after_implementation')}
                                </span>
                            </Grid>
                            <Grid item xs={2}>
                                {attraction.createdAt &&
                                <span>
                                    {format(new Date(attraction.createdAt), localeMap[i18n.language])}
                                </span>
                                }
                            </Grid>
                            {data.isAdmin &&
                                <Grid item xs={1} className="d-flex justify-content-center">
                                    <Checkbox
                                        padding="checkbox"
                                        checked={attraction.isPaid}
                                        sx={{
                                            color: '#715D53',
                                            padding: 0,
                                            '&.Mui-checked': {
                                            color: '#715D53',
                                            },
                                        }}
                                        onChange={async () => {
                                            await setAttractionPaid(attraction.id);

                                            onPaidChanged();
                                        }}
                                    />
                                </Grid>
                            }
                            {!data.isAdmin &&
                                <Grid item xs={1} className="d-flex justify-content-center">
                                    <span>{(attraction.isPaid) ? t('reservation.additional_attractions.yes') : t('reservation.additional_attractions.no')}</span>
                                </Grid>
                            }
                        </Grid>
                    })
                }
            </Grid>
        </div>
    )
}

AdditionalAttractions.propTypes = {
    data: PropTypes.object.isRequired,
    isCheckOut: PropTypes.bool,
    isAdmin: PropTypes.bool,
    onAttractionAssign: PropTypes.func.isRequired,
    onAttractionQuantityChange: PropTypes.func.isRequired,
    onAttractionRemoved: PropTypes.func.isRequired
}

export default AdditionalAttractions;