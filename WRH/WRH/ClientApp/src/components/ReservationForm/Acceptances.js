import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from "react-i18next";
import styles from './ReservationForm.module.css';

import {
    Checkbox,
    Grid,
    FormControlLabel
} from '@mui/material';

function Acceptances(props) {
    const {
        data,
        onChange
    } = props;
    const { t } = useTranslation();

    return (
        <Grid container>
            <Grid item xs={12} className={styles.Acceptance}>
                <FormControlLabel
                    className={`${styles.AcceptanceLabel} ${(data.acceptance1) ? styles.AcceptanceLabelChecked : ''}`}
                    control={
                        <Checkbox
                            className={styles.AcceptanceCheckbox}
                            checked={data.acceptance1}
                            onChange={(event) => {
                                onChange('acceptance1', event.target.checked)
                            }}
                        />
                    }
                    label={t('reservation.acceptances.acceptance1')} />
            </Grid>
            <Grid item xs={12} className={styles.Acceptance}>
                <FormControlLabel
                    className={`${styles.AcceptanceLabel} ${(data.acceptance2) ? styles.AcceptanceLabelChecked : ''}`}
                    control={
                        <Checkbox
                            className={styles.AcceptanceCheckbox}
                            checked={data.acceptance2}
                            onChange={(event) => {
                                onChange('acceptance2', event.target.checked)
                            }}
                        />
                    }
                    label={t('reservation.acceptances.acceptance2')} />
            </Grid>
            <Grid item xs={12} className={styles.Acceptance}>
                <FormControlLabel
                    className={`${styles.AcceptanceLabel} ${(data.needInvoice) ? styles.AcceptanceLabelChecked : ''}`}
                    control={
                        <Checkbox
                            className={styles.AcceptanceCheckbox}
                            checked={data.needInvoice}
                            onChange={(event) => {
                                onChange('needInvoice', event.target.checked);

                                if (!event.target.checked)
                                    data.clearCompanyData();
                            }}
                        />
                    }
                    label={t('reservation.acceptances.need_invoice')} />
            </Grid>
        </Grid>
    );
}

Acceptances.propTypes = {
    data: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired
}

export default Acceptances;