import * as React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from "react-i18next";

import {
    MenuItem,
    TextField
} from '@mui/material';

function CountrySelector(props) {
    const {
        disabled,
        country,
        onValueChange,
        error
    } = props;
    const { t, i18n } = useTranslation();
    const countries = require("localized-countries")(require("localized-countries/data/" + i18n.language));

    return (
        <TextField
            required
            disabled={disabled}
            error={error}
            select
            margin="dense"
            id="personal_data_country"
            label={t('reservation.personal_data.country')}
            type="text"
            fullWidth
            variant="filled"
            value={country}
            onChange={(event) => {
                onValueChange(event.target.value);
            }}
        >
            {countries.array().map((option) => (
                <MenuItem key={option.code} value={option.code}>
                    {option.label}
                </MenuItem>
            ))}
        </TextField>
    );
}

CountrySelector.propTypes = {
    country: PropTypes.string.isRequired,
    onValueChange: PropTypes.func.isRequired,
    error: PropTypes.bool.isRequired
};

export default CountrySelector;