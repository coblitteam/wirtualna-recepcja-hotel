import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from "react-i18next";
import { format } from 'date-fns';

import {
    Grid,
    TextField
} from '@mui/material';

function Data(props) {
    const {
        data,
        isAdmin
    } = props;
    const { t } = useTranslation();

    return (
        <div>
            <Grid container>
                <Grid item className="modified-form" md={6}>
                    <TextField
                        disabled
                        margin="dense"
                        id="creation_date"
                        label={t('reservation.data.creation_date.title')}
                        type="text"
                        fullWidth
                        variant="filled"
                        value={t('reservation.data.creation_date.value', { val: data.createdAt })}
                    />
                </Grid>
                <Grid item className="modified-form" md={6}>
                    <TextField
                        disabled
                        margin="dense"
                        id="stay_number"
                        label={t('reservation.data.stay_number')}
                        type="text"
                        fullWidth
                        variant="filled"
                        value={data.stayNumber}
                    />
                </Grid>
            </Grid>
            {isAdmin &&
                <Grid container>
                    <Grid item className="modified-form" md={6}>
                        <TextField
                            disabled
                            margin="dense"
                            id="status"
                            label="Status"
                            type="text"
                            fullWidth
                            variant="filled"
                            value={data.status}
                        />
                    </Grid>
                    <Grid item md={6} />
                    <Grid item className="modified-form" md={6}>
                        <TextField
                            disabled
                            margin="dense"
                            id="status"
                            label="Ostatnia aktualizacja"
                            type="text"
                            fullWidth
                            variant="filled"
                            value={data.lastEditUserName}
                        />
                    </Grid>
                    <Grid item className="modified-form" md={6}>
                        <TextField
                            disabled
                            margin="dense"
                            id="status"
                            label="Data aktualizacji"
                            type="text"
                            fullWidth
                            variant="filled"
                            value={(data.lastEditTimeStamp) ? format(new Date(data.lastEditTimeStamp), "dd.MM.yyyy HH:mm") : ""}
                        />
                    </Grid>
                </Grid>
            }
        </div>
    );
}

Data.propTypes = {
    data: PropTypes.object.isRequired,
    isAdmin: PropTypes.bool
};

export default Data;