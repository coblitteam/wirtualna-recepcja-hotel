import React, { Component } from 'react';

import {
    Alert,
    Button,
    Grid,
    TextField,
    MenuItem
} from '@mui/material';
import {
    DateTimePicker,
    Carousel
} from '../';
import Data from './Data';
import PersonalData from './PersonalData';
import Summary from './Summary';
import AdditionalAttractions from './AdditionalAttractions';
import Acceptances from './Acceptances';
import CompanyData from './CompanyData';
import Total from './Total';
import '@brainhubeu/react-carousel/lib/style.css';

import { withTranslation } from "react-i18next";
import { dateService } from '../../services/DateService';
import { apiBooking } from '../../services/api/bookings/Booking';
import { apiRoom } from '../../services/api/rooms/Room';
import styles from './ReservationForm.module.css';
import { Container } from 'reactstrap';
import CountryCodes from '../../locales/CountryCodes';

import { format, isThisSecond } from 'date-fns';

export class ReservationForm extends Component {
    constructor(props) {
        super(props);

        if (props.data) {
            this.booking = props.data.booking;
            this.isCheckIn = props.checkIn;
            this.isCheckOut = props.checkOut;
            this.isAdmin = props.isAdmin;
            this.view = props.view;

            const validateField = (list, name, value) => {
                if (!value || !value.toString().length) {
                    list.push(name);
                } else {
                    list = list.filter((fieldName) => fieldName !== name);
                }
            }

            const checkValid = (name) => {
                return this.state.fieldErrors.filter((fieldName) => fieldName === name).length > 0;
            }

            const clearCompanyData = () => {
                let companyDataFields = [
                    'nip',
                    'companyName',
                    'companyOwnerName',
                    'companyOwnerSurname',
                    'companyCountry',
                    'companyPostalCode',
                    'companyCity',
                    'companyStreet',
                    'companyHouseNumber'
                ];

                this.setState({
                    nip: '',
                    companyName: '',
                    companyOwnerName: '',
                    companyOwnerSurname: '',
                    companyCountry: 'PL',
                    companyPostalCode: '',
                    companyCity: '',
                    companyStreet: '',
                    companyHouseNumber: '',
                    companyDoorsNumber: '',
                    fieldErrors: this.state.fieldErrors.filter((fieldName) => !companyDataFields.includes(fieldName))
                });
            };

            this.state = {
                id: this.booking.id,
                bookingNumber: this.booking.bookingNumber,
                stayNumber: this.booking.stayNumber,
                createdAt: new Date(this.booking.createdAt),
                lastEditUserName: this.booking.lastEditUserName,
                lastEditTimeStamp: this.booking.lastEditTimeStamp,
                status: this.booking.status,
                arrivalDate: new Date(this.booking.arrivalDate),
                departureDate: new Date(this.booking.departureDate),
                numberOfAdults: this.booking.numberOfAdults,
                numberOfChildren: this.booking.numberOfChildren,
                numberOfChildrenToThree: this.booking.numberOfChildrenToThree,
                board: this.booking.board,
                name: this.booking.personalData?.name,
                surname: this.booking.personalData?.surname,
                country: this.booking.personalData?.country ?? 'PL',
                postalCode: this.booking.personalData?.postalCode,
                city: this.booking.personalData?.city,
                street: this.booking.personalData?.street,
                houseNumber: this.booking.personalData?.houseNumber,
                doorsNumber: this.booking.personalData?.doorsNumber,
                phoneCountryNumber: this.booking.personalData?.phoneCountryNumber ?? '+48',
                phoneNumber: this.booking.personalData?.phoneNumber,
                email: this.booking.personalData?.email,
                nip: this.booking.companyData?.nip,
                companyName: this.booking.companyData?.companyName,
                companyOwnerName: this.booking.companyData?.name,
                companyOwnerSurname: this.booking.companyData?.surname,
                companyCountry: this.booking.companyData?.country ?? 'PL',
                companyPostalCode: this.booking.companyData?.postalCode,
                companyCity: this.booking.companyData?.city,
                companyStreet: this.booking.companyData?.street,
                companyHouseNumber: this.booking.companyData?.houseNumber,
                companyDoorsNumber: this.booking.companyData?.doorsNumber,
                acceptance1: false,
                acceptance2: false,
                needInvoice: false,
                invalidDatesAlert: false,
                guests: (this.booking.guests?.length) ? this.booking.guests : [],
                roomsSelected: (this.booking.rooms?.length) ? this.booking.rooms : [],
                attractionsSelected: (this.booking.attractions?.length) ? this.booking.attractions : [],
                rooms: null,
                toPay: this.booking.toPay,
                validateField: validateField,
                checkValid: checkValid,
                clearCompanyData: clearCompanyData,
                fieldErrors: [],
                isCheckIn: this.isCheckIn,
                isAdmin: this.isAdmin,
                CountryCodes: CountryCodes,
                showForm: this.isAdmin || this.isCheckOut || this.view
            }
        };

        this.handleBoardChange = this.handleBoardChange.bind(this);
        this.handleRoomSelected = this.handleRoomSelected.bind(this);
    }

    validateDates(arrival, departure) {
        if (arrival < departure) {
            this.setState({ invalidDatesAlert: false });
            return true;
        } else {
            this.setState({ invalidDatesAlert: true });
            return false;
        }
    }

    loadRooms() {
        apiRoom.postCustom('/match', {
            arrivalDate: new Date(this.state.arrivalDate).toJSON(),
            departureDate: new Date(this.state.departureDate).toJSON()
        }).then((data) => {
            this.state.rooms = data.types;
            this.setState({
                rooms: this.state.rooms
            });
        });

        this.jumpToChooseRooms();
    }

    jumpToChooseRooms() {
        const element = document.getElementById("choose_rooms");

        if (element) {
            element.scrollIntoView({ behavior: "smooth", block: "start" })
        }
    }

    async validateForm() {
        let errors = this.state.fieldErrors ?? [];

        this.state.validateField(errors, 'board', this.state.board);
        this.state.validateField(errors, 'name', this.state.name);
        this.state.validateField(errors, 'surname', this.state.surname);
        this.state.validateField(errors, 'country', this.state.country);
        this.state.validateField(errors, 'postalCode', this.state.postalCode);
        this.state.validateField(errors, 'city', this.state.city);
        this.state.validateField(errors, 'houseNumber', this.state.houseNumber);
        this.state.validateField(errors, 'phoneCountryNumber', this.state.phoneCountryNumber);
        this.state.validateField(errors, 'phoneNumber', this.state.phoneNumber);
        this.state.validateField(errors, 'email', this.state.email);

        if (this.state.needInvoice) {
            this.state.validateField(errors, 'nip', this.state.nip);
            this.state.validateField(errors, 'companyName', this.state.companyName);
            this.state.validateField(errors, 'companyOwnerName', this.state.companyOwnerName);
            this.state.validateField(errors, 'companyOwnerSurname', this.state.companyOwnerSurname);
            this.state.validateField(errors, 'companyCountry', this.state.companyCountry);
            this.state.validateField(errors, 'companyPostalCode', this.state.companyPostalCode);
            this.state.validateField(errors, 'companyCity', this.state.companyCity);
            this.state.validateField(errors, 'companyHouseNumber', this.state.companyHouseNumber);
        }

        this.setState({
            fieldErrors: errors
        });
    }

    async post() {
        await this.validateForm();

        if (!this.isAdmin && !this.view && (!this.state.acceptance1 || !this.state.acceptance2)) {
            alert("Zaakceptuj zgody");
            return;
        }

        if (this.state.fieldErrors.length > 0) {
            alert("Popraw formularz");
            return;
        }

        let data = {
            id: this.state.id,
            arrivalDate: new Date(this.state.arrivalDate).toJSON(),
            departureDate: new Date(this.state.departureDate).toJSON(),
            numberOfAdults: this.state.numberOfAdults,
            numberOfChildren: this.state.numberOfChildren,
            numberOfChildrenToThree: this.state.numberOfChildrenToThree,
            board: this.state.board,
            personalData: {
                name: this.state.name,
                surname: this.state.surname,
                country: this.state.country,
                postalCode: this.state.postalCode,
                city: this.state.city,
                street: this.state.street,
                houseNumber: this.state.houseNumber,
                doorsNumber: this.state.doorsNumber,
                phoneCountryNumber: this.state.phoneCountryNumber,
                phoneNumber: this.state.phoneNumber,
                email: this.state.email
            },
            companyData: (this.state.needInvoice) ? {
                nip: this.state.nip,
                companyName: this.state.companyName,
                name: this.state.companyOwnerName,
                surname: this.state.companyOwnerSurname,
                country: this.state.companyCountry,
                postalCode: this.state.companyPostalCode,
                city: this.state.companyCity,
                street: this.state.companyStreet,
                houseNumber: this.state.companyHouseNumber,
                doorsNumber: this.state.companyDoorsNumber,
            } : null,
            guests: this.state.guests.filter((guest) => guest.name && guest.surname && guest.address && guest.birthday),
            rooms: this.state.roomsSelected.map((room) => {
                console.log(room);

                return {
                    id: room.id,
                    roomId: room.roomId,
                    quantity: room.quantity,
                    price: room.price,
                    isPaid: room.isPaid
                };
            }),
            attractions: this.state.attractionsSelected.map((attraction) => {
                return {
                    id: attraction.id,
                    attractionId: attraction.attractionId,
                    quantity: attraction.quantity,
                    price: attraction.price,
                    isPaid: attraction.isPaid
                };
            }),
            isAdmin: this.isAdmin
        }

        await apiBooking.put(data).then((response) => {
            if (response.status.statusCode === 'SUCCESS')
                window.location.href = response.redirectUri;
            else if (response.status.statusCode === 'NOT_NEEDED')
                window.location.href = (this.isAdmin) ? '/admin' : '/';
            else
                alert('Błąd przetwarzania płatności');
        });
    }

    async checkOut() {
        await apiBooking.getCustom('/check-out/' + this.state.id).then((response) => {
            if (response.success)
                window.location.href = "/checkout-summary"
        });
    }

    refreshSummary(id) {
        apiBooking.getSingle(id).then((data) => {
            this.setState({
                roomsSelected: (data.booking.rooms?.length) ? data.booking.rooms : [],
                attractionsSelected: (data.booking.rooms?.length) ? data.booking.attractions : [],
                toPay: data.booking.toPay
            });
        });
    }

    checkValidInitFields(arrival, departure, adults) {
        return arrival && departure && adults;
    }

    calculateAttractions() {
        let daysAmount = dateService.diffDays(this.state.arrivalDate, this.state.departureDate);

        let attractions = this.state.attractionsSelected.map((attraction) => {
            if (attraction.paymentType === 1)
                attraction.quantity = daysAmount;

            if (attraction.paymentType === 2) {
                if (attraction.canDelete || attraction.board)
                    attraction.price = attraction.grosPrice * daysAmount;
                else
                    attraction.price = attraction.grosPrice * (daysAmount > 1 ? daysAmount : 0);

                if (attraction.quantity > this.state.numberOfAdults + this.state.numberOfChildren)
                    attraction.quantity = this.state.numberOfAdults + this.state.numberOfChildren;
            }

            if (attraction.board && !attraction.forChildren)
                attraction.quantity = this.state.numberOfAdults;

            if (attraction.board && attraction.forChildren) {
                attraction.quantity = this.state.numberOfChildren;
                attraction.price = Math.round(attraction.price / 2);
            }

            if (!attraction.canDelete) {
                attraction.quantity = (this.state.numberOfAdults + this.state.numberOfChildren + this.state.numberOfChildrenToThree);
            }

            return attraction;
        });

        this.setState({
            attractionsSelected: attractions
        });
    }

    calculatePeriodDiscount() {
        let rooms = this.state.roomsSelected ?? [];
        let days = dateService.diffDays(this.state.arrivalDate, this.state.departureDate);

        rooms.forEach(element => {
            if (days >= 3 && days <= 4)
                element.price = Math.ceil(element.tempPrice * 0.85);
            else if (days >= 5)
                element.price = Math.ceil(element.tempPrice * 0.8);
            else
                element.price = element.tempPrice;
        });

        this.setState({
            roomsSelected: rooms
        });
    }

    handleBoardChange (event) {
        if (event)
            this.setState({
                board: event.target.value
            }, () => {
                let attractions = this.state.attractionsSelected.filter((attraction) => !attraction.board);
                let board = this.booking.boardAttractions.filter((attraction) => attraction.board == this.state.board)[0];

                if (board) {
                    let adultsBoard = Object.assign({}, board);
                    let childrenBoard = Object.assign({}, board);

                    if (this.state.numberOfAdults > 0) {
                        adultsBoard.forChildren = false;
                        adultsBoard.quantity = this.state.numberOfAdults;
                        adultsBoard.price = board.price * dateService.diffDays(this.state.arrivalDate, this.state.departureDate);

                        attractions.push(adultsBoard);
                    }

                    if (this.state.numberOfChildren > 0) {
                        adultsBoard.forChildren = true;
                        childrenBoard.quantity = this.state.numberOfChildren;
                        childrenBoard.price = Math.round(board.price / 2) * dateService.diffDays(this.state.arrivalDate, this.state.departureDate);

                        attractions.push(childrenBoard);
                    }

                    this.setState({
                        attractionsSelected: attractions
                    });
                }
            });
    }

    handleRoomSelected (roomsSelected) {
        let rooms = [];
        let adults = this.state.numberOfAdults;
        let children = this.state.numberOfChildren;

        roomsSelected.forEach(room => {
            for (let i = 0; i < room.quantity; i++) {
                let temp = Object.assign({}, room);
                temp.quantity = 1;

                if (room.capacity <= adults)
                {
                    temp.price = room.capacity * room.price;
                    temp.tempPrice = temp.price;
                    temp.test = 'only adults';

                    adults -= room.capacity;
                }
                else if (room.capacity > adults && room.capacity <= children) {
                    temp.price = room.capacity * Math.ceil(room.price * 0.7);
                    temp.tempPrice = temp.price;
                    temp.test = 'only children';

                    children -= room.capacity;
                }
                else if (adults > 0 && room.capacity > adults && room.capacity <= adults + children) {
                    let tempChildren = room.capacity - adults;
                    let adultsPrice = adults * room.price;
                    let childrenPrice = tempChildren * Math.ceil(room.price * 0.7);

                    children -= tempChildren;
                    adults = 0;

                    temp.price = adultsPrice + childrenPrice;
                    temp.tempPrice = temp.price;
                    temp.test = 'adults + children';
                }
                else {
                    temp.price = adults * ((adults > 1) ? room.price : room.priceOnce);
                    temp.tempPrice = temp.price;
                    adults = 0;
                    temp.test = 'once adults';
                }

                rooms.push(temp);
            }
        });

        this.setState({
            roomsSelected: rooms
        }, this.calculatePeriodDiscount);
    }

    render() {
        return (
            <div>
                <Container>
                    <Grid className="modified-form mb-5">
                        <DateTimePicker
                            disabled={!this.isCheckIn}
                            label="reservation.arrival_date"
                            helperText="reservation.arrival_min_time"
                            date={this.state.arrivalDate}
                            checkMinDate={this.isCheckIn}
                            onDateChange={(newDate) => {
                                if (this.validateDates(newDate, this.state.departureDate)) {
                                    this.setState({
                                        arrivalDate: newDate
                                    }, () => {
                                        this.calculateAttractions();
                                        this.calculatePeriodDiscount();
                                    });

                                    if (this.checkValidInitFields(newDate, this.state.departureDate, this.state.numberOfAdults)) {
                                        this.setState({
                                            showForm: true
                                        });

                                        this.loadRooms();
                                    }
                                }
                            }}
                        />
                        <DateTimePicker
                            disabled={!this.isCheckIn}
                            label="reservation.departure_date"
                            helperText="reservation.departure_max_time"
                            date={this.state.departureDate}
                            checkMinDate={this.isCheckIn}
                            onDateChange={(newDate) => {
                                if (this.validateDates(this.state.arrivalDate, newDate)) {
                                    this.setState({
                                        departureDate: newDate
                                    }, () => {
                                        this.calculateAttractions();
                                        this.calculatePeriodDiscount();
                                    });

                                    if (this.checkValidInitFields(this.state.arrivalDate, newDate, this.state.numberOfAdults)) {
                                        this.setState({
                                            showForm: true
                                        });

                                        this.loadRooms();
                                    }
                                }
                            }}
                        />
                    </Grid>
                    {this.state.invalidDatesAlert &&
                        <Alert
                            severity="error"
                            className="mt-3"
                        >
                            {this.props.t('reservation.invalid_dates')}
                        </Alert>
                    }
                    <Grid className="modified-form">
                        <TextField
                            disabled={!this.isCheckIn}
                            margin="dense"
                            id="number_of_adults"
                            label={this.props.t('reservation.number_of_adults')}
                            helperText={this.props.t('reservation.from_11_yo_included')}
                            type="number"
                            fullWidth
                            variant="filled"
                            inputProps={{ max: 4 }}
                            value={this.state.numberOfAdults}
                            onChange={(event) => {
                                let value = event.target.value;
                                this.setState({
                                    numberOfAdults: value === '' ? '' : value < 0 ? 0 : Number(value),
                                }, this.calculateAttractions);

                                if (this.checkValidInitFields(this.state.arrivalDate, this.state.departureDate, event.target.value)) {
                                    this.setState({
                                        showForm: true
                                    });

                                    this.loadRooms();
                                }
                            }}
                        />
                        <TextField
                            disabled={!this.isCheckIn}
                            margin="dense"
                            id="number_of_children"
                            label={this.props.t('reservation.number_of_children')}
                            type="number"
                            fullWidth
                            variant="filled"
                            inputProps={{ max: 4 }}
                            value={this.state.numberOfChildren}
                            onChange={(event) => {
                                let value = event.target.value;
                                this.setState({
                                    numberOfChildren: value === '' ? '' : value < 0 ? 0 : Number(value),
                                }, this.calculateAttractions);

                                if (this.checkValidInitFields(this.state.arrivalDate, this.state.departureDate, this.state.numberOfAdults)) {
                                    this.setState({
                                        showForm: true
                                    });

                                    this.loadRooms();
                                }
                            }}
                        />
                        <TextField
                            disabled={!this.isCheckIn}
                            margin="dense"
                            id="number_of_children_to_three_yo"
                            label={this.props.t('reservation.number_of_children_to_three_yo')}
                            type="number"
                            fullWidth
                            variant="filled"
                            inputProps={{ max: 4 }}
                            value={this.state.numberOfChildrenToThree}
                            onChange={(event) => {
                                let value = event.target.value;
                                this.setState({
                                    numberOfChildrenToThree: value === '' ? '' : value < 0 ? 0 : Number(value),
                                }, this.calculateAttractions);

                                if (this.checkValidInitFields(this.state.arrivalDate, this.state.departureDate, this.state.numberOfAdults)) {
                                    this.setState({
                                        showForm: true
                                    }, this.loadRooms);
                                }
                            }}
                        />
                    </Grid>
                </Container>
                {this.state.showForm &&
                    <div>
                        {(this.isCheckIn && !this.isCheckOut) &&
                            <Grid>
                                <hr id="choose_rooms" className="mt-5 mb-5" />
                                <h1 className={`${styles.SectionTitle} ${styles.Center}`}>{this.props.t('reservation.choose_room.title')}</h1>
                                {(this.state.rooms && this.state.rooms.length > 0) &&
                                    <Carousel data={this.state} rooms={this.state.rooms} onRoomsChange={this.handleRoomSelected} />
                                }
                                {!this.state.rooms || !this.state.rooms.length &&
                                    <h3 className={styles.Center}>
                                        Brak dostępnych pokoi w wybranym okresie czasu. Wybierz inny zakres dat.
                                    </h3>
                                }
                            </Grid>
                        }
                        <Container>
                            <hr className="mt-5 mb-5" />
                            <h1 className={styles.SectionTitle}>{this.props.t('reservation.data.title')}</h1>
                            <Data data={this.state} isAdmin={this.isAdmin} />
                            <hr className="mt-5 mb-5" />
                        </Container>
                        <Container>
                            <h1 className={styles.SectionTitle}>{this.props.t('reservation.personal_data.title')}</h1>
                            <PersonalData disabled={!this.isCheckIn && !this.isAdmin} data={this.state} onValueChange={(prop, value) => {
                                this.setState({
                                    [prop]: value
                                });
                            }} />
                            <hr className="mt-5 mb-5" />
                        </Container>
                        <Container>
                            <Summary
                                data={this.state}
                                rooms={this.state.roomsSelected}
                                isCheckIn={this.isCheckIn && !this.isCheckOut}
                                disabledBoardSelection={!this.isCheckIn}
                                roomSelectionRequest={this.jumpToChooseRooms}
                                onBoardChange={this.handleBoardChange}
                                onPaidChanged={() => {
                                    this.refreshSummary(this.state.id);
                                }}
                            />
                            <hr className="mt-5 mb-5" />
                        </Container>
                        {(!this.isAdmin || this.state.attractionsSelected.length > 0) &&
                            <Container>
                                <AdditionalAttractions
                                    data={this.state}
                                    isCheckOut={this.isCheckOut}
                                    isAdmin={this.isAdmin}
                                    onAttractionAssign={(attraction) => {
                                        let attractions = this.state.attractionsSelected;
                                        attraction.attractionId = attraction.id;
                                        attraction.id = 0;
                                        attraction.createdAt = new Date();
                                        attraction.quantity = attraction.quantity;
                                        attraction.price = attraction.grosPrice;

                                        if (attraction.paymentType === 1)
                                            attraction.quantity = dateService.diffDays(this.state.arrivalDate, this.state.departureDate);

                                        if (attraction.paymentType === 2 && attraction.quantity > this.state.numberOfAdults + this.state.numberOfChildren)
                                            attraction.quantity = this.state.numberOfAdults + this.state.numberOfChildren;

                                        if (attraction.paymentType === 2)
                                            attraction.price = attraction.price * dateService.diffDays(this.state.arrivalDate, this.state.departureDate);

                                        attractions.push(attraction);

                                        this.setState({
                                            attractionsSelected: attractions
                                        });
                                    }}
                                    onAttractionQuantityChange={(attraction) => {
                                        let attractions = this.state.attractionsSelected.map((e) => {
                                            if (e.id === attraction.id && e.name === attraction.name)
                                                if (e.paymentType === 2 && e.quantity > this.state.numberOfAdults + this.state.numberOfChildren)
                                                    e.quantity = this.state.numberOfAdults + this.state.numberOfChildren;
                                                else
                                                    e.quantity = attraction.quantity;

                                            return e;
                                        });

                                        this.setState({
                                            attractionsSelected: attractions
                                        });
                                    }}
                                    onPaidChanged={() => {
                                        this.refreshSummary(this.state.id);
                                    }}
                                    onAttractionRemoved={async (attraction) => {
                                        if (attraction.id)
                                            await apiBooking.getCustom('/attraction/remove/' + attraction.id);

                                        let attractions = this.state.attractionsSelected.filter(e => e !== attraction);

                                        this.setState({
                                            attractionsSelected: attractions
                                        });
                                    }} />
                                <hr className="mt-5 mb-5" />
                            </Container>
                        }
                        {!this.isAdmin &&
                            <Container>
                                <h1 className={styles.SectionTitle}>{this.props.t('reservation.total')}</h1>
                                <Total data={this.state} />
                                <hr className="mt-5 mb-5" />
                            </Container>
                        }
                        {this.isAdmin &&
                            <Container>
                                <Grid container>
                                    <Grid item md={6}>
                                        <span className={styles.ToPayLabel}>Do zapłaty</span>
                                    </Grid>
                                    <Grid item md={6}>
                                        <span className={styles.ToPayValue}>{this.state.toPay}zł</span>
                                    </Grid>
                                </Grid>
                            </Container>
                        }
                        {(this.isCheckIn && !this.isCheckOut) &&
                            <Container>
                                <Acceptances data={this.state} onChange={(prop, value) => {
                                    this.setState({
                                        [prop]: value
                                    })
                                }} />
                                <hr className="mt-5 mb-5" />
                            </Container>
                        }
                        {(this.state.needInvoice || (!this.isCheckIn && this.booking.companyData)) &&
                            <Container>
                                <h1 id="company_data" className={styles.SectionTitle}>{this.props.t('reservation.company_data.title')}</h1>
                                <CompanyData disabled={!this.isCheckIn && !this.isAdmin} data={this.state} onValueChange={(prop, value) => {
                                    this.setState({
                                        [prop]: value
                                    })
                                }} />
                                <hr className="mt-5 mb-5" />
                            </Container>
                        }
                        {((this.isCheckIn || this.view)&& !this.isCheckOut) &&
                            <Container>
                                <Grid container>
                                    <Grid item xs={12} className="container-center-flex">
                                        {this.state.fieldErrors.length > 0 &&
                                            <Alert
                                                severity="error"
                                                className="mt-3"
                                            >
                                                {this.props.t('reservation.invalid_form')}
                                            </Alert>
                                        }
                                    </Grid>
                                    <Grid item xs={12} className="container-center-flex">
                                        <Button className={styles.CheckInButton} onClick={async () => {
                                            await this.post();
                                        }}>
                                            {(this.isCheckIn) ? this.props.t('reservation.check_in') : this.props.t('button.order')}
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Container>
                        }
                        {(this.isCheckOut) &&
                            <Container>
                                <Grid container>
                                    <Grid item xs={12} className="container-center-flex">
                                        <Button className={styles.CheckInButton} onClick={async () => {
                                            await this.checkOut();
                                        }}>
                                            {this.props.t('reservation.check_out')}
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Container>
                        }
                        {this.isAdmin &&
                            <Container>
                                <Grid container>
                                    <Grid item xs={12} className="container-center-flex">
                                        <Button className={styles.CheckInButton} onClick={async () => {
                                            await this.post();
                                        }}>
                                            {this.props.t('button.save')}
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Container>
                        }
                    </div>
                }
            </div>
        );
    };
}

ReservationForm.propTypes = {};

ReservationForm.defaultProps = {};

export default withTranslation()(ReservationForm)
