import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from "react-i18next";
import { textService } from '../../services/TextService';

import {
    Grid,
    TextField
} from '@mui/material';


import {
    DateTimePicker
} from '../';
import styles from './ReservationForm.module.css';

function GuestData(props) {
    const { index, guest, disabled, checkIn, onChange, onRemove } = props;
    const { t } = useTranslation();

    const checkRequired = () => guest.name || guest.surname || guest.address || guest.birthday;

    return (
        <>

            <Grid item md={6}>
                <hr className="mt-5 mb-5" />
                <h1 className={styles.SectionTitle} style={{ fontSize: '25px', marginLeft: '20px' }}>{t('reservation.guest_data.person')} {index + 2} {checkIn}</h1>
            </Grid>
            <Grid item md={6} className="d-flex flex-column">
                <hr className="mt-5 mb-5" />
                {(!disabled && checkIn) &&
                    <button
                        style={{
                            background: 'transparent',
                            width: '200px',
                            alignSelf: 'end',
                            border: 'none',
                            padding: 0,
                            textAlign: 'right',
                            marginRight: '20px',
                            color: '#715D53',
                            fontFamily: 'Roboto',
                            fontStyle: 'normal',
                            fontWeight: 'bold',
                            fontSize: '20px',
                            lineHeight: '50px',
                        }}
                        onClick={onRemove}
                    >
                        {t('button.remove')}
                    </button>
                }
            </Grid>
            <Grid item className="modified-form" md={6}>
                <TextField
                    required={checkRequired()}
                    disabled={disabled}
                    margin="dense"
                    id={`guest_data_name_${index}`}
                    label={t('reservation.guest_data.name')}
                    type="text"
                    fullWidth
                    variant="filled"
                    inputProps={{ maxLength: 50 }}
                    value={guest.name}
                    onChange={(event) => {
                        guest.name = textService.capitalized(event.target.value);
                        onChange(guest);
                    }}
                />
            </Grid>
            <Grid item className="modified-form" md={6}>
                <TextField
                    required={checkRequired()}
                    disabled={disabled}
                    margin="dense"
                    id={`guest_data_surname_${index}`}
                    label={t('reservation.guest_data.surname')}
                    type="text"
                    fullWidth
                    variant="filled"
                    inputProps={{ maxLength: 100 }}
                    value={guest.surname}
                    onChange={(event) => {
                        guest.surname = textService.capitalized(event.target.value);
                        onChange(guest);
                    }}
                />
            </Grid>
            <Grid item className="modified-form" md={6}>
                <TextField
                    required={checkRequired()}
                    disabled={disabled}
                    margin="dense"
                    id={`guest_data_address_${index}`}
                    label={t('reservation.guest_data.address')}
                    type="text"
                    fullWidth
                    variant="filled"
                    inputProps={{ maxLength: 250 }}
                    value={guest.address}
                    onChange={(event) => {
                        guest.address = textService.capitalized(event.target.value);
                        onChange(guest);
                    }}
                />
            </Grid>
            <Grid item className="modified-form" md={6}>
                <div style={{ marginTop: '8px', width: '100%' }}>
                    <DateTimePicker
                        disabled={disabled}
                        required={checkRequired()}
                        className={styles.Birthday}
                        checkMinDate={false}
                        label="reservation.guest_data.birthday"
                        date={guest.birthday}
                        onDateChange={(newDate) => {
                            guest.birthday = newDate;
                            onChange(guest);
                        }}
                    />
                </div>
            </Grid>
        </>
    );
}

GuestData.propTypes = {
    index: PropTypes.number.isRequired,
    guest: PropTypes.objectOf({
        name: PropTypes.string,
        surname: PropTypes.string,
        address: PropTypes.string,
        birthday: PropTypes.date
    }).isRequired,
    checkIn: PropTypes.bool.isRequired,
    disabled: PropTypes.bool.isRequired,
    onChange: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired
};

export default GuestData;