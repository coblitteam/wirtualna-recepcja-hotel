import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from "react-i18next";
import { textService } from '../../services/TextService';

import CountrySelector from './CountrySelector';

import {
    Grid,
    MenuItem,
    TextField
} from '@mui/material';
import InputMask from "react-input-mask";
import styles from './ReservationForm.module.css';
import GuestData from './GuestData';


function PersonalData(props) {
    const {
        data,
        disabled,
        onValueChange
    } = props;
    const { t } = useTranslation();

    return (
        <Grid container>
            <Grid item className="modified-form" md={6}>
                <TextField
                    required
                    disabled={disabled}
                    error={data.checkValid('name')}
                    margin="dense"
                    id="personal_data_name"
                    label={t('reservation.personal_data.name')}
                    type="text"
                    fullWidth
                    variant="filled"
                    inputProps={{ maxLength: 50 }}
                    value={data.name}
                    onChange={(event) => {
                        onValueChange('name', textService.capitalized(event.target.value));
                    }}
                />
            </Grid>
            <Grid item className="modified-form" md={6}>
                <TextField
                    required
                    disabled={disabled}
                    error={data.checkValid('surname')}
                    margin="dense"
                    id="personal_data_surname"
                    label={t('reservation.personal_data.surname')}
                    type="text"
                    fullWidth
                    variant="filled"
                    inputProps={{ maxLength: 100 }}
                    value={data.surname}
                    onChange={(event) => {
                        onValueChange('surname', textService.capitalized(event.target.value));
                    }}
                />
            </Grid>
            <Grid item className="modified-form" md={6}>
                <CountrySelector
                    disabled={disabled}
                    error={data.checkValid('country')}
                    country={data.country}
                    onValueChange={(country) => {
                        onValueChange('country', country);
                    }}
                />
            </Grid>
            <Grid item className="modified-form" md={6}>
                {data.country === 'PL' &&
                    <InputMask
                        disabled={disabled}
                        mask="99-999"
                        maskChar="_"
                        value={data.postalCode}
                        onChange={(event) => {
                            onValueChange('postalCode', event.target.value);
                        }}
                    >
                        {() =>
                            <TextField
                                required
                                disabled={disabled}
                                error={data.checkValid('postalCode')}
                                margin="dense"
                                id="personal_data_postal_code"
                                label={t('reservation.personal_data.postal_code')}
                                type="text"
                                fullWidth
                                variant="filled"
                            />
                        }
                    </InputMask>
                }
                {data.country !== 'PL' &&
                    <TextField
                        required
                        disabled={disabled}
                        error={data.checkValid('postalCode')}
                        margin="dense"
                        id="personal_data_postal_code"
                        label={t('reservation.personal_data.postal_code')}
                        type="text"
                        fullWidth
                        inputProps={{ maxLength: 10 }}
                        variant="filled"value={data.postalCode}
                        onChange={(event) => {
                            onValueChange('postalCode', event.target.value);
                        }}
                    />
                }
            </Grid>
            <Grid item className="modified-form" md={6}>
                <TextField
                    required
                    disabled={disabled}
                    error={data.checkValid('city')}
                    margin="dense"
                    id="personal_data_city"
                    label={t('reservation.personal_data.city')}
                    type="text"
                    fullWidth
                    variant="filled"
                    inputProps={{ maxLength: 100 }}
                    value={data.city}
                    onChange={(event) => {
                        onValueChange('city', textService.capitalized(event.target.value));
                    }}
                />
            </Grid>
            <Grid item className="modified-form" md={6}>
                <TextField
                    disabled={disabled}
                    margin="dense"
                    id="personal_data_street"
                    label={t('reservation.personal_data.street')}
                    type="text"
                    fullWidth
                    variant="filled"
                    inputProps={{ maxLength: 200 }}
                    value={data.street}
                    onChange={(event) => {
                        onValueChange('street', textService.capitalized(event.target.value));
                    }}
                />
            </Grid>
            <Grid item className="modified-form" md={6}>
                <TextField
                    required
                    disabled={disabled}
                    error={data.checkValid('houseNumber')}
                    margin="dense"
                    id="personal_data_house_number"
                    label={t('reservation.personal_data.house_number')}
                    type="text"
                    fullWidth
                    variant="filled"
                    inputProps={{ maxLength: 20 }}
                    value={data.houseNumber}
                    onChange={(event) => {
                        onValueChange('houseNumber', event.target.value);
                    }}
                />
            </Grid>
            <Grid item className="modified-form" md={6}>
                <TextField
                    disabled={disabled}
                    margin="dense"
                    id="personal_data_doors_number"
                    label={t('reservation.personal_data.doors_number')}
                    type="text"
                    fullWidth
                    variant="filled"
                    inputProps={{ maxLength: 20 }}
                    value={data.doorsNumber}
                    onChange={(event) => {
                        onValueChange('doorsNumber', event.target.value);
                    }}
                />
            </Grid>
            <Grid item md={6}>
                <hr className="mt-5 mb-5" />
                <h1 className={styles.SectionTitle}>{t('reservation.guest_data.title')}</h1>
            </Grid>
            <Grid item md={6} className="d-flex flex-column">
                <hr className="mt-5 mb-5" />
                {!disabled &&
                    <button
                        className={styles.Button}
                        style={{width: '200px', alignSelf: 'end'}}
                        onClick={() => {
                            data.guests.push({ name: '', surname: '', address: '', birthday: null });
                            onValueChange('guests', [...data.guests]);
                        }}
                    >
                        {t('button.add')}
                    </button>
                }
            </Grid>
            {data.guests.map((guest, index) => (
                <GuestData
                    key={index}
                    index={index}
                    guest={guest}
                    disabled={disabled}
                    checkIn={data.isCheckIn}
                    onChange={(newGuest) => {
                        data.guests[index] = newGuest;
                        onValueChange('guests', [...data.guests]);
                    }}
                    onRemove={() => {
                        data.guests.splice(index, 1);
                        onValueChange('guests', [...data.guests]);
                    }}
                />
            ))}
            <Grid item md={12}>
                <hr className="mt-5 mb-5" />
                <h1 className={styles.SectionTitle}>{t('reservation.contact_data.title')}</h1>
            </Grid>
            <Grid item className="modified-form" md={6}>
                <TextField
                    select
                    required
                    disabled={disabled}
                    error={data.checkValid('phoneCountryNumber')}
                    margin="dense"
                    id="contact_data_country_code"
                    label={t('reservation.contact_data.country_code')}
                    type="text"
                    fullWidth
                    variant="filled"
                    value={data.phoneCountryNumber}
                    onChange={(event) => {
                        onValueChange('phoneCountryNumber', event.target.value);
                    }}
                >
                    {data.CountryCodes.map((code, i) => {
                        return (
                        <MenuItem key={i} value={code.dial_code}>
                            {code.name} {code.dial_code}
                        </MenuItem>
                        );
                    })}
                </TextField>
            </Grid>
            <Grid item className="modified-form" md={6}>
                <TextField
                    required
                    disabled={disabled}
                    error={data.checkValid('phoneNumber')}
                    margin="dense"
                    id="contact_data_phone_number"
                    label={t('reservation.contact_data.phone_number')}
                    type="text"
                    fullWidth
                    variant="filled"
                    inputProps={{ maxLength: 15 }}
                    value={data.phoneNumber}
                    onChange={(event) => {
                        onValueChange('phoneNumber', event.target.value);
                    }}
                />
            </Grid>
            <Grid item className="modified-form" md={6}>
                <TextField
                    required
                    disabled={disabled}
                    error={data.checkValid('email')}
                    margin="dense"
                    id="contact_data_email"
                    label={t('reservation.contact_data.email')}
                    type="text"
                    fullWidth
                    variant="filled"
                    inputProps={{ maxLength: 100 }}
                    value={data.email}
                    onChange={(event) => {
                        onValueChange('email', event.target.value);
                    }}
                />
            </Grid>
        </Grid>
    );
}

PersonalData.propTypes = {
    data: PropTypes.object.isRequired,
    disabled: PropTypes.bool,
    onValueChange: PropTypes.func.isRequired
};

export default PersonalData;