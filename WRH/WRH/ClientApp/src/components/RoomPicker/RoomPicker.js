import React from 'react';
import PropTypes from 'prop-types';
import styles from './RoomPicker.module.css';
import {
    Paper
} from '@mui/material';

import { useTranslation } from 'react-i18next';
import RoomImage from '../../images/rooms/1.png';
import {
    Add,
    Remove
} from '../../images/icons';

const RoomPicker = (props) => {
    const {
        room,
        quantityInit,
        isSelected,
        onSelected,
        onQuantityChange,
    } = props;
    const { t, i18n } = useTranslation();

    const [quantity, setQuantity] = React.useState(quantityInit ?? 0);

    return (
        <Paper className={styles.RoomPicker}>
            <img src={room.photo } alt="" />
            <div className={styles.Body}>
                <h1 className={styles.Header}>
                    {room.nameTranslations[i18n.language]}
                </h1>
                <span className={styles.Description}>
                    {room.shortDetailsTranslations[i18n.language]}
                </span>
                <div className={styles.Chooser}>
                    <div className={styles.QuantitySelect}>
                        <img src={Remove} alt="" onClick={() => {
                            if (quantity > 0) {
                                setQuantity(quantity - 1);
                                onQuantityChange(quantity - 1);
                            }
                        }} />
                        <span>{t('common.quantity', { val: quantity })}</span>
                        <img src={Add} alt="" onClick={() => {
                            if (quantity < room.count) {
                                setQuantity(quantity + 1);
                                onQuantityChange(quantity + 1);
                            }
                        }} />
                    </div>
                    <span className={styles.Price}>
                        {t('common.currency_price_per_day', { val: Math.floor(room.price) })}
                    </span>
                </div>
                <button checked className={`${styles.Button} ${isSelected ? styles.Checked : ''}`} onClick={onSelected}>
                    {isSelected ? t('button.chosen') : t('button.choose')}
                </button>
            </div>
        </Paper>
    );
}

RoomPicker.propTypes = {
    room: PropTypes.object.isRequired,
    quantityInit: PropTypes.number,
    isSelected: PropTypes.bool.isRequired,
    onSelected: PropTypes.func.isRequired,
    onQuantityChange: PropTypes.func.isRequired
};

RoomPicker.defaultProps = {};

export default RoomPicker;
