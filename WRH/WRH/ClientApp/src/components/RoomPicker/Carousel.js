import React, { useEffect } from 'react';
import styles from './RoomPicker.module.css';

import RoomPicker from './RoomPicker';

import Carousel, { slidesToShowPlugin } from '@brainhubeu/react-carousel';
import { useTranslation } from "react-i18next";

function CarouselComponent(props) {
    const {
        data,
        rooms,
        onRoomsChange
    } = props;
    const { t } = useTranslation();

    return (
        <Carousel
            className={styles.Carousel}
            plugins={[
                'arrows',
                {
                    resolve: slidesToShowPlugin,
                    options: {
                        numberOfSlides: window.innerWidth <= 1340 ? 2 : 4
                    }
                },
            ]}
        >
            {
                rooms.map((room) => {
                    return (
                        <RoomPicker
                            key={room.id}
                            room={room}
                            quantityInit={0}
                            isSelected={data.roomsSelected.filter((selectedRoom) => selectedRoom.roomId === room.roomId).length > 0}
                            onSelected={() => {
                                if (!room.quantity) {
                                    alert(t('reservation.choose_room.messages.empty_room_selection'));
                                    return;
                                }

                                room.selected = !room.selected;
                                onRoomsChange(rooms.filter((room) => room.selected).map((room) => ({
                                    roomId: room.roomId,
                                    name: room.name,
                                    nameTranslations: room.nameTranslations,
                                    shortDetails: room.shortDetails,
                                    capacity: room.capacity,
                                    price: room.price,
                                    priceOnce: room.priceOnce,
                                    quantity: room.quantity,
                                    isPaid: false
                                })));
                            }}
                            onQuantityChange={(quantity) => {
                                room.quantity = quantity;
                                onRoomsChange(rooms.filter((room) => room.selected));
                            }}
                        />
                    )
                })
            }
        </Carousel>
    );
}

export default CarouselComponent;