import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Button,
    Grid,
    InputAdornment,
    MenuItem,
    TextField,
    Dialog,
    DialogTitle,
    DialogContent} from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import { PL, EN, DE, ES, FR } from '../../../images/locales';

import styles from './RoomTypes.module.css';

import { textService } from '../../../services/TextService';
import { apiRoom } from '../../../services/api/rooms/Room';

const languages = {
    pl: { icon: PL },
    en: { icon: EN },
    de: { icon: DE },
    es: { icon: ES },
    fr: { icon: FR }
};

export class RoomTypesFormDialog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editMode: props.editMode ?? false,
            open: props.open ?? false,
            language: 'pl',
            onSaved: props.onSaved,
            typeId: props.typeId ?? 0,
            name_pl: '',
            name_en: '',
            name_de: '',
            name_es: '',
            name_fr: '',
            details_pl: '',
            details_en: '',
            details_de: '',
            details_es: '',
            details_fr: '',
            shortDetails_pl: '',
            shortDetails_en: '',
            shortDetails_de: '',
            shortDetails_es: '',
            shortDetails_fr: '',
            capacity: 0,
            extraBeds: 0,
            price: 0,
            priceOnce: 0,
            breakfastPrice: 0,
            fullBoardPrice: 0,
            photo: '',
            file: null
        };

        if (this.state.typeId)
            this.get();
    }

    changeLanguage = (lang) => {
        this.setState({
            language: lang
        });
    }

    compareForm = () => {
        let data = new FormData();
        data.append('id', this.state.typeId);
        data.append('name', this.state.name_pl);
        data.append('name_en', this.state.name_en);
        data.append('name_de', this.state.name_de);
        data.append('name_es', this.state.name_es);
        data.append('name_fr', this.state.name_fr);
        data.append('details', this.state.details_pl);
        data.append('details_en', this.state.details_en);
        data.append('details_de', this.state.details_de);
        data.append('details_es', this.state.details_es);
        data.append('details_fr', this.state.details_fr);
        data.append('shortDetails', this.state.shortDetails_pl);
        data.append('shortDetails_en', this.state.shortDetails_en);
        data.append('shortDetails_de', this.state.shortDetails_de);
        data.append('shortDetails_es', this.state.shortDetails_es);
        data.append('shortDetails_fr', this.state.shortDetails_fr);
        data.append('capacity', this.state.capacity);
        data.append('extraBeds', this.state.extraBeds);
        data.append('price', this.state.price);
        data.append('priceOnce', this.state.priceOnce);
        data.append('breakfastPrice', this.state.breakfastPrice);
        data.append('fullBoardPrice', this.state.fullBoardPrice);
        data.append('photo', this.state.file);

        return data;
    };

    get = () => {
        apiRoom.getCustom(`/type/${this.state.typeId}`).then((data) => {
            this.setState({
                name_pl: data.type.nameTranslations.pl,
                name_en: data.type.nameTranslations.en,
                name_de: data.type.nameTranslations.de,
                name_es: data.type.nameTranslations.es,
                name_fr: data.type.nameTranslations.fr,
                details_pl: data.type.detailsTranslations.pl,
                details_en: data.type.detailsTranslations.en,
                details_de: data.type.detailsTranslations.de,
                details_es: data.type.detailsTranslations.es,
                details_fr: data.type.detailsTranslations.fr,
                shortDetails_pl: data.type.shortDetailsTranslations.pl,
                shortDetails_en: data.type.shortDetailsTranslations.en,
                shortDetails_de: data.type.shortDetailsTranslations.de,
                shortDetails_es: data.type.shortDetailsTranslations.es,
                shortDetails_fr: data.type.shortDetailsTranslations.fr,
                capacity: data.type.capacity,
                extraBeds: data.type.extraBeds,
                price: data.type.price,
                priceOnce: data.type.priceOnce,
                breakfastPrice: data.type.breakfastPrice,
                fullBoardPrice: data.type.fullBoardPrice,
                photo: data.type.photo,
                file: null
            });
        });
    }

    post = () => {
        apiRoom.post(this.compareForm()).then(() => {
            this.setState({
                open: false
            });

            this.state.onSaved();
        });
    }

    put = () => {
        apiRoom.putCustom('/type', this.compareForm()).then(() => {
            this.setState({
                open: false
            });

            this.state.onSaved();
        });
    }

    saveFile = (e) => {
        if (e.target.files && e.target.files.length)
            this.setState({
                photo: URL.createObjectURL(e.target.files[0]),
                file: e.target.files[0]
            });
    }

    render () {
        return (
            <div className={styles.EditIconButton}>
                {!this.state.editMode &&
                    <Button
                        variant="text"
                        className={styles.CreateButton}
                        onClick={() => {
                            this.setState({
                                open: true
                            });
                        }}
                    >
                        Dodaj nowy
                    </Button>
                }
                {this.state.editMode &&
                    <EditIcon onClick={() => {
                        this.setState({
                            open: true
                        });
                    }} />
                }
                <Dialog
                    maxWidth="sm"
                    fullWidth
                    open={this.state.open}
                    onClose={() => {
                        this.setState({
                            open: false
                        });
                    }}>
                    <div>
                        <DialogTitle>
                            {(this.state.typeId) ? 'Edycja' : 'Tworzenie'} typu pokoju
                        </DialogTitle>
                        <DialogContent>
                            <Grid container>
                                <Grid item md={12} className={styles.Languages}>
                                    {Object.keys(languages)?.map(item => (
                                        <img
                                            key={item}
                                            src={languages[item].icon}
                                            alt=""
                                            className={`${this.state.language !== item ? "" : styles.selected}`}
                                            onClick={() => {
                                                this.changeLanguage(item);
                                            }}
                                        />
                                    ))}
                                </Grid>
                                <Grid item md={12} className="modified-form">
                                    {Object.keys(languages)?.map(item => (
                                        <TextField
                                            required={item === 'pl'}
                                            className={this.state.language === item ? '' : 'd-none'}
                                            autoFocus
                                            margin="dense"
                                            id={`name_${item}`}
                                            label={`Nazwa (${item.toUpperCase()})`}
                                            type="text"
                                            fullWidth
                                            variant="filled"
                                            inputProps={{ maxLength: 50 }}
                                            value={this.state[`name_${item}`]}
                                            onChange={(event) => {
                                                this.setState({
                                                    [`name_${item}`]: textService.capitalized(event.target.value)
                                                });
                                            }}
                                        />
                                    ))}
                                </Grid>
                                <Grid item md={12} className="modified-form">
                                    {Object.keys(languages)?.map(item => (
                                        <TextField
                                            multiline
                                            required={item === 'pl'}
                                            className={this.state.language === item ? '' : 'd-none'}
                                            rows={6}
                                            margin="dense"
                                            id={`details_${item}`}
                                            label={`Opis (${item.toUpperCase()})`}
                                            type="text"
                                            fullWidth
                                            variant="filled"
                                            value={this.state[`details_${item}`]}
                                            onChange={(event) => {
                                                this.setState({
                                                    [`details_${item}`]: event.target.value
                                                });
                                            }}
                                    />
                                    ))}
                                </Grid>
                                <Grid item md={12} className="modified-form">
                                    {Object.keys(languages)?.map(item => (
                                        <TextField
                                            multiline
                                            required={item === 'pl'}
                                            className={this.state.language === item ? '' : 'd-none'}
                                            rows={2}
                                            margin="dense"
                                            id={`short_details_${item}`}
                                            label={`Krótki opis (${item.toUpperCase()})`}
                                            type="text"
                                            fullWidth
                                            variant="filled"
                                            value={this.state[`shortDetails_${item}`]}
                                            onChange={(event) => {
                                                this.setState({
                                                    [`shortDetails_${item}`]: event.target.value
                                                });
                                            }}
                                    />
                                    ))}
                                </Grid>
                                <Grid item md={6} className="modified-form">
                                    <TextField
                                        required
                                        autoFocus
                                        margin="dense"
                                        id="type_capacity"
                                        label="Ilość miejsc"
                                        type="number"
                                        fullWidth
                                        variant="filled"
                                        inputProps={{ maxLength: 30 }}
                                        value={this.state.capacity}
                                        onChange={(event) => {
                                            this.setState({
                                                capacity: event.target.value
                                            });
                                        }}
                                    />
                                </Grid>
                                <Grid item md={6} className="modified-form">
                                    <TextField
                                        required
                                        autoFocus
                                        margin="dense"
                                        id="type_extra_beds"
                                        label="Ilość dostawek"
                                        type="number"
                                        fullWidth
                                        variant="filled"
                                        inputProps={{ maxLength: 30 }}
                                        value={this.state.extraBeds}
                                        onChange={(event) => {
                                            this.setState({
                                                extraBeds: event.target.value
                                            });
                                        }}
                                    />
                                </Grid>
                                <Grid item md={6} className="modified-form">
                                    <TextField
                                        required
                                        autoFocus
                                        margin="dense"
                                        id="type_price"
                                        label="Cena za noc"
                                        type="number"
                                        fullWidth
                                        variant="filled"
                                        inputProps={{ maxLength: 30 }}
                                        value={this.state.price}
                                        onChange={(event) => {
                                            this.setState({
                                                price: event.target.value
                                            });
                                        }}
                                    />
                                </Grid>
                                <Grid item md={6} className="modified-form">
                                    <TextField
                                        required
                                        autoFocus
                                        margin="dense"
                                        id="type_price"
                                        label="Pojedyncze wykorzystanie"
                                        type="number"
                                        fullWidth
                                        variant="filled"
                                        inputProps={{ maxLength: 30 }}
                                        value={this.state.priceOnce}
                                        onChange={(event) => {
                                            this.setState({
                                                priceOnce: event.target.value
                                            });
                                        }}
                                    />
                                </Grid>
                                {this.state.photo &&
                                    <Grid item md={12} className="text-center mt-3">
                                        <img src={this.state.photo} />
                                    </Grid>
                                }
                                <Grid item md={12} className="modified-form">
                                    <input type="file" className="w-100" onChange={this.saveFile} />
                                </Grid>
                                <Grid item md={12}>
                                    <small className="text-center">Uwaga! Preferowany rozmiar zdjęcia to 380px szerokości i 220px wysokości. <br/>Zdjęcie zostanie dopasowane do tych wymiarów.</small>
                                </Grid>
                                <Grid item md={2} />
                                <Grid item md={8} className="mt-5">
                                    {!this.state.editMode &&
                                        <Button className={styles.SubmitButton} onClick={this.post}>
                                            Dodaj nowy
                                        </Button>
                                    }
                                    {this.state.editMode &&
                                        <Button className={styles.SubmitButton} onClick={this.put}>
                                            Zapisz
                                        </Button>
                                    }
                                </Grid>
                                <Grid item md={2} />
                            </Grid>
                        </DialogContent>
                    </div>
                </Dialog>
            </div>
        )
    }
}

RoomTypesFormDialog.propTypes = {
    categories: PropTypes.array.isRequired,
    onSaved: PropTypes.func.isRequired
};

RoomTypesFormDialog.defaultProps = {}

export default RoomTypesFormDialog;
