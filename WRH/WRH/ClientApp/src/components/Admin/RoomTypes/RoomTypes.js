﻿import React, { Component } from 'react';

import {
    Button,
    Container,
    Grid,
    MenuItem,
    TextField
} from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';

import RoomTypesTable from './RoomTypesTable';

import styles from './RoomTypes.module.css';

import { apiRoom } from '../../../services/api/rooms/Room';

export class RoomTypes extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
            formDialogOpen: false,
            roomTypeId: 0
        };

        this.timer = null

        this.loadData();
    }

    loadData = () => {
        this.timer = setTimeout(async () => {
            let data = await apiRoom.get();

            this.setState({
                items: data.items
            });
        }, 500);
    }

    handleEditRequest = (newsId) => {
        this.setState({
            formDialogOpen: true,
            newsId: newsId
        });
    }

    render() {
        return (
            <div className={styles.AdminRoomTypes}>
                <RoomTypesTable
                    data={this.state}
                    onRequestSort={this.handleRequestSort}
                    onPageChange={this.handlePageChange}
                    onRowsPerPageChange={this.handleRowsPerPageChange}
                    onEditRequest={this.handleEditRequest}
                    onFormSaved={this.loadData}
                />
            </div>
        );
    };
}

export default RoomTypes;