﻿import React, { Component } from 'react';

import {
    Button,
    Container,
    Grid,
    MenuItem,
    TextField
} from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';

import styles from './Categories.module.css';

import { apiAttractionCategory } from '../../../services/api/attractionCategories/AttractionCategory';
import { apiNewsCategory } from '../../../services/api/news/NewsCategory';

export class Categories extends Component {
    constructor(props) {
        super(props);

        this.state = {
            categories: [],
            categoryForm: {
                id: 0,
                name: '',
                isActive: true
            },
            selectedType: 'offer',
            errorMessage: ''
        };

        this.getAttractionCategories();
    }

    getAttractionCategories = async () => {
        var data = await apiAttractionCategory.getCustom('/list');

        this.setState({
            categories: data.categories
        });
    }

    getNewsCategories = async () => {
        var data = await apiNewsCategory.getCustom('/list');

        this.setState({
            categories: data.categories
        });
    }

    validateCategoryName = () => {
        var words = this.state.categoryForm.name.split(' ');

        if (words.length > 5) {
            this.setState({errorMessage: 'Nazwa kategorii może się składać z maksymalnie pięciu wyrazów.'});
            return false;
        }

        for (var i = 0; i < 5; i++)
            if (words[i] && words[i].length > 13) {
                this.setState({errorMessage: 'Nazwa kategorii może się składać z wyrazów o maksymalnej długości 13 znaków.'});
                return false;
            }

        this.setState({errorMessage: ''})
        return true;
    }

    post = () => {
        if (!this.validateCategoryName())
            return;

        if (this.state.selectedType === 'offer')
            apiAttractionCategory.post(this.state.categoryForm)
                .then(async () => {
                    await this.getAttractionCategories();
                });

        if (this.state.selectedType === 'news')
            apiNewsCategory.post(this.state.categoryForm)
                .then(async () => {
                    await this.getNewsCategories();
                });

        this.setState({
            categoryForm: {
                id: 0,
                name: '',
                isActive: true
            }
        });
    }

    put = () => {
        if (!this.validateCategoryName())
            return;

        if (this.state.selectedType === 'offer')
            apiAttractionCategory.put(this.state.categoryForm)
                .then(async () => {
                    await this.getAttractionCategories();
                });

        if (this.state.selectedType === 'news')
            apiNewsCategory.put(this.state.categoryForm)
                .then(async () => {
                    await this.getNewsCategories();
                });

        this.setState({
            categoryForm: {
                id: 0,
                name: '',
                isActive: true
            }
        });
    }

    delete = (id) => {
        if (this.state.selectedType === 'offer')
            apiAttractionCategory.remove(id)
                .then(async () => {
                    await this.getAttractionCategories();
                });

        if (this.state.selectedType === 'news')
            apiNewsCategory.remove(id)
                .then(async () => {
                    await this.getAttractionCategories();
                });
    }

    render() {
        return (
            <Container maxWidth="md" className={styles.Categories}>
                <Grid container>
                    <Grid item md={6} className={styles.CategoryTypeContainer}>
                        <span
                            className={`${styles.CategoryType} ${this.state.selectedType === 'offer' ? styles.SelectedCategoryType : ''}`}
                            onClick={async () => {
                                this.setState({
                                    categoryForm: {
                                        id: 0,
                                        name: '',
                                        isActive: true
                                    },
                                    selectedType: 'offer'
                                });

                                await this.getAttractionCategories();
                            }}
                        >
                            Kategorie ofert hotelu
                        </span>
                    </Grid>
                    <Grid item md={6} className={styles.CategoryTypeContainer}>
                        <span
                            className={`${styles.CategoryType} ${this.state.selectedType === 'news' ? styles.SelectedCategoryType : ''}`}
                            onClick={async () => {
                                this.setState({
                                    categoryForm: {
                                        id: 0,
                                        name: '',
                                        isActive: true
                                    },
                                    selectedType: 'news'
                                });

                                await this.getNewsCategories();
                            }}
                        >
                            Kategorie aktualności
                        </span>
                    </Grid>
                    {this.state.errorMessage &&
                        <Grid item md={12}>
                            <h3 className="text-center mt-4 text-danger">{this.state.errorMessage}</h3>
                        </Grid>
                    }
                    <Grid item md={12} className="modified-form with-mt-50p">
                        <TextField
                            required
                            autoFocus
                            margin="dense"
                            id="category_name"
                            label="Nazwa"
                            type="text"
                            fullWidth
                            variant="filled"
                            inputProps={{ maxLength: 100 }}
                            value={this.state.categoryForm.name}
                            onChange={(event) => {
                                this.state.categoryForm.name = event.target.value;

                                this.setState({
                                    categoryForm: this.state.categoryForm
                                });
                            }}
                        />
                    </Grid>
                    <Grid item md={12} className="modified-form">
                        <TextField
                            required
                            select
                            margin="dense"
                            id="category_is_active"
                            label="Status"
                            type="text"
                            fullWidth
                            variant="filled"
                            value={this.state.categoryForm.isActive}
                            onChange={(event) => {
                                this.state.categoryForm.isActive = event.target.value;

                                this.setState({
                                    categoryForm: this.state.categoryForm
                                });
                            }}
                        >
                            <MenuItem key={true} value={true}>
                                Aktywne
                            </MenuItem>
                            <MenuItem key={false} value={false}>
                                Nieaktywne
                            </MenuItem>
                        </TextField>
                    </Grid>
                    <Grid item md={3} />
                    <Grid item md={6} className="modified-form with-mt-50p">
                        {!this.state.categoryForm.id &&
                            <Button className={styles.SubmitButton} onClick={async () => {
                                await this.post();
                            }}>
                                Dodaj nową
                            </Button>
                        }
                        {this.state.categoryForm.id > 0 &&
                            <Button className={styles.SubmitButton} onClick={async () => {
                                await this.put();
                            }}>
                                Zapisz
                            </Button>
                        }
                    </Grid>
                    <Grid item md={3} />
                    <Grid container className={styles.TableHead}>
                        <hr className="mt-3 mb-3" />
                        <Grid item md={5}>
                            Nazwa
                        </Grid>
                        <Grid item md={5}>
                            Status
                        </Grid>
                        <Grid item md={2} />
                    </Grid>
                    {this.state.categories.map((category, index) => {
                        return (
                            <Grid key={index} container className={styles.TableRow}>
                                <Grid item md={5}>
                                    {category.name}
                                </Grid>
                                <Grid item md={5}>
                                    {category.isActive ? 'Aktywne' : 'Nieaktywne'}
                                </Grid>
                                <Grid item md={2} className={styles.CrudCell}>
                                    {(!category.isBoard && category.canDelete) &&
                                        <DeleteIcon onClick={() => {
                                            this.delete(category.id);
                                        }}/>
                                    }
                                    <EditIcon onClick={() => {
                                        this.setState({
                                            categoryForm: {
                                                id: category.id,
                                                name: category.name,
                                                isActive: category.isActive
                                            }
                                        })
                                    }} />
                                </Grid>
                            </Grid>
                        )
                    })}
                </Grid>
            </Container>
        );
    };
}

export default Categories;