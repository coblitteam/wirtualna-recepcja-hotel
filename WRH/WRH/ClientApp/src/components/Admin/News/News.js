﻿import React, { Component } from 'react';

import {
    Button,
    Container,
    Grid,
    MenuItem,
    TextField
} from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';

import NewsTable from './NewsTable';
import NewsTableFilter from './NewsTableFilter';

import styles from './News.module.css';

import { apiNews } from '../../../services/api/news/News';
import { apiNewsCategory } from '../../../services/api/news/NewsCategory';

export class News extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
            orderBy: 'name',
            order: 'asc',
            perPage: 10,
            page: 0,
            size: 0,
            search: '',
            isActive: null,
            categories: [],
            formDialogOpen: false,
            newsId: 0
        };

        this.timer = null

        this.loadData();
    }

    loadData = () => {
        let query = {
            orderBy: this.state.orderBy,
            orderDestination: this.state.order,
            start: this.state.page * this.state.perPage,
            size: this.state.perPage,
            search: this.state.search,
            categoryId: this.state.categoryId,
            isActive: this.state.isActive
        };

        this.timer = setTimeout(async () => {
            let data = await apiNews.getList(query);
            let categories = await apiNewsCategory.getCustom('/list');

            this.setState({
                items: data.items,
                size: data.length,
                categories: categories.categories
            });
        }, 500);
    }

    handleRequestSort = (event, property) => {
        this.state.orderBy = property;
        this.state.order = property == this.state.orderBy && this.state.order != 'desc' ? 'desc' : 'asc';

        this.setState({
            orderBy: this.state.orderBy,
            order: this.state.order
        });


        this.loadData();
    }

    handlePageChange = (event, newPage) => {
        this.state.page = newPage;

        this.setState({ page: this.state.page });

        this.loadData();
    }

    handleRowsPerPageChange = (event) => {
        this.state.perPage = parseInt(event.target.value, 10);
        this.state.page = 0;

        this.setState({
            perPage: this.state.perPage,
            page: this.state.page
        });

        this.loadData();
    }

    handleFilterChanged = (prop, value) => {
        switch (prop) {
            case 'search':
                this.state.search = value;
                this.setState({
                    search: this.state.search
                });
                break;
            case 'categoryId':
                this.state.categoryId = value;
                this.setState({
                    categoryId: this.state.categoryId
                });
                break;
            case 'isActive':
                this.state.isActive = value;
                this.setState({
                    isActive: this.state.isActive
                });
                break;
        }

        this.loadData();
    }

    handleFilterCleared = () => {
        this.state.search = '';
        this.state.categoryId = '';
        this.state.isActive = '';

        this.setState(this.state);

        this.loadData();
    }

    handleEditRequest = (newsId) => {
        this.setState({
            formDialogOpen: true,
            newsId: newsId
        });
    }

    render() {
        return (
            <div className={styles.AdminNews}>
                {this.state.categories.length &&
                    <div>
                        <NewsTableFilter
                            search={this.state.search}
                            categoryId={this.state.categoryId}
                            isActive={this.state.isActive}
                            categories={this.state.categories}
                            formDialogOpen={this.state.formDialogOpen}
                            newsId={this.state.newsId}
                            onFilterChanged={this.handleFilterChanged}
                            onFilterCleared={this.handleFilterCleared}
                            onFormSaved={this.loadData}
                        />
                        <NewsTable
                            data={this.state}
                            onRequestSort={this.handleRequestSort}
                            onPageChange={this.handlePageChange}
                            onRowsPerPageChange={this.handleRowsPerPageChange}
                            onEditRequest={this.handleEditRequest}
                            onFormSaved={this.loadData}
                        />
                    </div>
                }
            </div>
        );
    };
}

export default News;