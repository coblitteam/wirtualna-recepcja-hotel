import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Button,
    Grid,
    InputAdornment,
    MenuItem,
    TextField,
    Dialog,
    DialogTitle,
    DialogContent} from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import { PL, EN, DE, ES, FR } from '../../../images/locales';

import styles from './News.module.css';

import { textService } from '../../../services/TextService';
import { apiNews } from '../../../services/api/news/News';

const languages = {
    pl: { icon: PL },
    en: { icon: EN },
    de: { icon: DE },
    es: { icon: ES },
    fr: { icon: FR }
};

export class NewsFormDialog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editMode: props.editMode ?? false,
            open: props.open ?? false,
            language: 'pl',
            categories: props.categories,
            onSaved: props.onSaved,
            newsId: props.newsId ?? 0,
            newsName_pl: '',
            newsName_en: '',
            newsName_de: '',
            newsName_es: '',
            newsName_fr: '',
            newsAddress: '',
            newsCategoryId: props.categories[0]?.id,
            isActive: true,
            newsDetails_pl: '',
            newsDetails_en: '',
            newsDetails_de: '',
            newsDetails_es: '',
            newsDetails_fr: '',
            newsShortDetails_pl: '',
            newsShortDetails_en: '',
            newsShortDetails_de: '',
            newsShortDetails_es: '',
            newsShortDetails_fr: '',
            newsPhoto: '',
            photo: '',
            file: null
        };

        if (this.state.newsId)
            this.get();
    }

    changeLanguage = (lang) => {
        this.setState({
            language: lang
        });
    }

    compareForm = () => {
        let data = new FormData();

        data.append('id', this.state.newsId);
        data.append('categoryId', this.state.newsCategoryId);
        data.append('name', this.state.newsName_pl);
        data.append('name_en', this.state.newsName_en);
        data.append('name_de', this.state.newsName_de);
        data.append('name_es', this.state.newsName_es);
        data.append('name_fr', this.state.newsName_fr);
        data.append('address', this.state.newsAddress);
        data.append('isActive', this.state.isActive);
        data.append('details', this.state.newsDetails_pl);
        data.append('details_en', this.state.newsDetails_en);
        data.append('details_de', this.state.newsDetails_de);
        data.append('details_es', this.state.newsDetails_es);
        data.append('details_fr', this.state.newsDetails_fr);
        data.append('shortDetails', this.state.newsShortDetails_pl);
        data.append('shortDetails_en', this.state.newsShortDetails_en);
        data.append('shortDetails_de', this.state.newsShortDetails_de);
        data.append('shortDetails_es', this.state.newsShortDetails_es);
        data.append('shortDetails_fr', this.state.newsShortDetails_fr);
        data.append('photo', this.state.file);

        return data;
    };

    clearForm = () => {
        this.setState({
            language: 'pl',
            newsId: 0,
            newsName_pl: '',
            newsName_en: '',
            newsName_de: '',
            newsName_es: '',
            newsName_fr: '',
            newsAddress: '',
            newsAddress: '',
            newsCategoryId: '',
            isActive: true,
            newsDetails_pl: '',
            newsDetails_en: '',
            newsDetails_de: '',
            newsDetails_es: '',
            newsDetails_fr: '',
            newsShortDetails_pl: '',
            newsShortDetails_en: '',
            newsShortDetails_de: '',
            newsShortDetails_es: '',
            newsShortDetails_fr: '',
            newsPhoto: '',
            photo: '',
            file: null
        });
    }

    get = () => {
        apiNews.getSingle(this.state.newsId).then((data) => {
            this.setState({
                newsCategoryId: data.news.categoryId,
                newsName_pl: data.news.nameTranslations.pl,
                newsName_en: data.news.nameTranslations.en,
                newsName_de: data.news.nameTranslations.de,
                newsName_es: data.news.nameTranslations.es,
                newsName_fr: data.news.nameTranslations.fr,
                newsAddress: data.news.address,
                newsDetails_pl: data.news.detailsTranslations.pl,
                newsDetails_en: data.news.detailsTranslations.en,
                newsDetails_de: data.news.detailsTranslations.de,
                newsDetails_es: data.news.detailsTranslations.es,
                newsDetails_fr: data.news.detailsTranslations.fr,
                newsShortDetails_pl: data.news.shortDetailsTranslations.pl,
                newsShortDetails_en: data.news.shortDetailsTranslations.en,
                newsShortDetails_de: data.news.shortDetailsTranslations.de,
                newsShortDetails_es: data.news.shortDetailsTranslations.es,
                newsShortDetails_fr: data.news.shortDetailsTranslations.fr,
                photo: data.news.photo,
                isActive: data.news.isActive,
            });
        });
    }

    post = () => {
        apiNews.post(this.compareForm()).then((data) => {
            this.setState({
                open: false
            });

            if (data.response?.status === 400)
                alert("Wystąpił błąd podczas zapisywania danych.");

            this.state.onSaved();

            if (!this.state.editMode)
                this.clearForm();
        });
    }

    put = () => {
        apiNews.put(this.compareForm()).then((data) => {
            this.setState({
                open: false
            });

            if (data.response?.status === 400)
                alert("Wystąpił błąd podczas zapisywania danych.");

            this.state.onSaved();

            if (!this.state.editMode)
                this.clearForm();
        });
    }

    saveFile = (e) => {
        if (e.target.files && e.target.files.length)
            this.setState({
                photo: URL.createObjectURL(e.target.files[0]),
                file: e.target.files[0]
            });
    }

    render () {
        return (
            <div className={styles.EditIconButton}>
                {!this.state.editMode &&
                    <Button
                        variant="text"
                        className={styles.CreateButton}
                        onClick={() => {
                            this.setState({
                                open: true
                            });
                        }}
                    >
                        Dodaj nową
                    </Button>
                }
                {this.state.editMode &&
                    <EditIcon onClick={() => {
                        this.setState({
                            open: true
                        });
                    }} />
                }
                <Dialog
                    maxWidth="sm"
                    fullWidth
                    open={this.state.open}
                    onClose={() => {
                        this.setState({
                            open: false
                        });
                    }}>
                    <div>
                        <DialogTitle>
                            {(this.state.newsId) ? 'Edycja' : 'Tworzenie'} aktualności
                        </DialogTitle>
                        <DialogContent>
                            <Grid container>
                                <Grid item md={12} className={styles.Languages}>
                                    {Object.keys(languages)?.map(item => (
                                        <img
                                            key={item}
                                            src={languages[item].icon}
                                            alt=""
                                            className={`${this.state.language !== item ? "" : styles.selected}`}
                                            onClick={() => {
                                                this.changeLanguage(item);
                                            }}
                                        />
                                    ))}
                                </Grid>
                                <Grid item md={12} className="modified-form">
                                    {Object.keys(languages)?.map(item => (
                                        <TextField
                                            required={item === 'pl'}
                                            className={this.state.language === item ? '' : 'd-none'}
                                            autoFocus
                                            margin="dense"
                                            id={`news_name_${item}`}
                                            label={`Nazwa (${item.toUpperCase()})`}
                                            type="text"
                                            fullWidth
                                            variant="filled"
                                            inputProps={{ maxLength: 50 }}
                                            value={this.state[`newsName_${item}`]}
                                            onChange={(event) => {
                                                this.setState({
                                                    [`newsName_${item}`]: textService.capitalized(event.target.value)
                                                });
                                            }}
                                        />
                                    ))}

                                </Grid>
                                <Grid item md={12} className="modified-form">
                                    <TextField
                                        select
                                        margin="dense"
                                        id="news_category"
                                        label="Kategoria"
                                        type="text"
                                        fullWidth
                                        variant="filled"
                                        value={this.state.newsCategoryId}
                                        onChange={(event) => {
                                            this.setState({
                                                newsCategoryId: event.target.value
                                            });
                                        }}
                                    >
                                    {
                                        this.state.categories.length &&
                                        this.state.categories.map(function (category, i) {
                                            return <MenuItem key={i} value={category.id}>
                                                {category.name}
                                            </MenuItem>
                                        })
                                    }
                                    </TextField>
                                </Grid>
                                <Grid item md={12} className="modified-form">
                                    <TextField
                                        margin="dense"
                                        id="news_address"
                                        label="Adres"
                                        type="text"
                                        fullWidth
                                        variant="filled"
                                        value={this.state.newsAddress}
                                        onChange={(event) => {
                                            this.setState({
                                                newsAddress: textService.capitalized(event.target.value)
                                            });
                                        }}
                                    />
                                </Grid>
                                <Grid item md={12} className="modified-form">
                                    <TextField
                                            select
                                            margin="dense"
                                            id="news_status"
                                            label="Status"
                                            type="text"
                                            fullWidth
                                            variant="filled"
                                            value={this.state.isActive}
                                            onChange={(event) => {
                                                this.setState({
                                                    isActive: event.target.value
                                                });
                                            }}
                                        >
                                            {this.state.isActive != null &&
                                                <MenuItem value={null}>
                                                    Wyczyść
                                                </MenuItem>
                                            }
                                            <MenuItem value={true}>
                                                Aktywne
                                            </MenuItem>
                                            <MenuItem value={false}>
                                                Nieaktywne
                                            </MenuItem>
                                    </TextField>
                                </Grid>
                                <Grid item md={12} className="modified-form">
                                    {Object.keys(languages)?.map(item => (
                                        <TextField
                                            multiline
                                            required={item === 'pl'}
                                            className={this.state.language === item ? '' : 'd-none'}
                                            rows={6}
                                            margin="dense"
                                            id={`news_details_${item}`}
                                            label={`Opis (${item.toUpperCase()})`}
                                            type="text"
                                            fullWidth
                                            variant="filled"
                                            value={this.state[`newsDetails_${item}`]}
                                            onChange={(event) => {
                                                this.setState({
                                                    [`newsDetails_${item}`]: event.target.value
                                                });
                                            }}
                                    />
                                    ))}
                                </Grid>
                                <Grid item md={12} className="modified-form">
                                    {Object.keys(languages)?.map(item => (
                                        <TextField
                                            multiline
                                            required={item === 'pl'}
                                            className={this.state.language === item ? '' : 'd-none'}
                                            rows={2}
                                            margin="dense"
                                            id={`news_short_details_${item}`}
                                            label={`Krótki opis (${item.toUpperCase()})`}
                                            type="text"
                                            fullWidth
                                            variant="filled"
                                            value={this.state[`newsShortDetails_${item}`]}
                                            onChange={(event) => {
                                                this.setState({
                                                    [`newsShortDetails_${item}`]: event.target.value
                                                });
                                            }}
                                    />
                                    ))}
                                </Grid>
                                {this.state.photo &&
                                    <Grid item md={12} className="text-center mt-3">
                                        <img src={this.state.photo} />
                                    </Grid>
                                }
                                <Grid item md={12} className="modified-form">
                                    <input type="file" onChange={this.saveFile} />
                                </Grid>
                                <Grid item md={2} />
                                <Grid item md={8}>
                                    {!this.state.editMode &&
                                        <Button className={styles.SubmitButton} onClick={this.post}>
                                            Dodaj nową
                                        </Button>
                                    }
                                    {this.state.editMode &&
                                        <Button className={styles.SubmitButton} onClick={this.put}>
                                            Zapisz
                                        </Button>
                                    }
                                </Grid>
                                <Grid item md={2} />
                            </Grid>
                        </DialogContent>
                    </div>
                </Dialog>
            </div>
        )
    }
}

NewsFormDialog.propTypes = {
    categories: PropTypes.array.isRequired,
    onSaved: PropTypes.func.isRequired
};

NewsFormDialog.defaultProps = {}

export default NewsFormDialog;
