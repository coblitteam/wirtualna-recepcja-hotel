import React from 'react';
import PropTypes from 'prop-types';
import {
    Button,
    Grid,
    InputAdornment,
    MenuItem,
    TextField
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';

import NewsFormDialog from './NewsFormDialog';

import styles from './News.module.css';

const NewsTableFilter = (props) => {
    const {
        search,
        categoryId,
        isActive,
        categories,
        formDialogOpen,
        newsId,
        onFilterChanged,
        onFilterCleared,
        onFormSaved
    } = props;

    const clearFilter = () => {
        onFilterChanged('search', '');
        onFilterChanged('categoryId', null);
        onFilterChanged('isActive', null);
    }

    return (
        <Grid container className={styles.Filter}>
            <Grid item md={3} className="modified-form-filter">
                <TextField
                    className={styles.SearchField}
                    autoFocus
                    margin="dense"
                    id="filter_search"
                    label="Szukaj"
                    type="text"
                    fullWidth
                    variant="outlined"
                    size="small"
                    value={search}
                    onChange={(event) => {
                        onFilterChanged('search', event.target.value);
                    }}
                    InputProps={{
                        startAdornment: <InputAdornment position="start">
                                            <SearchIcon />
                                        </InputAdornment>,
                    }}
                />
            </Grid>
            <Grid item md={2} className="vertical-center">
                <NewsFormDialog
                    open={formDialogOpen}
                    newsId={newsId}
                    categories={categories}
                    onSaved={onFormSaved}
                />
            </Grid>
            <Grid item md={3} />
            <Grid item md={1} className="d-flex align-items-center">
                <Grid container>
                    <Grid item md={12} className="d-flex justify-content-center">
                        <Button className={styles.ClearButton} onClick={onFilterCleared}>
                            Wyczyść filtry
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item md={2} className="modified-form-filter with-side-padding">
                <TextField
                    select
                    margin="dense"
                    id="filter_category"
                    label="Kategoria"
                    type="text"
                    fullWidth
                    variant="outlined"
                    size="small"
                    value={categoryId}
                    onChange={(event) => {
                        onFilterChanged('categoryId', event.target.value);
                    }}
                >
                    {categoryId != null &&
                        <MenuItem value={null}>
                            Wyczyść
                        </MenuItem>
                    }
                    {categories.map((category) => (
                        <MenuItem key={category.id} value={category.id}>
                            {category.name}
                        </MenuItem>
                    ))}
                </TextField>
            </Grid>
            <Grid item md={1} className="modified-form-filter with-side-padding">
                <TextField
                        select
                        margin="dense"
                        id="filter_status"
                        label="Status"
                        type="text"
                        fullWidth
                        variant="outlined"
                        size="small"
                        value={isActive}
                        onChange={(event) => {
                            onFilterChanged('isActive', event.target.value);
                        }}
                    >
                        {isActive != null &&
                            <MenuItem value={null}>
                                Wyczyść
                            </MenuItem>
                        }
                        <MenuItem value={true}>
                            Aktywne
                        </MenuItem>
                        <MenuItem value={false}>
                            Nieaktywne
                        </MenuItem>
                </TextField>
            </Grid>
        </Grid>
    )
}

NewsTableFilter.propTypes = {
    search: PropTypes.string,
    categoryId: PropTypes.number,
    isActive: PropTypes.bool,
    categories: PropTypes.array.isRequired,
    onFilterChanged: PropTypes.func.isRequired
};

NewsTableFilter.defaultProps = {
    search: '',
    categoryId: null,
    isActive: null
}

export default NewsTableFilter;