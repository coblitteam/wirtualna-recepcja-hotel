﻿import React from 'react';
import PropTypes from 'prop-types';

import {
    Grid,
    Table,
    TableContainer,
    TableHead,
    TableRow,
    TableCell,
    TableSortLabel,
    TableBody,
    TablePagination
} from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';

import styles from './News.module.css';
import NewsFormDialog from './NewsFormDialog';
import { apiNews } from '../../../services/api/news/News';

const headCells = [
    {
        id: 'name',
        numeric: false,
        label: 'Nazwa',
        sortEnabled: true
    },
    {
        id: 'category',
        numeric: false,
        label: 'Kategoria',
        sortEnabled: true
    },
    {
        id: 'isActive',
        numeric: false,
        label: 'Status',
        sortEnabled: true
    }
]

const NewsTable = (props) => {
    const {
        data,
        onRequestSort,
        onPageChange,
        onRowsPerPageChange,
        onFormSaved
    } = props;

    const sortHandler = (property) => (event) => {
        onRequestSort(event, property);
    };

    const labelDisplayedRows = ({ from, to, count }) => {
        return `${from}–${to} z ${count !== -1 ? count : `więcej niż ${to}`}`;
    }

    const remove = (id) => {
        apiNews.remove(id).then(onFormSaved);
    }

    return (
        <Grid container className={styles.AdminNewsTable}>
            <TableContainer className={styles.Container}>
                <Table>
                    <TableHead className={styles.TableHead}>
                        <TableRow>
                        {headCells.map((headCell, index) => {
                            if (headCell.sortEnabled)
                                return (
                                    <TableCell
                                        key={headCell.id}
                                        align="left"
                                        padding="none"
                                        sortDirection={data.orderBy === headCell.id ? data.order : false}
                                        className={
                                            (index === 0) ? "table-head-cell first" :
                                            (index === headCells.length - 1) ? 'table-head-cell last' :
                                                    'table-head-cell'
                                        }
                                    >
                                        <TableSortLabel
                                            active={data.orderBy === headCell.id}
                                            direction={data.orderBy === headCell.id ? data.order : 'asc'}
                                            onClick={sortHandler(headCell.id)}
                                        >
                                            {headCell.label}
                                        </TableSortLabel>
                                    </TableCell>
                                );
                            else
                                return (
                                    <TableCell
                                        key={headCell.id}
                                        align="left"
                                        padding="none"
                                        className={
                                            (index === 0) ? "table-head-cell first" :
                                            (index === headCells.length - 1) ? 'table-head-cell last' :
                                                    'table-head-cell'
                                        }
                                    >
                                        {headCell.label}
                                    </TableCell>
                                );
                        })}
                        <TableCell
                            padding="none"
                            align="right"
                        />
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.items.map((news) => {
                            return (
                                <TableRow key={news.id} className={styles.TableRow}>
                                    <TableCell align="left" padding="none" className={styles.ValueCell}>
                                        {news.name}
                                    </TableCell>
                                    <TableCell align="left" padding="none" className={styles.ValueCell}>
                                        {news.category}
                                    </TableCell>
                                    <TableCell align="left" padding="none" className={styles.ValueCell}>
                                        {news.isActive ? "Aktywne" : "Nieaktywne"}
                                    </TableCell>
                                    <TableCell align="left" padding="none" className={`${styles.ValueCell} ${styles.CrudCell}`}>
                                            <NewsFormDialog
                                                editMode={true}
                                                newsId={news.id}
                                                categories={data.categories}
                                                onSaved={onFormSaved}
                                            />
                                            <DeleteIcon onClick={() => {
                                                remove(news.id);
                                            }}/>
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                            className={styles.Pagination}
                            rowsPerPageOptions={[3, 5, 10]}
                            component="div"
                            count={data.size}
                            rowsPerPage={data.perPage}
                            page={data.page}
                            onPageChange={onPageChange}
                            onRowsPerPageChange={onRowsPerPageChange}
                            labelDisplayedRows={labelDisplayedRows}
                            labelRowsPerPage={"Wierszy na stronę:"}
                        />
        </Grid>
    );
}

NewsTable.propTypes = {
    data: PropTypes.object.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onPageChange: PropTypes.func.isRequired,
    onRowsPerPageChange: PropTypes.func.isRequired,
    onEditRequest: PropTypes.func.isRequired
};

NewsTable.defaultProps = {
    data: {
        items: [],
        orderBy: 'name',
        order: 'asc',
        rowsCount: 0,
        perPage: 5,
        page: 0
    }
};

export default NewsTable;