﻿import React from 'react';
import PropTypes from 'prop-types';

import {
    Grid,
    Table,
    TableContainer,
    TableHead,
    TableRow,
    TableCell,
    TableSortLabel,
    TableBody,
    TablePagination
} from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';

import styles from './Attractions.module.css';
import AttractionFormDialog from './AttractionsFormDialog';
import { apiAttraction } from '../../../services/api/attractions/Attraction';

const headCells = [
    {
        id: 'name',
        numeric: false,
        label: 'Nazwa',
        sortEnabled: true
    },
    {
        id: 'category',
        numeric: false,
        label: 'Kategoria',
        sortEnabled: true
    },
    {
        id: 'netPrice',
        numeric: false,
        label: 'Cena netto',
        sortEnabled: true
    },
    {
        id: 'vat',
        numeric: false,
        label: 'Stawka VAT',
        sortEnabled: true
    },
    {
        id: 'grosPrice',
        numeric: false,
        label: 'Cena brutto',
        sortEnabled: false
    },
    {
        id: 'isActive',
        numeric: false,
        label: 'Status',
        sortEnabled: true
    }
]

const AttractionsTable = (props) => {
    const {
        data,
        onRequestSort,
        onPageChange,
        onRowsPerPageChange,
        onEditRequest,
        onFormSaved
    } = props;

    const sortHandler = (property) => (event) => {
        onRequestSort(event, property);
    };

    const labelDisplayedRows = ({ from, to, count }) => {
        return `${from}–${to} z ${count !== -1 ? count : `więcej niż ${to}`}`;
    }

    const remove = (id) => {
        apiAttraction.remove(id).then(onFormSaved);
    }

    return (
        <Grid container className={styles.AdminAttractionsTable}>
            <TableContainer className={styles.Container}>
                <Table>
                    <TableHead className={styles.TableHead}>
                        <TableRow>
                        {headCells.map((headCell, index) => {
                            if (headCell.sortEnabled)
                                return (
                                    <TableCell
                                        key={headCell.id}
                                        align="left"
                                        padding="none"
                                        sortDirection={data.orderBy === headCell.id ? data.order : false}
                                        className={
                                            (index === 0) ? "table-head-cell first" :
                                            (index === headCells.length - 1) ? 'table-head-cell last' :
                                                    'table-head-cell'
                                        }
                                    >
                                        <TableSortLabel
                                            active={data.orderBy === headCell.id}
                                            direction={data.orderBy === headCell.id ? data.order : 'asc'}
                                            onClick={sortHandler(headCell.id)}
                                        >
                                            {headCell.label}
                                        </TableSortLabel>
                                    </TableCell>
                                );
                            else
                                return (
                                    <TableCell
                                        key={headCell.id}
                                        align="left"
                                        padding="none"
                                        className={
                                            (index === 0) ? "table-head-cell first" :
                                            (index === headCells.length - 1) ? 'table-head-cell last' :
                                                    'table-head-cell'
                                        }
                                    >
                                        {headCell.label}
                                    </TableCell>
                                );
                        })}
                        <TableCell
                            padding="none"
                            align="right"
                        />
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.items.map((attraction) => {
                            return (
                                <TableRow key={attraction.id} className={styles.TableRow}>
                                    <TableCell align="left" padding="none" className={styles.ValueCell}>
                                        {attraction.name}
                                    </TableCell>
                                    <TableCell align="left" padding="none" className={styles.ValueCell}>
                                        {attraction.category}
                                    </TableCell>
                                    <TableCell align="left" padding="none" className={styles.ValueCell}>
                                        {parseFloat(attraction.netPrice).toFixed(2)} zł
                                    </TableCell>
                                    <TableCell align="left" padding="none" className={styles.ValueCell}>
                                        {attraction.vat}%
                                    </TableCell>
                                    <TableCell align="left" padding="none" className={styles.ValueCell}>
                                        {parseFloat(attraction.grosPrice).toFixed(2)} zł
                                    </TableCell>
                                    <TableCell align="left" padding="none" className={styles.ValueCell}>
                                        {attraction.isActive ? "Aktywne" : "Nieaktywne"}
                                    </TableCell>
                                    <TableCell align="left" padding="none" className={`${styles.ValueCell} ${styles.CrudCell}`}>
                                        {(!attraction.isBoard && attraction.canDelete) &&
                                            <DeleteIcon onClick={() => {
                                                remove(attraction.id);
                                            }}/>
                                        }
                                        <AttractionFormDialog
                                            editMode={true}
                                            attractionId={attraction.id}
                                            categories={data.categories}
                                            onSaved={onFormSaved}
                                        />
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                            className={styles.Pagination}
                            rowsPerPageOptions={[3, 5, 10]}
                            component="div"
                            count={data.size}
                            rowsPerPage={data.perPage}
                            page={data.page}
                            onPageChange={onPageChange}
                            onRowsPerPageChange={onRowsPerPageChange}
                            labelDisplayedRows={labelDisplayedRows}
                            labelRowsPerPage={"Wierszy na stronę:"}
                        />
        </Grid>
    );
}

AttractionsTable.propTypes = {
    data: PropTypes.object.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onPageChange: PropTypes.func.isRequired,
    onRowsPerPageChange: PropTypes.func.isRequired,
    onEditRequest: PropTypes.func.isRequired
};

AttractionsTable.defaultProps = {
    data: {
        items: [],
        orderBy: 'name',
        order: 'asc',
        rowsCount: 0,
        perPage: 5,
        page: 0
    }
};

export default AttractionsTable;