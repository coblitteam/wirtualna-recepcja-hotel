﻿import React, { Component } from 'react';

import {
    Button,
    Container,
    Grid,
    MenuItem,
    TextField
} from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';

import AttractionsTable from './AttractionsTable';
import AttractionsTableFilter from './AttractionsTableFilter';

import styles from './Attractions.module.css';

import { apiAttraction } from '../../../services/api/attractions/Attraction';
import { apiAttractionCategory } from '../../../services/api/attractionCategories/AttractionCategory';

export class Attractions extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
            orderBy: 'name',
            order: 'asc',
            perPage: 10,
            page: 0,
            size: 0,
            search: '',
            categoryId: null,
            netPriceFrom: null,
            netPriceTo: null,
            grosPriceFrom: null,
            grosPriceTo: null,
            isActive: null,
            categories: [],
            formDialogOpen: false,
            attractionId: 0
        };

        this.timer = null

        this.loadData();
    }

    loadData = () => {
        let query = {
            orderBy: this.state.orderBy,
            orderDestination: this.state.order,
            start: this.state.page * this.state.perPage,
            size: this.state.perPage,
            search: this.state.search,
            categoryId: this.state.categoryId,
            netPriceFrom: this.state.netPriceFrom,
            netPriceTo: this.state.netPriceTo,
            grosPriceFrom: this.state.grosPriceFrom,
            grosPriceTo: this.state.grosPriceTo,
            isActive: this.state.isActive
        };

        this.timer = setTimeout(async () => {
            let data = await apiAttraction.getList(query);
            let categories = await apiAttractionCategory.getCustom('/list');

            this.setState({
                items: data.items,
                size: data.length,
                categories: categories.categories
            });
        }, 500);
    }

    handleRequestSort = (event, property) => {
        this.state.orderBy = property;
        this.state.order = property == this.state.orderBy && this.state.order != 'desc' ? 'desc' : 'asc';

        this.setState({
            orderBy: this.state.orderBy,
            order: this.state.order
        });


        this.loadData();
    }

    handlePageChange = (event, newPage) => {
        this.state.page = newPage;

        this.setState({ page: this.state.page });

        this.loadData();
    }

    handleRowsPerPageChange = (event) => {
        this.state.perPage = parseInt(event.target.value, 10);
        this.state.page = 0;

        this.setState({
            perPage: this.state.perPage,
            page: this.state.page
        });

        this.loadData();
    }

    handleFilterChanged = (prop, value) => {
        switch (prop) {
            case 'search':
                this.state.search = value;
                this.setState({
                    search: this.state.search
                });
                break;
            case 'categoryId':
                this.state.categoryId = value;
                this.setState({
                    categoryId: this.state.categoryId
                });
                break;
            case 'netPriceFrom':
                this.state.netPriceFrom = value;
                this.setState({
                    netPriceFrom: this.state.netPriceFrom
                });
                break;
            case 'netPriceTo':
                this.state.netPriceTo = value;
                this.setState({
                    netPriceTo: this.state.netPriceTo
                });
                break;
            case 'grosPriceFrom':
                this.state.grosPriceFrom = value;
                this.setState({
                    grosPriceFrom: this.state.grosPriceFrom
                });
                break;
            case 'grosPriceTo':
                this.state.grosPriceTo = value;
                this.setState({
                    grosPriceTo: this.state.grosPriceTo
                });
                break;
            case 'isActive':
                this.state.isActive = value;
                this.setState({
                    isActive: this.state.isActive
                });
                break;
        }

        this.loadData();
    }

    handleFilterCleared = () => {
        this.state.search = '';
        this.state.categoryId = '';
        this.state.netPriceFrom = '';
        this.state.netPriceTo = '';
        this.state.grosPriceFrom = '';
        this.state.grosPriceTo = '';
        this.state.isActive = '';

        this.setState(this.state);

        this.loadData();
    }

    handleEditRequest = (attractionId) => {
        console.log(this.state);
        this.setState({
            formDialogOpen: true,
            attractionId: attractionId
        });
    }

    render() {
        return (
            <div className={styles.AdminAttractions}>
                {this.state.categories.length &&
                    <div>
                        <AttractionsTableFilter
                            search={this.state.search}
                            categoryId={this.state.categoryId}
                            netPriceFrom={this.state.netPriceFrom}
                            netPriceTo={this.state.netPriceTo}
                            grosPriceFrom={this.state.grosPriceFrom}
                            grosPriceTo={this.state.grosPriceTo}
                            isActive={this.state.isActive}
                            categories={this.state.categories}
                            formDialogOpen={this.state.formDialogOpen}
                            attractionId={this.state.attractionId}
                            onFilterChanged={this.handleFilterChanged}
                            onFilterCleared={this.handleFilterCleared}
                            onFormSaved={this.loadData}
                        />
                        <AttractionsTable
                            data={this.state}
                            onRequestSort={this.handleRequestSort}
                            onPageChange={this.handlePageChange}
                            onRowsPerPageChange={this.handleRowsPerPageChange}
                            onEditRequest={this.handleEditRequest}
                            onFormSaved={this.loadData}
                        />
                    </div>
                }
            </div>
        );
    };
}

export default Attractions;