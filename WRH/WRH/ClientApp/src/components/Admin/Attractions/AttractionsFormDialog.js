import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Button,
    FormControl,
    Grid,
    InputLabel,
    InputAdornment,
    MenuItem,
    OutlinedInput,
    TextField,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions
} from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import { PL, EN, DE, ES, FR } from '../../../images/locales';
import {
    ControlledEditor
} from '../../';

import styles from './Attractions.module.css';

import { textService } from '../../../services/TextService';
import { apiAttraction } from '../../../services/api/attractions/Attraction';

const languages = {
    pl: { icon: PL },
    en: { icon: EN },
    de: { icon: DE },
    es: { icon: ES },
    fr: { icon: FR }
};

export class AttractionFormDialog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editMode: props.editMode ?? false,
            open: props.open ?? false,
            language: 'pl',
            categories: props.categories,
            onSaved: props.onSaved,
            attractionId: props.attractionId ?? 0,
            attractionName_pl: '',
            attractionName_en: '',
            attractionName_de: '',
            attractionName_es: '',
            attractionName_fr: '',
            attractionCategoryId: props.categories[0]?.id,
            attractionNetPrice: 0,
            attractionVat: 23,
            paymentType: 0,
            isActive: true,
            immediatePayment: false,
            attractionDetails_pl: '',
            attractionDetails_en: '',
            attractionDetails_de: '',
            attractionDetails_es: '',
            attractionDetails_fr: '',
            attractionShortDetails_pl: '',
            attractionShortDetails_en: '',
            attractionShortDetails_de: '',
            attractionShortDetails_es: '',
            attractionShortDetails_fr: '',
            attractionPhoto: '',
            notificationEmailAdresses: '',
            notificationHotelMessage: '',
            notificationGuestMessage: '',
            photo: '',
            file: null
        };

        if (this.state.attractionId)
            this.get();
    }

    changeLanguage = (lang) => {
        this.setState({
            language: lang
        });
    }

    compareForm = () => {
        let data = new FormData();

        data.append('id', this.state.attractionId);
        data.append('categoryId', this.state.attractionCategoryId);
        data.append('name', this.state.attractionName_pl);
        data.append('name_en', this.state.attractionName_en);
        data.append('name_de', this.state.attractionName_de);
        data.append('name_es', this.state.attractionName_es);
        data.append('name_fr', this.state.attractionName_fr);
        data.append('netPrice', this.state.attractionNetPrice.toString().replace('.',','));
        data.append('vat', this.state.attractionVat);
        data.append('isActive', this.state.isActive);
        data.append('details', this.state.attractionDetails_pl);
        data.append('details_en', this.state.attractionDetails_en);
        data.append('details_de', this.state.attractionDetails_de);
        data.append('details_es', this.state.attractionDetails_es);
        data.append('details_fr', this.state.attractionDetails_fr);
        data.append('shortDetails', this.state.attractionShortDetails_pl);
        data.append('shortDetails_en', this.state.attractionShortDetails_en);
        data.append('shortDetails_de', this.state.attractionShortDetails_de);
        data.append('shortDetails_es', this.state.attractionShortDetails_es);
        data.append('shortDetails_fr', this.state.attractionShortDetails_fr);
        data.append('photo', this.state.file);
        data.append('immediatePayment', this.state.immediatePayment);
        data.append('notificationEmailAdresses', this.state.notificationEmailAdresses ?? '');
        data.append('notificationHotelMessage', this.state.notificationHotelMessage ?? '');
        data.append('notificationGuestMessage', this.state.notificationGuestMessage ?? '');
        data.append('paymentType', this.state.paymentType);

        return data;
    };

    clearForm = () => {
        this.setState({
            language: 'pl',
            attractionId: 0,
            attractionName_pl: '',
            attractionName_en: '',
            attractionName_de: '',
            attractionName_es: '',
            attractionName_fr: '',
            attractionCategoryId: '',
            attractionNetPrice: 0,
            attractionVat: 23,
            paymentType: 0,
            isActive: true,
            immediatePayment: false,
            attractionDetails_pl: '',
            attractionDetails_en: '',
            attractionDetails_de: '',
            attractionDetails_es: '',
            attractionDetails_fr: '',
            attractionShortDetails_pl: '',
            attractionShortDetails_en: '',
            attractionShortDetails_de: '',
            attractionShortDetails_es: '',
            attractionShortDetails_fr: '',
            attractionPhoto: '',
            notificationEmailAdresses: '',
            notificationHotelMessage: '',
            notificationGuestMessage: '',
            photo: '',
            file: null
        });
    }

    get = () => {
        apiAttraction.getSingle(this.state.attractionId).then((data) => {
            this.setState({
                attractionCategoryId: data.attraction.categoryId,
                attractionName_pl: data.attraction.nameTranslations.pl,
                attractionName_en: data.attraction.nameTranslations.en,
                attractionName_de: data.attraction.nameTranslations.de,
                attractionName_es: data.attraction.nameTranslations.es,
                attractionName_fr: data.attraction.nameTranslations.fr,
                attractionNetPrice: data.attraction.netPrice,
                attractionVat: data.attraction.vat,
                attractionDetails_pl: data.attraction.detailsTranslations.pl,
                attractionDetails_en: data.attraction.detailsTranslations.en,
                attractionDetails_de: data.attraction.detailsTranslations.de,
                attractionDetails_es: data.attraction.detailsTranslations.es,
                attractionDetails_fr: data.attraction.detailsTranslations.fr,
                attractionShortDetails_pl: data.attraction.shortDetailsTranslations.pl,
                attractionShortDetails_en: data.attraction.shortDetailsTranslations.en,
                attractionShortDetails_de: data.attraction.shortDetailsTranslations.de,
                attractionShortDetails_es: data.attraction.shortDetailsTranslations.es,
                attractionShortDetails_fr: data.attraction.shortDetailsTranslations.fr,
                photo: data.attraction.photo,
                paymentType: data.attraction.paymentType,
                isActive: data.attraction.isActive,
                immediatePayment: data.attraction.immediatePayment,
                notificationEmailAdresses: data.attraction.notificationEmailAdresses ?? '',
                notificationHotelMessage: data.attraction.notificationHotelMessage ?? '<b>test</b>',
                notificationGuestMessage: data.attraction.notificationGuestMessage ?? ''
            });
        });
    }

    post = () => {
        apiAttraction.post(this.compareForm()).then((data) => {
            this.setState({
                open: false
            });

            if (data.response?.status === 400)
                alert("Wystąpił błąd podczas zapisywania danych.");

            this.state.onSaved();

            if (!this.state.editMode)
                this.clearForm();
        });
    }

    put = () => {
        apiAttraction.put(this.compareForm()).then((data) => {
            this.setState({
                open: false
            });

            if (data.response?.status === 400)
                alert("Wystąpił błąd podczas zapisywania danych.");

            this.state.onSaved();

            if (!this.state.editMode)
                this.clearForm();
        });
    }

    saveFile = (e) => {
        if (e.target.files && e.target.files.length)
            this.setState({
                photo: URL.createObjectURL(e.target.files[0]),
                file: e.target.files[0]
            });
    }

    render () {
        return (
            <div className={styles.EditIconButton}>
                {!this.state.editMode &&
                    <Button
                        variant="text"
                        className={styles.CreateButton}
                        onClick={() => {
                            this.setState({
                                open: true
                            });
                        }}
                    >
                        Dodaj nową
                    </Button>
                }
                {this.state.editMode &&
                    <EditIcon onClick={() => {
                        this.setState({
                            open: true
                        });
                    }} />
                }
                <Dialog
                    maxWidth="sm"
                    fullWidth
                    open={this.state.open}
                    onClose={() => {
                        this.setState({
                            open: false
                        });
                    }}>
                    <div>
                        <DialogTitle>
                            {(this.state.attractionId) ? 'Edycja' : 'Tworzenie'} atrakcji
                        </DialogTitle>
                        <DialogContent>
                            <Grid container>
                                <Grid item md={12} className={styles.Languages}>
                                    {Object.keys(languages)?.map(item => (
                                        <img
                                            key={item}
                                            src={languages[item].icon}
                                            alt=""
                                            className={`${this.state.language !== item ? "" : styles.selected}`}
                                            onClick={() => {
                                                this.changeLanguage(item);
                                            }}
                                        />
                                    ))}
                                </Grid>
                                <Grid item md={12} className="modified-form">
                                    {Object.keys(languages)?.map(item => (
                                        <TextField
                                            required={item === 'pl'}
                                            className={this.state.language === item ? '' : 'd-none'}
                                            autoFocus
                                            margin="dense"
                                            id={`attraction_name_${item}`}
                                            label={`Nazwa (${item.toUpperCase()})`}
                                            type="text"
                                            fullWidth
                                            variant="filled"
                                            inputProps={{ maxLength: 50 }}
                                            value={this.state[`attractionName_${item}`]}
                                            onChange={(event) => {
                                                this.setState({
                                                    [`attractionName_${item}`]: textService.capitalized(event.target.value)
                                                });
                                            }}
                                        />
                                    ))}
                                </Grid>
                                <Grid item md={12} className="modified-form">
                                    <TextField
                                        select
                                        margin="dense"
                                        id="attraction_category"
                                        label="Kategoria"
                                        type="text"
                                        fullWidth
                                        variant="filled"
                                        value={this.state.attractionCategoryId}
                                        onChange={(event) => {
                                            this.setState({
                                                attractionCategoryId: event.target.value
                                            });
                                        }}
                                    >
                                    {
                                        this.state.categories.length &&
                                        this.state.categories.map(function (category, i) {
                                            return <MenuItem key={i} value={category.id}>
                                                {category.name}
                                            </MenuItem>
                                        })
                                    }
                                    </TextField>
                                </Grid>
                                <Grid item md={4} className="modified-form">
                                    <TextField
                                        margin="dense"
                                        id="attraction_net_price"
                                        label="Cena netto"
                                        type="number"
                                        fullWidth
                                        variant="filled"
                                        value={this.state.attractionNetPrice}
                                        onChange={(event) => {
                                            this.setState({
                                                attractionNetPrice: event.target.value
                                            });
                                        }}
                                        InputProps={{
                                            endAdornment: <InputAdornment position="end">
                                                                zł
                                                            </InputAdornment>,
                                        }}
                                        inputProps={{
                                            step: 0.01
                                        }}
                                    />
                                </Grid>
                                <Grid item md={4} className="modified-form">
                                    <TextField
                                        margin="dense"
                                        id="attraction_vat"
                                        label="Stawka VAT"
                                        type="number"
                                        fullWidth
                                        variant="filled"
                                        value={this.state.attractionVat}
                                        onChange={(event) => {
                                            this.setState({
                                                attractionVat: event.target.value
                                            });
                                        }}
                                        InputProps={{
                                            endAdornment: <InputAdornment position="end">
                                                                %
                                                            </InputAdornment>,
                                        }}
                                    />
                                </Grid>
                                <Grid item md={4} className="modified-form">
                                    <TextField
                                        disabled
                                        margin="dense"
                                        id="attraction_gros_price"
                                        label="Cena brutto"
                                        type="number"
                                        fullWidth
                                        variant="filled"
                                        value={parseFloat(this.state.attractionNetPrice * (1 + (this.state.attractionVat / 100))).toFixed(2)}
                                        InputProps={{
                                            endAdornment: <InputAdornment position="end">
                                                                zł
                                                            </InputAdornment>,
                                        }}
                                    />
                                </Grid>
                                <Grid item md={12} className="modified-form">
                                    <TextField
                                            select
                                            margin="dense"
                                            id="attraction_immediate_payment"
                                            label="Płatność"
                                            type="text"
                                            fullWidth
                                            variant="filled"
                                            value={this.state.immediatePayment}
                                            onChange={(event) => {
                                                this.setState({
                                                    immediatePayment: event.target.value
                                                });
                                            }}
                                        >
                                            <MenuItem value={true}>
                                                Przy zamówieniu
                                            </MenuItem>
                                            <MenuItem value={false}>
                                                Po zrealizowaniu
                                            </MenuItem>
                                    </TextField>
                                </Grid><Grid item md={12} className="modified-form">
                                    <TextField
                                            select
                                            margin="dense"
                                            id="attraction_payment_type"
                                            label="Rodzaj płatności"
                                            type="text"
                                            fullWidth
                                            variant="filled"
                                            value={this.state.paymentType}
                                            onChange={(event) => {
                                                this.setState({
                                                    paymentType: event.target.value
                                                });
                                            }}
                                        >
                                            <MenuItem value={0}>
                                                Płatność za sztukę
                                            </MenuItem>
                                            <MenuItem value={1}>
                                                Płatność za dobę
                                            </MenuItem>
                                            <MenuItem value={2}>
                                                Płatność za dobę/osoba
                                            </MenuItem>
                                    </TextField>
                                </Grid>
                                <Grid item md={12} className="modified-form">
                                    <TextField
                                            select
                                            margin="dense"
                                            id="attraction_status"
                                            label="Status"
                                            type="text"
                                            fullWidth
                                            variant="filled"
                                            value={this.state.isActive}
                                            onChange={(event) => {
                                                this.setState({
                                                    isActive: event.target.value
                                                });
                                            }}
                                        >
                                            {this.state.isActive != null &&
                                                <MenuItem value={null}>
                                                    Wyczyść
                                                </MenuItem>
                                            }
                                            <MenuItem value={true}>
                                                Aktywne
                                            </MenuItem>
                                            <MenuItem value={false}>
                                                Nieaktywne
                                            </MenuItem>
                                    </TextField>
                                </Grid>
                                <Grid item md={12} className="modified-form">
                                    {Object.keys(languages)?.map(item => (
                                        <TextField
                                            multiline
                                            required={item === 'pl'}
                                            className={this.state.language === item ? '' : 'd-none'}
                                            rows={6}
                                            margin="dense"
                                            id={`attraction_details_${item}`}
                                            label={`Opis (${item.toUpperCase()})`}
                                            type="text"
                                            fullWidth
                                            variant="filled"
                                            value={this.state[`attractionDetails_${item}`]}
                                            onChange={(event) => {
                                                this.setState({
                                                    [`attractionDetails_${item}`]: event.target.value
                                                });
                                            }}
                                    />
                                    ))}
                                </Grid>
                                <Grid item md={12} className="modified-form">
                                    {Object.keys(languages)?.map(item => (
                                        <TextField
                                            multiline
                                            required={item === 'pl'}
                                            className={this.state.language === item ? '' : 'd-none'}
                                            rows={2}
                                            margin="dense"
                                            id={`attraction_short_details_${item}`}
                                            label={`Krótki opis (${item.toUpperCase()})`}
                                            type="text"
                                            fullWidth
                                            variant="filled"
                                            value={this.state[`attractionShortDetails_${item}`]}
                                            onChange={(event) => {
                                                this.setState({
                                                    [`attractionShortDetails_${item}`]: event.target.value
                                                });
                                            }}
                                    />
                                    ))}
                                </Grid>
                                {/* <Grid item md={12} className="modified-form">
                                    <TextField
                                            rows={2}
                                            margin="dense"
                                            id="attraction_notification_adresses"
                                            label="Poinformuj o nabyciu/rezerwacji"
                                            helperText="Adresy e-mail oddzielaj przecinkiem (,)"
                                            type="text"
                                            fullWidth
                                            variant="filled"
                                            value={this.state.notificationEmailAdresses}
                                            onChange={(event) => {
                                                this.setState({
                                                    notificationEmailAdresses: event.target.value
                                                });
                                            }}
                                    />
                                </Grid>
                                <Grid item md={12}>
                                    <label className={styles.Label}>Treść wiadomości - hotel</label>
                                </Grid>
                                <Grid item md={12} style={{padding: '5px 20px'}}>
                                    <ControlledEditor value={this.state.notificationHotelMessage} onChange={(content) => {
                                        this.setState({
                                            notificationHotelMessage: content
                                        });
                                    }}/>
                                </Grid>
                                <Grid item md={12}>
                                    <label className={styles.Label}>Treść wiadomości - gość</label>
                                </Grid>
                                <Grid item md={12} style={{padding: '5px 20px'}}>
                                    <ControlledEditor value={this.state.notificationGuestMessage} onChange={(content) => {
                                        this.setState({
                                            notificationGuestMessage: content
                                        });
                                    }}/>
                                </Grid> */}
                                {this.state.photo &&
                                    <Grid item md={12} className="text-center mt-3">
                                        <img src={this.state.photo} />
                                    </Grid>
                                }
                                <Grid item md={12} className="modified-form">
                                    <input type="file" onChange={this.saveFile} />
                                </Grid>
                                <Grid item md={2} />
                                <Grid item md={8} className="mt-5">
                                    {!this.state.editMode &&
                                        <Button className={styles.SubmitButton} onClick={this.post}>
                                            Dodaj nową
                                        </Button>
                                    }
                                    {this.state.editMode &&
                                        <Button className={styles.SubmitButton} onClick={this.put}>
                                            Zapisz
                                        </Button>
                                    }
                                </Grid>
                                <Grid item md={2} />
                            </Grid>
                        </DialogContent>
                    </div>
                </Dialog>
            </div>
        )
    }
}

AttractionFormDialog.propTypes = {
    categories: PropTypes.array.isRequired,
    onSaved: PropTypes.func.isRequired
};

AttractionFormDialog.defaultProps = {}

export default AttractionFormDialog;
