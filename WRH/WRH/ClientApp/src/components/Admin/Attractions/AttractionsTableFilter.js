import React from 'react';
import PropTypes from 'prop-types';
import {
    Button,
    FormControl,
    Grid,
    InputLabel,
    InputAdornment,
    MenuItem,
    OutlinedInput,
    TextField
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';

import AttractionFormDialog from './AttractionsFormDialog';

import styles from './Attractions.module.css';

const AttractionsTableFilter = (props) => {
    const {
        search,
        categoryId,
        netPriceFrom,
        netPriceTo,
        grosPriceFrom,
        grosPriceTo,
        isActive,
        categories,
        formDialogOpen,
        attractionId,
        onFilterChanged,
        onFilterCleared,
        onFormSaved
    } = props;

    return (
        <Grid container className={styles.Filter}>
            <Grid item md={2} className="modified-form-filter">
                <TextField
                    className={styles.SearchField}
                    autoFocus
                    margin="dense"
                    id="filter_search"
                    label="Szukaj"
                    type="text"
                    fullWidth
                    variant="outlined"
                    size="small"
                    value={search}
                    onChange={(event) => {
                        onFilterChanged('search', event.target.value);
                    }}
                    InputProps={{
                        startAdornment: <InputAdornment position="start">
                                            <SearchIcon />
                                        </InputAdornment>,
                    }}
                />
            </Grid>
            <Grid item md={2} className="vertical-center">
                <AttractionFormDialog
                    open={formDialogOpen}
                    attractionId={attractionId}
                    categories={categories}
                    onSaved={onFormSaved}
                />
            </Grid>
            <Grid item md={1} className="d-flex align-items-center">
                <Grid container>
                    <Grid item md={12} className="d-flex justify-content-center">
                        <Button className={styles.ClearButton} onClick={onFilterCleared}>
                            Wyczyść filtry
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item md={2} className="modified-form-filter with-side-padding">
                <TextField
                    select
                    margin="dense"
                    id="filter_category"
                    label="Kategoria"
                    type="text"
                    fullWidth
                    variant="outlined"
                    size="small"
                    value={categoryId}
                    onChange={(event) => {
                        onFilterChanged('categoryId', event.target.value);
                    }}
                >
                    {categoryId != null &&
                        <MenuItem value={null}>
                            Wyczyść
                        </MenuItem>
                    }
                    {categories.map((category) => (
                        <MenuItem key={category.id} value={category.id}>
                            {category.name}
                        </MenuItem>
                    ))}
                </TextField>
            </Grid>
            <Grid item md={2} className="modified-form-filter with-side-padding">
                <Grid container>
                    <Grid item md={6} className={styles.PriceFields}>
                        <TextField
                            margin="dense"
                            id="filter_net_price_from"
                            label="Cena netto"
                            type="number"
                            fullWidth
                            variant="outlined"
                            size="small"
                            value={netPriceFrom}
                            onChange={(event) => {
                                onFilterChanged('netPriceFrom', event.target.value);
                            }}
                            InputProps={{
                                startAdornment: <InputAdornment position="start">
                                                    Od:
                                                </InputAdornment>,
                            }}
                        />
                    </Grid>
                    <Grid item md={6} className={styles.PriceFields}>
                        <TextField
                            margin="dense"
                            id="filter_net_price_to"
                            label="Cena netto"
                            type="number"
                            fullWidth
                            variant="outlined"
                            size="small"
                            value={netPriceTo}
                            onChange={(event) => {
                                onFilterChanged('netPriceTo', event.target.value);
                            }}
                            InputProps={{
                                startAdornment: <InputAdornment position="start">
                                                    Do:
                                                </InputAdornment>,
                            }}
                        />
                    </Grid>
                </Grid>
            </Grid>
            <Grid item md={2} className="modified-form-filter with-side-padding">
                <Grid container>
                    <Grid item md={6} className={styles.PriceFields}>
                        <TextField
                            margin="dense"
                            id="filter_gros_price_from"
                            label="Cena brutto"
                            type="number"
                            fullWidth
                            variant="outlined"
                            size="small"
                            value={grosPriceFrom}
                            onChange={(event) => {
                                onFilterChanged('grosPriceFrom', event.target.value);
                            }}
                            InputProps={{
                                startAdornment: <InputAdornment position="start">
                                                    Od:
                                                </InputAdornment>,
                            }}
                        />
                    </Grid>
                    <Grid item md={6} className={styles.PriceFields}>
                        <TextField
                            margin="dense"
                            id="filter_gros_price_to"
                            label="Cena brutto"
                            type="number"
                            fullWidth
                            variant="outlined"
                            size="small"
                            value={grosPriceTo}
                            onChange={(event) => {
                                onFilterChanged('grosPriceTo', event.target.value);
                            }}
                            InputProps={{
                                startAdornment: <InputAdornment position="start">
                                                    Do:
                                                </InputAdornment>,
                            }}
                        />
                    </Grid>
                </Grid>
            </Grid>
            <Grid item md={1} className="modified-form-filter with-side-padding">
                <TextField
                        select
                        margin="dense"
                        id="filter_status"
                        label="Status"
                        type="text"
                        fullWidth
                        variant="outlined"
                        size="small"
                        value={isActive}
                        onChange={(event) => {
                            onFilterChanged('isActive', event.target.value);
                        }}
                    >
                        {isActive != null &&
                            <MenuItem value={null}>
                                Wyczyść
                            </MenuItem>
                        }
                        <MenuItem value={true}>
                            Aktywne
                        </MenuItem>
                        <MenuItem value={false}>
                            Nieaktywne
                        </MenuItem>
                </TextField>
            </Grid>
        </Grid>
    )
}

AttractionsTableFilter.propTypes = {
    search: PropTypes.string,
    categoryId: PropTypes.number,
    netPriceFrom: PropTypes.number,
    netPriceTo: PropTypes.number,
    grosPriceFrom: PropTypes.number,
    grosPriceTo: PropTypes.number,
    isActive: PropTypes.bool,
    categories: PropTypes.array.isRequired,
    onFilterChanged: PropTypes.func.isRequired
};

AttractionsTableFilter.defaultProps = {
    search: '',
    categoryId: null,
    netPriceFrom: null,
    netPriceTo: null,
    grosPriceFrom: null,
    grosPriceTo: null,
    isActive: null
}

export default AttractionsTableFilter;