import React, { Component } from 'react';
import {
    Button,
    Grid,
    TextField
} from '@mui/material';
import { apiConfig } from '../../../services/api/config/Config';
import styles from './Config.module.css';

export class Config extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: 0,
            receptionEmail: ''
        };

        this.timer = null;

        this.loadData();
    }

    loadData = () => {
        this.timer = setTimeout(async () => {
            let data = await apiConfig.get();

            this.setState({
                id: data.config.id,
                receptionEmail: data.config.receptionEmail
            });
        })
    }

    put = () => {
        this.timer = setTimeout(async () => {
            let data = await apiConfig.put(this.state);

            this.setState({
                id: data.config.id,
                receptionEmail: data.config.receptionEmail
            });
        })
    }

    render() {
        return (
            <div className={styles.AdminConfig}>
                <Grid container>
                    <Grid item md={3} className="modified-form">
                        <TextField
                            autoFocus
                            margin="dense"
                            id="reception-email"
                            label="Email recepcji"
                            type="text"
                            fullWidth
                            variant="filled"
                            value={this.state.receptionEmail}
                            onChange={(event) => {
                                this.setState({
                                    receptionEmail: event.target.value
                                });
                            }}
                        />
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item md={3} className="mt-3" style={{padding: '0 20px'}}>
                        <Button className={styles.SubmitButton} onClick={this.put}>
                            Zapisz
                        </Button>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default Config;