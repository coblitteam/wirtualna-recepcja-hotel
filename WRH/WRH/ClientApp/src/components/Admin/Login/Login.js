﻿import React, { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import { apiLogin } from '../../../services/api/users/Login';

import {
    Container,
    Grid,
    TextField
} from '@mui/material';
import styles from './Login.module.css';

const Login = (props) => {
    const { register, handleSubmit, setError, formState } = useForm();
    const [validationMessage, setValidationMessage] = useState('');

    const {
        onLoggedIn
    } = props;

    const onSubmit = async (data) => {
        let result = await apiLogin.post(data);

        console.log(result);

        if (result.error) {
            console.error(result.error);
        } else {
            localStorage.setItem('token', result.token);
            onLoggedIn();
        }
    };

    return (
        <Container component="main" maxWidth="sm" className={styles.LoginContainer}>
            <Grid container direction="column">
                <Grid>
                    <form onSubmit={handleSubmit(onSubmit)} className={styles.LoginForm}>
                        <div className="validation-error">{validationMessage}</div>
                        <Grid container spacing={3}>
                            <Grid item xs={12}>
                                <h1>Zaloguj się</h1>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField label="Login" required autoFocus fullWidth {...register("login", { required: true })} /><br />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField label="Hasło" required type="password" name="password" fullWidth {...register("password", { required: true })} /><br />
                            </Grid>
                            <Grid item xs={12}>
                                <button type="submit" className={styles.LoginButton}>Zaloguj</button>
                            </Grid>
                        </Grid>
                    </form>
                </Grid>
            </Grid>
        </Container>
    );
}

export default Login;