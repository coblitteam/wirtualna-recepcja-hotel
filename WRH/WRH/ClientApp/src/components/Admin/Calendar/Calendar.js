import React, { Component } from 'react';
import { format } from 'date-fns';

import {
    Button,
    Container,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Grid,
    MenuItem,
    TextField
} from '@mui/material';

import styles from './Calendar.module.css';
import { apiRoom } from '../../../services/api/rooms/Room';

export class Bookings extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedMonth: new Date().getMonth(),
            selectedYear: new Date().getFullYear(),
            rooms: [],
            room: null,
            roomTypes: [],
            dialogOpen: false
        }

        this.getRooms();
    }

    daysInMonth (month, year) {
        return new Date(year, month, 0).getDate();
    }

    daysColumns() {
        let days = [];

        for (let i = 1; i <= this.daysInMonth(this.state.selectedMonth + 1, this.state.selectedYear); i++) {
            days.push(<th width="40px">{i}</th>);
        }

        return days;
    }

    daysCells(room) {
        let days = [];

        if (!room)
            return days;

        for (let i = 1; i <= this.daysInMonth(this.state.selectedMonth + 1, this.state.selectedYear); i++) {
            var date = new Date(this.state.selectedYear, this.state.selectedMonth, i);

            if (room.busyDays.filter((day) => day == format(date, "dd.MM.yyyy")).length)
                days.push(<td className={(room.isActive) ? `cursor-pointer ${styles.Busy}` : `cursor-pointer ${styles.Inactive}`} onClick={() => { if (room.isActive) this.setBusy(room.id, `${this.state.selectedYear}-${this.state.selectedMonth+1}-${i}`); }}></td>);
            else
                days.push(<td className={(room.isActive) ? "cursor-pointer" : `cursor-pointer ${styles.Inactive}`} onClick={() => { if (room.isActive) this.setBusy(room.id, `${this.state.selectedYear}-${this.state.selectedMonth+1}-${i}`); }}></td>)
        }

        return days;
    }

    getRooms() {
        apiRoom.getCustom('/list').then((data) => {
            this.setState({
                rooms: data.items
            });
        })
    }

    getRoom(id) {
        apiRoom.getSingle(id).then((data) => {
            this.setState({
                room: data.room,
                roomTypes: data.types,
                dialogOpen: true
            });
        })
    }

    updateRoom(room) {
        apiRoom.put(room).then(() => {
            this.getRooms();

            this.setState({
                room: null,
                dialogOpen: false
            });
        })
    }

    setBusy(roomId, date) {
        apiRoom.postCustom('/busy', { roomId, date}).then(() => {
            this.getRooms();
        })
    }

    render() {
        return (
            <Container maxWidth="xl">
                <Grid container className="mt-5 mb-5" spacing={2}>
                    <Grid item md={8} />
                    <Grid item md={2} className="pl-1 pr-1">
                        <TextField
                            select
                            id="filter_status"
                            type="text"
                            fullWidth
                            variant="outlined"
                            size="small"
                            label="Miesiąc"
                            value={this.state.selectedMonth}
                            onChange={(event) => {
                                this.setState({
                                    selectedMonth: event.target.value
                                });
                            }}
                        >
                            <MenuItem value={0}>
                                Styczeń
                            </MenuItem>
                            <MenuItem value={1}>
                                Luty
                            </MenuItem>
                            <MenuItem value={2}>
                                Marzec
                            </MenuItem>
                            <MenuItem value={3}>
                                Kwiecień
                            </MenuItem>
                            <MenuItem value={4}>
                                Maj
                            </MenuItem>
                            <MenuItem value={5}>
                                Czerwiec
                            </MenuItem>
                            <MenuItem value={6}>
                                Lipiec
                            </MenuItem>
                            <MenuItem value={7}>
                                Sierpień
                            </MenuItem>
                            <MenuItem value={8}>
                                Wrzesień
                            </MenuItem>
                            <MenuItem value={9}>
                                Październik
                            </MenuItem>
                            <MenuItem value={10}>
                                Listopad
                            </MenuItem>
                            <MenuItem value={11}>
                                Grudzień
                            </MenuItem>
                        </TextField>
                    </Grid>
                    <Grid item md={2}>
                        <TextField
                            select
                            id="filter_status"
                            type="text"
                            fullWidth
                            variant="outlined"
                            size="small"
                            label="Rok"
                            value={this.state.selectedYear}
                            onChange={(event) => {
                                this.setState({
                                    selectedYear: event.target.value
                                });
                            }}
                        >
                            <MenuItem value={2022}>
                                2022
                            </MenuItem>
                            <MenuItem value={2023}>
                                2023
                            </MenuItem>
                        </TextField>
                    </Grid>
                </Grid>
                <Grid container justifyContent="center">
                    <table className={styles.Calendar}>
                        <thead>
                            <tr>
                                <th>Pokój</th>
                                <th>Nr</th>
                                {this.daysColumns()}
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.rooms.map((room, index) => {
                                return (
                                    <tr key={index}>
                                        <td className="font-weight-bold">{room.name}</td>
                                        <td className="font-weight-bold cursor-pointer" onClick={() => {
                                            this.getRoom(room.id);
                                        }}>
                                            {room.number}
                                        </td>
                                        {this.daysCells(room)}
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </Grid>
                <Grid container>
                    <Dialog maxWidth="sm" fullWidth open={this.state.dialogOpen} onClose={() => this.setState({dialogOpen: false, room: null})}>
                        <DialogTitle>
                            Edycja pokoju nr {this.state.room?.number}
                        </DialogTitle>
                        <DialogContent>
                            {this.state.room &&
                            <Grid container>
                                <Grid item md={6} className="modified-form">
                                    <TextField
                                        autoFocus
                                        margin="dense"
                                        id="room_number"
                                        label="Numer pokoju"
                                        type="number"
                                        fullWidth
                                        variant="filled"
                                        inputProps={{ maxLength: 5 }}
                                        value={this.state.room.number}
                                        onChange={(event) => {
                                            this.state.room.number = event.target.value;

                                            this.setState({
                                                room: this.state.room
                                            });
                                        }}
                                    />
                                </Grid>
                                <Grid item md={6} className="modified-form">
                                    <TextField
                                        autoFocus
                                        margin="dense"
                                        id="room_code"
                                        label="Kod pokoju"
                                        type="text"
                                        fullWidth
                                        variant="filled"
                                        inputProps={{ maxLength: 5 }}
                                        value={this.state.room.code}
                                        onChange={(event) => {
                                            this.state.room.code = event.target.value;

                                            this.setState({
                                                room: this.state.room
                                            });
                                        }}
                                    />
                                </Grid>
                                <Grid item md={12} className="modified-form">
                                    <TextField
                                        select
                                        margin="dense"
                                        id="room_type"
                                        label="Typ"
                                        type="text"
                                        fullWidth
                                        variant="filled"
                                        value={this.state.room.roomTypeId}
                                        onChange={(event) => {
                                            this.state.room.roomTypeId = event.target.value;

                                            this.setState({
                                                room: this.state.room
                                            });
                                        }}
                                    >
                                    {
                                        this.state.roomTypes.length &&
                                        this.state.roomTypes.map(function (type, i) {
                                            return <MenuItem key={i} value={type.id}>
                                                {type.name}
                                            </MenuItem>
                                        })
                                    }
                                    </TextField>
                                </Grid>
                                <Grid item md={6} className="modified-form">
                                    <TextField
                                        select
                                        margin="dense"
                                        id="room_active"
                                        label="Pokój aktywny"
                                        type="text"
                                        fullWidth
                                        variant="filled"
                                        value={this.state.room.isActive}
                                        onChange={(event) => {
                                            this.state.room.isActive = event.target.value;

                                            this.setState({
                                                room: this.state.room
                                            });
                                        }}
                                    >
                                        <MenuItem value={true}>
                                            Aktywny
                                        </MenuItem>
                                        <MenuItem value={false}>
                                            Zablokowany
                                        </MenuItem>
                                    </TextField>
                                </Grid>
                            </Grid>
                            }
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={() => this.setState({dialogOpen: false, room: null})}>Anuluj</Button>
                            <Button onClick={() => this.updateRoom(this.state.room)}>Zapisz</Button>
                        </DialogActions>
                    </Dialog>
                </Grid>
            </Container>
        )
    }
}

export default Bookings;