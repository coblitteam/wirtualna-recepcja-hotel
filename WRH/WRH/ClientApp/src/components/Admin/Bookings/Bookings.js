﻿import React, { Component } from 'react';

import {
    Button,
    Container,
    Grid,
    MenuItem,
    TextField
} from '@mui/material';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';

import BookingsTable from './BookingsTable';
import BookingsTableFilter from './BookingsTableFilter';

import styles from './Bookings.module.css';

import { apiBooking } from '../../../services/api/bookings/Booking';
import { apiRoom } from '../../../services/api/rooms/Room';
import ReservationForm from '../../ReservationForm/ReservationForm';

export class Bookings extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
            orderBy: 'createdAt',
            order: 'desc',
            perPage: 5,
            page: 0,
            size: 0,
            search: '',
            arrivalDateFrom: null,
            arrivalDateTo: null,
            departureDateFrom: null,
            departureDateTo: null,
            roomTypeSelected: null,
            roomTypes: [],
            priceFrom: null,
            priceTo: null,
            status: null,
            bookingId: 0,
            booking: null
        };

        this.timer = null

        this.loadData();
    }

    loadData = () => {
        let query = {
            orderBy: this.state.orderBy,
            orderDestination: this.state.order,
            start: this.state.page * this.state.perPage,
            size: this.state.perPage,
            search: this.state.search,
            arrivalDateFrom: this.state.arrivalDateFrom,
            arrivalDateTo: this.state.arrivalDateTo,
            roomType: this.state.roomTypeSelected,
            departureDateFrom: this.state.departureDateFrom,
            departureDateTo: this.state.departureDateTo,
            priceFrom: this.state.priceFrom,
            priceTo: this.state.priceTo,
            status: this.state.status,
        };

        this.timer = setTimeout(async () => {
            let data = await apiBooking.getList(query);
            let roomTypes = await apiRoom.getCustom('/types');

            this.setState({
                items: data.items,
                size: data.length,
                roomTypes: roomTypes
            });
        }, 500);
    }

    handleRequestSort = (event, property) => {
        this.state.orderBy = property;
        this.state.order = property == this.state.orderBy && this.state.order != 'desc' ? 'desc' : 'asc';

        this.setState({
            orderBy: this.state.orderBy,
            order: this.state.order
        });


        this.loadData();
    }

    handlePageChange = (event, newPage) => {
        this.state.page = newPage;

        this.setState({ page: this.state.page });

        this.loadData();
    }

    handleRowsPerPageChange = (event) => {
        this.state.perPage = parseInt(event.target.value, 10);
        this.state.page = 0;

        this.setState({
            perPage: this.state.perPage,
            page: this.state.page
        });

        this.loadData();
    }

    handleFilterChanged = (prop, value) => {
        switch (prop) {
            case 'search':
                this.state.search = value;
                this.setState({
                    search: this.state.search
                });
                break;
            case 'arrivalDateFrom':
                this.state.arrivalDateFrom = value;
                this.setState({
                    arrivalDateFrom: this.state.arrivalDateFrom
                });
                break;
            case 'arrivalDateTo':
                this.state.arrivalDateTo = value;
                this.setState({
                    arrivalDateTo: this.state.arrivalDateTo
                });
                break;
            case 'departureDateFrom':
                this.state.departureDateFrom = value;
                this.setState({
                    departureDateFrom: this.state.departureDateFrom
                });
                break;
            case 'departureDateTo':
                this.state.departureDateTo = value;
                this.setState({
                    departureDateTo: this.state.departureDateTo
                });
                break;
            case 'priceFrom':
                this.state.priceFrom = value;
                this.setState({
                    priceFrom: this.state.priceFrom
                });
                break;
            case 'priceTo':
                this.state.priceTo = value;
                this.setState({
                    priceTo: this.state.priceTo
                });
                break;
            case 'roomTypeSelected':
                this.state.roomTypeSelected = value;
                this.setState({
                    roomTypeSelected: this.state.roomTypeSelected
                });
            case 'status':
                this.state.status = value;
                this.setState({
                    status: this.state.status
                });
                break;
        }

        this.state.page = 0;
        this.setState({
            page: this.state.page
        });

        this.loadData();
    }

    handleFilterCleared = () => {
        this.state.search = '';
        this.state.arrivalDateFrom = '';
        this.state.arrivalDateTo = '';
        this.state.departureDateFrom = '';
        this.state.departureDateTo = '';
        this.state.roomTypeSelected = '';
        this.state.priceFrom = '';
        this.state.priceTo = '';
        this.state.status = '';
        this.state.roomTypes = '';

        this.setState(this.state);

        this.loadData();
    }

    handleShowRequest = (bookingId) => {
        apiBooking.getSingle(bookingId).then((booking) => {
            this.setState({
                bookingId: bookingId,
                booking: booking
            });
        });
    }

    render() {
        return (
            <div className={styles.AdminBookings}>
                {!this.state.bookingId &&
                    <div>
                        <BookingsTableFilter
                            search={this.state.search}
                            arrivalDateFrom={this.state.arrivalDateFrom}
                            arrivalDateTo={this.state.arrivalDateTo}
                            departureDateFrom={this.state.departureDateFrom}
                            departureDateTo={this.state.departureDateTo}
                            roomTypeSelected={this.state.roomTypeSelected}
                            priceFrom={this.state.priceFrom}
                            priceTo={this.state.priceTo}
                            status={this.state.status}
                            roomTypes={this.state.roomTypes}
                            onFilterChanged={this.handleFilterChanged}
                            onFilterCleared={this.handleFilterCleared}
                            onFormSaved={this.loadData}
                        />
                        <BookingsTable
                            data={this.state}
                            onRequestSort={this.handleRequestSort}
                            onPageChange={this.handlePageChange}
                            onRowsPerPageChange={this.handleRowsPerPageChange}
                            onShowRequest={this.handleShowRequest}
                            onFormSaved={this.loadData}
                        />
                    </div>
                }
                {this.state.bookingId > 0 &&
                    <Grid container className="mt-5">
                        <Grid item md={1} className="d-flex justify-content-center">
                            <ArrowBackIosNewIcon className={styles.BackButton} onClick={() => {
                                this.setState({
                                    bookingId: 0,
                                    booking: null
                                })
                            }} />
                        </Grid>
                        <Grid item md={10}>
                            <ReservationForm isAdmin data={this.state.booking} />
                        </Grid>
                        <Grid item md={1} />
                    </Grid>
                }
            </div>
        );
    };
}

export default Bookings;