import React from 'react';
import PropTypes from 'prop-types';
import {
    Button,
    Grid,
    InputAdornment,
    MenuItem,
    TextField
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';

import styles from './Bookings.module.css';
import plLocale from 'date-fns/locale/pl';

const BookingsTableFilter = (props) => {
    const {
        search,
        arrivalDateFrom,
        arrivalDateTo,
        departureDateFrom,
        departureDateTo,
        roomTypeSelected,
        priceFrom,
        priceTo,
        status,
        roomTypes,
        onFilterChanged,
        onFilterCleared,
        onFormSaved
    } = props;

    return (
        <Grid container className={styles.Filter}>
            <Grid item md={3} className="modified-form-filter">
                <Grid container>
                    <Grid item md={12} className={styles.Label}>
                        Szukaj
                    </Grid>
                    <Grid item md={12}>
                        <TextField
                            className={styles.SearchField}
                            autoFocus
                            id="filter_search"
                            type="text"
                            fullWidth
                            variant="outlined"
                            size="small"
                            value={search}
                            onChange={(event) => {
                                onFilterChanged('search', event.target.value);
                            }}
                            InputProps={{
                                startAdornment: <InputAdornment position="start">
                                                    <SearchIcon />
                                                </InputAdornment>,
                            }}
                        />
                    </Grid>
                </Grid>
            </Grid>
            <Grid item md={1} className="d-flex align-items-end">
                <Grid container>
                    <Grid item md={12} className="d-flex justify-content-center">
                        <Button className={styles.ClearButton} onClick={onFilterCleared}>
                            Wyczyść filtry
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item md={2} className="modified-form-filter with-side-padding">
                <Grid container>
                    <Grid item md={12} className={styles.Label}>
                        Rozpoczęcie pobytu
                    </Grid>
                    <Grid item md={6} className={styles.DateFields}>
                        <LocalizationProvider dateAdapter={AdapterDateFns} locale={plLocale}>
                            <DatePicker
                                clearable
                                clearText="Wyczyść"
                                renderInput={(props) => <TextField variant="outlined"
                                                                   size="small"
                                                                   fullWidth
                                                                   {...props}/>}
                                label="Od"
                                value={arrivalDateFrom}
                                mask="dd.MM.yyyy"
                                onChange={(newValue) => {
                                    onFilterChanged('arrivalDateFrom', newValue);
                                }}
                            />
                        </LocalizationProvider>
                    </Grid>
                    <Grid item md={6} className={styles.PriceFields}>
                        <LocalizationProvider dateAdapter={AdapterDateFns} locale={plLocale}>
                            <DatePicker
                                clearable
                                clearText="Wyczyść"
                                renderInput={(props) => <TextField variant="outlined"
                                                                   size="small"
                                                                   fullWidth
                                                                   {...props}/>}
                                label="Do"
                                value={arrivalDateTo}
                                mask="dd.MM.yyyy"
                                onChange={(newValue) => {
                                    onFilterChanged('arrivalDateTo', newValue);
                                }}
                            />
                        </LocalizationProvider>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item md={2} className="modified-form-filter with-side-padding">
            <Grid container>
                    <Grid item md={12} className={styles.Label}>
                        Zakończenie pobytu
                    </Grid>
                    <Grid item md={6} className={styles.PriceFields}>
                        <LocalizationProvider dateAdapter={AdapterDateFns} locale={plLocale}>
                            <DatePicker
                                clearable
                                clearText="Wyczyść"
                                renderInput={(props) => <TextField variant="outlined"
                                                                   size="small"
                                                                   fullWidth
                                                                   {...props}/>}
                                label="Od"
                                value={departureDateFrom}
                                mask="dd.MM.yyyy"
                                onChange={(newValue) => {
                                    onFilterChanged('departureDateFrom', newValue);
                                }}
                            />
                        </LocalizationProvider>
                    </Grid>
                    <Grid item md={6} className={styles.PriceFields}>
                        <LocalizationProvider dateAdapter={AdapterDateFns} locale={plLocale}>
                            <DatePicker
                                clearable
                                clearText="Wyczyść"
                                renderInput={(props) => <TextField variant="outlined"
                                                                   size="small"
                                                                   fullWidth
                                                                   {...props}/>}
                                label="Do"
                                value={departureDateTo}
                                mask="dd.MM.yyyy"
                                onChange={(newValue) => {
                                    onFilterChanged('departureDateTo', newValue);
                                }}
                            />
                        </LocalizationProvider>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item md={1} className="modified-form-filter with-side-padding">
                <Grid container>
                    <Grid item md={12} className={styles.Label}>
                        Pokoje
                    </Grid>
                    <Grid item md={12}>
                        <TextField
                            select
                            id="filter_status"
                            type="text"
                            fullWidth
                            variant="outlined"
                            size="small"
                            value={roomTypeSelected}
                            onChange={(event) => {
                                onFilterChanged('roomTypeSelected', event.target.value);
                            }}
                        >
                            {roomTypeSelected != null &&
                                <MenuItem value={null}>
                                    Wyczyść
                                </MenuItem>
                            }
                            {roomTypes.types &&
                                roomTypes.types.map((roomType, index) => {
                                    return (
                                        <MenuItem key={index} value={roomType.id}>
                                            {roomType.name}
                                        </MenuItem>
                                    );
                                })
                            }
                        </TextField>
                    </Grid>
                </Grid>


            </Grid>
            <Grid item md={2} className="modified-form-filter with-side-padding">
                <Grid container>
                    <Grid item md={12} className={styles.Label}>
                        Do zapłaty
                    </Grid>
                    <Grid item md={6} className={styles.PriceFields}>
                        <TextField
                            id="filter_priceFrom"
                            label="Od"
                            type="number"
                            fullWidth
                            variant="outlined"
                            size="small"
                            value={priceFrom}
                            onChange={(event) => {
                                onFilterChanged('priceFrom', event.target.value);
                            }}
                            InputProps={{
                                endAdornment: <InputAdornment position="end">
                                                zł
                                            </InputAdornment>,
                            }}
                        />
                    </Grid>
                    <Grid item md={6} className={styles.PriceFields}>
                        <TextField
                            id="filter_priceTo"
                            label="Do"
                            type="number"
                            fullWidth
                            variant="outlined"
                            size="small"
                            value={priceTo}
                            onChange={(event) => {
                                onFilterChanged('priceTo', event.target.value);
                            }}
                            InputProps={{
                                endAdornment: <InputAdornment position="end">
                                                zł
                                            </InputAdornment>,
                            }}
                        />
                    </Grid>
                </Grid>
            </Grid>
            <Grid item md={1} className="modified-form-filter with-side-padding">
                <Grid container>
                    <Grid item md={12} className={styles.Label}>
                        Status
                    </Grid>
                    <Grid item md={12}>
                        <TextField
                            select
                            id="filter_status"
                            type="text"
                            fullWidth
                            variant="outlined"
                            size="small"
                            value={status}
                            onChange={(event) => {
                                onFilterChanged('status', event.target.value);
                            }}
                        >
                            {status != null &&
                                <MenuItem value={null}>
                                    Wyczyść
                                </MenuItem>
                            }
                            <MenuItem value={0}>
                                Nierozpoczęta
                            </MenuItem>
                            <MenuItem value={1}>
                                W trakcie
                            </MenuItem>
                            <MenuItem value={2}>
                                Nierozliczona
                            </MenuItem>
                            <MenuItem value={3}>
                                Zakończona
                            </MenuItem>
                            <MenuItem value={4}>
                                Wymeldowano
                            </MenuItem>
                        </TextField>
                    </Grid>
                </Grid>

            </Grid>
        </Grid>
    )
}

BookingsTableFilter.propTypes = {
    search: PropTypes.string,
    categoryId: PropTypes.number,
    isActive: PropTypes.bool,
    onFilterChanged: PropTypes.func.isRequired
};

BookingsTableFilter.defaultProps = {
    search: '',
    categoryId: null,
    isActive: null
}

export default BookingsTableFilter;