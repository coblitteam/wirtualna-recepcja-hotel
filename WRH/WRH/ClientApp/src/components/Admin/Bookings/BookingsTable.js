﻿import React from 'react';
import PropTypes from 'prop-types';
import { format } from 'date-fns';

import {
    Grid,
    Table,
    TableContainer,
    TableHead,
    TableRow,
    TableCell,
    TableSortLabel,
    TableBody,
    TablePagination
} from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import VisibilityIcon from '@mui/icons-material/Visibility';

import styles from './Bookings.module.css';
import { apiBooking } from '../../../services/api/bookings/Booking';

const headCells = [
    {
        id: 'bookingNumber',
        numeric: false,
        label: 'Numer rezerwacji',
        sortEnabled: true
    },
    {
        id: 'stayNumber',
        numeric: false,
        label: 'Numer pobytu',
        sortEnabled: true
    },
    {
        id: 'createdAt',
        numeric: false,
        label: 'Data utworzenia',
        sortEnabled: true
    },
    {
        id: 'arrivalDate',
        numeric: false,
        label: 'Rozpoczęcie pobytu',
        sortEnabled: true
    },
    {
        id: 'departureDate',
        numeric: false,
        label: 'Zakończenie pobytu',
        sortEnabled: true
    },
    {
        id: 'client',
        numeric: false,
        label: 'Klient',
        sortEnabled: true
    },
    {
        id: 'rooms',
        numeric: false,
        label: 'Pokoje',
        sortEnabled: true
    },
    {
        id: 'price',
        numeric: true,
        label: 'Do zapłaty',
        sortEnabled: true
    },
    {
        id: 'status',
        numeric: true,
        label: 'Status',
        sortEnabled: false
    }
]

const BookingsTable = (props) => {
    const {
        data,
        onRequestSort,
        onPageChange,
        onRowsPerPageChange,
        onFormSaved,
        onShowRequest
    } = props;

    const sortHandler = (property) => (event) => {
        onRequestSort(event, property);
    };

    const labelDisplayedRows = ({ from, to, count }) => {
        return `${from}–${to} z ${count !== -1 ? count : `więcej niż ${to}`}`;
    }

    const show = (id) => {
        onShowRequest(id);
    }

    const remove = (id) => {
        apiBooking.remove(id).then(onFormSaved);
    }

    return (
        <Grid container className={styles.AdminBookingsTable}>
            <TableContainer className={styles.Container}>
                <Table>
                    <TableHead className={styles.TableHead}>
                        <TableRow>
                        {headCells.map((headCell, index) => {
                            if (headCell.sortEnabled)
                                return (
                                    <TableCell
                                        key={headCell.id}
                                        align="left"
                                        padding="none"
                                        sortDirection={data.orderBy === headCell.id ? data.order : false}
                                        className={
                                            (index === 0) ? "table-head-cell first" :
                                            (index === headCells.length - 1) ? 'table-head-cell last' :
                                                    'table-head-cell'
                                        }
                                    >
                                        <TableSortLabel
                                            active={data.orderBy === headCell.id}
                                            direction={data.orderBy === headCell.id ? data.order : 'asc'}
                                            onClick={sortHandler(headCell.id)}
                                        >
                                            {headCell.label}
                                        </TableSortLabel>
                                    </TableCell>
                                );
                            else
                                return (
                                    <TableCell
                                        key={headCell.id}
                                        align="left"
                                        padding="none"
                                        className={
                                            (index === 0) ? "table-head-cell first" :
                                            (index === headCells.length - 1) ? 'table-head-cell last' :
                                                    'table-head-cell'
                                        }
                                    >
                                        {headCell.label}
                                    </TableCell>
                                );
                        })}
                        <TableCell
                            padding="none"
                            align="right"
                        />
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.items.map((booking) => {
                            return (
                                <TableRow key={booking.id} className={styles.TableRow}>
                                    <TableCell align="left" padding="none" className={styles.ValueCell}>
                                        {booking.bookingNumber}
                                    </TableCell>
                                    <TableCell align="left" padding="none" className={styles.ValueCell}>
                                        {booking.stayNumber}
                                    </TableCell>
                                    <TableCell align="left" padding="none" className={styles.ValueCell}>
                                        {
                                            format(new Date(booking.createdAt), "dd.MM.yyyy")
                                        }
                                    </TableCell>
                                    <TableCell align="left" padding="none" className={styles.ValueCell}>
                                        {
                                            format(new Date(booking.arrivalDate), "dd.MM.yyyy")
                                        }
                                    </TableCell>
                                    <TableCell align="left" padding="none" className={styles.ValueCell}>
                                        {   (booking.departureDate) ?
                                            format(new Date(booking.departureDate), "dd.MM.yyyy") : ""
                                        }
                                    </TableCell>
                                    <TableCell align="left" padding="none" className={styles.ValueCell}>
                                        {booking.client}
                                    </TableCell>
                                    <TableCell align="left" padding="none" className={styles.ValueCell}>
                                        {booking.rooms}
                                    </TableCell>
                                    <TableCell align="left" padding="none" className={styles.ValueCell}>
                                        {booking.toPay}zł
                                    </TableCell>
                                    <TableCell align="left" padding="none" className={styles.ValueCell}>
                                        {booking.status}
                                    </TableCell>
                                    <TableCell align="left" padding="none" className={`${styles.ValueCell} ${styles.CrudCell}`}>
                                        <div className="d-flex">
                                            <VisibilityIcon onClick={() => {
                                                onShowRequest(booking.id);
                                            }}/>
                                            <DeleteIcon onClick={() => {
                                                remove(booking.id);
                                            }}/>
                                        </div>
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                            className={styles.Pagination}
                            rowsPerPageOptions={[5, 10, 15]}
                            component="div"
                            count={data.size}
                            rowsPerPage={data.perPage}
                            page={data.page}
                            onPageChange={onPageChange}
                            onRowsPerPageChange={onRowsPerPageChange}
                            labelDisplayedRows={labelDisplayedRows}
                            labelRowsPerPage={"Wierszy na stronę:"}
                        />
        </Grid>
    );
}

BookingsTable.propTypes = {
    data: PropTypes.object.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onPageChange: PropTypes.func.isRequired,
    onRowsPerPageChange: PropTypes.func.isRequired,
    onShowRequest: PropTypes.func.isRequired
};

BookingsTable.defaultProps = {
    data: {
        items: [],
        orderBy: 'name',
        order: 'asc',
        rowsCount: 0,
        perPage: 5,
        page: 0
    }
};

export default BookingsTable;