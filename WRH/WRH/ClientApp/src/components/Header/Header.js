import React from 'react';
import PropTypes from 'prop-types';

import {
    Grid
} from '@mui/material';
import LanguageSelect from '../LanguageSelect/LanguageSelect';
import { Link } from 'react-router-dom';

import { useHistory } from "react-router-dom";
import styles from './Header.module.css';
import { Home } from '../../images/icons';
import ArrowBackOutlinedIcon from '@mui/icons-material/ArrowBackOutlined';

const Header = (props) => {
    const {
        title,
        showBackButton,
        onBackClick
    } = props;

    let history = useHistory();

    return (
        <div className={styles.HeaderContainer}>
            <Grid className={styles.Header}>
                <div className={styles.Navigation}>
                    <Link to="/">
                        <img src={Home} alt="" />
                    </Link>
                    {showBackButton &&
                        <ArrowBackOutlinedIcon sx={{
                            color: '#715D53',
                            fontSize: '2.5rem',
                            cursor: 'pointer',
                            marginLeft: '10px'
                        }}
                        onClick={() => {
                            if (onBackClick)
                                onBackClick()
                            else
                                history.goBack();
                        }}
                        />
                    }
                </div>
                <h1>
                    {title}
                </h1>
                <LanguageSelect />
            </Grid>
        </div>
    );
}

Header.propTypes = {
    title: PropTypes.string.isRequired,
    showBackButton: PropTypes.bool,
    onBackClick: PropTypes.func
};

Header.defaultProps = {};

export default Header;
