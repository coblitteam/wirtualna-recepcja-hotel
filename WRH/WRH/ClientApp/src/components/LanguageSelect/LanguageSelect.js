import React from 'react';
import { useTranslation } from 'react-i18next';

import {
    Grid
} from '@mui/material';

import { PL, EN, DE, ES, FR } from '../../images/locales';
import styles from './LanguageSelect.module.css';

const languages = {
    pl: { icon: PL },
    en: { icon: EN },
    de: { icon: DE },
    es: { icon: ES },
    fr: { icon: FR }
};

function LanguageSelect() {
    const { i18n } = useTranslation();

    return (
        <Grid className={styles.Languages}>
            {Object.keys(languages)?.map(item => (
                <img
                    key={item}
                    src={languages[item].icon}
                    alt=""
                    className={`${i18n.language !== item ? "" : styles.selected}`}
                    onClick={() => {
                        i18n.changeLanguage(item);
                    }}
                />
            ))}
        </Grid>
    );
}

export default LanguageSelect;
