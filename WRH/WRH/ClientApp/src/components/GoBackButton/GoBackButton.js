﻿import React from 'react'
import PropTypes from 'prop-types';

import ArrowBackOutlinedIcon from '@mui/icons-material/ArrowBackOutlined';

import { useHistory } from "react-router-dom";


const GoBackButton = (props) => {
    const {
        color,
        fontSize,
        marginLeft,
        marginRight,
        marginTop,
        marginBottom,
        onBackClick,
    } = props;
    let history = useHistory();

    return (
        <ArrowBackOutlinedIcon sx={props}
            onClick={(onBackClick) ? onBackClick : history.goBack}
        />
    );
}

GoBackButton.propTypes = {
    color: PropTypes.string,
    fontSize: PropTypes.string,
    marginLeft: PropTypes.string,
    marginRight: PropTypes.string,
    marginTop: PropTypes.string,
    marginBottom: PropTypes.string,
    onBackClick: PropTypes.func
}

GoBackButton.defaultProps = {
    color: '#715D53',
    fontSize: '2.5rem',
    marginLeft: '10px'
}

export default GoBackButton;