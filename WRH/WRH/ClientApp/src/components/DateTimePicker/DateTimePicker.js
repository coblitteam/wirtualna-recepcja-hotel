import * as React from 'react';
import PropTypes from 'prop-types';

import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';

import { useTranslation } from 'react-i18next';

import plLocale from 'date-fns/locale/pl';
import deLocale from 'date-fns/locale/de';
import enLocale from 'date-fns/locale/en-GB';
import frLocale from 'date-fns/locale/fr';
import esLocale from 'date-fns/locale/es';

const localeMap = {
    en: enLocale,
    fr: frLocale,
    pl: plLocale,
    de: deLocale,
    es: esLocale
};

const maskMap = {
    fr: '__/__/____',
    en: '__/__/____',
    pl: '__.__.____',
    de: '__.__.____',
    es: '__.__.____'
};

function BasicDateTimePicker(props) {
    const {
        disabled,
        required,
        label,
        helperText,
        date,
        onDateChange,
        checkMinDate
    } = props;
    const { t, i18n } = useTranslation();

    return (
        <LocalizationProvider dateAdapter={AdapterDateFns} locale={localeMap[i18n.language]}>
            <DatePicker
                disabled={disabled}
                minDate={checkMinDate ? new Date() : undefined}
                renderInput={(props) => <TextField required={required} variant="filled" {...props} helperText={t(helperText)}/>}
                label={t(label)}
                value={date}
                mask={maskMap[i18n.language]}
                onChange={(newValue) => {
                    onDateChange(newValue);
                }}
            />
        </LocalizationProvider>
    );
}

BasicDateTimePicker.propTypes = {
    label: PropTypes.string.isRequired,
    helperText: PropTypes.string,
    date: PropTypes.instanceOf(Date),
    onDateChange: PropTypes.func.isRequired,
    checkMinDate: PropTypes.bool.isRequired
};

export default BasicDateTimePicker;