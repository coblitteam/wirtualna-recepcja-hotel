﻿import React from 'react';
import styles from './AttractionPicker.module.css';

import AttractionPicker from './AttractionPicker';

import Carousel, { slidesToShowPlugin } from '@brainhubeu/react-carousel';
import { Route, useHistory } from "react-router-dom";
import { useEffect } from 'react';

function CarouselComponent(props) {
    const {
        attractions,
        onSelectAttraction
    } = props;
    const [didMount, setDidMount] = React.useState(false);
    const history = useHistory();

    useEffect(() => {
        setDidMount(true);
        return () => setDidMount(false);
    }, [])

    if (!didMount) {
        return null;
    }

    return (
        <Carousel
            className={styles.Carousel}
            plugins={[
                'arrows',
                {
                    resolve: slidesToShowPlugin,
                    options: {
                        numberOfSlides: window.innerWidth <= 1340 ? 2 : 4
                    }
                },
            ]}
        >
            {
                attractions.map((attraction, index) => {
                    return (
                        <AttractionPicker
                            key={attraction.id}
                            attraction={attraction}
                            onSelectHandler={(quantity) => {
                                if (onSelectAttraction)
                                    onSelectAttraction(attraction);
                                else
                                    history.push(`/attraction/${attraction.id}`);
                            }}
                        />
                    )
                })
            }
        </Carousel>
    );
}

export default CarouselComponent;