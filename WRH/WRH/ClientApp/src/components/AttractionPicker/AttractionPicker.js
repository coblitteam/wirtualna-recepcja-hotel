import React from 'react';
import styles from './AttractionPicker.module.css';

import {
    Paper,
} from '@mui/material';

import { useTranslation } from 'react-i18next';

const AttractionPicker = (props, context) => {
    const {
        attraction,
        onSelectHandler
    } = props;

    const { t, i18n } = useTranslation();

    return (
        <Paper className={styles.AttractionPicker}>
            <img src={attraction.photo} alt="" />
            <div className={styles.Body}>
                <h1 className={styles.Header}>
                    {attraction.nameTranslations[i18n.language]}
                </h1>
                <span className={styles.Description}>
                    {attraction.shortDetailsTranslations[i18n.language]}
                </span>
                <div className={styles.Chooser}>
                    <span className={styles.Price}>
                        {t('common.currency_price', { val: attraction.grosPrice })}
                    </span>
                </div>
                <a
                    href
                    className={styles.Button}
                    onClick={() => onSelectHandler(attraction.id)}
                >
                    {t('button.choose')}
                </a>
            </div>
        </Paper>
    );
}

AttractionPicker.propTypes = {};

AttractionPicker.defaultProps = {};

export default AttractionPicker;
