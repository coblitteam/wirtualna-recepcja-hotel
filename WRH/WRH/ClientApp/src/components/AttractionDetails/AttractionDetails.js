import React, { useState, useEffect } from 'react';
import styles from './AttractionDetails.module.css';
import AWS from 'aws-sdk'
import { useTranslation } from 'react-i18next';
import PlayCircleIcon from '@mui/icons-material/PlayCircle';
import PauseCircleIcon from '@mui/icons-material/PauseCircle';

const Polly = new AWS.Polly({
    credentials: {
        accessKeyId: 'AKIARIPJ3CEFTR32YSUO',
        secretAccessKey: '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'
    },
    signatureVersion: 'v4',
    region: 'eu-central-1'
});

const speakers = {
    pl: 'Ewa',
    en: 'Emma',
    de: 'Vicki',
    es: 'Lucia',
    fr: 'Lea'
}

const AttractionDetails = (props) => {
    const {
        attraction
    } = props;
    const { t, i18n } = useTranslation();
    const [audio, setAudio] = useState(new Audio());
    const [playing, setPlaying] = useState(false);
    const [playerInitializing, setPlayerInitializing] = useState(false);

    useEffect(async () => {
        pause();
        setPlayerInitializing(true);
        let params = {
            'Text': attraction.detailsTranslations[i18n.language],
            'OutputFormat': 'mp3',
            'VoiceId': speakers[i18n.language]
        };

        Polly.synthesizeSpeech(params, (err, data) => {
            if (err) {
                console.error('error', err);
            } else if (data) {
                var blob = new Blob([data.AudioStream.buffer]);
                var url = URL.createObjectURL(blob);
                setAudio(new Audio(url));
                setPlayerInitializing(false);
            }
        })
    }, [i18n.language]);

    const play = () => {
        audio.play();
        setPlaying(true);
    }

    const pause = () => {
        audio.pause();
        setPlaying(false);
    }

    return (
        <div className={styles.Attraction}>
            <img src={attraction.photo} alt="" />
            <p>
                <button className={styles.SpeakerButton} onClick={!playing ? play : pause} disabled={playerInitializing}>
                    {!playing ? <><PlayCircleIcon /> {t('button.read')}</> : <><PauseCircleIcon /> {t('button.pause')}</>}
                </button>
                <br />
                {attraction.detailsTranslations[i18n.language]}
            </p>
            <span className={styles.Price}>
                Netto: {t('common.currency_price', { val: Math.floor(attraction.netPrice) })}
            </span>
            <br />
            <span className={styles.Price}>
                Brutto: {t('common.currency_price', { val: Math.floor(attraction.grosPrice) })}
            </span>
            <br />
            <span className={styles.Price}>
                VAT: {attraction.vat}%
            </span>
            <hr className="mt-5 mb-5" />
        </div>
    );
}

AttractionDetails.propTypes = {
};

AttractionDetails.defaultProps = {};

export default AttractionDetails;
