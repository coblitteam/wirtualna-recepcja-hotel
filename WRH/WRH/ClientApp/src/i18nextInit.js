import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import translationPL from "./locales/pl/translation.json";
import translationDE from "./locales/de/translation.json";
import translationEN from "./locales/en/translation.json";
import translationFR from "./locales/fr/translation.json";
import translationES from "./locales/es/translation.json";

const availableLanguages = ['pl', 'en', 'fr', 'es', 'de'];

const resources = {
    pl: {
        translation: translationPL
    },
    de: {
        translation: translationDE
    },
    en: {
        translation: translationEN
    },
    es: {
        translation: translationES
    },
    fr: {
        translation: translationFR
    }
};

i18n
    .use(initReactI18next)
    .init({
        resources,
        //lng: "pl",
        fallbackLng: 'pl',
        detection: {
            checkWhitelist: true
        },
        debug: false,
        whitelist: availableLanguages,
        interpolation: {
            escapeValue: false
        }
    });

export default i18n;
