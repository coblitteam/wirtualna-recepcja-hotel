using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using WRH.Application.Initializers;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;
var configuration = builder.Configuration;

services.InitDatabase(builder.Configuration.GetConnectionString("DefaultConnection"));
services.InitApplication();
services.InitSwagger();

services.InitAuthenticationDI(configuration["JWTIssuer"], configuration["JWTIssuer"], configuration["JWTKey"]);
services.InitContextDI();
services.InitRepositoriesDI();
services.InitServicesDI();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
else
{
    app.UseHsts();
}

app.UseStaticFiles();
app.UseSpaStaticFiles();
app.UseAuthentication();
app.UseRouting();
app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(
        name: "default",
        pattern: "{controller}/{action=Index}/{id?}");
});

app.UseSpa(spa =>
{
    spa.Options.SourcePath = "ClientApp";

    if (app.Environment.IsDevelopment())
    {
        spa.UseReactDevelopmentServer(npmScript: "start");
    }
});

app.Run();
