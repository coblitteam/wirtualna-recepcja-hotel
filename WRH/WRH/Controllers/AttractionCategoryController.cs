﻿using Microsoft.AspNetCore.Mvc;
using WRH.Application.Services.Interfaces;
using WRH.Domain.Transfer.AttractionCategory;

namespace WRH.Controllers
{
    [ApiController]
    [Route("api/attraction/category")]
    public class AttractionCategoryController : ControllerBase
    {
        private readonly IAttractionCategoryService _attractionCategoryService;

        public AttractionCategoryController(IAttractionCategoryService attractionCategoryService)
        {
            _attractionCategoryService = attractionCategoryService;
        }

        [HttpGet("list/active")]
        public async Task<IActionResult> GetActives()
        {
            var categories = await _attractionCategoryService.ActiveList();

            return Ok(new
            {
                categories = categories.Select(c => new
                {
                    c.Id,
                    c.Name,
                    c.IsBoard,
                    c.CanDelete,
                    c.Photo
                })
            });
        }

        [HttpGet("list")]
        public async Task<IActionResult> Get()
        {
            var categories = await _attractionCategoryService.AllList();

            return Ok(new
            {
                categories = categories.Select(c => new
                {
                    c.Id,
                    c.Name,
                    c.IsActive,
                    c.IsBoard,
                    c.CanDelete,
                    c.Photo
                })
            });
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AttractionCategoryTransfer transfer)
        {
            var model = await _attractionCategoryService.Create(transfer);

            return Ok(new
            {
                category = new
                {
                    model.Id,
                    model.Name,
                    model.IsActive,
                    model.CanDelete
                }
            });
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] AttractionCategoryTransfer transfer)
        {
            var model = await _attractionCategoryService.Update(transfer);

            return Ok(new
            {
                category = new
                {
                    model.Id,
                    model.Name,
                    model.IsActive,
                    model.CanDelete
                }
            });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _attractionCategoryService.Delete(id);

            return Ok();
        }
    }
}
