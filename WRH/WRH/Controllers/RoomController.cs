﻿using Microsoft.AspNetCore.Mvc;
using WRH.Application.Services.Interfaces;
using WRH.Domain.Transfer.Room;
using WRH.Domain.Transfer.RoomBusy;
using WRH.Domain.Transfer.RoomType;

namespace WRH.Controllers
{
    [ApiController]
    [Route("api/room")]
    public class RoomController : ControllerBase
    {
        private readonly IRoomTypeService _typeService;
        private readonly IRoomService _roomService;

        public RoomController(IRoomTypeService typeService, IRoomService roomService)
        {
            _typeService = typeService;
            _roomService = roomService;
        }

        [HttpGet("list")]
        public async Task<IActionResult> List()
        {
            var rooms = await _roomService.GetAllAsync();

            return Ok(new
            {
                items = rooms.OrderBy(e => e.Number).Select(x => new
                {
                    x.Id,
                    x.Number,
                    x.Type.Name,
                    nameTranslations = new
                    {
                        pl = x.Type.Name,
                        en = (!string.IsNullOrEmpty(x.Type.Name_En)) ? x.Type.Name_En : x.Type.Name,
                        de = (!string.IsNullOrEmpty(x.Type.Name_De)) ? x.Type.Name_De : x.Type.Name,
                        es = (!string.IsNullOrEmpty(x.Type.Name_Es)) ? x.Type.Name_Es : x.Type.Name,
                        fr = (!string.IsNullOrEmpty(x.Type.Name_Fr)) ? x.Type.Name_Fr : x.Type.Name,
                    },
                    x.BusyDays,
                    x.IsActive
                })
            });
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            var room = await _roomService.GetByIdAsync(id);
            var types = await _typeService.GetAllAsync();

            return Ok(new
            {
                room = new
                {
                    room.Id,
                    room.RoomTypeId,
                    room.Number,
                    room.Code,
                    room.IsActive
                },
                types = types.Select(x => new
                {
                    x.Id,
                    x.Name
                })
            });
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] RoomTransferModel model)
        {
            await _roomService.Update(model);

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllTypes()
        {
            var rooms = await _typeService.GetAllAsync();

            return Ok(new
            {
                items = rooms.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.NameTranslations,
                    x.DetailsTranslations,
                    x.ShortDetails,
                    x.ShortDetailsTranslations,
                    x.Capacity,
                    x.ExtraBeds,
                    x.Price,
                    x.PriceOnce,
                    x.BreakfastPrice,
                    x.FullBoardPrice
                })
            });
        }

        [HttpGet("types")]
        public async Task<IActionResult> GetTypes()
        {
            var rooms = await _typeService.GetAllAsync();

            return Ok(new
            {
                types = rooms.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.NameTranslations,
                })
            });
        }

        [HttpPost("busy")]
        public async Task<IActionResult> SetBusy([FromBody] RoomBusySetModel model)
        {
            await _roomService.SetBusy(model);

            return Ok();
        }

        [HttpGet("type/{id}")]
        public async Task<IActionResult> GetType([FromRoute] int id)
        {
            var type = await _typeService.GetAsync(id);

            return Ok(new
            {
                type = new
                {
                    type.Id,
                    type.Name,
                    type.NameTranslations,
                    type.ShortDetails,
                    type.ShortDetailsTranslations,
                    type.Details,
                    type.DetailsTranslations,
                    type.Capacity,
                    type.ExtraBeds,
                    type.Price,
                    type.PriceOnce,
                    type.BreakfastPrice,
                    type.FullBoardPrice,
                    type.Photo
                }
            });
        }

        [HttpPut("type")]
        public async Task<IActionResult> PutType([FromForm] RoomTypeTransferModel model)
        {
            await _typeService.Update(model, Directory.GetCurrentDirectory());

            return Ok();
        }

        [HttpPost("match")]
        public async Task<IActionResult> RoomMatch([FromBody] RoomMatchQueryModel query)
        {
            await _roomService.RemoveBusyPendingExpired();
            var rooms = await _roomService.GetMatchRooms(query);

            return Ok(new
            {
                types = rooms.GroupBy(e => e.RoomTypeId).Select(e => new
                {
                    roomId = e.FirstOrDefault()?.RoomTypeId,
                    e.FirstOrDefault()?.Type.Name,
                    e.FirstOrDefault()?.Type.NameTranslations,
                    e.FirstOrDefault()?.Type.ShortDetails,
                    e.FirstOrDefault()?.Type.ShortDetailsTranslations,
                    e.FirstOrDefault()?.Type.Capacity,
                    e.FirstOrDefault()?.Type.ExtraBeds,
                    e.FirstOrDefault()?.Type.Photo,
                    e.FirstOrDefault()?.Type.Price,
                    e.FirstOrDefault()?.Type.PriceOnce,
                    count = e.Count()
                })
            });
        }
    }
}
