﻿using Microsoft.AspNetCore.Mvc;
using WRH.Application.Services.Interfaces;
using WRH.Domain.Transfer.NewsCategory;

namespace WRH.Controllers
{
    [ApiController]
    [Route("api/news/category")]
    public class NewsCategoryController : ControllerBase
    {
        private readonly INewsCategoryService _newsCategoryService;

        public NewsCategoryController(INewsCategoryService newsCategoryService)
        {
            _newsCategoryService = newsCategoryService;
        }

        [HttpGet("list/active")]
        public async Task<IActionResult> GetActives()
        {
            var categories = await _newsCategoryService.ActiveList();

            return Ok(new
            {
                categories = categories.Select(c => new
                {
                    id = c.Id,
                    name = c.Name,
                    photo = c.Photo
                })
            });
        }

        [HttpGet("list")]
        public async Task<IActionResult> Get()
        {
            var categories = await _newsCategoryService.AllList();

            return Ok(new
            {
                categories = categories.Select(c => new
                {
                    id = c.Id,
                    name = c.Name,
                    isActive = c.IsActive,
                    photo = c.Photo
                })
            });
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] NewsCategoryTransfer transfer)
        {
            var model = await _newsCategoryService.Create(transfer);

            return Ok(new
            {
                category = new
                {
                    id = model.Id,
                    name = model.Name,
                    isActive = model.IsActive
                }
            });
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] NewsCategoryTransfer transfer)
        {
            var model = await _newsCategoryService.Update(transfer);

            return Ok(new
            {
                category = new
                {
                    id = model.Id,
                    name = model.Name,
                    isActive = model.IsActive
                }
            });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _newsCategoryService.Delete(id);

            return Ok();
        }
    }
}
