﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using WRH.Application.Services.Interfaces;
using WRH.Domain.Transfer.User;

namespace WRH.Controllers
{
    [ApiController]
    [Route("api/login")]
    public class LoginController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IConfiguration _configuration;

        public LoginController(IUserService userService, IConfiguration configuration)
        {
            _userService = userService;
            _configuration = configuration;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] UserLoginModel model)
        {
            var user = await _userService.GetByLoginAsync(model.Login);

            if (user != null && user.Password != null && user.Password == CommonComponent.Security.PasswordEncoding.Hash(model.Password))
            {
                var key = _configuration["JWTKey"];
                var issuer = _configuration["JWTIssuer"];

                var securityKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(key));
                var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

                var permClaims = new List<Claim>();
                permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
                permClaims.Add(new Claim(ClaimTypes.Name, user.Id.ToString()));
                permClaims.Add(new Claim("Id", user.Id.ToString()));

                var token = new JwtSecurityToken(
                        issuer,
                        issuer,
                        permClaims,
                        expires: DateTime.Now.AddDays(1),
                        signingCredentials: credentials);

                var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);

                return Ok(new
                {
                    token = jwt_token
                });
            }

            return Ok(new
            {
                error = "Niepoprawny dane logowania lub konto nieaktywne."
            });
        }
    }
}
