﻿using Microsoft.AspNetCore.Mvc;
using WRH.Application.Services.Interfaces;
using WRH.Domain.Transfer.News;

namespace WRH.Controllers
{
    [ApiController]
    [Route("api/news")]
    public class NewsController : ControllerBase
    {
        private readonly INewsService _newsService;

        public NewsController(INewsService newsService)
        {
            _newsService = newsService;
        }

        [HttpPost("list")]
        public async Task<IActionResult> List([FromBody] NewsQuery query)
        {
            var attractions = await _newsService.GetNewsList(query);

            return Ok(new
            {
                items = attractions.Select(e => new
                {
                    id = e.Id,
                    name = e.Name,
                    nameTranslations = new
                    {
                        pl = e.Name,
                        en = (!string.IsNullOrEmpty(e.Name_En)) ? e.Name_En : e.Name,
                        de = (!string.IsNullOrEmpty(e.Name_De)) ? e.Name_De : e.Name,
                        es = (!string.IsNullOrEmpty(e.Name_Es)) ? e.Name_Es : e.Name,
                        fr = (!string.IsNullOrEmpty(e.Name_Fr)) ? e.Name_Fr : e.Name,
                    },
                    category = e.Category?.Name ?? "",
                    isActive = e.IsActive
                }),
                length = await _newsService.Count(query)
            });
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            var news = await _newsService.GetNews(id);

            return Ok(new
            {
                news = new
                {
                    id = news.Id,
                    categoryId = news.CategoryId,
                    name = news.Name,
                    nameTranslations = new
                    {
                        pl = news.Name,
                        en = (!string.IsNullOrEmpty(news.Name_En)) ? news.Name_En : news.Name,
                        de = (!string.IsNullOrEmpty(news.Name_De)) ? news.Name_De : news.Name,
                        es = (!string.IsNullOrEmpty(news.Name_Es)) ? news.Name_Es : news.Name,
                        fr = (!string.IsNullOrEmpty(news.Name_Fr)) ? news.Name_Fr : news.Name,
                    },
                    address = news.Address,
                    details = news.Details,
                    detailsTranslations = new
                    {
                        pl = news.Details,
                        en = (!string.IsNullOrEmpty(news.Details_En)) ? news.Details_En : news.Details,
                        de = (!string.IsNullOrEmpty(news.Details_De)) ? news.Details_De : news.Details,
                        es = (!string.IsNullOrEmpty(news.Details_Es)) ? news.Details_Es : news.Details,
                        fr = (!string.IsNullOrEmpty(news.Details_Fr)) ? news.Details_Fr : news.Details,
                    },
                    shortDetails = news.ShortDetails,
                    shortDetailsTranslations = new
                    {
                        pl = news.ShortDetails,
                        en = (!string.IsNullOrEmpty(news.ShortDetails_En)) ? news.ShortDetails_En : news.ShortDetails,
                        de = (!string.IsNullOrEmpty(news.ShortDetails_De)) ? news.ShortDetails_De : news.ShortDetails,
                        es = (!string.IsNullOrEmpty(news.ShortDetails_Es)) ? news.ShortDetails_Es : news.ShortDetails,
                        fr = (!string.IsNullOrEmpty(news.ShortDetails_Fr)) ? news.ShortDetails_Fr : news.ShortDetails,
                    },
                    photo = news.Photo,
                    isActive = news.IsActive
                }
            });
        }

        [HttpGet("by/category/{categoryId}")]
        public async Task<IActionResult> GetByCategory([FromRoute] int categoryId)
        {
            var newss = await _newsService.ListByCategory(categoryId);

            return Ok(new
            {
                news = newss.Select(e => new
                {
                    id = e.Id,
                    name = e.Name,
                    nameTranslations = new
                    {
                        pl = e.Name,
                        en = (!string.IsNullOrEmpty(e.Name_En)) ? e.Name_En : e.Name,
                        de = (!string.IsNullOrEmpty(e.Name_De)) ? e.Name_De : e.Name,
                        es = (!string.IsNullOrEmpty(e.Name_Es)) ? e.Name_Es : e.Name,
                        fr = (!string.IsNullOrEmpty(e.Name_Fr)) ? e.Name_Fr : e.Name,
                    },
                    details = e.ShortDetails,
                    shortDetailsTranslations = new
                    {
                        pl = e.ShortDetails,
                        en = (!string.IsNullOrEmpty(e.ShortDetails_En)) ? e.ShortDetails_En : e.ShortDetails,
                        de = (!string.IsNullOrEmpty(e.ShortDetails_De)) ? e.ShortDetails_De : e.ShortDetails,
                        es = (!string.IsNullOrEmpty(e.ShortDetails_Es)) ? e.ShortDetails_Es : e.ShortDetails,
                        fr = (!string.IsNullOrEmpty(e.ShortDetails_Fr)) ? e.ShortDetails_Fr : e.ShortDetails,
                    },
                    photo = e.Photo
                })
            });
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromForm] NewsTransfer transfer)
        {
            try
            {
                var model = await _newsService.Create(transfer, Directory.GetCurrentDirectory());

                return Ok(new
                {
                    news = new
                    {
                        id = model.Id,
                        categoryId = model.CategoryId,
                        categoryName = model.Category.Name,
                        name = model.Name,
                        address = model.Address,
                        details = model.Details,
                        shortDetails = model.ShortDetails,
                        photo = model.Photo,
                        isActive = model.IsActive
                    }
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromForm] NewsTransfer transfer)
        {
            var model = await _newsService.Update(transfer, Directory.GetCurrentDirectory());

            return Ok(new
            {
                news = new
                {
                    id = model.Id,
                    categoryId = model.CategoryId,
                    categoryName = model.Category.Name,
                    name = model.Name,
                    address = model.Address,
                    details = model.Details,
                    shortDetails = model.ShortDetails,
                    photo = model.Photo,
                    isActive = model.IsActive
                }
            });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _newsService.Delete(id);

            return Ok();
        }
    }
}
