﻿using CommonComponents.Utils;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WRH.Application.Services;
using WRH.Application.Services.Interfaces;
using WRH.Domain;
using WRH.Domain.Transfer.Booking;
using WRH.Domain.Transfer.BookingAttraction;

namespace WRH.Controllers
{
    [ApiController]
    [Route("api/booking")]
    public class BookingController : ControllerBase
    {
        private readonly IBookingService _bookingService;
        private readonly IAttractionService _attractionService;
        private readonly IEmailService _emailService;
        private readonly IPayUService _payService;
        private readonly IPDFService _pdfService;
        private readonly IConfigurationService _configurationService;
        private readonly IInvoiceNumberService _invoiceNumberService;
        private readonly IRoomService _roomService;

        public BookingController(IBookingService bookingService, IAttractionService attractionService,
            IEmailService emailService, IPayUService payService, IPDFService pdfService,
            IConfigurationService configurationService, IInvoiceNumberService invoiceNumberService,
            IRoomService roomService)
        {
            _bookingService = bookingService;
            _attractionService = attractionService;
            _emailService = emailService;
            _payService = payService;
            _pdfService = pdfService;
            _configurationService = configurationService;
            _invoiceNumberService = invoiceNumberService;
            _roomService = roomService;
        }

        [HttpPost("list")]
        public async Task<IActionResult> List([FromBody] BookingQuery query)
        {
            var bookings = await _bookingService.GetBookingList(query);

            return Ok(new
            {
                items = bookings.Select(e => new
                {
                    id = e.Id,
                    bookingNumber = e.BookingNumber,
                    stayNumber = e.StayNumber,
                    createdAt = e.CreatedAt,
                    arrivalDate = e.ArrivalDate,
                    departureDate = e.DepartureDate,
                    client = e.PersonalData?.FullPersonalName,
                    rooms = (query.RoomType.HasValue) ? e.Rooms.FirstOrDefault(e => e.RoomId == query.RoomType)?.Room?.Name : e.Rooms.FirstOrDefault()?.Room?.Name,
                    toPay = e.ToPayLeft(),
                    status = e.Status.GetDescription()
                }),
                length = await _bookingService.Count(query)
            });
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            var booking = await _bookingService.GetBooking(id);

            return BookingData(booking);
        }

        [HttpGet("by/number/{stayNumber}")]
        public async Task<IActionResult> Get([FromRoute] string stayNumber)
        {
            var booking = await _bookingService.GetBooking(stayNumber);

            if (booking is null || booking.Status == Domain.Enums.BookingStatus.CheckedOut)
                return NotFound();

            return BookingData(booking);
        }

        [HttpPost("init")]
        public async Task<IActionResult> Init()
        {
            var booking = await _bookingService.Initialize(true);
            var boards = await _attractionService.GetBoards();

            return Ok(new
            {
                booking = new
                {
                    booking.Id,
                    booking.BookingNumber,
                    booking.StayNumber,
                    booking.CreatedAt,
                    booking.ArrivalDate,
                    booking.NumberOfAdults,
                    booking.NumberOfChildren,
                    booking.NumberOfChildrenToThree,
                    booking.Board,
                    boardAttractions = boards.Select(e => new
                    {
                        attractionId = e.Id,
                        createdAt = DateTime.Now,
                        grosPrice = Math.Round(e.GrosPrice, 2),
                        id = 0,
                        immediatePayment = e.ImmediatePayment,
                        e.Category.IsDefault,
                        e.Name,
                        e.NameTranslations,
                        e.NetPrice,
                        e.PaymentType,
                        price = Math.Round(e.GrosPrice),
                        quantity = 0,
                        e.Board
                    }),
                    attractions = booking.Attractions.Select(e => new
                    {
                        e.Id,
                        e.AttractionId,
                        e.Attraction.Name,
                        e.Attraction.NameTranslations,
                        e.Quantity,
                        netPrice = e.Attraction.NetPrice,
                        grosPrice = Math.Round(e.Attraction.GrosPrice, 2),
                        price = Math.Round(e.Attraction.GrosPrice),
                        e.IsPaid,
                        isDefault = e.Attraction.Category.IsDefault,
                        e.Attraction.PaymentType,
                        immediatePayment = e.Attraction.ImmediatePayment,
                        e.CreatedAt,
                        canDelete = e.Attraction.Category?.CanDelete ?? true
                    }),
                }
            });
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] BookingTransfer model)
        {
            var freeRooms = await _roomService.GetMatchRooms(new Domain.Transfer.Room.RoomMatchQueryModel
            {
                ArrivalDate = model.ArrivalDate,
                DepartureDate = model.DepartureDate
            });
            var booking = await _bookingService.Update(model, freeRooms);

            if (model.IsAdmin == true)
                return Ok(new 
                { 
                    status = new 
                    { 
                        statusCode = "NOT_NEEDED"
                    }
                }); 

            var result = await _payService.SendNewOrder(BookingPayUOrderModel.Create(booking, "127.0.0.1", "http://wrh2.coblit.com"));

            return Ok(JsonConvert.DeserializeObject(result));
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _bookingService.Delete(id);

            return Ok();
        }

        [HttpGet("check-out/{id}")]
        public async Task<IActionResult> CheckOut([FromRoute] int id)
        {
            var config = await _configurationService.Get();
            var booking = await _bookingService.CheckOut(id);
            await _emailService.SendCheckOutConfirmationGuest(booking);
            await _emailService.SendCheckOutConfirmationHotel(booking, config.ReceptionEmail);

            return Ok(new
            {
                success = true
            });
        }

        [HttpPost("attraction/assign")]
        public async Task<IActionResult> AssignAttraction([FromBody] BookingAttractionTransfer model)
        {
            var config = await _configurationService.Get();
            var booking = await _bookingService.AssignAttraction(model);

            if (booking == null)
                return NotFound();

            var attraction = await _attractionService.GetAttraction(model.AttractionId);

            await _emailService.SendAttractionConfirmationHotel(booking, attraction.Name, (attraction.ImmediatePayment) ? "Przy zamówieniu" : "Po realizacji", config.ReceptionEmail);

            return Ok(new
            {
                result = (booking != null)
            });
        }

        [HttpGet("attraction/remove/{id}")]
        public async Task<IActionResult> RemoveAttraction([FromRoute] int id)
        {
            await _bookingService.RemoveAttraction(id);

            return Ok();
        }

        [HttpGet("room/paid/{id}")]
        public async Task<IActionResult> SetRoomPaid([FromRoute] int id)
        {
            var room = await _bookingService.SetRoomIsPaid(id);

            return Ok(new
            {
                result = (room != null)
            });
        }

        [HttpGet("attraction/paid/{id}")]
        public async Task<IActionResult> SetAttractionPaid([FromRoute] int id)
        {
            var attraction = await _bookingService.SetAttractionIsPaid(id);

            return Ok(new
            {
                result = (attraction != null)
            });
        }

        [HttpGet("payment/summary/{bookingId}/{paymentId}")]
        public async Task<IActionResult> PaymentSummary([FromRoute] int bookingId, [FromRoute] int paymentId)
        {
            var booking = await _bookingService.GetBooking(bookingId);
            var summary = await _bookingService.GetPaymentSummary(paymentId);

            return Ok(new
            {
                booking = new
                {
                    booking.StayNumber,
                    arrivalDate = booking.ArrivalDate.ToString("yyyy-MM-dd"),
                    departureDate = booking.DepartureDate?.ToString("yyyy-MM-dd"),
                    total = summary.Sum(e => e.Quantity * e.UnitPrice)
                },
                summary
            });
        }

        [HttpPost("payment/notify")]
        public async Task<IActionResult> PaymentNotify([FromBody] BookingPaymentNotifyTransfer notify)
        {
            if (notify.Order.Status == "COMPLETED")
            {
                var bookingId = await _bookingService.SetIsPaidByPaymentNumber(notify.Order.ExtOrderId, notify.Order.OrderId);
                var booking = await _bookingService.GetBooking(bookingId);

                if (booking.RoomsBusy.All(e => e.Status != Domain.Enums.BusyStatus.Confirmed))
                    await _roomService.CompleteBusy(booking);

                var config = await _configurationService.Get();

                try
                {
                    var lastInvoice = await _invoiceNumberService.LastNumber();
                    var invoiceNumber = $"{(lastInvoice + 1).ToString().PadLeft(3, '0')}/{DateTime.Now.Month.ToString().PadLeft(2, '0')}/{DateTime.Now.Year}";
                    var invoicePath = _pdfService.Invoice(booking, notify.Order.ExtOrderId, invoiceNumber, DateTime.Now, Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Invoices"));

                    await _emailService.SendBookingConfirmationGuest(booking, notify.Order.OrderId, invoicePath);
                    await _emailService.SendBookingConfirmationHotel(booking, config.ReceptionEmail, notify.Order.OrderId, invoicePath);

                    await _invoiceNumberService.Save(bookingId, lastInvoice + 1);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }

            }


            return Ok();
        }

        private ObjectResult BookingData(BookingModel booking)
        {
            return Ok(new
            {
                booking = new
                {
                    booking.Id,
                    booking.BookingNumber,
                    booking.StayNumber,
                    booking.CreatedAt,
                    booking.NumberOfAdults,
                    booking.NumberOfChildren,
                    booking.NumberOfChildrenToThree,
                    booking.Board,
                    status = booking.Status.GetDescription(),
                    booking.LastEditUserName,
                    booking.LastEditTimeStamp,
                    booking.ArrivalDate,
                    booking.DepartureDate,
                    personalData = new
                    {
                        name = booking.PersonalData?.Name,
                        surname = booking.PersonalData?.Surname,
                        country = booking.PersonalData?.Country,
                        postalCode = booking.PersonalData?.PostalCode,
                        city = booking.PersonalData?.City,
                        street = booking.PersonalData?.Street,
                        houseNumber = booking.PersonalData?.HouseNumber,
                        doorsNumber = booking.PersonalData?.DoorsNumber,
                        phoneCountryNumber = booking.PersonalData?.PhoneCountryNumber,
                        phoneNumber = booking.PersonalData?.PhoneNumber,
                        email = booking.PersonalData?.Email
                    },
                    companyData = (booking.CompanyData == null) ? null : new
                    {
                        nip = booking.CompanyData?.Nip,
                        companyName = booking.CompanyData?.CompanyName,
                        name = booking.CompanyData?.Name,
                        surname = booking.CompanyData?.Surname,
                        country = booking.CompanyData?.Country,
                        postalCode = booking.CompanyData?.PostalCode,
                        city = booking.CompanyData?.City,
                        street = booking.CompanyData?.Street,
                        houseNumber = booking.CompanyData?.HouseNumber,
                        doorsNumber = booking.CompanyData?.DoorsNumber
                    },
                    guests = booking.GuestsData.Select(e => new
                    {
                        e.Id,
                        e.Name,
                        e.Surname,
                        e.Address,
                        e.Birthday
                    }),
                    rooms = booking.Rooms.Select(e => new
                    {
                        e.Id,
                        e.RoomId,
                        e.Room.Name,
                        e.Room.NameTranslations,
                        e.Room.Capacity,
                        e.Quantity,
                        e.Price,
                        e.IsPaid
                    }),
                    attractions = booking.Attractions.Select(e => new
                    {
                        e.Id,
                        e.AttractionId,
                        e.Attraction.Name,
                        e.Attraction.NameTranslations,
                        e.Quantity,
                        grosPrice = e.Price,
                        e.Price,
                        e.IsPaid,
                        e.Attraction.Category.IsDefault,
                        e.Attraction.PaymentType,
                        e.Attraction.ImmediatePayment,
                        e.CreatedAt
                    }),
                    toPay = booking.ToPayLeft()
                }
            });
        }
    }
}
