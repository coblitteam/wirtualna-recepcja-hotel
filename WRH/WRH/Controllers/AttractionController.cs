﻿using Microsoft.AspNetCore.Mvc;
using WRH.Application.Services.Interfaces;
using WRH.Domain.Transfer.Attraction;

namespace WRH.Controllers
{
    [ApiController]
    [Route("api/attraction")]
    public class AttractionController : ControllerBase
    {
        private readonly IAttractionService _attractionService;

        public AttractionController(IAttractionService attractionService)
        {
            _attractionService = attractionService;
        }

        [HttpPost("list")]
        public async Task<IActionResult> List([FromBody] AttractionQuery query)
        {
            var attractions = await _attractionService.GetAttractionList(query);

            if (query.GrosPriceFrom.HasValue)
                attractions = attractions.Where(e => e.GrosPrice >= query.GrosPriceFrom.Value).ToList();

            if (query.GrosPriceTo.HasValue)
                attractions = attractions.Where(e => e.GrosPrice <= query.GrosPriceTo.Value).ToList();

            return Ok(new
            {
                items = attractions.Select(e => new
                {
                    e.Id,
                    e.Name,
                    nameTranslations = new {
                        pl = e.Name,
                        en = (!string.IsNullOrEmpty(e.Name_En)) ? e.Name_En : e.Name,
                        de = (!string.IsNullOrEmpty(e.Name_De)) ? e.Name_De : e.Name,
                        es = (!string.IsNullOrEmpty(e.Name_Es)) ? e.Name_Es : e.Name,
                        fr = (!string.IsNullOrEmpty(e.Name_Fr)) ? e.Name_Fr : e.Name,
                    },
                    category = e.Category?.Name ?? "",
                    e.PaymentType,
                    e.NetPrice,
                    e.VAT,
                    e.GrosPrice,
                    e.IsActive,
                    e.ImmediatePayment,
                    isBoard = e.Category?.IsBoard ?? false,
                    canDelete = e.Category?.CanDelete ?? true
                }),
                length = await _attractionService.Count(query)
            });
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            var attraction = await _attractionService.GetAttraction(id);

            return Ok(new
            {
                attraction = new
                {
                    attraction.Id,
                    attraction.CategoryId,
                    attraction.PaymentType,
                    attraction.Name,
                    attraction.NameTranslations,
                    attraction.Details,
                    attraction.DetailsTranslations,
                    attraction.ShortDetails,
                    attraction.ShortDetailsTranslations,
                    attraction.NetPrice,
                    attraction.VAT,
                    attraction.GrosPrice,
                    attraction.Photo,
                    attraction.IsActive,
                    attraction.ImmediatePayment,
                    attraction.NotificationEmailAdresses,
                    attraction.NotificationHotelMessage,
                    attraction.NotificationGuestMessage
                }
            });
        }

        [HttpGet("by/category/{categoryId}")]
        public async Task<IActionResult> GetByCategory([FromRoute] int categoryId)
        {
            var attractions = await _attractionService.ListByCategory(categoryId);

            return Ok(new
            {
                attractions = attractions.Select(e => new
                {
                    e.Id,
                    e.Name,
                    nameTranslations = new
                    {
                        pl = e.Name,
                        en = (!string.IsNullOrEmpty(e.Name_En)) ? e.Name_En : e.Name,
                        de = (!string.IsNullOrEmpty(e.Name_De)) ? e.Name_De : e.Name,
                        es = (!string.IsNullOrEmpty(e.Name_Es)) ? e.Name_Es : e.Name,
                        fr = (!string.IsNullOrEmpty(e.Name_Fr)) ? e.Name_Fr : e.Name,
                    },
                    e.PaymentType,
                    e.Details,
                    detailsTranslations = new
                    {
                        pl = e.Details,
                        en = (!string.IsNullOrEmpty(e.Details_En)) ? e.Details_En : e.Details,
                        de = (!string.IsNullOrEmpty(e.Details_De)) ? e.Details_De : e.Details,
                        es = (!string.IsNullOrEmpty(e.Details_Es)) ? e.Details_Es : e.Details,
                        fr = (!string.IsNullOrEmpty(e.Details_Fr)) ? e.Details_Fr : e.Details,
                    },
                    e.ShortDetails,
                    shortDetailsTranslations = new
                    {
                        pl = e.ShortDetails,
                        en = (!string.IsNullOrEmpty(e.ShortDetails_En)) ? e.ShortDetails_En : e.ShortDetails,
                        de = (!string.IsNullOrEmpty(e.ShortDetails_De)) ? e.ShortDetails_De : e.ShortDetails,
                        es = (!string.IsNullOrEmpty(e.ShortDetails_Es)) ? e.ShortDetails_Es : e.ShortDetails,
                        fr = (!string.IsNullOrEmpty(e.ShortDetails_Fr)) ? e.ShortDetails_Fr : e.ShortDetails,
                    },
                    e.NetPrice,
                    e.GrosPrice,
                    e.VAT,
                    e.Photo,
                    e.ImmediatePayment
                })
            });
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromForm] AttractionTransfer transfer)
        {
            var model = await _attractionService.Create(transfer, Directory.GetCurrentDirectory());

            return Ok(new
            {
                attraction = new
                {
                    model.Id,
                    model.CategoryId,
                    model.PaymentType,
                    category = model.Category.Name,
                    model.Name,
                    model.Details,
                    model.ShortDetails,
                    model.NetPrice,
                    model.VAT,
                    model.Photo,
                    model.IsActive,
                    model.ImmediatePayment,
                    model.NotificationEmailAdresses,
                    model.NotificationHotelMessage,
                    model.NotificationGuestMessage
                }
            });
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromForm] AttractionTransfer transfer)
        {
            var model = await _attractionService.Update(transfer, Directory.GetCurrentDirectory());

            return Ok(new
            {
                attraction = new
                {
                    model.Id,
                    model.CategoryId,
                    model.PaymentType,
                    category = model.Category.Name,
                    model.Name,
                    model.Details,
                    model.ShortDetails,
                    model.NetPrice,
                    model.VAT,
                    model.Photo,
                    model.IsActive,
                    model.ImmediatePayment,
                    model.NotificationEmailAdresses,
                    model.NotificationHotelMessage,
                    model.NotificationGuestMessage
                }
            });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _attractionService.Delete(id);

            return Ok();
        }
    }
}
