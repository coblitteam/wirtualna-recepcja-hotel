﻿using Microsoft.AspNetCore.Mvc;
using WRH.Application.Services.Interfaces;
using WRH.Domain;

namespace WRH.Controllers
{
    [ApiController]
    [Route("api/config")]
    public class ConfigurationController : ControllerBase
    {
        private readonly IConfigurationService _configurationService;

        public ConfigurationController(IConfigurationService configurationService)
        {
            _configurationService = configurationService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var configuration = await _configurationService.Get();

            return Ok(new
            {
                config = new
                {
                    configuration.Id,
                    configuration.ReceptionEmail
                }
            });
        }

        [HttpPut]
        public async Task<IActionResult> Put(ConfigurationModel model)
        {
            var configuration = await _configurationService.Update(model);

            return Ok(new
            {
                config = new
                {
                    configuration.Id,
                    configuration.ReceptionEmail
                }
            });
        }
    }
}
