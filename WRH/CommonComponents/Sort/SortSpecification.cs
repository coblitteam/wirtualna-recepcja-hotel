﻿using System.ComponentModel;
using System.Linq.Expressions;

namespace CommonComponents.Sort
{
    public class SortSpecification<T>
    {
        public SortSpecification(Expression<Func<T, object>> predicate, ListSortDirection direction)
        {
            Predicate = predicate;
            Direction = direction;
        }

        public override string ToString()
        {
            return Predicate.ToString() + Direction.ToString();
        }

        public ListSortDirection Direction { get; set; }

        public Expression<Func<T, object>> Predicate { get; set; }
    }
}
