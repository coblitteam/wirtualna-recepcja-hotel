﻿/*
Inspiracje:
http://www.codeproject.com/Articles/670115/Specification-pattern-in-Csharp
http://lostechies.com/chrismissal/2009/09/11/using-the-specification-pattern-for-querying/
http://stackoverflow.com/questions/457316/combining-two-expressions-expressionfunct-bool
http://stackoverflow.com/questions/13513134/combining-two-linq-expressions
http://stackoverflow.com/questions/19433316/combine-two-linq-lambda-expressions
http://blogs.msdn.com/b/meek/archive/2008/05/02/linq-to-entities-combining-predicates.aspx
http://www.albahari.com/nutshell/predicatebuilder.aspx
*/

using System.Linq.Expressions;

namespace CommonComponents.QueryCriteria.Filtering
{
    public abstract class AbstractSpecification<T>
    {
        public abstract Expression<Func<T, bool>> GetPredicate();

        public static AbstractSpecification<T> operator &(AbstractSpecification<T> spec1, AbstractSpecification<T> spec2)
        {
            return new AndSpecification<T>(spec1, spec2);
        }

        public static bool operator false(AbstractSpecification<T> spec1)
        {
            return false;
        }

        public static bool operator true(AbstractSpecification<T> spec1)
        {
            return false;
        }

        public static AbstractSpecification<T> operator |(AbstractSpecification<T> spec1, AbstractSpecification<T> spec2)
        {
            return new OrSpecification<T>(spec1, spec2);
        }

        public static AbstractSpecification<T> operator !(AbstractSpecification<T> spec1)
        {
            return new NotSpecification<T>(spec1);
        }
    }
}
