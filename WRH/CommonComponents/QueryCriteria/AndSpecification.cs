﻿using System.Linq.Expressions;

namespace CommonComponents.QueryCriteria.Filtering
{
    public class AndSpecification<T> : AbstractSpecification<T>
    {
        private readonly AbstractSpecification<T> leftSpec;
        private readonly AbstractSpecification<T> rightSpec;

        public AndSpecification(AbstractSpecification<T> leftSpec, AbstractSpecification<T> rightSpec)
        {
            this.leftSpec = leftSpec;
            this.rightSpec = rightSpec;
        }

        public override Expression<Func<T, bool>> GetPredicate()
        {
            var left = leftSpec.GetPredicate();
            var right = rightSpec.GetPredicate();

            if (ReferenceEquals(left.Parameters[0], right.Parameters[0]))
                return Expression.Lambda<Func<T, bool>>(Expression.AndAlso(left.Body, right.Body), left.Parameters[0]);
            else
                return Expression.Lambda<Func<T, bool>>(Expression.AndAlso(left.Body, Expression.Invoke(right, left.Parameters[0])), left.Parameters[0]);
        }
    }
}
