﻿using System.Linq.Expressions;

namespace CommonComponents.QueryCriteria.Filtering
{
    public class Specification<T> : AbstractSpecification<T>
    {
        public Specification() { }

        public Specification(Expression<Func<T, bool>> predicate)
        {
            this._predicate = predicate;
        }

        public override Expression<Func<T, bool>> GetPredicate()
        {
            return this._predicate;
        }

        private Expression<Func<T, bool>> _predicate { get; set; }
    }
}
