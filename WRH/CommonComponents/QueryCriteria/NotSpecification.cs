﻿using System.Linq.Expressions;

namespace CommonComponents.QueryCriteria.Filtering
{
    public class NotSpecification<T> : AbstractSpecification<T>
    {
        private readonly AbstractSpecification<T> specification;

        public NotSpecification(AbstractSpecification<T> specification)
        {
            this.specification = specification;
        }

        public override Expression<Func<T, bool>> GetPredicate()
        {
            return Expression.Lambda<Func<T, bool>>(Expression.Not(specification.GetPredicate().Body), specification.GetPredicate().Parameters);
        }
    }
}
