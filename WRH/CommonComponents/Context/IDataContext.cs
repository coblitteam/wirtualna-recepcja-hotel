﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace CommonComponents.Context
{
    public interface IDataContext : IDisposable
    {
        DatabaseFacade Database { get; }

        DbSet<T> Set<T>() where T : class;

        EntityEntry Entry(object entity);

        int SaveChanges();

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}
