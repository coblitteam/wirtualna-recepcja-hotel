﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonComponents.Utils
{
    public static class Converters
    {
        public static dynamic ToDynamic(this object value)
        {
            IDictionary<string, object> expando = new ExpandoObject();

            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(value.GetType()))
                expando.Add(property.Name, property.GetValue(value));

            return (ExpandoObject)expando;
        }

        public static string ToPriceText(this decimal price)
        {
            var main = (int)price;
            var sub = (int)(100 * price) % 100;

            return String.Format("{0} {1}, {2} {3}",
            NumberText(main),
            CurrencyText(main, "PLN"),
            NumberText(sub),
            CurrencyText(sub, ".PLN"));
        }

        public static string NumberText(int number)
        {
            return NumberTextBase(number).Replace("  ", " ").Trim();
        }

        public static string CurrencyText(int liczba, string kodWaluty)
        {
            var key = Currencies[kodWaluty];
            return key[DeclinationCurrencyIndex(liczba)];
        }

        private static string NumberTextBase(int value)
        {
            StringBuilder sb = new StringBuilder();
            //0-999
            if (value == 0)
                return zero;
            int jednosc = value % 10;
            int para = value % 100;
            int set = (value % 1000) / 100;
            if (para > 10 && para < 20)
                sb.Insert(0, nascie[jednosc]);
            else
            {
                sb.Insert(0, jednosci[jednosc]);
                sb.Insert(0, dziesiatki[para / 10]);
            }
            sb.Insert(0, setki[set]);

            //Pozostałe rzędy wielkości
            value = value / 1000;
            int row = 0;
            while (value > 0)
            {
                jednosc = value % 10;
                para = value % 100;
                set = (value % 1000) / 100;
                bool rowExist = value % 1000 != 0;
                if ((value % 1000) / 10 == 0)
                {
                    //nie ma dziesiątek i setek
                    if (jednosc == 1)
                        sb.Insert(0, rzedy[row, 0]);
                    else if (jednosc >= 2 && jednosc <= 4)
                        sb.Insert(0, rzedy[row, 1]);
                    else if (rowExist)
                        sb.Insert(0, rzedy[row, 2]);
                    if (jednosc > 1)
                        sb.Insert(0, jednosci[jednosc]);
                }
                else
                {
                    if (para >= 10 && para < 20)
                    {
                        sb.Insert(0, rzedy[row, 2]);
                        sb.Insert(0, nascie[para % 10]);
                    }
                    else
                    {
                        if (jednosc >= 2 && jednosc <= 4)
                            sb.Insert(0, rzedy[row, 1]);
                        else if (rowExist)
                            sb.Insert(0, rzedy[row, 2]);
                        sb.Insert(0, jednosci[jednosc]);
                        sb.Insert(0, dziesiatki[para / 10]);
                    }
                    sb.Insert(0, setki[set]);
                }
                row++;
                value = value / 1000;
            }
            return sb.ToString();
        }

        private static int DeclinationCurrencyIndex(int number)
        {
            if (number == 1)
                return 0;

            int para = number % 100;
            if (para >= 10 && para < 20)
                return 2;

            int jednosc = number % 10;
            if (jednosc >= 2 && jednosc <= 4)
                return 1;

            return 2;
        }

        private static string zero = "zero";
        private static string[] jednosci = { "", " jeden ", " dwa ", " trzy ",
        " cztery ", " pięć ", " sześć ", " siedem ", " osiem ", " dziewięć " };
        private static string[] dziesiatki = { "", " dziesięć ", " dwadzieścia ",
        " trzydzieści ", " czterdzieści ", " pięćdziesiąt ",
        " sześćdziesiąt ", " siedemdziesiąt ", " osiemdziesiąt ",
        " dziewięćdziesiąt "};
        private static string[] nascie = { "dziesięć", " jedenaście ", " dwanaście ",
        " trzynaście ", " czternaście ", " piętnaście ", " szesnaście ",
        " siedemnaście ", " osiemnaście ", " dziewiętnaście "};
        private static string[] setki = { "", " sto ", " dwieście ", " trzysta ",
        " czterysta ", " pięćset ", " sześćset ",
        " siedemset ", " osiemset ", " dziewięćset " };
        private static string[,] rzedy = {{" tysiąc ", " tysiące ", " tysięcy "},
                            {" milion ", " miliony ", " milionów "},
                            {" miliard ", " miliardy ", " miliardów "}};

        private static Dictionary<string, string[]> Currencies = new Dictionary<string, string[]>() {
            { "PLN", new string[]{ "złoty", "złote", "złotych" } },
            { ".PLN", new string[]{ "grosz", "grosze", "groszy" } }
        };
    };
}
