﻿using CommonComponents.Context;
using CommonComponents.Sort;
using CommonComponents.Utils;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.Linq.Expressions;

namespace CommonComponents.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly IDataContext _db;

        public Repository(IDataContext db)
        {
            _db = db;
        }

        #region Synchronous

        public virtual IQueryable<T> All() =>
            _db.Set<T>();

        public virtual bool Any(Expression<Func<T, bool>> expression) =>
            All().Any(expression);

        public virtual int Count(Expression<Func<T, bool>> expression) =>
            All().Count(expression);

        public virtual T Find(int entityId) =>
            _db.Set<T>().Find(entityId);

        public virtual IList<T> Find(Expression<Func<T, bool>> expression) =>
            All().Where(expression).ToList();

        public virtual IList<T> Find(Expression<Func<T, bool>> expression, int startRow, int size, SortSpecification<T> sortSpecification) =>
            ApplySorting(All().Where(expression), sortSpecification).Skip(startRow).Take(size).ToList();

        public virtual List<T> FindAll() =>
            All().ToList();

        public virtual T First() =>
            All().FirstOrDefault();

        public virtual T First(Expression<Func<T, bool>> expression) =>
            All().FirstOrDefault(expression);

        public virtual int GetTotalNumber() =>
            All().Count();

        public virtual void Remove(T entity) =>
            _db.Entry(entity).State = EntityState.Deleted;

        public virtual void Remove(IList<T> entities)
        {
            foreach (var entity in entities)
                Remove(entity);
        }

        public virtual void Save(T entity) =>
            _db.Entry(entity).State = entity.GetObjectId() > 0 ? EntityState.Modified : EntityState.Added;

        public virtual void Save(IList<T> entities)
        {
            foreach (var entity in entities)
                Save(entity);
        }

        public virtual void SaveChanges() =>
            _db.SaveChanges();

        #endregion

        #region Asynchronous

        public virtual async Task<bool> AnyAsync(Expression<Func<T, bool>> expression) =>
            await _db.Set<T>().AnyAsync(expression);

        public virtual async Task<int> CountAsync(Expression<Func<T, bool>> expression) =>
            await All().CountAsync(expression);

        public virtual async Task<T> FindAsync(int entityId) =>
            await _db.Set<T>().FindAsync(entityId);

        public virtual async Task<IList<T>> FindAsync(Expression<Func<T, bool>> expression) =>
            await All().Where(expression).ToListAsync();

        public virtual async Task<IList<T>> FindAsync(Expression<Func<T, bool>> expression, int startRow, int size, SortSpecification<T> sortSpecification) =>
            await ApplySorting(All().Where(expression), sortSpecification).Skip(startRow).Take(size).ToListAsync();

        public virtual async Task<List<T>> FindAllAsync() =>
            await All().ToListAsync();

        public virtual async Task<T> FirstAsync() =>
            await All().FirstOrDefaultAsync();

        public virtual async Task<T> FirstAsync(Expression<Func<T, bool>> expression) =>
            await All().FirstOrDefaultAsync(expression);

        public virtual async Task<int> GetTotalNumberAsync() =>
            await All().CountAsync();

        public virtual async Task<int> SaveChangesAsync() =>
            await _db.SaveChangesAsync();

        #endregion

        public virtual void Dispose() =>
            _db.Dispose();

        private IQueryable<T> ApplySorting(IQueryable<T> query, SortSpecification<T> sortSpecification)
        {
            if (sortSpecification != null && sortSpecification.Predicate != null)
            {
                switch (sortSpecification.Direction)
                {
                    case ListSortDirection.Ascending:
                        return query.OrderBy(sortSpecification.Predicate);

                    case ListSortDirection.Descending:
                        return query.OrderByDescending(sortSpecification.Predicate);

                    default:
                        return query;
                }
            }
            else
                return query;
        }
    }
}
