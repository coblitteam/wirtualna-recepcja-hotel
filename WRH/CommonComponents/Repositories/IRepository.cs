﻿using CommonComponents.Sort;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CommonComponents.Repositories
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> All();

        bool Any(Expression<Func<T, bool>> expression);
        Task<bool> AnyAsync(Expression<Func<T, bool>> expression);

        int Count(Expression<Func<T, bool>> expression);
        Task<int> CountAsync(Expression<Func<T, bool>> expression);

        T Find(int entityId);
        Task<T> FindAsync(int entityId);

        IList<T> Find(Expression<Func<T, bool>> expression);
        Task<IList<T>> FindAsync(Expression<Func<T, bool>> expression);

        IList<T> Find(Expression<Func<T, bool>> expression, int startRow, int size, SortSpecification<T> sortSpecification);
        Task<IList<T>> FindAsync(Expression<Func<T, bool>> expression, int startRow, int size, SortSpecification<T> sortSpecification);

        List<T> FindAll();
        Task<List<T>> FindAllAsync();

        T First();
        Task<T> FirstAsync();

        T First(Expression<Func<T, bool>> expression);
        Task<T> FirstAsync(Expression<Func<T, bool>> expression);

        int GetTotalNumber();
        Task<int> GetTotalNumberAsync();

        void Remove(T entity);

        void Remove(IList<T> entities);

        void Save(T entity);

        void Save(IList<T> entities);

        void SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
