﻿using System.Text;
using System.Security.Cryptography;

namespace CommonComponent.Security
{
    public static class PasswordEncoding
    {
        public static string Hash(string clearText)
        {
            if (clearText == null)
                return "";

            string result = string.Empty;
            using (MD5 md5Hash = MD5.Create())
            {
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(clearText));
                StringBuilder sBuilder = new StringBuilder();

                for (int i = 0; i < data.Length; i++)
                    sBuilder.Append(data[i].ToString("x2"));

                result = sBuilder.ToString();
            }
            return result;
        }
    }
}
