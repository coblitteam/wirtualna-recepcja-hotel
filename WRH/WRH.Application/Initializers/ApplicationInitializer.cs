﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;

namespace WRH.Application.Initializers
{
    public static class ApplicationInitializer
    {
        public static IServiceCollection InitApplication(this IServiceCollection services)
        {
            services.AddControllersWithViews()
                .AddNewtonsoftJson(o =>
            o.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddSpaStaticFiles(c =>
                c.RootPath = "ClientApp/build"
            );

            services.AddEndpointsApiExplorer();

            return services;
        }

        public static IApplicationBuilder InitApplicationUsings(this IApplicationBuilder app, bool isDevelopment)
        {
            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            app.UseAuthentication();
            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (isDevelopment)
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });

            return app;
        }
    }
}
