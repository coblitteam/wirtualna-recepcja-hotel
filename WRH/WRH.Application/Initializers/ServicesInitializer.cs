﻿using Microsoft.Extensions.DependencyInjection;
using WRH.Application.Services;
using WRH.Application.Services.Interfaces;

namespace WRH.Application.Initializers
{
    public static class ServicesInitializer
    {
        public static IServiceCollection InitServicesDI(this IServiceCollection services)
        {
            services.AddScoped<IAttractionService, AttractionService>();
            services.AddScoped<IAttractionCategoryService, AttractionCategoryService>();
            services.AddScoped<IConfigurationService, ConfigurationService>();
            services.AddScoped<IBookingService, BookingService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IGuestDataService, GuestDataService>();
            services.AddScoped<IImageService, ImageService>();
            services.AddScoped<IInvoiceNumberService, InvoiceNumberService>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<INewsService, NewsService>();
            services.AddScoped<INewsCategoryService, NewsCategoryService>();
            services.AddScoped<IPayUService, PayUService>();
            services.AddScoped<IPDFService, PDFService>();
            services.AddScoped<IRoomService, RoomService>();
            services.AddScoped<IRoomTypeService, RoomTypeService>();
            services.AddScoped<IUserService, UserService>();

            return services;
        }
    }
}
