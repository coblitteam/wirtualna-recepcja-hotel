﻿using Microsoft.Extensions.DependencyInjection;
using WRH.Repository;
using WRH.Repository.Interfaces;

namespace WRH.Application.Initializers
{
    public static class RepositoriesInitializer
    {
        public static IServiceCollection InitRepositoriesDI(this IServiceCollection services)
        {
            services.AddScoped<IAttractionRepository, AttractionRepository>();
            services.AddScoped<IAttractionCategoryRepository, AttractionCategoryRepository>();
            services.AddScoped<IConfigurationRepository, ConfigurationRepository>();
            services.AddScoped<IBookingRepository, BookingRepository>();
            services.AddScoped<IBookingAttractionRepository, BookingAttractionRepository>();
            services.AddScoped<IBookingRoomRepository, BookingRoomRepository>();
            services.AddScoped<ICustomerDataRepository, CustomerDataRepository>();
            services.AddScoped<IGuestDataRepository, GuestDataRepository>();
            services.AddScoped<IInvoiceNumberRepository, InvoiceNumberRepository>();
            services.AddScoped<INewsRepository, NewsRepository>();
            services.AddScoped<INewsCategoryRepository, NewsCategoryRepository>();
            services.AddScoped<IRoomRepository, RoomRepository>();
            services.AddScoped<IRoomBusyRepository, RoomBusyRepository>();
            services.AddScoped<IRoomTypeRepository, RoomTypeRepository>();
            services.AddScoped<IUserRepository, UserRepository>();

            return services;
        }
    }
}
