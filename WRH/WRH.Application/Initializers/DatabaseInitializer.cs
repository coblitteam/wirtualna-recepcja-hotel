﻿using CommonComponents.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using WRH.Database;
using WRH.Domain;

namespace WRH.Application.Initializers
{
    public static class DatabaseInitializer
    {
        public static IServiceCollection InitDatabase(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<DataContext>(o =>
                o.UseSqlServer(connectionString));
            services.AddDatabaseDeveloperPageExceptionFilter();

            return services;
        }

        public static IServiceCollection InitContextDI(this IServiceCollection services)
        {
            services.AddScoped<IDataContext, DataContext>();

            return services;
        }

        public static IApplicationBuilder CreateDatabaseIfNotExists(this IApplicationBuilder app, IServiceScope scope)
        {
            var services = scope.ServiceProvider;

            try
            {
                var db = services.GetRequiredService<DataContext>();
                Initialize(db);
            }
            catch
            {
            }

            return app;
        }

        private static void Initialize(DataContext context)
        {
            context.Database.EnsureCreated();
        }
    }
}
