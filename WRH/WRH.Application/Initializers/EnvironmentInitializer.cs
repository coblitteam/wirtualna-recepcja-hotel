﻿using Microsoft.AspNetCore.Builder;

namespace WRH.Application.Initializers
{
    public static class EnvironmentInitializer
    {
        public static IApplicationBuilder InitUsingsByEnvironment(this IApplicationBuilder app, bool isDevelopment)
        {
            if (isDevelopment)
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }
            else
            {
                app.UseHsts();
            }

            return app;
        }
    }
}
