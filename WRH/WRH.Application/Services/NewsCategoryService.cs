﻿using WRH.Application.Services.Interfaces;
using WRH.Domain;
using WRH.Domain.Transfer.NewsCategory;
using WRH.Repository.Interfaces;

namespace WRH.Application.Services
{
    public class NewsCategoryService : INewsCategoryService
    {
        private readonly INewsCategoryRepository _newsCategoryRepository;

        public NewsCategoryService(INewsCategoryRepository newsCategoryRepository)
        {
            _newsCategoryRepository = newsCategoryRepository;
        }

        public async Task<List<NewsCategoryModel>> ActiveList() =>
            (await _newsCategoryRepository.FindAsync(e => e.IsActive == true)).ToList();

        public async Task<List<NewsCategoryModel>> AllList() =>
            (await _newsCategoryRepository.FindAllAsync()).ToList();

        public async Task<NewsCategoryModel> Create(NewsCategoryTransfer transfer)
        {
            var model = NewsCategoryModel.CreateData(transfer);

            _newsCategoryRepository.Save(model);
            await _newsCategoryRepository.SaveChangesAsync();

            return model;
        }

        public async Task<NewsCategoryModel> Update(NewsCategoryTransfer transfer)
        {
            var model = await _newsCategoryRepository.FindAsync(transfer.Id);
            model.OverrideData(transfer);

            _newsCategoryRepository.Save(model);
            await _newsCategoryRepository.SaveChangesAsync();

            return model;
        }

        public void Delete(int id)
        {
            var model = _newsCategoryRepository.Find(id);

            _newsCategoryRepository.Remove(model);
            _newsCategoryRepository.SaveChanges();
        }
    }
}
