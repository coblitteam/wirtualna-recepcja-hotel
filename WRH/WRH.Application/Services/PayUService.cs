﻿using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text;
using WRH.Application.Services.Interfaces;
using WRH.Domain.Transfer.Booking;

namespace WRH.Application.Services
{
    public class PayUService : IPayUService
    {
        private readonly Uri BaseAddress = new Uri("https://secure.payu.com/");
        private readonly string Pos = "4196393";
        private readonly string Secret = "2565beb74cec0035e3a93f2b6cbbbb25";

        private AuthData authData;

        public async Task<string> SendNewOrder(BookingPayUOrderModel order)
        {
            if (order.TotalAmount <= 0)
                return "{status: { statusCode: \"NOT_NEEDED\"}}";

            order.MerchantPosId = Pos;
            string responseData;

            if (authData == null)
                await Authenticate();

            using (var httpClient = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false }) { BaseAddress = BaseAddress })
            {
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("authorization", $"{authData.token_type} {authData.access_token}");
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                using (var content = new StringContent(JsonConvert.SerializeObject(order), Encoding.Default, "application/json"))
                {
                    using (var response = await httpClient.PostAsync("api/v2_1/orders/", content))
                    {
                        responseData = await response.Content.ReadAsStringAsync();
                    }
                }
            }

            return responseData;
        }

        private async Task Authenticate()
        {
            using (var httpClient = new HttpClient { BaseAddress = BaseAddress })
            {
                using (var content = new StringContent($"grant_type=client_credentials&client_id={Pos}&client_secret={Secret}", System.Text.Encoding.Default, "application/x-www-form-urlencoded"))
                {
                    using (var response = await httpClient.PostAsync("pl/standard/user/oauth/authorize", content))
                    {
                        try
                        {
                            response.EnsureSuccessStatusCode();
                            var res = await response.Content.ReadAsStringAsync();
                            authData = JsonConvert.DeserializeObject<AuthData>(res);

                            Console.WriteLine(res);
                            Console.Write(authData.access_token);
                        }
                        catch (Exception ex)
                        {
                            authData = null;
                            Console.WriteLine(ex);
                        }
                    }
                }
            }
        }

        private class AuthData
        {
            public string access_token { get; set; }
            public string token_type { get; set; }
            public int expires_in { get; set; }
            public string grant_type { get; set; }
        }
    }
}
