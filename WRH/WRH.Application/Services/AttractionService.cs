﻿using CommonComponents.QueryCriteria.Filtering;
using CommonComponents.Sort;
using System.ComponentModel;
using WRH.Application.Services.Interfaces;
using WRH.Domain;
using WRH.Domain.Transfer.Attraction;
using WRH.Repository.Interfaces;

namespace WRH.Application.Services
{
    public class AttractionService : IAttractionService
    {
        private readonly IAttractionRepository _attractionRepository;
        private readonly IAttractionCategoryRepository _attractionCategoryRepository;
        private readonly IFileService _fileService;

        public AttractionService(IAttractionRepository attractionRepository, IAttractionCategoryRepository attractionCategoryRepository,
            IFileService fileService)
        {
            _attractionRepository = attractionRepository;
            _attractionCategoryRepository = attractionCategoryRepository;
            _fileService = fileService;
        }

        public async Task<AttractionModel> Create(AttractionTransfer transfer, string fileDestination)
        {
            var category = await _attractionCategoryRepository.FindAsync(transfer.CategoryId);

            var model = AttractionModel.CreateData(transfer, category);

            if (transfer.Photo != null)
            {
                var path = Path.Combine("Images", "Attraction");
                var fileName = _fileService.Save(transfer.Photo, Path.Combine(fileDestination, "wwwroot", path));
                model.Photo = Path.Combine(path, fileName);
            }

            _attractionRepository.Save(model);
            await _attractionRepository.SaveChangesAsync();

            return model;
        }

        public async Task<AttractionModel> Update(AttractionTransfer transfer, string fileDestination)
        {
            var model = await _attractionRepository.FindAsync(transfer.Id);
            model.OverrideData(transfer);

            if (transfer.Photo != null)
            {
                var path = Path.Combine("Images", "Attraction");
                var fileName = _fileService.Save(transfer.Photo, Path.Combine(fileDestination, "wwwroot", path));
                model.Photo = Path.Combine(path, fileName);
            }

            _attractionRepository.Save(model);
            await _attractionRepository.SaveChangesAsync();

            return model;
        }

        public void Delete(int id)
        {
            var model = _attractionRepository.Find(id);

            _attractionRepository.Remove(model);
            _attractionRepository.SaveChanges();
        }

        public async Task<long> Count(AttractionQuery query) =>
            await _attractionRepository.CountAsync(InitSpecification(query).GetPredicate());

        public async Task<AttractionModel> GetAttraction(int id) =>
            await _attractionRepository.FindAsync(id);

        public async Task<List<AttractionModel>> GetAttractionList(AttractionQuery query)
        {

            var data = await _attractionRepository.FindAsync(
                            InitSpecification(query).GetPredicate(),
                            query.Start,
                            query.Size,
                            InitSort(query)
                        );

            return data.ToList();
        }


        public async Task<List<AttractionModel>> ListByCategory(int categoryId) =>
            (await _attractionRepository.FindAsync(e => e.CategoryId == categoryId && e.IsActive)).ToList();

        public async Task<List<AttractionModel>> GetBoards() =>
            (await _attractionRepository.FindAsync(e => e.Category.IsBoard && e.IsActive)).ToList();

        private AbstractSpecification<AttractionModel> InitSpecification(AttractionQuery query)
        {
            AbstractSpecification<AttractionModel> specification = new Specification<AttractionModel>(e => true);

            if (!string.IsNullOrEmpty(query.Search) && query.Search.Length > 3)
                specification &= new Specification<AttractionModel>(e => e.Name.Contains(query.Search));

            if (query.CategoryId.HasValue)
                specification &= new Specification<AttractionModel>(e => e.CategoryId == query.CategoryId);

            if (query.NetPriceFrom.HasValue)
                specification &= new Specification<AttractionModel>(e => e.NetPrice >= query.NetPriceFrom.Value);

            if (query.NetPriceTo.HasValue)
                specification &= new Specification<AttractionModel>(e => e.NetPrice <= query.NetPriceTo.Value);

            //if (query.GrosPriceFrom.HasValue)
            //    specification &= new Specification<AttractionModel>(e => e.GrosPrice >= query.GrosPriceFrom.Value);

            //if (query.GrosPriceTo.HasValue)
            //    specification &= new Specification<AttractionModel>(e => e.GrosPrice <= query.GrosPriceTo.Value);

            if (query.IsActive.HasValue)
                specification &= new Specification<AttractionModel>(e => e.IsActive == query.IsActive);

            return specification;
        }

        private SortSpecification<AttractionModel> InitSort(AttractionQuery query)
        {
            var sortDirection = (query.OrderDestination == "asc") ? ListSortDirection.Ascending : ListSortDirection.Descending;

            SortSpecification<AttractionModel> sortQuery;
            switch (query.OrderBy)
            {
                case "name":
                    sortQuery = new SortSpecification<AttractionModel>(e => e.Name, sortDirection);
                    break;
                case "category":
                    sortQuery = new SortSpecification<AttractionModel>(e => e.Category.Name, sortDirection);
                    break;
                case "netPrice":
                    sortQuery = new SortSpecification<AttractionModel>(e => e.NetPrice, sortDirection);
                    break;
                case "vat":
                    sortQuery = new SortSpecification<AttractionModel>(e => e.VAT, sortDirection);
                    break;
                case "isActive":
                    sortQuery = new SortSpecification<AttractionModel>(e => e.IsActive, sortDirection);
                    break;
                default:
                    sortQuery = new SortSpecification<AttractionModel>(e => e.Name, ListSortDirection.Ascending);
                    break;
            }

            return sortQuery;
        }
    }
}
