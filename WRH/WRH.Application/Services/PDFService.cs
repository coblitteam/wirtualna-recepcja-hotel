﻿using System.Data;
using WRH.Application.Services.Interfaces;
using WRH.Domain;
using System.Web.UI;
using System.Text;
using CommonComponents.Utils;
using PdfSharpCore.Pdf;
using PdfSharpCore.Drawing;
using HtmlRendererCore.PdfSharp;
using PdfSharpCore;

namespace WRH.Application.Services
{
    public class PDFService : IPDFService
    {
        public string Invoice(BookingModel booking, int paymentId, string invoiceNumber, DateTime soldDate, string destination)
        {
            DataTable data = new DataTable();

            data.Columns.Add(new DataColumn("Lp", typeof(int)));
            data.Columns.Add(new DataColumn("Nazwa usługi", typeof(string)));
            data.Columns.Add(new DataColumn("J.m", typeof(string)));
            data.Columns.Add(new DataColumn("Ilość", typeof(int)));
            data.Columns.Add(new DataColumn("Cena jedn.", typeof(string)));
            data.Columns.Add(new DataColumn("Wartość brutto", typeof(string)));

            var lp = 1;
            var sum = 0m;

            foreach (var room in booking.Rooms.Where(e => e.PaymentId == paymentId))
            {
                var totalPrice = room.Price * room.Quantity * booking.Days();
                sum += totalPrice;

                data.Rows.Add(lp, room.Room.Name, "szt", room.Quantity, (room.Price * booking.Days()).ToString("C2"), totalPrice.ToString("C2"));
                lp++;
            }

            foreach (var attraction in booking.Attractions.Where(e => e.PaymentId == paymentId))
            {
                var totalPrice = attraction.Price * attraction.Quantity;
                sum += totalPrice;

                data.Rows.Add(lp, attraction.Attraction.Name, "szt", attraction.Quantity, attraction.Price.ToString("C2"), totalPrice.ToString("C2"));
                lp++;
            }

            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    StringBuilder sb = new StringBuilder();

                    //Header
                    sb.Append("<table width='100%' cellspacing='0' cellpadding='2'>");
                    sb.Append("<tr><td align='left'>Mazowsze Medi Spa Sp. z o.o.</td><td align='right'>Miejscowość: Ustroń</td></tr>");
                    sb.Append($"<tr><td align='left'>ul. Stroma 6, 43-450 Ustroń</td><td align='right'>Data wystawienia: {DateTime.Now:dd.MM.yyyy}</td></tr>");
                    sb.Append($"<tr><td align='left'>NIP: 548-20-10-702</td><td align='right'></td></tr>");
                    sb.Append("<tr><td align='left' colspan='2'>tel: +48 33 854-26-59 / +48 33 857-72-71</td></tr>");
                    sb.Append("<tr><td align='left' colspan='2'>Konto bankowe: 82 8129 0004 2001 0025 8212 0001</td></tr>");
                    sb.Append("<tr><td colspan='2'></td><br /></tr>");
                    sb.Append("<tr><td colspan='2'></td><br /></tr>");
                    sb.Append($"<tr><td align='center' colspan='2'>Potwierdzenie wpłaty Nr <strong>{invoiceNumber}</strong></td></tr>");
                    sb.Append("<tr><td colspan='2'></td><br /></tr>");
                    sb.Append("<tr><td colspan='2'></td><br /></tr>");
                    sb.Append("<tr><td>Sprzedawca:</td><td>Nabywca:</td></tr>");
                    sb.Append($"<tr><td>Mazowsze Medi Spa Sp. z o.o.</td><td>{((booking.CompanyData == null) ? booking.PersonalData?.FullPersonalName : booking.CompanyData?.CompanyName)}</td></tr>");
                    sb.Append($"<tr><td>ul. Stroma 6, 43-450 Ustroń</td><td>{((booking.CompanyData == null) ? booking.PersonalData?.Address : booking.CompanyData?.Address)}</td></tr>");
                    sb.Append($"<tr><td>NIP: 548-20-10-702</td><td>NIP: {((booking.CompanyData != null) ? booking.CompanyData?.Nip : "")}</td></tr>");
                    sb.Append("</table>");

                    sb.Append("<br /><br /><br /><br />");

                    //Table
                    sb.Append("<table width='100%' border='1' cellspacing='0' cellpadding='2'>");
                    sb.Append("<tr>");
                    foreach (DataColumn column in data.Columns)
                    {
                        sb.Append("<td><strong>");
                        sb.Append(column.ColumnName);
                        sb.Append("</strong></td>");
                    }
                    sb.Append("</tr>");
                    foreach (DataRow row in data.Rows)
                    {
                        sb.Append("<tr>");
                        foreach (DataColumn column in data.Columns)
                        {
                            sb.Append("<td>");
                            sb.Append(row[column]);
                            sb.Append("</td>");
                        }
                        sb.Append("</tr>");
                    }
                    sb.Append("<tr><td align = 'right' colspan = '");
                    sb.Append(data.Columns.Count - 1);
                    sb.Append("'>Do zapłaty</td>");
                    sb.Append("<td>");
                    sb.Append(sum.ToString("C2"));
                    sb.Append("</td>");
                    sb.Append("</tr></table>");

                    sb.Append("<br />");

                    sb.Append($"<p>Słownie: {sum.ToPriceText()}</p>");

                    sb.Append("<br />");

                    sb.Append($"<p>Zapłacono poprzez system PayU: <strong>{sum:C2}</strong></p>");
                    sb.Append($"<p>Unikalny kod pobytu: <strong>{booking.StayNumber}</strong></p>");

                    sb.Append("<br /><br /><br /><br />");
                    sb.Append("<p style='text-align: center'>Celem otrzymania dokumentu sprzedażowego proszę o udanie się do recepcji obiektu Mazowsze Medi SPA</p>");

                    var fileName = Guid.NewGuid() + ".pdf";
                    var path = Path.Combine(destination, fileName);

                    PdfDocument document = PdfGenerator.GeneratePdf(sb.ToString(), PageSize.A4);
                    document.Save(path);

                    return path;
                }
            }
        }
    }
}
