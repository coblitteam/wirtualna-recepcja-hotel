﻿using WRH.Application.Services.Interfaces;
using WRH.Domain;
using WRH.Repository.Interfaces;

namespace WRH.Application.Services
{
    public class UserService : IUserService
    {
        private IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<UserModel> GetByLoginAsync(string login)
            => await _userRepository.FirstAsync(e => e.UserName == login);
    }
}
