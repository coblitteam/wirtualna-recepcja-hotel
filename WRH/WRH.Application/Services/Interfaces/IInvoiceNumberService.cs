﻿namespace WRH.Application.Services.Interfaces
{
    public interface IInvoiceNumberService
    {
        public Task<int> LastNumber();

        public Task Save(int bookingId, int number);
    }
}