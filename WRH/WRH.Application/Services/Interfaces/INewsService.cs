﻿using WRH.Domain;
using WRH.Domain.Transfer.News;

namespace WRH.Application.Services.Interfaces
{
    public interface INewsService
    {
        Task<NewsModel> Create(NewsTransfer transfer, string fileDestination);
        Task<NewsModel> Update(NewsTransfer transfer, string fileDestination);
        void Delete(int id);
        Task<long> Count(NewsQuery query);
        Task<NewsModel> GetNews(int id);
        Task<List<NewsModel>> GetNewsList(NewsQuery query);
        Task<List<NewsModel>> ListByCategory(int categoryId);
    }
}
