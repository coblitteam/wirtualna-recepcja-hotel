﻿using WRH.Domain;

namespace WRH.Application.Services.Interfaces
{
    public interface IUserService
    {
        Task<UserModel> GetByLoginAsync(string name);
    }
}
