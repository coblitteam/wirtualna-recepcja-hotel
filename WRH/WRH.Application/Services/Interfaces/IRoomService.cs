﻿using WRH.Domain;
using WRH.Domain.Transfer.Room;
using WRH.Domain.Transfer.RoomBusy;

namespace WRH.Application.Services.Interfaces
{
    public interface IRoomService
    {
        Task<List<RoomModel>> GetAllAsync();

        Task<RoomModel> GetByIdAsync(int id);

        Task<RoomModel> Update(RoomTransferModel transfer);

        Task<RoomBusyModel> SetBusy(RoomBusySetModel transfer);

        Task CompleteBusy(BookingModel booking);

        Task<List<RoomModel>> GetMatchRooms(RoomMatchQueryModel query);

        Task RemoveBusyPendingExpired();
    }
}
