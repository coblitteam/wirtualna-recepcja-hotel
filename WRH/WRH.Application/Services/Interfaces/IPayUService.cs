﻿using WRH.Domain.Transfer.Booking;

namespace WRH.Application.Services.Interfaces
{
    public interface IPayUService
    {
        Task<string> SendNewOrder(BookingPayUOrderModel order);
    }
}
