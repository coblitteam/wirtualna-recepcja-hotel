﻿using WRH.Domain;
using WRH.Domain.Transfer.AttractionCategory;

namespace WRH.Application.Services.Interfaces
{
    public interface IAttractionCategoryService
    {
        Task<AttractionCategoryModel> Create(AttractionCategoryTransfer transfer);
        Task<AttractionCategoryModel> Update(AttractionCategoryTransfer transfer);
        void Delete(int id);
        Task<List<AttractionCategoryModel>> ActiveList();
        Task<List<AttractionCategoryModel>> AllList();
    }
}
