﻿using WRH.Domain;
using WRH.Domain.Transfer.Attraction;

namespace WRH.Application.Services.Interfaces
{
    public interface IAttractionService
    {
        Task<AttractionModel> Create(AttractionTransfer transfer, string fileDestination);
        Task<AttractionModel> Update(AttractionTransfer transfer, string fileDestination);
        void Delete(int id);
        Task<long> Count(AttractionQuery query);
        Task<AttractionModel> GetAttraction(int id);
        Task<List<AttractionModel>> GetAttractionList(AttractionQuery query);
        Task<List<AttractionModel>> ListByCategory(int categoryId);
        Task<List<AttractionModel>> GetBoards();
    }
}
