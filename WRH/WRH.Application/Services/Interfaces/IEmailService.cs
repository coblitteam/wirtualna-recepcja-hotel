﻿using System.Net.Mail;
using WRH.Domain;

namespace WRH.Application.Services.Interfaces
{
    public interface IEmailService
    {
        Task SendBookingConfirmationGuest(BookingModel booking, string paymentNumber, string pdfPath);
        Task SendBookingConfirmationHotel(BookingModel booking, string hotelEmail, string paymentNumber, string pdfPath);
        Task SendAttractionConfirmationHotel(BookingModel booking, string attractionName, string attractionPayment, string hotelEmail);
        Task SendCheckOutConfirmationHotel(BookingModel booking, string hotelEmail);
        Task SendCheckOutConfirmationGuest(BookingModel booking);
        Task SendMail(string to, string subject, string body, Attachment attachment);
    }
}
