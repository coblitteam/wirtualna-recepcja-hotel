﻿using WRH.Domain;

namespace WRH.Application.Services.Interfaces
{
    public interface IPDFService
    {
        string Invoice(BookingModel booking, int paymentId, string invoiceNumber, DateTime soldDate, string destination);
    }
}
