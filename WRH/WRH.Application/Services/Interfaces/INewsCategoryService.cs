﻿using WRH.Domain;
using WRH.Domain.Transfer.NewsCategory;

namespace WRH.Application.Services.Interfaces
{
    public interface INewsCategoryService
    {
        Task<List<NewsCategoryModel>> ActiveList();
        Task<List<NewsCategoryModel>> AllList();
        Task<NewsCategoryModel> Create(NewsCategoryTransfer transfer);
        Task<NewsCategoryModel> Update(NewsCategoryTransfer transfer);
        void Delete(int id);
    }
}
