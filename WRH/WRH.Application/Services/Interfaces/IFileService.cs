﻿using Microsoft.AspNetCore.Http;

namespace WRH.Application.Services.Interfaces
{
    public interface IFileService
    {
        string Save(IFormFile file, string destination);
    }
}
