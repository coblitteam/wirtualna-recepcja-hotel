﻿using WRH.Domain;
using WRH.Domain.Transfer.GuestData;

namespace WRH.Application.Services.Interfaces
{
    public interface IGuestDataService
    {
        Task<GuestDataModel> Create(GuestDataTransfer transfer);
        Task Update(GuestDataTransfer transfer);
        Task Delete(int id);
    }
}
