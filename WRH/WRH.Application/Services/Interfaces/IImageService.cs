﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WRH.Application.Services.Interfaces
{
    public interface IImageService
    {
        void ResizeImage(Stream origFileStream, string newFileLocation, int newWidth, int maxHeight, bool resizeIfWider);
        void ResizeImageAndRatio(string origFileLocation, string newFileLocation, string origFileName, string newFileName, int newWidth, int newHeight, bool resizeIfWider);
    }
}
