﻿using WRH.Domain;
using WRH.Domain.Transfer.RoomType;

namespace WRH.Application.Services.Interfaces
{
    public interface IRoomTypeService
    {
        Task<RoomTypeModel> GetAsync(int id);
        Task<List<RoomTypeModel>> GetAllAsync();
        Task<RoomTypeModel> Update(RoomTypeTransferModel transfer, string fileDestination);
    }
}
