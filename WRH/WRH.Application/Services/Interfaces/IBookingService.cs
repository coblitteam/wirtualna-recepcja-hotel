﻿using WRH.Domain;
using WRH.Domain.Transfer.Booking;
using WRH.Domain.Transfer.BookingAttraction;

namespace WRH.Application.Services.Interfaces
{
    public interface IBookingService
    {
        Task<BookingModel> Initialize(bool reservation = false);
        Task<BookingModel> Update(BookingTransfer transfer, List<RoomModel> freeRooms);
        void Delete(int id);
        Task<BookingModel> CheckOut(int id);
        Task<long> Count(BookingQuery query);
        Task<BookingModel> GetBooking(int id);
        Task<BookingModel> GetBooking(string stayNumber);
        Task<List<BookingModel>> GetBookingList(BookingQuery query);
        Task<BookingModel?> AssignAttraction(BookingAttractionTransfer transfer);
        Task RemoveAttraction(int id);
        Task<BookingRoomModel> SetRoomIsPaid(int id);
        Task<BookingAttractionModel> SetAttractionIsPaid(int id);
        Task<List<BookingPaymentSummaryTransfer>> GetPaymentSummary(int id);
        Task<int> SetIsPaidByPaymentNumber(int paymentId, string paymentNumber);
    }
}
