﻿using WRH.Domain;

namespace WRH.Application.Services.Interfaces
{
    public interface IConfigurationService
    {
        Task<ConfigurationModel> Get();

        Task<ConfigurationModel> Update(ConfigurationModel model);
    }
}
