﻿using Microsoft.Extensions.Configuration;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using WRH.Application.Services.Interfaces;
using WRH.Domain;

namespace WRH.Application.Services
{
    public class EmailService : IEmailService
    {
        private readonly IConfiguration _config;

        public EmailService(IConfiguration config)
        {
            _config = config;
        }

        public async Task SendBookingConfirmationGuest(BookingModel booking, string paymentNumber, string pdfPath)
        {
            var configSection = _config.GetSection("BookingConfirmationGuestEmail");

            var to = booking.PersonalData?.Email;
            var subject = configSection["Subject"].Replace("##BOOKING_NUMBER##", booking.StayNumber);
            var body = configSection["Body"].Replace("##BOOKING_NUMBER##", booking.StayNumber)
                .Replace("##FULL_NAME##", booking.PersonalData.FullPersonalName)
                .Replace("##ARRIVAL_DATE##", booking.ArrivalDate.ToString("dd.MM.yyyy"))
                .Replace("##DEPARTURE_DATE##", booking.DepartureDate?.ToString("dd.MM.yyyy"))
                .Replace("##ROOM_CAPACITY##", (booking.Rooms.FirstOrDefault()?.Room.Capacity ?? 0).ToString())
                .Replace("##EXTRA_BEDS##", (booking.Rooms.FirstOrDefault()?.Room.ExtraBeds ?? 0).ToString())
                .Replace("##ROOMS_PRICE##", booking.RoomsTotalPrice().ToString())
                .Replace("##ATTRACTIONS##", string.Join("", booking.Attractions.Where(e => e.Quantity > 0).Select(e => $"{e.Attraction.Name} ilość {e.Quantity} cena {e.Price}<br/>")))
                .Replace("##PAYMENT_NUMBER##", paymentNumber);

            await SendMail(to, subject, body, AttachmentPDF(pdfPath));
        }

        public async Task SendBookingConfirmationHotel(BookingModel booking, string hotelEmail, string paymentNumber, string pdfPath)
        {
            var configSection = _config.GetSection("BookingConfirmationHotelEmail");

            var to = hotelEmail;
            var subject = configSection["Subject"].Replace("##BOOKING_NUMBER##", booking.StayNumber);
            var body = configSection["Body"].Replace("##BOOKING_NUMBER##", booking.StayNumber)
                .Replace("##FULL_NAME##", booking.PersonalData?.FullPersonalName)
                .Replace("##ADDRESS##", booking.PersonalData?.Address)
                .Replace("##PHONE_NUMBER##", booking.PersonalData?.PhoneCountryNumber + booking.PersonalData?.PhoneNumber)
                .Replace("##EMAIL##", booking.PersonalData?.Email)
                .Replace("##ARRIVAL_DATE##", booking.ArrivalDate.ToString("dd.MM.yyyy"))
                .Replace("##DEPARTURE_DATE##", booking.DepartureDate?.ToString("dd.MM.yyyy"))
                .Replace("##ROOM_CAPACITY##", (booking.Rooms.FirstOrDefault()?.Room.Capacity ?? 0).ToString())
                .Replace("##EXTRA_BEDS##", (booking.Rooms.FirstOrDefault()?.Room.ExtraBeds ?? 0).ToString())
                .Replace("##ROOMS_PRICE##", booking.RoomsTotalPrice().ToString())
                .Replace("##ATTRACTIONS##", string.Join("", booking.Attractions.Where(e => e.Quantity > 0).Select(e => $"{e.Attraction.Name} ilość {e.Quantity} cena {e.Price}<br/>")))
                .Replace("##PAYMENT_NUMBER##", paymentNumber);

            await SendMail(to, subject, body, AttachmentPDF(pdfPath));
        }

        public async Task SendAttractionConfirmationHotel(BookingModel booking, string attractionName, string attractionPayment, string hotelEmail)
        {
            var configSection = _config.GetSection("AttractionConfirmationHotelEmail");

            var to = hotelEmail;
            var subject = configSection["Subject"].Replace("##BOOKING_NUMBER##", booking.StayNumber);
            var body = configSection["Body"].Replace("##BOOKING_NUMBER##", booking.StayNumber)
                .Replace("##FULL_NAME##", booking.PersonalData.FullPersonalName)
                .Replace("##PHONE_NUMBER##", booking.PersonalData?.PhoneCountryNumber + booking.PersonalData?.PhoneNumber)
                .Replace("##ATTRACTION_NAME##", attractionName)
                .Replace("##PAYMENT_TYPE##", attractionPayment);

            await SendMail(to, subject, body);
        }

        public async Task SendCheckOutConfirmationHotel(BookingModel booking, string hotelEmail)
        {
            var configSection = _config.GetSection("CheckOutConfirmationHotelEmail");

            var rooms = booking.RoomsBusy.Select(e => e.Room.Number).Distinct();

            var to = hotelEmail;
            var subject = configSection["Subject"].Replace("##BOOKING_NUMBER##", booking.StayNumber);
            var body = configSection["Body"].Replace("##BOOKING_NUMBER", booking.StayNumber)
                .Replace("##ROOMS_NUMBER##", string.Join(", ", rooms));

            await SendMail(to, subject, body);
        }

        public async Task SendCheckOutConfirmationGuest(BookingModel booking)
        {
            var configSection = _config.GetSection("CheckOutConfirmationGuestEmail");

            var to = booking.PersonalData?.Email;
            var subject = configSection["Subject"];
            var body = configSection["Body"];

            await SendMail(to, subject, body);
        }

        public async Task SendMail(string to, string subject, string body, Attachment attachment = null)
        {
            MailAddress from = new MailAddress(_config.GetSection("Email")["Sender"], _config.GetSection("Email")["SenderName"]);
            MailAddress toMail = new MailAddress(to);

            MailMessage message = new MailMessage(from, toMail);
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.Body = body;

            if (attachment != null)
                message.Attachments.Add(attachment);

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12;

            SmtpClient client = new SmtpClient(_config.GetSection("Email")["SmtpServer"], Convert.ToInt32(_config.GetSection("Email")["SmtpPort"]));
            client.EnableSsl = false;
            client.Credentials = new NetworkCredential(_config.GetSection("Email")["Username"], _config.GetSection("Email")["Password"]);

            client.Send(message);
        }

        private static Attachment AttachmentPDF(string path)
        {
            Attachment attachment = new Attachment(path, MediaTypeNames.Application.Pdf);
            ContentDisposition disposition = attachment.ContentDisposition;
            disposition.CreationDate = File.GetCreationTime(path);
            disposition.ModificationDate = File.GetLastWriteTime(path);
            disposition.ReadDate = File.GetLastAccessTime(path);
            disposition.FileName = Path.GetFileName(path);
            disposition.Size = new FileInfo(path).Length;
            disposition.DispositionType = DispositionTypeNames.Attachment;

            return attachment;
        }

    }
}
