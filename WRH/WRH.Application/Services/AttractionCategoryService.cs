﻿using WRH.Application.Services.Interfaces;
using WRH.Domain;
using WRH.Domain.Transfer.AttractionCategory;
using WRH.Repository.Interfaces;

namespace WRH.Application.Services
{
    public class AttractionCategoryService : IAttractionCategoryService
    {
        private readonly IAttractionCategoryRepository _attractionCategoryRepository;

        public AttractionCategoryService(IAttractionCategoryRepository attractionCategoryRepository)
        {
            _attractionCategoryRepository = attractionCategoryRepository;
        }

        public async Task<AttractionCategoryModel> Create(AttractionCategoryTransfer transfer)
        {
            var model = AttractionCategoryModel.CreateData(transfer);

            _attractionCategoryRepository.Save(model);
            await _attractionCategoryRepository.SaveChangesAsync();

            return model;
        }

        public async Task<AttractionCategoryModel> Update(AttractionCategoryTransfer transfer)
        {
            var model = await _attractionCategoryRepository.FindAsync(transfer.Id);
            model.OverrideData(transfer);

            _attractionCategoryRepository.Save(model);
            await _attractionCategoryRepository.SaveChangesAsync();

            return model;
        }

        public void Delete(int id)
        {
            var model = _attractionCategoryRepository.Find(id);

            _attractionCategoryRepository.Remove(model);
            _attractionCategoryRepository.SaveChanges();
        }

        public async Task<List<AttractionCategoryModel>> ActiveList() =>
            (await _attractionCategoryRepository.FindAsync(e => e.IsActive == true)).ToList();

        public async Task<List<AttractionCategoryModel>> AllList() =>
            (await _attractionCategoryRepository.FindAllAsync()).ToList();

    }
}
