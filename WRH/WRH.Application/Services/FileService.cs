﻿using Microsoft.AspNetCore.Http;
using WRH.Application.Services.Interfaces;

namespace WRH.Application.Services
{
    public class FileService : IFileService
    {
        private readonly IImageService _imageService;

        public FileService(IImageService imageService)
        {
            _imageService = imageService;
        }

        public string Save(IFormFile file, string destination)
        {
            var fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
            var path = Path.Combine(destination, fileName);

            using (Stream stream = file.OpenReadStream())
                _imageService.ResizeImage(stream, path, 380, 220, false);

            //using (Stream stream = new FileStream(path, FileMode.Create))
            //    file.CopyTo(stream);

            return fileName;
        }
    }
}
