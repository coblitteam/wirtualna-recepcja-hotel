﻿using CommonComponents.QueryCriteria.Filtering;
using CommonComponents.Sort;
using System.ComponentModel;
using WRH.Application.Services.Interfaces;
using WRH.Domain;
using WRH.Domain.Transfer.News;
using WRH.Repository.Interfaces;

namespace WRH.Application.Services
{
    public class NewsService : INewsService
    {
        private readonly INewsRepository _newsRepository;
        private readonly INewsCategoryRepository _newsCategoryRepository;
        private readonly IFileService _fileService;

        public NewsService(INewsRepository newsRepository, INewsCategoryRepository newsCategoryRepository,
            IFileService fileService)
        {
            _newsRepository = newsRepository;
            _newsCategoryRepository = newsCategoryRepository;
            _fileService = fileService;
        }

        public async Task<NewsModel> Create(NewsTransfer transfer, string fileDestination)
        {
            var category = await _newsCategoryRepository.FindAsync(transfer.CategoryId);

            var model = NewsModel.CreateData(transfer, category);

            if (transfer.Photo != null)
            {
                var path = "Images/News";
                var fileName = _fileService.Save(transfer.Photo, Path.Combine(fileDestination, "wwwroot", path));
                model.Photo = $"{path}/{fileName}";
            }

            _newsRepository.Save(model);
            await _newsRepository.SaveChangesAsync();

            return model;
        }

        public async Task<NewsModel> Update(NewsTransfer transfer, string fileDestination)
        {
            var model = await _newsRepository.FindAsync(transfer.Id);
            model.OverrideData(transfer);

            if (transfer.Photo != null)
            {
                var path = "Images/News";
                var fileName = _fileService.Save(transfer.Photo, Path.Combine(fileDestination, "wwwroot", path));
                model.Photo = $"{path}/{fileName}";
            }

            _newsRepository.Save(model);
            await _newsRepository.SaveChangesAsync();

            return model;
        }

        public void Delete(int id)
        {
            var model = _newsRepository.Find(id);

            _newsRepository.Remove(model);
            _newsRepository.SaveChanges();
        }

        public async Task<long> Count(NewsQuery query) =>
            await _newsRepository.CountAsync(InitSpecification(query).GetPredicate());

        public async Task<NewsModel> GetNews(int id) =>
            await _newsRepository.FindAsync(id);

        public async Task<List<NewsModel>> GetNewsList(NewsQuery query) =>
            (await _newsRepository.FindAsync(
                    InitSpecification(query).GetPredicate(),
                    query.Start,
                    query.Size,
                    InitSort(query)
                )).ToList();

        public async Task<List<NewsModel>> ListByCategory(int categoryId) =>
            (await _newsRepository.FindAsync(e => e.CategoryId == categoryId && e.IsActive)).ToList();

        private AbstractSpecification<NewsModel> InitSpecification(NewsQuery query)
        {
            AbstractSpecification<NewsModel> specification = new Specification<NewsModel>(e => true);

            if (!string.IsNullOrEmpty(query.Search))
                specification &= new Specification<NewsModel>(e => e.Name.Contains(query.Search));

            if (query.CategoryId.HasValue)
                specification &= new Specification<NewsModel>(e => e.CategoryId == query.CategoryId);

            if (query.IsActive.HasValue)
                specification &= new Specification<NewsModel>(e => e.IsActive == query.IsActive);

            return specification;
        }

        private SortSpecification<NewsModel> InitSort(NewsQuery query)
        {
            var sortDirection = (query.OrderDestination == "asc") ? ListSortDirection.Ascending : ListSortDirection.Descending;

            SortSpecification<NewsModel> sortQuery;
            switch (query.OrderBy)
            {
                case "name":
                    sortQuery = new SortSpecification<NewsModel>(e => e.Name, sortDirection);
                    break;
                case "category":
                    sortQuery = new SortSpecification<NewsModel>(e => e.Category.Name, sortDirection);
                    break;
                case "isActive":
                    sortQuery = new SortSpecification<NewsModel>(e => e.IsActive, sortDirection);
                    break;
                default:
                    sortQuery = new SortSpecification<NewsModel>(e => e.Name, ListSortDirection.Ascending);
                    break;
            }

            return sortQuery;
        }
    }
}
