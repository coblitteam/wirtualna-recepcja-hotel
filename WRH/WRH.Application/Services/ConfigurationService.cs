﻿using WRH.Application.Services.Interfaces;
using WRH.Domain;
using WRH.Repository.Interfaces;

namespace WRH.Application.Services
{
    public class ConfigurationService : IConfigurationService
    {
        private readonly IConfigurationRepository _configurationRepository;

        public ConfigurationService(IConfigurationRepository configurationRepository)
        {
            _configurationRepository = configurationRepository;
        }

        public async Task<ConfigurationModel> Get() =>
            await _configurationRepository.FirstAsync();

        public async Task<ConfigurationModel> Update(ConfigurationModel model)
        {
            var data = await Get();
            data.OverrideData(model);

            _configurationRepository.Save(data);
            await _configurationRepository.SaveChangesAsync();

            return data;
        }
    }
}
