﻿using CommonComponents.QueryCriteria.Filtering;
using WRH.Application.Services.Interfaces;
using WRH.Domain;
using WRH.Domain.Transfer.Room;
using WRH.Domain.Transfer.RoomBusy;
using WRH.Repository.Interfaces;

namespace WRH.Application.Services
{
    public class RoomService : IRoomService
    {
        private readonly IRoomRepository _roomRepository;
        private readonly IRoomBusyRepository _busyRepository;

        public RoomService(IRoomRepository roomRepository, IRoomBusyRepository busyRepository)
        {
            _roomRepository = roomRepository;
            _busyRepository = busyRepository;
        }

        public async Task<List<RoomModel>> GetAllAsync() =>
            await _roomRepository.FindAllAsync();

        public async Task<RoomModel> GetByIdAsync(int id) =>
            await _roomRepository.FindAsync(id);

        public async Task<RoomModel> Update(RoomTransferModel transfer)
        {
            var room = await GetByIdAsync(transfer.Id);

            room.OverrideData(transfer);

            _roomRepository.Save(room);
            await _roomRepository.SaveChangesAsync();

            return room;
        }

        public async Task<RoomBusyModel> SetBusy(RoomBusySetModel transfer)
        {
            var date = DateTime.Parse(transfer.Date);
            var room = await GetByIdAsync(transfer.RoomId);
            var busy = room?.Busy.FirstOrDefault(e => e.BusyDateFrom == date);

            if (busy == null)
            {
                busy = new RoomBusyModel
                {
                    RoomId = transfer.RoomId,
                    BusyDateFrom = date,
                    BusyDateTo = date.AddDays(1),
                };

                _busyRepository.Save(busy);
            }
            else
                _busyRepository.Remove(busy);

            await _busyRepository.SaveChangesAsync();

            return busy;
        }

        public async Task CompleteBusy(BookingModel booking)
        {
            foreach (var busy in booking.RoomsBusy)
            {
                busy.PendingExpiration = null;
                busy.Status = Domain.Enums.BusyStatus.Confirmed;
                _busyRepository.Save(busy);
            }

            await _busyRepository.SaveChangesAsync();
        }

        public async Task<List<RoomModel>> GetMatchRooms(RoomMatchQueryModel query)
        {
            var data = await _roomRepository.FindAsync(InitMatchingSpecification(query).GetPredicate());

            return data.ToList();
        }

        public async Task RemoveBusyPendingExpired()
        {
            var busies = await _busyRepository.FindAsync(e => e.Status == Domain.Enums.BusyStatus.Pending && e.PendingExpiration < DateTime.Now);

            _busyRepository.Remove(busies);
            await _busyRepository.SaveChangesAsync();
        }

        private AbstractSpecification<RoomModel> InitMatchingSpecification(RoomMatchQueryModel query)
        {
            AbstractSpecification<RoomModel> specification = new Specification<RoomModel>(e => e.IsActive);

            specification &= new Specification<RoomModel>(e => !e.Busy.Any(x => x.BusyDateFrom >= query.ArrivalDate && x.BusyDateFrom <= query.DepartureDate));

            return specification;
        }
    }
}
