﻿using WRH.Application.Services.Interfaces;
using WRH.Domain;
using WRH.Domain.Transfer.GuestData;
using WRH.Repository.Interfaces;

namespace WRH.Application.Services
{
    public class GuestDataService : IGuestDataService
    {
        private readonly IGuestDataRepository _guestDataRepository;

        public GuestDataService(IGuestDataRepository guestDataRepository)
        {
            _guestDataRepository = guestDataRepository;
        }

        public async Task<GuestDataModel> Create(GuestDataTransfer transfer)
        {
            var model = GuestDataModel.Create(transfer);

            _guestDataRepository.Save(model);
            await _guestDataRepository.SaveChangesAsync();

            return model;
        }

        public async Task Update(GuestDataTransfer transfer)
        {
            var model = await _guestDataRepository.FindAsync(transfer.Id ?? 0);

            if (model is null)
                return;

            model.Override(transfer);

            _guestDataRepository.Save(model);
            await _guestDataRepository.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var model = await _guestDataRepository.FindAsync(id);

            _guestDataRepository.Remove(model);
            await _guestDataRepository.SaveChangesAsync();
        }
    }
}
