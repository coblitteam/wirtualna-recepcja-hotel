﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WRH.Application.Services.Interfaces;
using WRH.Domain;
using WRH.Repository.Interfaces;

namespace WRH.Application.Services
{
    public class InvoiceNumberService : IInvoiceNumberService
    {
        private readonly IInvoiceNumberRepository _invoiceNumberRepository;

        public InvoiceNumberService(IInvoiceNumberRepository invoiceNumberRepository)
        {
            _invoiceNumberRepository = invoiceNumberRepository;
        }

        public async Task<int> LastNumber()
        {
            var month = DateTime.Now.Month;
            var year = DateTime.Now.Year;

            var invoices = await _invoiceNumberRepository.FindAsync(e => e.Month == month && e.Year == e.Year);

            return invoices.Count > 0 ? invoices.Max(e => e.Number) : 0;
        }

        public async Task Save(int bookingId, int number)
        {
            var model = new InvoiceNumberModel
            {
                BookingId = bookingId,
                Number = number,
                Month = DateTime.Now.Month,
                Year = DateTime.Now.Year
            };

            _invoiceNumberRepository.Save(model);
            await _invoiceNumberRepository.SaveChangesAsync();
        }
    }
}
