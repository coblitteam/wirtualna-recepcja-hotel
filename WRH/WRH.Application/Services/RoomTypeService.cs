﻿using WRH.Application.Services.Interfaces;
using WRH.Domain;
using WRH.Domain.Transfer.RoomType;
using WRH.Repository.Interfaces;

namespace WRH.Application.Services
{
    public class RoomTypeService : IRoomTypeService
    {
        private readonly IRoomTypeRepository _roomRepository;
        private readonly IFileService _fileService;

        public RoomTypeService(IRoomTypeRepository roomRepository, IFileService fileService)
        {
            _roomRepository = roomRepository;
            _fileService = fileService;
        }

        public async Task<List<RoomTypeModel>> GetAllAsync() =>
            await _roomRepository.FindAllAsync();

        public async Task<RoomTypeModel> GetAsync(int id) =>
            await _roomRepository.FindAsync(id);

        public async Task<RoomTypeModel> Update(RoomTypeTransferModel transfer, string fileDestination)
        {
            var type = await GetAsync(transfer.Id);

            type.OverrideData(transfer);

            if (transfer.Photo != null)
            {
                var path = "Images/RoomType";
                var fileName = _fileService.Save(transfer.Photo, Path.Combine(fileDestination, "wwwroot", path));
                type.Photo = $"{path}/{fileName}";
            }

            _roomRepository.Save(type);
            await _roomRepository.SaveChangesAsync();

            return type;
        }
    }
}
