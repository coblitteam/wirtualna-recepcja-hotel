﻿using CommonComponents.QueryCriteria.Filtering;
using CommonComponents.Sort;
using System.ComponentModel;
using WRH.Application.Services.Interfaces;
using WRH.Domain;
using WRH.Domain.Transfer.Booking;
using WRH.Domain.Transfer.BookingAttraction;
using WRH.Repository.Interfaces;

namespace WRH.Application.Services
{
    public class BookingService : IBookingService
    {
        private readonly IBookingRepository _bookingRepository;
        private readonly IBookingRoomRepository _roomRepository;
        private readonly IRoomTypeRepository _roomTypeRepository;
        private readonly IBookingAttractionRepository _bookingAttractionRepository;
        private readonly IBookingRoomRepository _bookingRoomRepository;
        private readonly IAttractionRepository _attractionRepository;
        private readonly IGuestDataRepository _guestDataRepository;

        public BookingService(IBookingRepository bookingRepository, IBookingRoomRepository roomRepository,
            IBookingAttractionRepository bookingAttractionRepository, IAttractionRepository attractionRepository,
            IBookingRoomRepository bookingRoomRepository, IRoomTypeRepository roomTypeRepository,
            IGuestDataRepository guestDataRepository)
        {
            _bookingRepository = bookingRepository;
            _roomRepository = roomRepository;
            _roomTypeRepository = roomTypeRepository;
            _bookingAttractionRepository = bookingAttractionRepository;
            _bookingRoomRepository = bookingRoomRepository;
            _attractionRepository = attractionRepository;
            _guestDataRepository = guestDataRepository;
        }

        public async Task<BookingModel> Initialize(bool reservation = false)
        {
            var booking = new BookingModel
            {
                BookingNumber = (reservation) ? GenerateBookingNumber() : string.Empty,
                StayNumber = GenerateStayNumber(),
                CreatedAt = DateTime.Now,
                ArrivalDate = DateTime.Today
            };

            _bookingRepository.Save(booking);
            await _bookingRepository.SaveChangesAsync();

            var defaultAttractions = _attractionRepository.Find(e => e.Category.IsDefault).Select(e => new BookingAttractionModel
            {
                BookingId = booking.Id,
                AttractionId = e.Id,
                Quantity = 0,
                Price = e.GrosPrice,
                IsPaid = false,
                CreatedAt = DateTime.Now
            }).ToList();

            _bookingAttractionRepository.Save(defaultAttractions);
            await _bookingAttractionRepository.SaveChangesAsync();

            booking.Attractions = defaultAttractions;

            return booking;
        }

        public async Task<BookingModel> Update(BookingTransfer transfer, List<RoomModel> freeRooms)
        {
            if (transfer == null)
                throw new ArgumentNullException(nameof(transfer));

            var booking = _bookingRepository.Find(transfer.Id);

            booking.ArrivalDate = transfer.ArrivalDate.ToLocalTime();
            booking.DepartureDate = transfer.DepartureDate.ToLocalTime();
            booking.NumberOfAdults = transfer.NumberOfAdults;
            booking.NumberOfChildren = transfer.NumberOfChildren;
            booking.NumberOfChildrenToThree = transfer.NumberOfChildrenToThree;
            booking.Board = transfer.Board;

            if (booking.PersonalData != null)
                booking.PersonalData.OverridePersonalData(transfer.PersonalData);
            else
                booking.PersonalData = CustomerDataModel.CreatePersonalData(transfer.PersonalData);

            if (transfer.CompanyData != null)
                if (booking.CompanyData != null)
                    booking.CompanyData.OverrideCompanyData(transfer.CompanyData);
                else
                    booking.CompanyData = CustomerDataModel.CreateCompanyData(transfer.CompanyData);

            booking.GuestsData = transfer.Guests.Select(e =>
            {
                if (e.Id.HasValue)
                {
                    var guest = _guestDataRepository.Find(e.Id.Value);
                    guest.Override(e);

                    return guest;
                }
                else return GuestDataModel.Create(e);
            }).ToList();

            var paymentId = GeneratePaymentId();
            booking.Rooms = transfer.Rooms.Select(e =>
            {
                var room = _roomTypeRepository.Find(e.RoomId);

                if (e.Id.HasValue)
                {
                    var bookingRoom = _roomRepository.Find(e.Id.Value);
                    bookingRoom.Override(e);

                    if (!bookingRoom.IsPaid)
                        bookingRoom.PaymentId = paymentId;

                    return bookingRoom;
                }
                else
                    return BookingRoomModel.Create(e, paymentId, room);
            }).ToList();
            booking.Attractions = transfer.Attractions.Select(e =>
            {
                var attraction = _attractionRepository.Find(e.AttractionId);

                if (e.Id.HasValue && e.Id > 0)
                {
                    var bookingAttraction = _bookingAttractionRepository.Find(e.Id.Value);
                    bookingAttraction.Override(e);

                    if (!bookingAttraction.IsPaid && attraction.ImmediatePayment)
                        bookingAttraction.PaymentId = paymentId;

                    return bookingAttraction;
                }
                else
                {
                    return BookingAttractionModel.Create(e, paymentId, attraction);
                }
            }).Where(e => e.Quantity > 0).ToList();
            booking.RoomsBusy = RoomBusyModel.MakeBusies(booking, freeRooms);

            if (transfer.IsAdmin.HasValue)
            {
                booking.LastEditUserName = "Admin";
                booking.LastEditTimeStamp = DateTime.Now;
            }

            _bookingRepository.Save(booking);
            await _bookingRepository.SaveChangesAsync();

            return booking;
        }

        public void Delete(int id)
        {
            var model = _bookingRepository.Find(id);

            _bookingRepository.Remove(model);
            _bookingRepository.SaveChanges();
        }

        public async Task<BookingModel> CheckOut(int id)
        {
            var booking = await _bookingRepository.FindAsync(id);
            booking.IsCheckedOut = true;

            _bookingRepository.Save(booking);
            await _bookingRepository.SaveChangesAsync();

            return booking;
        }

        public async Task<long> Count(BookingQuery query) =>
            await _bookingRepository.CountAsync(InitSpecification(query).GetPredicate());

        public async Task<BookingModel> GetBooking(int id) =>
            await _bookingRepository.FindAsync(id);

        public async Task<BookingModel> GetBooking(string stayNumber) =>
            await _bookingRepository.FirstAsync(e => e.StayNumber == stayNumber);

        public async Task<List<BookingModel>> GetBookingList(BookingQuery query)
        {
            var data = await _bookingRepository.FindAsync(
                    InitSpecification(query).GetPredicate(),
                    query.Start,
                    query.Size,
                    InitSort(query)
                );

            return data.ToList();
        }

        public async Task<BookingModel?> AssignAttraction(BookingAttractionTransfer transfer)
        {
            var booking = await _bookingRepository.FirstAsync(e => e.StayNumber == transfer.StayNumber && !e.IsCheckedOut);

            if (booking == null)
                return null;

            transfer.BookingId = booking.Id;

            booking.Attractions.Add(BookingAttractionModel.Create(transfer));

            _bookingRepository.Save(booking);
            await _bookingRepository.SaveChangesAsync();

            return booking;
        }

        public async Task RemoveAttraction(int id)
        {
            var attraction = await _bookingAttractionRepository.FindAsync(id);
            _bookingAttractionRepository.Remove(attraction);

            await _bookingAttractionRepository.SaveChangesAsync();
        }

        public async Task<BookingRoomModel> SetRoomIsPaid(int id)
        {
            var room = await _roomRepository.FindAsync(id);
            room.IsPaid = !room.IsPaid;

            _roomRepository.Save(room);
            await _roomRepository.SaveChangesAsync();

            return room;
        }

        public async Task<BookingAttractionModel> SetAttractionIsPaid(int id)
        {
            var attraction = await _bookingAttractionRepository.FindAsync(id);
            attraction.IsPaid = !attraction.IsPaid;

            _bookingAttractionRepository.Save(attraction);
            await _bookingAttractionRepository.SaveChangesAsync();

            return attraction;
        }

        public async Task<List<BookingPaymentSummaryTransfer>> GetPaymentSummary(int id)
        {
            var summary = new List<BookingPaymentSummaryTransfer>();

            var rooms = await _roomRepository.FindAsync(e => e.PaymentId == id);
            var attractions = await _bookingAttractionRepository.FindAsync(e => e.PaymentId == id);

            var days = rooms.FirstOrDefault()?.Booking.Days() ?? 0;

            var roomsSummary = rooms.Select(r => new BookingPaymentSummaryTransfer
            {
                Name = r.Room.Name,
                Quantity = r.Quantity,
                UnitPrice = r.Price * days,
            });
            summary.AddRange(roomsSummary);

            var attractionsSummary = attractions.Select(r => new BookingPaymentSummaryTransfer
            {
                Name = r.Attraction.Name,
                Quantity = r.Quantity,
                UnitPrice = r.Price
            });
            summary.AddRange(attractionsSummary);

            return summary;
        }

        public async Task<int> SetIsPaidByPaymentNumber(int paymentId, string paymentNumber)
        {
            var bookingId = 0;

            var rooms = await _bookingRoomRepository.FindAsync(e => e.PaymentId == paymentId);
            foreach (var room in rooms)
            {
                room.IsPaid = true;
                room.PaymentNumber = paymentNumber;
                _bookingRoomRepository.Save(room);

                if (bookingId == 0)
                    bookingId = room.BookingId;
            }

            var attractions = await _bookingAttractionRepository.FindAsync(e => e.PaymentId == paymentId);
            foreach (var attraction in attractions)
            {
                attraction.IsPaid = true;
                attraction.PaymentNumber = paymentNumber;
                _bookingAttractionRepository.Save(attraction);

                if (bookingId == 0)
                    bookingId = attraction.BookingId;
            }

            await _bookingRoomRepository.SaveChangesAsync();
            await _bookingAttractionRepository.SaveChangesAsync();

            return bookingId;
        }

        private string GenerateStayNumber()
        {
            var rand = new Random();
            var number = rand.Next(1000).ToString().PadLeft(3, '0');

            return $"{DateTime.Now:yyMMdd}{number}";
        }

        private string GenerateBookingNumber()
        {
            var rand = new Random();
            var chars = "0123456789";

            return new string(Enumerable.Repeat(chars, 11).Select(s => s[rand.Next(chars.Length)]).ToArray());
        }

        private int GeneratePaymentId() => new Random().Next(100000, 999999);

        private AbstractSpecification<BookingModel> InitSpecification(BookingQuery query)
        {
            AbstractSpecification<BookingModel> specification = new Specification<BookingModel>(e => e.DepartureDate.HasValue);

            if (!string.IsNullOrEmpty(query.Search) && query.Search.Length > 3)
                specification &= new Specification<BookingModel>(e => e.BookingNumber.Contains(query.Search.Trim()) || e.StayNumber.Contains(query.Search.Trim()));

            if (query.ArrivalDateFrom.HasValue)
                specification &= new Specification<BookingModel>(e => e.ArrivalDate.Date >= query.ArrivalDateFrom.Value.Date);

            if (query.ArrivalDateTo.HasValue)
                specification &= new Specification<BookingModel>(e => e.ArrivalDate.Date <= query.ArrivalDateTo.Value.Date);

            if (query.DepartureDateFrom.HasValue)
                specification &= new Specification<BookingModel>(e => e.DepartureDate.Value.Date >= query.DepartureDateFrom.Value.Date);

            if (query.DepartureDateTo.HasValue)
                specification &= new Specification<BookingModel>(e => e.DepartureDate.Value.Date <= query.DepartureDateTo.Value.Date);

            if (query.RoomType.HasValue)
                specification &= new Specification<BookingModel>(e => e.Rooms.FirstOrDefault(x => x.RoomId == query.RoomType.Value) != null);

            if (query.PriceFrom.HasValue)
                specification &= new Specification<BookingModel>(e => e.Rooms.Sum(x => x.Price) + e.Attractions.Sum(x => x.Price) >= query.PriceFrom.Value);

            if (query.PriceTo.HasValue)
                specification &= new Specification<BookingModel>(e => e.Rooms.Sum(x => x.Price) + e.Attractions.Sum(x => x.Price) <= query.PriceTo.Value);

            if (query.Status.HasValue)
                switch (query.Status.Value)
                {
                    case Domain.Enums.BookingStatus.CheckedOut:
                        specification &= new Specification<BookingModel>(e => e.IsCheckedOut);
                        break;
                    case Domain.Enums.BookingStatus.NotStarted:
                        specification &= new Specification<BookingModel>(e => !e.IsCheckedOut && e.ArrivalDate > DateTime.Today);
                        break;
                    case Domain.Enums.BookingStatus.InProgress: 
                        specification &= new Specification<BookingModel>(e => !e.IsCheckedOut && e.ArrivalDate <= DateTime.Today && e.DepartureDate > DateTime.Today);
                        break;
                    case Domain.Enums.BookingStatus.Undone:
                        specification &= new Specification<BookingModel>(e => !e.IsCheckedOut && e.DepartureDate < DateTime.Today && (!e.Rooms.All(x => x.IsPaid) || !e.Attractions.All(x => x.IsPaid)));
                        break;
                    case Domain.Enums.BookingStatus.Done:
                        specification &= new Specification<BookingModel>(e => !e.IsCheckedOut && e.DepartureDate < DateTime.Today && e.Rooms.All(x => x.IsPaid) && e.Attractions.All(x => x.IsPaid));
                        break;
                }

            return specification;
        }

        private SortSpecification<BookingModel> InitSort(BookingQuery query)
        {
            var sortDirection = query.OrderDestination == "asc" ? ListSortDirection.Ascending : ListSortDirection.Descending;

            SortSpecification<BookingModel> sortQuery;

            switch (query.OrderBy)
            {
                case "bookingNumber":
                    sortQuery = new SortSpecification<BookingModel>(e => e.BookingNumber, sortDirection);
                    break;
                case "stayNumber":
                    sortQuery = new SortSpecification<BookingModel>(e => e.StayNumber, sortDirection);
                    break;
                case "createdAt":
                    sortQuery = new SortSpecification<BookingModel>(e => e.CreatedAt, sortDirection);
                    break;
                case "arrivalDate":
                    sortQuery = new SortSpecification<BookingModel>(e => e.ArrivalDate, sortDirection);
                    break;
                case "departureDate":
                    sortQuery = new SortSpecification<BookingModel>(e => e.DepartureDate, sortDirection);
                    break;
                case "client":
                    sortQuery = new SortSpecification<BookingModel>(e => e.PersonalData.Surname, sortDirection);
                    break;
                case "rooms":
                    sortQuery = new SortSpecification<BookingModel>(e => e.Rooms.FirstOrDefault().Room.Name, sortDirection);
                    break;
                case "price":
                    sortQuery = new SortSpecification<BookingModel>(e => e.Rooms.Sum(x => x.Price) + e.Attractions.Sum(x => x.Price), sortDirection);
                    break;
                default:
                    sortQuery = new SortSpecification<BookingModel>(e => e.ArrivalDate, ListSortDirection.Ascending);
                    break;
            }

            return sortQuery;
        }
    }
}
