﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WRH.Domain;

namespace WRH.Database.Seed
{
    public static class DefaultData
    {
        public static void SeedAll(this ModelBuilder modelBuilder)
        {
            modelBuilder.SeedConfiguration();
            modelBuilder.SeedRooms();
            modelBuilder.SeedRoomTypes();
            modelBuilder.SeedAttracionCategories();
            modelBuilder.SeedAttractions();
            //modelBuilder.SeedNewsCategories();
            //modelBuilder.SeedNews();
            modelBuilder.SeedUsers();
        }

        public static void SeedConfiguration(this ModelBuilder modelBuilder) =>
            modelBuilder.Entity<ConfigurationModel>().HasData(ConfigurationModel.DefaultData());

        public static void SeedRooms(this ModelBuilder modelBuilder) =>
            modelBuilder.Entity<RoomModel>().HasData(RoomModel.DefaultData());

        public static void SeedRoomTypes(this ModelBuilder modelBuilder) =>
            modelBuilder.Entity<RoomTypeModel>().HasData(RoomTypeModel.DefaultData());

        public static void SeedAttracionCategories(this ModelBuilder modelBuilder) =>
            modelBuilder.Entity<AttractionCategoryModel>().HasData(AttractionCategoryModel.DefaultData());

        public static void SeedAttractions(this ModelBuilder modelBuilder) =>
            modelBuilder.Entity<AttractionModel>().HasData(AttractionModel.DefaultData());

        public static void SeedNewsCategories(this ModelBuilder modelBuilder) =>
            modelBuilder.Entity<NewsCategoryModel>().HasData(NewsCategoryModel.DefaultData());

        public static void SeedNews(this ModelBuilder modelBuilder) =>
            modelBuilder.Entity<NewsModel>().HasData(NewsModel.DefaultData());

        public static void SeedUsers(this ModelBuilder modelBuilder) =>
            modelBuilder.Entity<UserModel>().HasData(UserModel.DefaultData());
    }
}
