﻿using CommonComponents.Context;
using Microsoft.EntityFrameworkCore;
using WRH.Database.Seed;
using WRH.Domain;

namespace WRH.Database
{
    public class DataContext : DbContext, IDataContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public virtual DbSet<AttractionModel> Attractions { get; set; }
        public virtual DbSet<AttractionCategoryModel> AttractionCategories { get; set; }

        public virtual DbSet<BookingModel> Bookings { get; set; }
        public virtual DbSet<BookingAttractionModel> BookingsAttractions { get; set; }
        public virtual DbSet<BookingRoomModel> BookingsRooms { get; set; }

        public virtual DbSet<ConfigurationModel> Configurations { get; set; }
        public virtual DbSet<CustomerDataModel> CustomersData { get; set; }

        public virtual DbSet<GuestDataModel> GuestsData { get; set; }

        public virtual DbSet<InvoiceNumberModel> InvoiceNumbers { get; set; }

        public virtual DbSet<NewsModel> News { get; set; }
        public virtual DbSet<NewsCategoryModel> NewsCategories { get; set; }

        public virtual DbSet<RoomModel> Rooms { get; set; }
        public virtual DbSet<RoomBusyModel> RoomsBusy { get; set; }
        public virtual DbSet<RoomTypeModel> RoomTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) =>
            modelBuilder.SeedAll();

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }
    }
}
