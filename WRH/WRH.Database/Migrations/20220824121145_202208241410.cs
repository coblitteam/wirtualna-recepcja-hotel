﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WRH.Database.Migrations
{
    public partial class _202208241410 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "BreakfastPrice",
                table: "RoomType",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "FullBoardPrice",
                table: "RoomType",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "PriceOnce",
                table: "RoomType",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "RoomType",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BreakfastPrice", "FullBoardPrice", "PriceOnce" },
                values: new object[] { 36m, 74m, 182.0m });

            migrationBuilder.UpdateData(
                table: "RoomType",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BreakfastPrice", "FullBoardPrice", "PriceOnce" },
                values: new object[] { 36m, 74m, 286.0m });

            migrationBuilder.UpdateData(
                table: "RoomType",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BreakfastPrice", "FullBoardPrice", "PriceOnce" },
                values: new object[] { 36m, 74m, 234.0m });

            migrationBuilder.UpdateData(
                table: "RoomType",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BreakfastPrice", "FullBoardPrice", "PriceOnce" },
                values: new object[] { 36m, 74m, 234.0m });

            migrationBuilder.UpdateData(
                table: "RoomType",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BreakfastPrice", "FullBoardPrice", "PriceOnce" },
                values: new object[] { 36m, 74m, 182.0m });

            migrationBuilder.UpdateData(
                table: "RoomType",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BreakfastPrice", "FullBoardPrice", "PriceOnce" },
                values: new object[] { 36m, 74m, 286.0m });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BreakfastPrice",
                table: "RoomType");

            migrationBuilder.DropColumn(
                name: "FullBoardPrice",
                table: "RoomType");

            migrationBuilder.DropColumn(
                name: "PriceOnce",
                table: "RoomType");
        }
    }
}
