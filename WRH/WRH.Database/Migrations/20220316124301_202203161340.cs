﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WRH.Database.Migrations
{
    public partial class _202203161340 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AttractionCategory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Photo = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttractionCategory", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "AttractionCategory",
                columns: new[] { "Id", "IsActive", "Name", "Photo" },
                values: new object[] { 1, true, "Kategoria 1", "" });

            migrationBuilder.InsertData(
                table: "AttractionCategory",
                columns: new[] { "Id", "IsActive", "Name", "Photo" },
                values: new object[] { 2, true, "Kategoria 2", "" });

            migrationBuilder.InsertData(
                table: "AttractionCategory",
                columns: new[] { "Id", "IsActive", "Name", "Photo" },
                values: new object[] { 3, true, "Kategoria 3", "" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AttractionCategory");
        }
    }
}
