﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WRH.Database.Migrations
{
    public partial class _202205051610 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "NotificationEmailAdresses",
                table: "Attraction",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NotificationGuestMessage",
                table: "Attraction",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NotificationHotelMessage",
                table: "Attraction",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NotificationEmailAdresses",
                table: "Attraction");

            migrationBuilder.DropColumn(
                name: "NotificationGuestMessage",
                table: "Attraction");

            migrationBuilder.DropColumn(
                name: "NotificationHotelMessage",
                table: "Attraction");
        }
    }
}
