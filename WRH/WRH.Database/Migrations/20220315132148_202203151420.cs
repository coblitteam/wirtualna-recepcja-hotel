﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WRH.Database.Migrations
{
    public partial class _202203151420 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CompanyName",
                table: "CustomerData",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Nip",
                table: "CustomerData",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CompanyName",
                table: "CustomerData");

            migrationBuilder.DropColumn(
                name: "Nip",
                table: "CustomerData");
        }
    }
}
