﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WRH.Database.Migrations
{
    public partial class _202208242305 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Attraction",
                keyColumn: "Id",
                keyValue: 9997,
                columns: new[] { "ImmediatePayment", "PaymentType" },
                values: new object[] { true, 2 });

            migrationBuilder.UpdateData(
                table: "Attraction",
                keyColumn: "Id",
                keyValue: 9998,
                columns: new[] { "ImmediatePayment", "PaymentType" },
                values: new object[] { true, 2 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Attraction",
                keyColumn: "Id",
                keyValue: 9997,
                columns: new[] { "ImmediatePayment", "PaymentType" },
                values: new object[] { false, 0 });

            migrationBuilder.UpdateData(
                table: "Attraction",
                keyColumn: "Id",
                keyValue: 9998,
                columns: new[] { "ImmediatePayment", "PaymentType" },
                values: new object[] { false, 0 });
        }
    }
}
