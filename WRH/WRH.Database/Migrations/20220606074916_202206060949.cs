﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WRH.Database.Migrations
{
    public partial class _202206060949 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BookingId",
                table: "RoomBusy",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "PendingExpiration",
                table: "RoomBusy",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "RoomBusy",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_RoomBusy_BookingId",
                table: "RoomBusy",
                column: "BookingId");

            migrationBuilder.AddForeignKey(
                name: "FK_RoomBusy_Booking_BookingId",
                table: "RoomBusy",
                column: "BookingId",
                principalTable: "Booking",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RoomBusy_Booking_BookingId",
                table: "RoomBusy");

            migrationBuilder.DropIndex(
                name: "IX_RoomBusy_BookingId",
                table: "RoomBusy");

            migrationBuilder.DropColumn(
                name: "BookingId",
                table: "RoomBusy");

            migrationBuilder.DropColumn(
                name: "PendingExpiration",
                table: "RoomBusy");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "RoomBusy");
        }
    }
}
