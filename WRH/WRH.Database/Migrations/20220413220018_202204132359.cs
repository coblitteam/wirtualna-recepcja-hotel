﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WRH.Database.Migrations
{
    public partial class _202204132359 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "LastEditTimeStamp",
                table: "Booking",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastEditUserName",
                table: "Booking",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastEditTimeStamp",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "LastEditUserName",
                table: "Booking");
        }
    }
}
