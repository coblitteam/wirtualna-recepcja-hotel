﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WRH.Database.Migrations
{
    public partial class _202209082047 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "CanDelete",
                table: "AttractionCategory",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "AttractionCategory",
                columns: new[] { "Id", "CanDelete", "IsActive", "IsBoard", "IsDefault", "Name", "Photo" },
                values: new object[] { 1000, false, true, false, true, "Podatek", "" });

            migrationBuilder.InsertData(
                table: "Attraction",
                columns: new[] { "Id", "Board", "CategoryId", "Details", "Details_De", "Details_En", "Details_Es", "Details_Fr", "ImmediatePayment", "IsActive", "Name", "Name_De", "Name_En", "Name_Es", "Name_Fr", "NetPrice", "NotificationEmailAdresses", "NotificationGuestMessage", "NotificationHotelMessage", "PaymentType", "Photo", "ShortDetails", "ShortDetails_De", "ShortDetails_En", "ShortDetails_Es", "ShortDetails_Fr", "VAT" },
                values: new object[] { 9999, null, 1000, "", null, null, null, null, true, true, "Opłata klimatyczna", null, null, null, null, 3.3m, null, null, null, 2, "", "", null, null, null, null, 0m });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Attraction",
                keyColumn: "Id",
                keyValue: 9999);

            migrationBuilder.DeleteData(
                table: "AttractionCategory",
                keyColumn: "Id",
                keyValue: 1000);

            migrationBuilder.DropColumn(
                name: "CanDelete",
                table: "AttractionCategory");
        }
    }
}
