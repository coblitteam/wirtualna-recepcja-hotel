﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WRH.Database.Migrations
{
    public partial class _202204220943 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Room",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Room",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "Code", "IsActive" },
                values: new object[] { "00000", true });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "Room");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Room");
        }
    }
}
