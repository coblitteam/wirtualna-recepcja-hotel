﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WRH.Database.Migrations
{
    public partial class _202204211434 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Room",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoomTypeId = table.Column<int>(type: "int", nullable: false),
                    Number = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Room", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Room_RoomType_RoomTypeId",
                        column: x => x.RoomTypeId,
                        principalTable: "RoomType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RoomBusy",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoomId = table.Column<int>(type: "int", nullable: false),
                    BusyDateFrom = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BusyDateTo = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomBusy", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoomBusy_Room_RoomId",
                        column: x => x.RoomId,
                        principalTable: "Room",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Room",
                columns: new[] { "Id", "Number", "RoomTypeId" },
                values: new object[,]
                {
                    { 1, 1, 1 },
                    { 2, 2, 2 },
                    { 3, 3, 3 },
                    { 4, 4, 4 },
                    { 5, 5, 5 },
                    { 6, 6, 6 },
                    { 7, 7, 1 },
                    { 8, 8, 2 },
                    { 9, 9, 3 },
                    { 10, 10, 4 },
                    { 11, 11, 5 },
                    { 12, 12, 6 },
                    { 13, 13, 1 },
                    { 14, 14, 2 },
                    { 15, 15, 3 },
                    { 16, 16, 4 },
                    { 17, 17, 5 },
                    { 18, 18, 6 },
                    { 19, 19, 1 },
                    { 20, 20, 2 },
                    { 21, 21, 3 },
                    { 22, 22, 4 },
                    { 23, 23, 5 },
                    { 24, 24, 6 },
                    { 25, 25, 1 },
                    { 26, 26, 2 },
                    { 27, 27, 3 },
                    { 28, 28, 4 },
                    { 29, 29, 5 },
                    { 30, 30, 6 },
                    { 31, 31, 1 },
                    { 32, 32, 2 },
                    { 33, 33, 3 },
                    { 34, 34, 4 },
                    { 35, 35, 5 },
                    { 36, 36, 6 },
                    { 37, 37, 1 },
                    { 38, 38, 2 },
                    { 39, 39, 3 },
                    { 40, 40, 4 },
                    { 41, 41, 5 },
                    { 42, 42, 6 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Room_RoomTypeId",
                table: "Room",
                column: "RoomTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_RoomBusy_RoomId",
                table: "RoomBusy",
                column: "RoomId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RoomBusy");

            migrationBuilder.DropTable(
                name: "Room");
        }
    }
}
