﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WRH.Database.Migrations
{
    public partial class _202204281236 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "NumberOfAdults",
                table: "Booking",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NumberOfChildren",
                table: "Booking",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NumberOfAdults",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "NumberOfChildren",
                table: "Booking");
        }
    }
}
