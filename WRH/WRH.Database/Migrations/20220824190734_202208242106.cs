﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WRH.Database.Migrations
{
    public partial class _202208242106 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsBoard",
                table: "AttractionCategory",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "Board",
                table: "Attraction",
                type: "int",
                nullable: true);

            migrationBuilder.InsertData(
                table: "AttractionCategory",
                columns: new[] { "Id", "IsActive", "IsBoard", "IsDefault", "Name", "Photo" },
                values: new object[] { 999, true, true, false, "Wyżywienie", "" });

            migrationBuilder.InsertData(
                table: "Attraction",
                columns: new[] { "Id", "Board", "CategoryId", "Details", "Details_De", "Details_En", "Details_Es", "Details_Fr", "ImmediatePayment", "IsActive", "Name", "Name_De", "Name_En", "Name_Es", "Name_Fr", "NetPrice", "NotificationEmailAdresses", "NotificationGuestMessage", "NotificationHotelMessage", "PaymentType", "Photo", "ShortDetails", "ShortDetails_De", "ShortDetails_En", "ShortDetails_Es", "ShortDetails_Fr", "VAT" },
                values: new object[] { 9997, 1, 999, "", null, null, null, null, false, true, "Śniadanie", null, null, null, null, 40m, null, null, null, 0, "", "", null, null, null, null, 8m });

            migrationBuilder.InsertData(
                table: "Attraction",
                columns: new[] { "Id", "Board", "CategoryId", "Details", "Details_De", "Details_En", "Details_Es", "Details_Fr", "ImmediatePayment", "IsActive", "Name", "Name_De", "Name_En", "Name_Es", "Name_Fr", "NetPrice", "NotificationEmailAdresses", "NotificationGuestMessage", "NotificationHotelMessage", "PaymentType", "Photo", "ShortDetails", "ShortDetails_De", "ShortDetails_En", "ShortDetails_Es", "ShortDetails_Fr", "VAT" },
                values: new object[] { 9998, 2, 999, "", null, null, null, null, false, true, "Pełne wyżywienie", null, null, null, null, 76m, null, null, null, 0, "", "", null, null, null, null, 8m });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Attraction",
                keyColumn: "Id",
                keyValue: 9997);

            migrationBuilder.DeleteData(
                table: "Attraction",
                keyColumn: "Id",
                keyValue: 9998);

            migrationBuilder.DeleteData(
                table: "AttractionCategory",
                keyColumn: "Id",
                keyValue: 999);

            migrationBuilder.DropColumn(
                name: "IsBoard",
                table: "AttractionCategory");

            migrationBuilder.DropColumn(
                name: "Board",
                table: "Attraction");
        }
    }
}
