﻿using CommonComponents.Context;
using CommonComponents.Repositories;
using WRH.Domain;
using WRH.Repository.Interfaces;

namespace WRH.Repository
{
    public class RoomBusyRepository : Repository<RoomBusyModel>, IRoomBusyRepository
    {
        public RoomBusyRepository(IDataContext db) : base(db) { }
    }
}