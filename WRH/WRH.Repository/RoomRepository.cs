﻿using CommonComponents.Context;
using CommonComponents.Repositories;
using WRH.Domain;
using WRH.Repository.Interfaces;

namespace WRH.Repository
{
    public class RoomRepository : Repository<RoomModel>, IRoomRepository
    {
        public RoomRepository(IDataContext db) : base(db) { }
    }
}