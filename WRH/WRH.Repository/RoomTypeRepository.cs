﻿using CommonComponents.Context;
using CommonComponents.Repositories;
using WRH.Domain;
using WRH.Repository.Interfaces;

namespace WRH.Repository
{
    public class RoomTypeRepository : Repository<RoomTypeModel>, IRoomTypeRepository
    {
        public RoomTypeRepository(IDataContext db) : base(db) { }
    }
}