﻿using CommonComponents.Context;
using CommonComponents.Repositories;
using WRH.Domain;
using WRH.Repository.Interfaces;

namespace WRH.Repository
{
    public class ConfigurationRepository : Repository<ConfigurationModel>, IConfigurationRepository
    {
        public ConfigurationRepository(IDataContext db) : base(db) { }
    }
}
