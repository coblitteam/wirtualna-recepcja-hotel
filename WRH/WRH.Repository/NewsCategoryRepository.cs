﻿using CommonComponents.Context;
using CommonComponents.Repositories;
using WRH.Domain;
using WRH.Repository.Interfaces;

namespace WRH.Repository
{
    public class NewsCategoryRepository : Repository<NewsCategoryModel>, INewsCategoryRepository
    {
        public NewsCategoryRepository(IDataContext db) : base(db)
        {
        }
    }
}
