﻿using CommonComponents.Context;
using CommonComponents.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WRH.Domain;
using WRH.Repository.Interfaces;

namespace WRH.Repository
{
    public class InvoiceNumberRepository : Repository<InvoiceNumberModel>, IInvoiceNumberRepository
    {
        public InvoiceNumberRepository(IDataContext db) : base(db)
        {

        }
    }
}
