﻿using CommonComponents.Context;
using CommonComponents.Repositories;
using WRH.Domain;
using WRH.Repository.Interfaces;

namespace WRH.Repository
{
    public class CustomerDataRepository : Repository<CustomerDataModel>, ICustomerDataRepository
    {
        public CustomerDataRepository(IDataContext db) : base(db)
        {
        }
    }
}
