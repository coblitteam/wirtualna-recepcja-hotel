﻿using CommonComponents.Context;
using CommonComponents.Repositories;
using WRH.Domain;
using WRH.Repository.Interfaces;

namespace WRH.Repository
{
    public class GuestDataRepository : Repository<GuestDataModel>, IGuestDataRepository
    {
        public GuestDataRepository(IDataContext db) : base(db)
        {
        }
    }
}
