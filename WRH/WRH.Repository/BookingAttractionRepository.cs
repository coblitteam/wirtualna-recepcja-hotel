﻿using CommonComponents.Context;
using CommonComponents.Repositories;
using WRH.Domain;
using WRH.Repository.Interfaces;

namespace WRH.Repository
{
    public class BookingAttractionRepository : Repository<BookingAttractionModel>, IBookingAttractionRepository
    {
        public BookingAttractionRepository(IDataContext db) : base(db)
        {
        }
    }
}
