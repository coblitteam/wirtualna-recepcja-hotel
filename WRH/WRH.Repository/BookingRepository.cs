﻿using CommonComponents.Context;
using CommonComponents.Repositories;
using WRH.Domain;
using WRH.Repository.Interfaces;

namespace WRH.Repository
{
    public class BookingRepository : Repository<BookingModel>, IBookingRepository
    {
        public BookingRepository(IDataContext db) : base(db) { }
    }
}