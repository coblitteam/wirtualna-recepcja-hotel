﻿using CommonComponents.Context;
using CommonComponents.Repositories;
using WRH.Domain;
using WRH.Repository.Interfaces;

namespace WRH.Repository
{
    public class AttractionCategoryRepository : Repository<AttractionCategoryModel>, IAttractionCategoryRepository
    {
        public AttractionCategoryRepository(IDataContext db) : base(db)
        {
        }
    }
}
