﻿using CommonComponents.Context;
using CommonComponents.Repositories;
using WRH.Domain;
using WRH.Repository.Interfaces;

namespace WRH.Repository
{
    public class NewsRepository : Repository<NewsModel>, INewsRepository
    {
        public NewsRepository(IDataContext db) : base(db)
        {
        }
    }
}
