﻿using CommonComponents.Context;
using CommonComponents.Repositories;
using WRH.Domain;
using WRH.Repository.Interfaces;

namespace WRH.Repository
{
    public class AttractionRepository : Repository<AttractionModel>, IAttractionRepository
    {
        public AttractionRepository(IDataContext db) : base(db)
        {
        }
    }
}
