﻿using CommonComponents.Repositories;
using WRH.Domain;

namespace WRH.Repository.Interfaces
{
    public interface IConfigurationRepository : IRepository<ConfigurationModel>
    {

    }
}
