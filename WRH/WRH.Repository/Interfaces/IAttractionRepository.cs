﻿using CommonComponents.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WRH.Domain;

namespace WRH.Repository.Interfaces
{
    public interface IAttractionRepository : IRepository<AttractionModel>
    {
    }
}
