﻿using CommonComponents.Repositories;
using WRH.Domain;

namespace WRH.Repository.Interfaces
{
    public interface ICustomerDataRepository : IRepository<CustomerDataModel>
    {
    }
}
