﻿using CommonComponents.Repositories;
using WRH.Domain;

namespace WRH.Repository.Interfaces
{
    public interface IBookingRoomRepository : IRepository<BookingRoomModel>
    {
    }
}
