﻿using CommonComponents.Context;
using CommonComponents.Repositories;
using WRH.Domain;
using WRH.Repository.Interfaces;

namespace WRH.Repository
{
    public class BookingRoomRepository : Repository<BookingRoomModel>, IBookingRoomRepository
    {
        public BookingRoomRepository(IDataContext db) : base(db)
        {

        }
    }
}