﻿using CommonComponents.Context;
using CommonComponents.Repositories;
using WRH.Domain;
using WRH.Repository.Interfaces;

namespace WRH.Repository
{
    public class UserRepository : Repository<UserModel>, IUserRepository
    {
        public UserRepository(IDataContext db) : base(db)
        {
        }
    }
}
